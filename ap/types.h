#ifndef _TYPES_H_
#define _TYPES_H_

#include <string>
#include <limits>

using namespace std;

typedef unsigned long long int ImageID;
typedef string ObjectID;

#ifdef ISMAIN

int INT_NULL=2147483647;
float FLT_NULL=-numeric_limits<float>::max();
double DBL_NULL=-numeric_limits<double>::max();
signed char BYT_NULL=-128;
signed short int SHT_NULL=-32768;
signed short int USHT_NULL=0;

#else

extern int INT_NULL;
extern float FLT_NULL;
extern double DBL_NULL;
extern signed char BYT_NULL;
extern signed short int SHT_NULL;
extern signed short int USHT_NULL;

#endif


#define isnotnull(_x_) ((_x_)!=FLT_NULL)
#define isnull(_x_) ((_x_)==FLT_NULL)

#define  sqr(_a_) ((_a_)*(_a_))

typedef struct {
  int first, last;
} UTRange;

#endif
