#include <iostream>
#include <string>
#include <iomanip>
#include <fstream>

class csvfile { 
  std::ofstream os;
  bool first;

public:
  csvfile(std::string file) {
    os.open(file.c_str());
    first=true;
  }

  ~csvfile() {
    os.close();
  }

  void close() {
    os.close();
  }

  csvfile& operator<< (bool const &val) {
    if (!first) os << ",";
    os << val;
    first = false;
    return *this;
  };

  csvfile& operator<< (short const &val) {
    if (!first) os << ",";
    os << val;
    first = false;
    return *this;
  };

  csvfile& operator<< (unsigned short const &val) {
    if (!first) os << ",";
    os << val;
    first = false;
    return *this;
  };

  csvfile& operator<< (int const &val) {
    if (!first) os << ",";
    os << val;
    first = false;
    return *this;
  };

  csvfile& operator<< (unsigned int const &val) {
    if (!first) os << ",";
    os << val;
    first = false;
    return *this;
  };

  csvfile& operator<< (long const &val) {
    if (!first) os << ",";
    os << val;
    first = false;
    return *this;
  };

  csvfile& operator<< (unsigned long const &val) {
    if (!first) os << ",";
    os << val;
    first = false;
    return *this;
  };

  csvfile& operator<< (float const &val) {
    if (!first) os << ",";
    os << std::setprecision(7) << val;
    first = false;
    return *this;
  };

  csvfile& operator<< (double const &val) {
    if (!first) os << ",";
    os << std::setprecision(16) << val;
    first = false;
    return *this;
  };

  csvfile& operator<< (long double const &val) {
    if (!first) os << ",";
    os << std::setprecision(23) << val;
    first = false;
    return *this;
  };

  csvfile& operator<< (const void* val) {
    if (!first) os << ",";
    os << val;
    first = false;
    return *this;
  };

  csvfile& operator<< (string const &val) {
    if (!first) os << ",";
    os << "\"" << val << "\"";
    first = false;
    return *this;
  };

  csvfile& operator<< (const char* val) {
    if (!first) os << ",";
    os << "\"" << val << "\"";
    first = false;
    return *this;
  };

  csvfile& operator<< (const signed char* val) {
    if (!first) os << ",";
    os << "\"" << val << "\"";
    first = false;
    return *this;
  };

  csvfile& operator<< (const unsigned char* val) {
    if (!first) os << ",";
    os << "\"" << val << "\"";
    first = false;
    return *this;
  };

  csvfile& operator<<(std::ostream &manip(std::ostream &o) ) {
    if (manip == static_cast<std::ostream& (*)(std::ostream&)>(std::endl))
      reset();
    manip(os);
    return *this;
  }

  void reset() { first = true; }
  
  operator void *() { return (void *)os; }
};
