#!/usr/bin/env python
"""
The module fetches data from the archive for detrending or for running orion transit searches on. The module is run as:

./archive_fetch.py KCAM_US_T01D01 002000+450000 C7 dia

which will fetch the dia data for Camera KCAM_US_T01D01, field 002000+450000 and campaign C7.

By default fetch assumes that you are fetching new data that has just been ingested into the archive. It will therefore
create a new campaign version for you. For example if the field has never been fetch before it will be fetch with the
campaign version C7A, if there is a C7A and from an older data set it will be fetched as C7B etc.

If you are not fetching new data that has just been ingested but instead want to fetch the latest data that already has
a campaign version then you must add --rerun to your fetch call. That is:

./archive_fetch.py KCAM_US_T01D01 002000+450000 C7 dia --rerun

will fetch the latest campaign version in the archive for this field and will not create a new campaign version.

--archive_code can be used to overload the location of the archive code although if the code moves permanently the
value in the archive_constants file should be changed.

--job_id is used for when the code is being run by the archive_jobs.py code and should not be used if the code is
being used from the command line

--work_dir is the root directory where the working archive directories are. The code will fetch the file into a sub
directory inside the working directory of for example 002000+450000_KCAM_US_T01D01_C7_dia.
"""
__author__ = 'Neil Parley'
import os
import sys
import subprocess
try:
    import qesutil
except ImportError:
    import qes_pipeline.qesutil as qesutil
import archive_jobs
import archive_constants
import archive_util
import archive_db
import archive_mysql


@archive_jobs.register_job
@qesutil.check_nearly_all_param_for_none(can_be_none=["job_id"])
def fetch(camera=None, field=None, campaign=None, work_dir=None, code_dir=None, camera_id=None, photometry_info=None,
          rerun=False, job_id=None):
    """
    Routine for fetching archive date. Will fetch data for a field / campaign and automatically create a new campaign
    version is rerun is false or fetch as the current latest campaign version if rerun is true.

    This routine, calculates the global campaign (i.e. removes any version or photometry information), computes the
    subdirectory inside the work directory where the data is to be fetch to. Then gets the photometry campaign before
    either creating a new campaign version or reading the latest one from the database. Finally run fetch runs the fetch
    code to get the data as that campaign version.

    :param camera: Camera string e.g. KCAM_US_T01D01
    :param field: Field string e.g. 002000+450000
    :param campaign: Campaign string e.g. C7
    :param work_dir: Base working directory should be set to os.path.join(qes_path, archive_constants.archive_work) if
                     using the default
    :param code_dir: Location of the archive code
    :param camera_id: The database camera id, can be loaded from the configuration files
    :param photometry_info: The photometry type of the run, types defined in archive_constants
    :param rerun: If equal false then this routine creates a new campaign version i.e. C7A C7B ... etc if set True then
                  will fetch a file with the latest campaign version in the archive.
    :param job_id: This is given to the function when it is started as a job by archive_jobs. It is used by the
                   decorator archive_jobs.register_job to update the database with when the job starts and finishes.
    """
    campaign_global = archive_util.get_global_campaign(campaign)

    work_root = archive_util.create_work_root(field=field, campaign=campaign_global, camera=camera,
                                              photometry_info=photometry_info, work_dir=work_dir)

    campaign_global_phot = photometry_info.campaign_letter + campaign_global[1:]

    if rerun:
        campaign_global_phot = current_field_season(field=field, camera_id=camera_id, campaign=campaign_global_phot)
    else:
        campaign_global_phot = make_field_season(field=field, camera_id=camera_id, campaign=campaign_global_phot)

    run_fetch(field=field, camera_id=camera_id, campaign=campaign_global_phot, work_root=work_root, code_dir=code_dir)


@qesutil.check_all_param_for_none
def run_fetch(field=None, camera_id=None, campaign=None, work_root=None, code_dir=None):
    """
    Routine for calling the archive fetch code. See aofetch.C for the code that is called.

    :param field: Field string e.g. 002000+450000
    :param camera_id: The database camera id, can be loaded from the configuration files
    :param campaign: Photometry Campaign version string e.g. C7A or A7B
    :param work_root: Directory where the fits file is going to be fetched to
    :param code_dir: Location of the archive code
    """

    fetch_root = os.path.join(code_dir, "apfetch")
    print ("Running {command}".format(command=fetch_root))
    fetch_command = [fetch_root, field, camera_id, campaign]
    child = subprocess.Popen(fetch_command, cwd=work_root)
    return_code = child.wait()
    if return_code:
        raise subprocess.CalledProcessError(return_code, cmd=fetch_command)


@qesutil.check_all_param_for_none
def current_field_season(field=None, camera_id=None, campaign=None):
    """
    Returns the current (latest) field season in the archive. Calls get_current_max_field_season on the database model
    field_season to and returns the latest campaign version, e.g. C7B being used for the field / campaign in the
    archive

    :param field: Field string e.g. 002000+450000
    :param camera_id: The database camera id, can be loaded from the configuration files
    :param campaign: Base photometry Campaign string e.g. C7 or A7
    :return: Campaign photometry campaign version string e.g. C7A or A7A
    """
    field_season = archive_db.FieldSeason(user=archive_constants.db_settings.user,
                                          password=archive_constants.db_settings.password,
                                          host=archive_constants.db_settings.host,
                                          db=archive_constants.db_settings.database)

    return field_season.get_current_max_field_season(field=field, camera_id=camera_id, base_campaign=campaign)


@qesutil.check_all_param_for_none
def make_field_season(field=None, camera_id=None, campaign=None):
    """
    Creates a new campaign version for the field and returns the created campaign version.

    This function loads the database model field_season and then calls get_image_stats_for_field to get information of
    the data held for the current field campaign. It then gets the next version letter by calling the method
    get_field_season_version_letter. Depending on this the field / campaign has been seen before it then either creates
    new database eateries or updates the current eateries for the new campaign version. Update star count updates the
    number of detected stars for the field in the corresponding database table.

    :param field: Field string e.g. 002000+450000
    :param camera_id: The database camera id, can be loaded from the configuration files
    :param campaign: Base photometry Campaign string e.g. C7 or A7
    :return: Campaign photometry campaign version string e.g. C7A or A7A
    """
    field_season = archive_db.FieldSeason(user=archive_constants.db_settings.user,
                                          password=archive_constants.db_settings.password,
                                          host=archive_constants.db_settings.host,
                                          db=archive_constants.db_settings.database)
    base_campaign = campaign

    stats = field_season.get_image_stats_for_field(field=field, camera_id=camera_id, campaign=campaign)
    letter = field_season.get_field_season_version_letter(field=field, camera_id=camera_id, base_campaign=campaign)
    campaign += letter

    print("New campaign will be {campaign} for this field".format(campaign=campaign))
    print("Inserting pseudo into field season tables")

    field_season.add_field_season_pseudo(field=field, camera_id=camera_id, campaign=campaign,
                                         base_campaign=base_campaign, first_night=stats.first_night,
                                         last_night=stats.last_night, nimage=stats.nimage, nnight=stats.nnight,
                                         complete='no', psf_fwhm_med=stats.psf_fwhm_med,
                                         psf_fwhm_iqr=stats.psf_fwhm_iqr)

    field_season.add_field_season(field=field, camera_id=camera_id, campaign=campaign, first_night=stats.first_night,
                                  last_night=stats.last_night, nimage=stats.nimage, nnight=stats.nnight, complete='no',
                                  psf_fwhm_med=stats.psf_fwhm_med, psf_fwhm_iqr=stats.psf_fwhm_iqr,
                                  season_type='pseudo')

    if field_season.field_season_new(field=field, camera_id=camera_id, campaign=base_campaign):
        print("Adding base campaign into field season tables")
        field_season.add_field_season(field=field, camera_id=camera_id, campaign=base_campaign,
                                      first_night=stats.first_night, last_night=stats.last_night, nimage=stats.nimage,
                                      nnight=stats.nnight, complete='no', psf_fwhm_med=stats.psf_fwhm_med,
                                      psf_fwhm_iqr=stats.psf_fwhm_iqr, season_type='real')
    else:
        print("Updating base campaign into field season tables")
        field_season.update_field_season(field=field, camera_id=camera_id, campaign=base_campaign,
                                         first_night=stats.first_night, last_night=stats.last_night,
                                         nimage=stats.nimage, nnight=stats.nnight, complete='no',
                                         psf_fwhm_med=stats.psf_fwhm_med, psf_fwhm_iqr=stats.psf_fwhm_iqr)

    archive_mysql.update_star_count(field=field, camera_id=camera_id, campaign=base_campaign)
    return campaign

if __name__ == '__main__':

    try:
        import configparser
    except ImportError:
        import ConfigParser as configparser

    import argparse

    try:
        _qes_path = os.environ['QES_PATH']
    except NameError as n_error:
        print("Environmental variable not set: %s" % n_error)
        sys.exit(2)

    _data_dir_default = os.path.join(_qes_path, "qes_data")
    _work_dir_default = os.path.join(_qes_path, archive_constants.archive_work)
    _archive_code_default = os.path.join(_qes_path, archive_constants.archive_code)

    parser = argparse.ArgumentParser(description='Runs the archive fetch for one camera field campaign photometry')
    parser.add_argument('camera', metavar='CAMERA', help='Camera to ingest (e.g. KCAM_US_T01D01)')
    parser.add_argument('field', metavar='FIELD', help='Field to ingest (e.g. 002000+350000)')
    parser.add_argument('campaign', metavar='CAMPAIGN', help='Campaign to ingest (e.g. C7)')
    parser.add_argument('photometry', metavar='TYPE', choices=['dia', 'aperture'],
                        help='Photometry type (dia or aperture)')
    parser.add_argument("--work_dir", default=_work_dir_default,
                        help="Work directory of ingest Files (default: {dir})".format(dir=_work_dir_default))
    parser.add_argument("--archive_code", default=_archive_code_default,
                        help="Directory of archive code (default: {dir})".format(dir=_archive_code_default))
    parser.add_argument("--rerun", help="Don't create a new field / season", action="store_true")
    parser.add_argument("--job_id", default=None, type=int, help="Job id if running on the cluster")
    args = parser.parse_args()

    _alsubai_control_path = os.path.join(_qes_path, 'qes_pipeline', 'Alsubai_Control')
    _config_cam_campaign = configparser.ConfigParser()

    if not _config_cam_campaign.read(os.path.join(_alsubai_control_path,
                                                  'cfg', args.campaign, 'config.' + args.camera + '.cfg')):
        print('ERROR - The configuration file is missing for the camera: ' +
              os.path.join('cfg', args.campaign, 'config.' + args.camera + '.cfg'))
        sys.exit(1)

    fetch(camera=args.camera, field=args.field, campaign=args.campaign, work_dir=args.work_dir,
          code_dir=args.archive_code, camera_id=_config_cam_campaign.get('main', 'camID'),
          rerun=args.rerun,
          photometry_info=getattr(archive_constants.photometry, args.photometry), job_id=args.job_id)
