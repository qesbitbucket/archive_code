#!/usr/bin/env python
"""
This module runs the ingest code to ingest new data into the archive. Normally calling for this module is:

    ./archive_ingest.py KCAM_US_T01D01 002000+450000 C7 dia

which will ingest the new dia data for camera KCAM_US_T01D01 field 002000+450000 and campaign C7.

--data_dir can be used to override the location of the qes pipeline data output. The default location is
  $QES_PATH/qes_data and can be changed in the code below.

--archive_code can be used to overload the location of the archive code although if the code moves permanently the
value in the archive_constants file should be changed.

--job_id is used for when the code is being run by the archive_jobs.py code and should not be used if the code is
being used from the command line

--work_dir is the root directory where the working archive directories are. The code will look in a sub directory
of for example 002000+450000_KCAM_US_T01D01_C7_dia for the files.
"""
from __future__ import print_function
import os
import sys
import glob
import subprocess
try:
    import qesutil
except ImportError:
    import qes_pipeline.qesutil as qesutil
import archive_constants
import archive_jobs
import archive_mysql
import archive_util

__author__ = 'Neil Parley'


@archive_jobs.register_job
@qesutil.check_nearly_all_param_for_none(can_be_none=["job_id"])
def ingest(field=None, campaign=None, data_dir=None, work_dir=None, code_dir=None, camera=None, camera_id=None,
           photometry_info=None, filter_name=None, job_id=None):
    """
    Main routine for ingesting data for a camera / field / campaign / photometry type. Data is read from the pipeline
    output files and uploading into the archive file storage.

    The global campaign is calculated (i.e. removing any campaign version or photometry type) and the working
    subdirectory is computed. The root data directory is also computed from the field name etc. and the data directory
    location given. The routine then searches for all the nights in the data directory for the field. It then loops over
    each night in turn and if it finds fits files it calls ingest_night which ingests those fits files into the archive.

    :param field: Field string e.g. 002000+450000
    :param campaign: Campaign string e.g. C7
    :param data_dir: Base directory for the pipeline data. E.g. $QES_PATH/qes_data
    :param work_dir: Base working directory should be set to os.path.join(qes_path, archive_constants.archive_work) if
                     using the default
    :param code_dir: Location of the archive code
    :param camera: Camera string e.g. KCAM_US_T01D01
    :param camera_id: The database camera id, can be loaded from the configuration files
    :param photometry_info: The photometry type of the run, types defined in archive_constants
    :param filter_name: Filter name used for the photometry. For example K2
    :param job_id: This is given to the function when it is started as a job by archive_jobs. It is used by the
                   decorator archive_jobs.register_job to update the database with when the job starts and finishes.
    """
    campaign_global = archive_util.get_global_campaign(campaign)

    work_root = archive_util.create_work_root(field=field, campaign=campaign_global, camera=camera,
                                              photometry_info=photometry_info, work_dir=work_dir)

    data_root = os.path.join(data_dir, camera, field, campaign_global)

    print("Searching in {dir}".format(dir=data_root))
    search_night_string = data_root+"/20*"
    nights = glob.glob(search_night_string)
    print("Found {num_nights} nights for field".format(num_nights=len(nights)))

    total = 0

    campaign_global_phot = photometry_info.campaign_letter + campaign_global[1:]

    for night in nights:
        print("---- Doing night {night} ----".format(night=os.path.basename(night)))

        local_path = os.path.join(data_root, night, photometry_info.directory, filter_name)
        search_string = local_path+"/*_lc.fits"
        lc_files = glob.glob(search_string)
        total += len(lc_files)
        print("{files} files in {name} directory for {night}".format(files=len(lc_files), name=photometry_info.name,
                                                                     night=night))
        if len(lc_files) > 0:
            ingest_night(camera_id=camera_id, field=field, campaign=campaign_global_phot, night=os.path.basename(night),
                         work_root=work_root, sub_dir=local_path, code_dir=code_dir)

    print("---- Finished ----")
    print("Total {name} data points expected ~{total}".format(name=photometry_info.name, total=total))


@qesutil.check_all_param_for_none
def ingest_night(camera_id=None, field=None, campaign=None, night=None, work_root=None, sub_dir=None, code_dir=None):
    """
    Ingests new pipeline data for a night into the archive.

    This code runs the ingest scripts from Richards code to ingest data into the archive. apbackout removes the night
    from the database / file store (if it already existed). apprepingest and apfileimport then read the files and
    import the data into the archive file store. The mysql calls then upload the database names with the metadata that
    has been ingested into the file store.

    :param camera_id: The database camera id, can be loaded from the configuration files
    :param field: Field string e.g. 002000+450000
    :param campaign: Campaign string e.g. C7
    :param night: Night string e.g. 20150101
    :param work_root: Directory where the working temp files are stored. Made from archive_util.create_work_root
    :param sub_dir: The path to the pipeline fits files (different for different photometry types)
    :param code_dir: Location of the archive code
    """

    ap_commands = ("apbackout", "apprepingest", "apfileimport")

    ap_args = {"apbackout": ["--field", field, "--camera", camera_id, "--campaign", campaign, "--night", night],
               "apprepingest": ["--indir={sub_dir}".format(sub_dir=sub_dir),
                                "--outdir={work_dir}".format(work_dir=work_root)],
               "apfileimport": [work_root]}

    print("Running scripts for {dir}".format(dir=sub_dir))

    for command in ap_commands:
        print("Running {command}".format(command=command))
        command_array = [os.path.join(code_dir, command)] + ap_args[command]
        child = subprocess.Popen(command_array, cwd=work_root)
        return_code = child.wait()
        if return_code:
            raise subprocess.CalledProcessError(return_code, cmd=command_array)

    mysql_calls = ((archive_mysql.field_load, "Fieldnames", "fieldname.dump"),
                   (archive_mysql.image_load, "Imagelist", "imagelist.dump"),
                   (archive_mysql.title_load, "Titlefiles", "tilefiles.dump"))

    for mysql_call in mysql_calls:
        mysql_func, table_name, mysql_file = mysql_call
        mysql_file = os.path.join(work_root, mysql_file)
        if os.path.exists(mysql_file):
            print("Updated {table_name} table table - effected {rows}".
                  format(table_name=table_name, rows=mysql_func(mysql_file)))
            os.remove(mysql_file)


if __name__ == '__main__':

    try:
        import configparser
    except ImportError:
        import ConfigParser as configparser

    import argparse

    try:
        _qes_path = os.environ['QES_PATH']
    except NameError as n_error:
        print("Environmental variable not set: %s" % n_error)
        sys.exit(2)

    _data_dir_default = os.path.join(_qes_path, "qes_data")
    _work_dir_default = os.path.join(_qes_path, archive_constants.archive_work)
    _archive_code_default = os.path.join(_qes_path, archive_constants.archive_code)

    parser = argparse.ArgumentParser(description='Runs the ingest for one camera field campaign photometry')
    parser.add_argument('camera', metavar='CAMERA', help='Camera to ingest (e.g. KCAM_US_T01D01)')
    parser.add_argument('field', metavar='FIELD', help='Field to ingest (e.g. 002000+350000)')
    parser.add_argument('campaign', metavar='CAMPAIGN', help='Campaign to ingest (e.g. C7)')
    parser.add_argument('photometry', metavar='TYPE', choices=['dia', 'aperture'],
                        help='Photometry type (dia or aperture)')
    parser.add_argument("--data_dir", default=_data_dir_default,
                        help="Root directory of qes Files (default: {dir})".format(dir=_data_dir_default))
    parser.add_argument("--work_dir", default=_work_dir_default,
                        help="Work directory of ingest Files (default: {dir})".format(dir=_work_dir_default))
    parser.add_argument("--archive_code", default=_archive_code_default,
                        help="Directory of archive code (default: {dir})".format(dir=_archive_code_default))
    parser.add_argument("--job_id", default=None, type=int, help="Job id if running on the cluster")
    args = parser.parse_args()

    _alsubai_control_path = os.path.join(_qes_path, 'qes_pipeline', 'Alsubai_Control')
    _config_cam_campaign = configparser.ConfigParser()

    if not _config_cam_campaign.read(os.path.join(_alsubai_control_path,
                                                  'cfg', args.campaign, 'config.' + args.camera + '.cfg')):
        print('ERROR - The configuration file is missing for the camera: ' +
              os.path.join('cfg', args.campaign, 'config.' + args.camera + '.cfg'))
        sys.exit(1)

    ingest(camera=args.camera, field=args.field, campaign=args.campaign, data_dir=args.data_dir,
           work_dir=args.work_dir, code_dir=args.archive_code,
           camera_id=_config_cam_campaign.get('main', 'camID'),
           photometry_info=getattr(archive_constants.photometry, args.photometry),
           filter_name=_config_cam_campaign.get('main', 'filtername'), job_id=args.job_id)
