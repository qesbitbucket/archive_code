"""
This module contains all the routines that interact with the archive database through the database models
"""
__author__ = 'Neil Parley'
import sqlalchemy
from sqlalchemy.ext.declarative import declarative_base
import sqlalchemy.exc
from sqlalchemy import func
import sqlalchemy.dialects.mysql
import sqlalchemy.orm
import datetime
try:
    import qesutil
    import Alsubai_Control.pbs as pbs
    import qesdb
except ImportError:
    import qes_pipeline.qesutil as qesutil
    import qes_pipeline.Alsubai_Control.pbs as pbs
    import qes_pipeline.qesdb as qesdb

Base = declarative_base()


class ImageListTable(Base):
    """
    Database model for the imagelist table in the archive database
    """
    __tablename__ = 'imagelist'
    image_id = sqlalchemy.Column(sqlalchemy.Integer, primary_key=True)
    tstart = sqlalchemy.Column(sqlalchemy.DateTime)
    tmid = sqlalchemy.Column(sqlalchemy.Integer)
    mount_id = sqlalchemy.Column(sqlalchemy.Integer)
    camera_id = sqlalchemy.Column(sqlalchemy.Integer)
    exptime = sqlalchemy.Column(sqlalchemy.Float)
    ambtemp = sqlalchemy.Column(sqlalchemy.Float)
    rdnoise = sqlalchemy.Column(sqlalchemy.Float)
    gain = sqlalchemy.Column(sqlalchemy.Float)
    filter = sqlalchemy.Column(sqlalchemy.String(8))
    field = sqlalchemy.Column(sqlalchemy.String(13), index=True)
    night = sqlalchemy.Column(sqlalchemy.Date, index=True)
    fileleaf = sqlalchemy.Column(sqlalchemy.String(50))
    refname1 = sqlalchemy.Column(sqlalchemy.String(50))
    crval1 = sqlalchemy.Column(sqlalchemy.Float)
    crpix1 = sqlalchemy.Column(sqlalchemy.Float)
    crval2 = sqlalchemy.Column(sqlalchemy.Float)
    crpix2 = sqlalchemy.Column(sqlalchemy.Float)
    cd1_1 = sqlalchemy.Column(sqlalchemy.Float)
    cd1_2 = sqlalchemy.Column(sqlalchemy.Float)
    cd2_1 = sqlalchemy.Column(sqlalchemy.Float)
    cd2_2 = sqlalchemy.Column(sqlalchemy.Float)
    pv2_1 = sqlalchemy.Column(sqlalchemy.Float)
    pv2_3 = sqlalchemy.Column(sqlalchemy.Float)
    zpfit = sqlalchemy.Column(sqlalchemy.Float)
    zmag = sqlalchemy.Column(sqlalchemy.Float)
    sfits = sqlalchemy.Column(sqlalchemy.Float)
    fwhm1 = sqlalchemy.Column(sqlalchemy.Float)
    fwhm2 = sqlalchemy.Column(sqlalchemy.Float)
    astrms = sqlalchemy.Column(sqlalchemy.Float)
    centaz = sqlalchemy.Column(sqlalchemy.Float)
    centalt = sqlalchemy.Column(sqlalchemy.Float)
    jdmid = sqlalchemy.Column(sqlalchemy.Float)
    refstars = sqlalchemy.Column(sqlalchemy.Float)
    a = sqlalchemy.Column(sqlalchemy.Float)
    b = sqlalchemy.Column(sqlalchemy.Float)
    c = sqlalchemy.Column(sqlalchemy.Float)
    d = sqlalchemy.Column(sqlalchemy.Float)
    shift_x = sqlalchemy.Column(sqlalchemy.Float)
    shift_y = sqlalchemy.Column(sqlalchemy.Float)
    det = sqlalchemy.Column(sqlalchemy.Float)
    c0 = sqlalchemy.Column(sqlalchemy.Float)
    c1 = sqlalchemy.Column(sqlalchemy.Float)
    c2 = sqlalchemy.Column(sqlalchemy.Float)
    c3 = sqlalchemy.Column(sqlalchemy.Float)
    c4 = sqlalchemy.Column(sqlalchemy.Float)
    c5 = sqlalchemy.Column(sqlalchemy.Float)
    c6 = sqlalchemy.Column(sqlalchemy.Float)
    c7 = sqlalchemy.Column(sqlalchemy.Float)
    c8 = sqlalchemy.Column(sqlalchemy.Float)
    c9 = sqlalchemy.Column(sqlalchemy.Float)
    c10 = sqlalchemy.Column(sqlalchemy.Float)
    c11 = sqlalchemy.Column(sqlalchemy.Float)
    moffset = sqlalchemy.Column(sqlalchemy.Float)
    jkoffset = sqlalchemy.Column(sqlalchemy.Float)
    xoffset = sqlalchemy.Column(sqlalchemy.Float)
    yoffset = sqlalchemy.Column(sqlalchemy.Float)
    catrms = sqlalchemy.Column(sqlalchemy.Float)
    computer = sqlalchemy.Column(sqlalchemy.String(50))
    pipevers = sqlalchemy.Column(sqlalchemy.Integer)
    blurfwhm = sqlalchemy.Column(sqlalchemy.Float)
    campaign = sqlalchemy.Column(sqlalchemy.String(12), primary_key=True)
    refname2 = sqlalchemy.Column(sqlalchemy.String(50))
    refname3 = sqlalchemy.Column(sqlalchemy.String(50))
    refname4 = sqlalchemy.Column(sqlalchemy.String(50))


class FieldSeasonTable(Base):
    """
    Database model for the field season archive database table
    """
    __tablename__ = 'fieldseason'
    field = sqlalchemy.Column(sqlalchemy.String(14), primary_key=True)
    camera_id = sqlalchemy.Column(sqlalchemy.Integer, primary_key=True)
    campaign = sqlalchemy.Column(sqlalchemy.String(12), primary_key=True)
    first_night = sqlalchemy.Column(sqlalchemy.Date)
    last_night = sqlalchemy.Column(sqlalchemy.Date)
    nimage = sqlalchemy.Column(sqlalchemy.Integer)
    nnight = sqlalchemy.Column(sqlalchemy.Integer)
    complete = sqlalchemy.Column(sqlalchemy.Enum('no', 'yes'))
    psf_fwhm_med = sqlalchemy.Column(sqlalchemy.Float)
    psf_fwhm_iqr = sqlalchemy.Column(sqlalchemy.Float)
    type = sqlalchemy.Column(sqlalchemy.Enum('real', 'pseudo', 'combo'))


class FieldSeasonPseudoTable(Base):
    """
    Database model for the fieldseason pseudo database table
    """
    __tablename__ = 'fieldseason_pseudo'
    field = sqlalchemy.Column(sqlalchemy.String(14), primary_key=True)
    camera_id = sqlalchemy.Column(sqlalchemy.Integer, primary_key=True)
    campaign = sqlalchemy.Column(sqlalchemy.String(12), primary_key=True)
    base_campaign = sqlalchemy.Column(sqlalchemy.String(12))
    first_night = sqlalchemy.Column(sqlalchemy.Date)
    last_night = sqlalchemy.Column(sqlalchemy.Date)
    nimage = sqlalchemy.Column(sqlalchemy.Integer)
    nnight = sqlalchemy.Column(sqlalchemy.Integer)
    complete = sqlalchemy.Column(sqlalchemy.Enum('no', 'yes'))
    psf_fwhm_med = sqlalchemy.Column(sqlalchemy.Float)
    psf_fwhm_iqr = sqlalchemy.Column(sqlalchemy.Float)


class DiffListTable(Base):
    """
    Database model for the difference image list table
    """
    __tablename__ = 'diffimagelist'
    image_id = sqlalchemy.Column(sqlalchemy.Integer, primary_key=True)
    filepath = sqlalchemy.Column(sqlalchemy.String(100))


class PhotsummaryTable(Base):
    """
    Database model for the photsummary database table
    """
    __tablename__ = 'photsummary'
    obj_id = sqlalchemy.Column(sqlalchemy.String(26), primary_key=True)
    npts = sqlalchemy.Column(sqlalchemy.Integer)
    obs_start = sqlalchemy.Column(sqlalchemy.DateTime)
    obs_stop = sqlalchemy.Column(sqlalchemy.DateTime)
    obs_interval = sqlalchemy.Column(sqlalchemy.Integer)
    flux_mean = sqlalchemy.Column(sqlalchemy.Float)
    flux_chisq = sqlalchemy.Column(sqlalchemy.Float)
    flux_skew = sqlalchemy.Column(sqlalchemy.Float)
    flux_kurtosis = sqlalchemy.Column(sqlalchemy.Float)
    flux_rms = sqlalchemy.Column(sqlalchemy.Float)
    mag_mean = sqlalchemy.Column(sqlalchemy.Float)
    blend_fraction = sqlalchemy.Column(sqlalchemy.Float)


class ArchiveJobsTable(Base):
    """
    Database model for the archive jobs database table
    """
    __tablename__ = "archive_jobs"
    id = sqlalchemy.Column(sqlalchemy.Integer, primary_key=True)
    pipeline = sqlalchemy.Column(sqlalchemy.Integer)
    field = sqlalchemy.Column(sqlalchemy.String(16))
    camera = sqlalchemy.Column(sqlalchemy.String(16))
    campaign = sqlalchemy.Column(sqlalchemy.String(16))
    photometry = sqlalchemy.Column(sqlalchemy.String(16))
    stage = sqlalchemy.Column(sqlalchemy.Integer)
    name = sqlalchemy.Column(sqlalchemy.String(16))
    status = sqlalchemy.Column(sqlalchemy.String(16))
    start_date = sqlalchemy.Column(sqlalchemy.DateTime)
    end_date = sqlalchemy.Column(sqlalchemy.DateTime)
    in_queue = sqlalchemy.Column(sqlalchemy.Integer)
    duration = sqlalchemy.Column(sqlalchemy.Integer)
    username = sqlalchemy.Column(sqlalchemy.String(16))
    qsub_id = sqlalchemy.Column(sqlalchemy.String(16))
    tfa = sqlalchemy.Column(sqlalchemy.Boolean)


class HunterRunsTable(Base):
    """
    Database model for the hunters run table
    """
    __tablename__ = "hunter_runs"
    field = sqlalchemy.Column(sqlalchemy.String(14), primary_key=True)
    camera_id = sqlalchemy.Column(sqlalchemy.Integer, primary_key=True)
    tag = sqlalchemy.Column(sqlalchemy.String(16), primary_key=True)
    ingest = sqlalchemy.Column(sqlalchemy.DateTime)
    ncand = sqlalchemy.Column(sqlalchemy.Integer)
    avgpoints = sqlalchemy.Column(sqlalchemy.Integer)
    tstart = sqlalchemy.Column(sqlalchemy.Date)
    tstop = sqlalchemy.Column(sqlalchemy.Date)
    campaign = sqlalchemy.Column(sqlalchemy.String(20))
    code = sqlalchemy.Column(sqlalchemy.Enum('HUNTER', 'ORION'))
    code_version = sqlalchemy.Column(sqlalchemy.String(10))
    first_night = sqlalchemy.Column(sqlalchemy.Date)
    last_night = sqlalchemy.Column(sqlalchemy.Date)


class FieldSeason(qesdb.Connection):
    """
    Object model view class for routines involving the field seasons (campaign)
    """
    @qesutil.check_all_param_for_none
    def get_image_stats_for_field(self, field=None, camera_id=None, campaign=None):
        """
        Next stats on the images for a given field, camera and campaign from the imagelist table. For example the first
        night, median fwhm etc.

        :param field: Field string
        :param camera_id: Archive camera id
        :param campaign: Campaign string
        :return: Named tuple of the stat results. (field, camera_id, campaign, first_night, last_night, nimage, nnight,
                 psf_fwhm_med, psf_fwhm_iqr)
        """
        with self.session_scope() as session:
            return session.query(ImageListTable.field, ImageListTable.camera_id, ImageListTable.campaign,
                                 func.min(ImageListTable.night).label('first_night'),
                                 func.max(ImageListTable.night).label('last_night'),
                                 func.count(ImageListTable.image_id).label('nimage'),
                                 func.count(func.distinct(ImageListTable.night)).label('nnight'),
                                 func.median(ImageListTable.fwhm1).label('psf_fwhm_med'),
                                 func.iqr(ImageListTable.fwhm1).label('psf_fwhm_iqr'))\
                .filter((ImageListTable.camera_id == camera_id) & (ImageListTable.field == field)
                        & (ImageListTable.campaign == campaign))\
                .group_by(ImageListTable.camera_id, ImageListTable.field, ImageListTable.campaign).one()

    @qesutil.check_all_param_for_none
    def get_current_max_field_season(self, field=None, camera_id=None, base_campaign=None):
        """
        Get the maximum campaign version, e.g. C7A etc in the hunter run table. That is the current latest campaign
        version for the campaign in the archive. If the campaign is not in the hunter table return the base campaign
        plus A.

        :param field: Field string
        :param camera_id: archive camera id
        :param base_campaign: Base campaign string
        :return: campaign version string which is the latest version in the archive for that data
        :rtype: str

        """
        with self.session_scope() as session:
            max_field_season = session.query(func.max(HunterRunsTable.campaign).label('max_campaign'))\
                .filter((HunterRunsTable.field == field) & (HunterRunsTable.camera_id == camera_id)
                        & (HunterRunsTable.campaign.like(base_campaign + '%'))).one()
            if max_field_season is not None:
                return max_field_season.max_campaign
            else:
                return base_campaign + "A"

    @qesutil.check_all_param_for_none
    def get_field_season_version_letter(self, field=None, camera_id=None, base_campaign=None):
        """
        Get the next campaign version letter to be used when creating a new campaign version in the archive. The routine
        counts the number of campaigns in the field pseudo table and uses that to index the alphabet list.

        :param field: Field string
        :param camera_id: archive camera id
        :param base_campaign: base campaign string
        :return: letter for the next campaign version
        :rtype: str

        """
        import string
        alphabet = list(string.ascii_uppercase)
        with self.session_scope() as session:
            pseudo_fields = session.query(FieldSeasonPseudoTable)\
                .filter((FieldSeasonPseudoTable.camera_id == camera_id) & (FieldSeasonPseudoTable.field == field) &
                        (FieldSeasonPseudoTable.base_campaign == base_campaign)).count()
            return alphabet[pseudo_fields]

    @qesutil.check_all_param_for_none
    def field_season_new(self, field=None, camera_id=None, campaign=None):
        """
        Test to see if the field season (campaign) is new. Do this by counting the number of times it is in the
        field season table

        :param field: Field string
        :param camera_id: archive camera id
        :param campaign: campaign string
        :return: Return False if the field is found if the field is new return True
        :rtype: bool
        """
        with self.session_scope() as session:
            count = session.query(FieldSeasonTable).filter((FieldSeasonTable.field == field) &
                                                           (FieldSeasonTable.camera_id == camera_id) &
                                                           (FieldSeasonTable.campaign == campaign)).count()

            return False if count else True

    @qesutil.check_all_param_for_none
    def add_field_season_pseudo(self, field=None, camera_id=None, campaign=None, base_campaign=None,
                                first_night=None, last_night=None, nimage=None, nnight=None, complete=None,
                                psf_fwhm_med=None, psf_fwhm_iqr=None):
        """
        Add a new field campaign version to the field season pseudo table. The table contains stats on the field
        campaign version such as date range etc which need to be provided to this function.

        :param field: Field string
        :param camera_id: archive camera id
        :param campaign: campaign string
        :param base_campaign: campaign string base (i.e. no version letter)
        :param first_night: first night data was taken for the field
        :param last_night: last night data was taken for the field
        :param nimage: number of images for the field
        :param nnight: number of night for the field
        :param complete: if the field is complete, normally set to the string 'no'
        :param psf_fwhm_med: median fwhm
        :param psf_fwhm_iqr: the fwhm inter quartile range
        :return: True on success if not false
        :rtype: bool
        """
        with self.session_scope() as session:
            new_field_season_pseudo = FieldSeasonPseudoTable(field=field, camera_id=camera_id, campaign=campaign,
                                                             base_campaign=base_campaign, first_night=first_night,
                                                             last_night=last_night, nimage=nimage, nnight=nnight,
                                                             complete=complete, psf_fwhm_med=psf_fwhm_med,
                                                             psf_fwhm_iqr=psf_fwhm_iqr)
            try:
                session.add(new_field_season_pseudo)
                session.commit()
                return True
            except sqlalchemy.exc.DBAPIError:
                return False

    @qesutil.check_all_param_for_none
    def add_field_season(self, field=None, camera_id=None, campaign=None, first_night=None, last_night=None,
                         nimage=None, nnight=None, complete=None, psf_fwhm_med=None, psf_fwhm_iqr=None,
                         season_type=None):
        """
        Add a new field campaign to the field season table. The table contains stats on the field campaign such as
        date range etc which need to be provided to this function. If this is a campaign version that is being added
        then the season type is set to pseudo, if this is the main entry for the field then the string real is used.

        :param field: Field string
        :param camera_id: archive camera id
        :param campaign: campaign string
        :param first_night: first night data was taken for the field
        :param last_night: last night data was taken for the field
        :param nimage: number of images for the field
        :param nnight: number of night for the field
        :param complete: if the field is complete, normally set to the string 'no'
        :param psf_fwhm_med: median fwhm
        :param psf_fwhm_iqr: the fwhm inter quartile range
        :param season_type: 'pseudo' or 'real' depending on his this is the main entry into the table for the field
                            or a campaign version (pseudo) field
        :return: True on success if not false
        :rtype: bool
        """
        with self.session_scope() as session:
            new_field_season = FieldSeasonTable(field=field, camera_id=camera_id, campaign=campaign,
                                                first_night=first_night, last_night=last_night, nimage=nimage,
                                                nnight=nnight, complete=complete, psf_fwhm_med=psf_fwhm_med,
                                                psf_fwhm_iqr=psf_fwhm_iqr, type=season_type)
            try:
                session.add(new_field_season)
                session.commit()
                return True
            except sqlalchemy.exc.DBAPIError:
                return False

    @qesutil.check_all_param_for_none
    def update_field_season(self, field=None, camera_id=None, campaign=None, first_night=None, last_night=None,
                            nimage=None, nnight=None, complete=None, psf_fwhm_med=None, psf_fwhm_iqr=None,
                            season_type="NULL"):
        """
        Update the stats for a field season in the field season table. For example this might be called when new data
        has been ingested and a new campaign version has been added to update the 'real' field with the full stats from
        the new data.

        :param field: Field string
        :param camera_id: archive camera id
        :param campaign: campaign string
        :param first_night: first night data was taken for the field
        :param last_night: last night data was taken for the field
        :param nimage: number of images for the field
        :param nnight: number of night for the field
        :param complete: if the field is complete, normally set to the string 'no'
        :param psf_fwhm_med: median fwhm
        :param psf_fwhm_iqr: the fwhm inter quartile range
        :param season_type: Not used as this is not expected to be changed
        :return: True on success if not false
        :rtype: bool
        """
        with self.session_scope() as session:
            session.query(FieldSeasonTable).filter((FieldSeasonTable.field == field) &
                                                   (FieldSeasonTable.camera_id == camera_id) &
                                                   (FieldSeasonTable.campaign == campaign))\
                .update({'first_night': first_night, 'last_night': last_night, 'nimage': nimage, 'nnight': nnight,
                         'complete': complete, 'psf_fwhm_med': psf_fwhm_med, 'psf_fwhm_iqr': psf_fwhm_iqr})
            try:
                session.commit()
                return True
            except sqlalchemy.exc.DBAPIError:
                return False

    @qesutil.check_all_param_for_none
    def get_field_season(self, field=None, camera_id=None, campaign=None):
        """
        Get the entry for a field season (campaign) from the field season table for a given field, camera and campaign.

        :param field: Field string
        :param camera_id: archive camera id
        :param campaign: campaign string
        :return: Named tuple with the field season table info (field, camera_id, campaign, first_night, last_night,
                 nimage, nnight, complete, psf_fwhm_med, psf_fwhm_iqr, type)
        """
        with self.session_scope() as session:
            return session.query(FieldSeasonTable.field, FieldSeasonTable.camera_id, FieldSeasonTable.campaign,
                                 FieldSeasonTable.first_night, FieldSeasonTable.last_night, FieldSeasonTable.nimage,
                                 FieldSeasonTable.nnight, FieldSeasonTable.complete, FieldSeasonTable.psf_fwhm_med,
                                 FieldSeasonTable.psf_fwhm_iqr, FieldSeasonTable.type)\
                .filter((FieldSeasonTable.field == field) & (FieldSeasonTable.camera_id == camera_id) &
                        (FieldSeasonTable.campaign == campaign)).one()

    @qesutil.check_all_param_for_none
    def get_field_season_pseudo(self, field=None, camera_id=None, campaign=None):
        """
        Get the entry for a field season (campaign) from the field season pseudo table for a given field, camera and
        campaign.

        :param field: Field string
        :param camera_id: archive camera id
        :param campaign: campaign string
        :return: Named tuple with the field season table info (field, camera_id, campaign, base_campaign, first_night,
                 last_night, nimage, nnight, complete, psf_fwhm_med, psf_fwhm_iqr)
        """
        with self.session_scope() as session:
            return session.query(FieldSeasonPseudoTable.field, FieldSeasonPseudoTable.camera_id,
                                 FieldSeasonPseudoTable.campaign, FieldSeasonPseudoTable.base_campaign,
                                 FieldSeasonPseudoTable.first_night, FieldSeasonPseudoTable.last_night,
                                 FieldSeasonPseudoTable.nimage, FieldSeasonPseudoTable.nnight,
                                 FieldSeasonPseudoTable.complete, FieldSeasonPseudoTable.psf_fwhm_med,
                                 FieldSeasonPseudoTable.psf_fwhm_iqr)\
                .filter((FieldSeasonPseudoTable.field == field) & (FieldSeasonPseudoTable.camera_id == camera_id) &
                        (FieldSeasonPseudoTable.campaign == campaign)).one()

    @qesutil.check_all_param_for_none
    def delete_field_season(self, field=None, camera_id=None, campaign=None):
        """
        Delete an entry from the field season table

        :param field: Field string
        :param camera_id: archive camera id
        :param campaign: campaign string
        :return: True if a row was deleted if not False
        :rtype: bool
        """
        with self.session_scope() as session:
            rows = session.query(FieldSeasonTable).filter((FieldSeasonTable.field == field) &
                                                          (FieldSeasonTable.camera_id == camera_id) &
                                                          (FieldSeasonTable.campaign == campaign)).delete()
            session.commit()
            return True if rows else False

    @qesutil.check_all_param_for_none
    def delete_field_season_pseudo(self, field=None, camera_id=None, campaign=None):
        """
        Delete an entry from the field season pseudo table

        :param field: Field string
        :param camera_id: archive camera id
        :param campaign: campaign string
        :return: True if a row was deleted if not False
        :rtype: bool
        """
        with self.session_scope() as session:
            rows = session.query(FieldSeasonPseudoTable).filter((FieldSeasonPseudoTable.field == field) &
                                                                (FieldSeasonPseudoTable.camera_id == camera_id) &
                                                                (FieldSeasonPseudoTable.campaign == campaign)).delete()
            session.commit()
            return True if rows else False


class DiffList(qesdb.Connection):
    """
    Object model view class for routines involving the difference image list
    """
    @qesutil.check_all_param_for_none
    def get_new_diff_images(self, field=None, camera_id=None, campaign=None):
        """
        Return the list of images for a field that to not have a difference image filepath. That is new images that have
        been added for which difference image data has not been added to the database yet.

        :param field: Field string
        :param camera_id: archive camera id
        :param campaign: campaign string
        :return: Named tuple of images which have no difference image file path. (image_id, fileleaf, filepath)
        """
        with self.session_scope() as session:
            return session.query(ImageListTable.image_id, ImageListTable.fileleaf, DiffListTable.filepath)\
                .outerjoin(DiffListTable, ImageListTable.image_id == DiffListTable.image_id)\
                .filter((ImageListTable.field == field) & (ImageListTable.camera_id == camera_id) &
                        (ImageListTable.campaign == campaign) & (DiffListTable.filepath.is_(None))).all()

    @qesutil.check_all_param_for_none
    def add_diff_image(self, image_id=None, file_path=None):
        """
        At a new entry into the difference image database for supplied image_id and the filepath of the difference
        image

        :param image_id: Image id of the difference image
        :param file_path: File path of the difference image
        :return: True if success else false
        :rtype: bool
        """
        with self.session_scope() as session:
            new_diff_image = DiffListTable(image_id=image_id, filepath=file_path)
            try:
                session.add(new_diff_image)
                session.commit()
                return True
            except sqlalchemy.exc.DBAPIError:
                return False


class ArchiveJobs(qesdb.Connection):
    """
    Object model view class for routines involving the archive jobs
    """
    @qesutil.check_all_param_for_none
    def end_job_in_queue(self, job_id=None, status=pbs.status.running):
        with self.session_scope() as session:
            job = session.query(ArchiveJobsTable).filter(ArchiveJobsTable.id == job_id).one()
            job.status = status
            in_queue_delta = datetime.datetime.now() - job.start_date
            try:
                job.in_queue = int(round(in_queue_delta.total_seconds()))
            # timedelta.total_seconds() requires Python 2.7+
            except AttributeError:
                job.in_queue = int(round(in_queue_delta.seconds + (in_queue_delta.days * 24 * 3600)))
            job.start_date = datetime.datetime.now()
            try:
                session.commit()
                return True
            except sqlalchemy.exc.DBAPIError:
                return False

    @qesutil.check_all_param_for_none
    def end_job(self, job_id=None, status=None):
        with self.session_scope() as session:
            job = session.query(ArchiveJobsTable).filter(ArchiveJobsTable.id == job_id).one()
            job.status = status
            job.end_date = datetime.datetime.now()
            duration_delta = job.end_date - job.start_date
            try:
                job.duration = int(round(duration_delta.total_seconds()))
            # timedelta.total_seconds() requires Python 2.7+
            except AttributeError:
                job.duration = int(round(duration_delta.seconds + (duration_delta.days * 24 * 3600)))
            try:
                session.commit()
                return True
            except sqlalchemy.exc.DBAPIError:
                return False

    @qesutil.check_all_param_for_none
    def get_job(self, job_id=None):
        with self.session_scope() as session:
            return session.query(ArchiveJobsTable.name, ArchiveJobsTable.status, ArchiveJobsTable.in_queue,
                                 ArchiveJobsTable.id)\
                .filter(ArchiveJobsTable.id == job_id).one()

    @qesutil.check_nearly_all_param_for_none(can_be_none=["tfa"])
    def queue_next_in_pipeline(self, job_id=None, tfa=None, status=pbs.status.queued):
        with self.session_scope() as session:
            job = session.query(ArchiveJobsTable.pipeline).filter(ArchiveJobsTable.id == job_id).one()
            pipeline = job.pipeline
            if tfa is not None:
                try:
                    next_job = session.query(ArchiveJobsTable).filter((ArchiveJobsTable.pipeline == pipeline)
                                                                      & (ArchiveJobsTable.id > job_id)
                                                                      & (ArchiveJobsTable.tfa == tfa)).limit(1).one()
                    next_job.status = status
                    next_job.start_date = datetime.datetime.now()
                    try:
                        session.commit()
                        return True
                    except sqlalchemy.exc.DBAPIError:
                        return False
                except sqlalchemy.orm.exc.NoResultFound:
                    return

            else:
                try:
                    next_job = session.query(ArchiveJobsTable).filter((ArchiveJobsTable.pipeline == pipeline) &
                                                                      (ArchiveJobsTable.id > job_id)).limit(1).one()

                    next_job.status = status
                    next_job.start_date = datetime.datetime.now()
                    if next_job.tfa is not None:
                        try:

                            next_job_tfa = session.query(ArchiveJobsTable)\
                                .filter((ArchiveJobsTable.pipeline == pipeline) &
                                        (ArchiveJobsTable.id > next_job.id)).limit(1).one()

                            next_job_tfa.status = status
                            next_job_tfa.start_date = datetime.datetime.now()
                        except sqlalchemy.orm.exc.NoResultFound:
                            pass
                    try:
                        session.commit()
                        return True
                    except sqlalchemy.exc.DBAPIError:
                        return False

                except sqlalchemy.orm.exc.NoResultFound:
                    return

    @qesutil.check_nearly_all_param_for_none(can_be_none=["tfa"])
    def fail_rest_of_pipeline(self, job_id=None, tfa=None, status=pbs.status.failed):
        with self.session_scope() as session:
            job = session.query(ArchiveJobsTable.pipeline).filter(ArchiveJobsTable.id == job_id).one()
            pipeline = job.pipeline
            if tfa is not None:
                session.query(ArchiveJobsTable).filter((ArchiveJobsTable.pipeline == pipeline) &
                                                       (ArchiveJobsTable.id > job_id) &
                                                       (ArchiveJobsTable.tfa == tfa)).update({'status': status})
                session.query(ArchiveJobsTable).filter((ArchiveJobsTable.pipeline == pipeline) &
                                                       (ArchiveJobsTable.id > job_id) &
                                                       (ArchiveJobsTable.tfa.is_(None))).update({'status': status})
            else:
                session.query(ArchiveJobsTable).filter((ArchiveJobsTable.pipeline == pipeline) &
                                                       (ArchiveJobsTable.id > job_id)).update({'status': status})
            try:
                session.commit()
                return True
            except sqlalchemy.exc.DBAPIError:
                return False

    @qesutil.check_nearly_all_param_for_none(can_be_none=["tfa"])
    def get_rest_of_pipeline_ids(self, job_id=None, tfa=None):
        with self.session_scope() as session:
            job = session.query(ArchiveJobsTable.pipeline).filter(ArchiveJobsTable.id == job_id).one()
            pipeline = job.pipeline
            if tfa is not None:
                ids = session.query(ArchiveJobsTable.qsub_id).filter((ArchiveJobsTable.pipeline == pipeline) &
                                                                     (ArchiveJobsTable.id > job_id) &
                                                                     (ArchiveJobsTable.tfa == tfa)).all()
                ids += session.query(ArchiveJobsTable.qsub_id).filter((ArchiveJobsTable.pipeline == pipeline) &
                                                                      (ArchiveJobsTable.id > job_id) &
                                                                      (ArchiveJobsTable.tfa.is_(None))).all()
            else:
                ids = session.query(ArchiveJobsTable.qsub_id).filter((ArchiveJobsTable.pipeline == pipeline) &
                                                                     (ArchiveJobsTable.id > job_id)).all()
            return ids

    @qesutil.check_nearly_all_param_for_none(can_be_none=["pipeline_id", "tfa"])
    def save_job_get_id(self, field=None, campaign=None, camera=None, photometry=None, stage=None, name=None,
                        username=None, status=None, pipeline_id=None, tfa=False):

        if pipeline_id is None:
            pipeline_id = self.get_new_pipeline_id()

        session = self.Session()
        try:
            new_job = ArchiveJobsTable(field=field, campaign=campaign, camera=camera, photometry=photometry,
                                       stage=stage, username=username, status=status, name=name, tfa=tfa,
                                       pipeline=pipeline_id, start_date=datetime.datetime.now())
            session.add(new_job)
            session.flush()
            return session, new_job
        except:
            session.close()
            raise

    def get_new_pipeline_id(self):
        with self.session_scope() as session:
            max_id = session.query(func.max(ArchiveJobsTable.pipeline).label('pipeline_id')).one()
            if max_id.pipeline_id:
                return max_id.pipeline_id+1
            else:
                return 1

    @staticmethod
    def commit_saved_job(session):
        try:
            session.commit()
            return True
        except sqlalchemy.exc.DBAPIError:
            session.rollback()
            return False
        finally:
            session.close()