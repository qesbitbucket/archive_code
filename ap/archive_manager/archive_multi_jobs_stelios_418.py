#!/usr/bin/env python

import os
import sys
import archive_constants
import archive_util
try:
    import qesutil
    import Alsubai_Control.pbs as pbs
except ImportError:
    import qes_pipeline.qesutil as qesutil
    import qes_pipeline.Alsubai_Control.pbs as pbs

from archive_jobs import ArchivePipelineProgram
import archive_jobs
import archive_orion_multi_stelios

__author__ = 'Neil Parley'

_ORION_STAGE_MULTI = ArchivePipelineProgram(name="orion_multi", script="archive_orion_multi_stelios_418.py",
                                            tfa=False,
                                            dependency=None, cpus=8, memory=32, nodes=1,
                                            length=165,
                                            work_dir=True, orion_code=True, out_name="orion",
                                            tag=True, single_field=False)
_MCMC_STAGE = ArchivePipelineProgram(name="mcmc_multi", script="archive_mcmc_stelios_418.py", tfa=False,
                                     dependency=[_ORION_STAGE_MULTI], cpus=8, memory=32, nodes=1,
                                     length=165,
                                     work_dir=True, mcmc_code=True, out_name="orion", tag=True)
_UPDATE_STAGE = ArchivePipelineProgram(name="update", script="archive_update_stelios_418.py", tfa=False,
                                       dependency=[_MCMC_STAGE],
                                       cpus=1, memory=4, nodes=1, length=100,
                                       work_dir=True, orion_code=True, out_name=None, tag=True)
#_CLEAN_STAGE = ArchivePipelineProgram(name="clean", script="archive_clean.py", tfa=None,
#                                      dependency=[_UPDATE_STAGE],
#                                      cpus=1, memory=4, nodes=1, length=5, work_dir=True)

#archive_pipeline_multi = (_ORION_STAGE_MULTI, _MCMC_STAGE, _UPDATE_STAGE, _CLEAN_STAGE)
archive_pipeline_multi = (_ORION_STAGE_MULTI, _MCMC_STAGE, _UPDATE_STAGE)

archive_pipeline_names = [_stage.name for _stage in archive_pipeline_multi]


@qesutil.check_nearly_all_param_for_none(can_be_none=["data_dir", "code_dir",
                                                      "archive_code", "orion_code", "mcmc_code",
                                                      "pbs_server", "start_with"])
def submit_archive_job_multi(fields=None, qes_path=None, username=None,
                             code_dir=archive_constants.archive_manager, data_dir=None,
                             work_dir=None, archive_code=None, orion_code=None, mcmc_code=None,
                             tfa=False, pbs_server=None, multi_only=False, _dry_run=False,
                             start_with=None):

    field_pipelines = []
    field_args = []
    _ORION_STAGE_MULTI.extra_commands = []

    if tfa:
        _ORION_STAGE_MULTI.tfa = True
        _MCMC_STAGE.tfa = True
        _UPDATE_STAGE.tfa = True

    if multi_only:
        _ORION_STAGE_MULTI.extra_commands += ["--multionly"]

    multi_info = archive_orion_multi_stelios.get_info(fields=fields, work_dir=work_dir, create=False)

    for field in fields:
        camera = field[0]
        field_name = field[1]
        campaign = field[2]
        photometry = field[3]
        camera_id = field[4]
        field_args += ["--field", camera, field_name, campaign, photometry.name]

        if start_with is None:
            pipeline = archive_jobs.ArchivePipeline(photometry=photometry, camera=camera,
                                                    username=username, camera_id=camera_id,
                                                    tag="MULTI",
                                                    field=field_name, campaign=campaign,
                                                    qes_path=qes_path, code_dir=code_dir,
                                                    data_dir=data_dir,
                                                    work_dir=multi_info.work_root,
                                                    archive_code=archive_code,
                                                    orion_code=orion_code,
                                                    mcmc_code=mcmc_code, exclude_tfa=not tfa,
                                                    only_tfa=tfa, rerun=True, pbs_server=pbs_server,
                                                    start_with="ingest", end_with="sysrem")
            pipeline.submit_pipeline(dry_run=_dry_run)
            field_pipelines.append(pipeline)

    _ORION_STAGE_MULTI.extra_commands += field_args
    pipeline = archive_jobs.ArchivePipeline(photometry=multi_info.photometry_info,
                                            camera=multi_info.camera,
                                            username=username, camera_id=multi_info.camera_id,
                                            tag="NEW_MULTI", field=multi_info.field_name,
                                            campaign=multi_info.campaign,
                                            qes_path=qes_path, code_dir=code_dir,
                                            data_dir=data_dir, work_dir=work_dir,
                                            archive_code=archive_code, orion_code=orion_code,
                                            mcmc_code=mcmc_code, exclude_tfa=not tfa,
                                            only_tfa=tfa, rerun=True, pbs_server=pbs_server,
                                            _pipeline=archive_pipeline_multi, start_with=start_with,
                                            dependency=field_pipelines)
    pipeline.submit_pipeline(dry_run=_dry_run)

if __name__ == '__main__':

    try:
        import configparser
    except ImportError:
        import ConfigParser as configparser

    import argparse

    try:
        _qes_path = os.environ['QES_PATH']
        _username = os.environ['USER']
    except NameError as n_error:
        print("Environmental variable not set: %s" % n_error)
        sys.exit(2)

    _data_dir_default = os.path.join(_qes_path, "qes_data")
    _work_dir_default = os.path.join(_qes_path, archive_constants.archive_work)
    _archive_code_default = os.path.join(_qes_path, archive_constants.archive_code)
    _mcmc_code_defaualt = os.path.join(_qes_path, archive_constants.mcmc_code)
    _orion_code_default = os.path.join(_qes_path, archive_constants.orion_code)

    parser = argparse.ArgumentParser(description='Runs an archive job')
    parser.add_argument('--field', action='append', nargs=4, required=True,
                        metavar=('CAMERA', 'FIELD', 'CAMPAIGN', 'TYPE'),
                        help="Add as many fields as you wish to combine, each --field needs:\n "
                             "Camera to ingest (e.g. KCAM_US_T01D01), "
                             "Field to ingest (e.g. 002000+350000), "
                             "Campaign to ingest (e.g. C7), "
                             "Photometry type (dia or aperture)")

    parser.add_argument("--data_dir", default=None,
                        help="Root directory of qes Files (default: {dir})"
                        .format(dir=_data_dir_default))
    parser.add_argument("--work_dir", default=_work_dir_default,
                        help="Work directory of ingest Files (default: {dir})"
                        .format(dir=_work_dir_default))
    parser.add_argument("--archive_code", default=None,
                        help="Directory of archive code (default: {dir})"
                        .format(dir=_archive_code_default))
    parser.add_argument("--mcmc_code", default=None,
                        help="Directory of mcmc code (default: {dir})"
                        .format(dir=_mcmc_code_defaualt))
    parser.add_argument("--orion_code", default=None,
                        help="Directory of orion code (default: {dir})"
                        .format(dir=_orion_code_default))
    parser.add_argument("--tfa", help="Run the tfa stages aswell as the Tamuz ones (default False)",
                        action="store_true")
    parser.add_argument("--dry_run", help="Don't submit the jobs (testing)",
                        action="store_true")
    parser.add_argument("--multionly", help="Only stars in all the fields (default False)",
                        action="store_true")
    parser.add_argument("--start_with", default=None, choices=archive_pipeline_names,
                        help="Pipeline Stage to start the pipeline ")

    c_args = parser.parse_args()

    if len(c_args.field) < 2:
        print("Error: You need to provide at least two different fields to archive_multi_jobs")
        sys.exit(1)

    _alsubai_control_path = os.path.join(_qes_path, 'qes_pipeline', 'Alsubai_Control')

    for f in c_args.field:

        if f[3] not in ('dia', 'aperture'):
            print("Error: Type must be dia or aperture")
        else:
            f[3] = getattr(archive_constants.photometry, f[3])

        _config_cam_campaign = configparser.ConfigParser()
        _campaign = archive_util.get_global_campaign(f[2])

        if not _config_cam_campaign.read(os.path.join(_alsubai_control_path, 'cfg', _campaign,
                                                      'config.' + f[0] + '.cfg')):
            print('ERROR - The configuration file is missing for the camera: ' +
                  os.path.join('cfg', _campaign, 'config.' + f[0] + '.cfg'))
            sys.exit(1)

        _camera_id = _config_cam_campaign.get('main', 'camID')
        f.append(_camera_id)

    submit_archive_job_multi(fields=c_args.field, qes_path=_qes_path, username=_username,
                             code_dir=archive_constants.archive_manager, data_dir=c_args.data_dir,
                             work_dir=c_args.work_dir, archive_code=c_args.archive_code,
                             orion_code=c_args.orion_code, mcmc_code=c_args.mcmc_code,
                             tfa=c_args.tfa, multi_only=c_args.multionly, _dry_run=c_args.dry_run,
                             start_with=c_args.start_with)
