#!/usr/bin/env python
"""
This module runs Richards sysrem code. The main calling function is archive_search.run_sysrem. The normal calling
procedure for this module would be:

./archive_sysrem.py KCAM_US_T01D01 002000+450000 C7 dia

which would run sysrem on the dia file for field 002000+450000 / camera KCAM_US_T01D01. By default this script will
search for a file with the pattern {field}_{camera_id}_{campaign}.fits if the file is different i.e. you might have
wanted to run something before sysrem you can use --fname value and this will then search for a file with the pattern
{field}_{camera_id}_{campaign}_value.fits

--archive_code can be used to overload the location of the archive code although if the code moves permanently the
value in the archive_constants file should be changed.

--job_id is used for when the code is being run by the archive_jobs.py code and should not be used if the code is
being used from the command line

--work_dir is the root directory where the working archive directories are. The code will look in a sub directory
of for example 002000+450000_KCAM_US_T01D01_C7_dia for the files.
"""
from __future__ import print_function
__author__ = 'Neil Parley'
import os
import sys
try:
    import qesutil
except ImportError:
    import qes_pipeline.qesutil as qesutil
import archive_constants
import archive_util
import archive_search
import archive_jobs


@archive_jobs.register_job
@qesutil.check_nearly_all_param_for_none(can_be_none=["job_id", "fname"])
def sysrem(camera=None, field=None, campaign=None, work_dir=None, camera_id=None, code_dir=None,
           photometry_info=None, job_id=None, fname=None):
    """
    Runs sysrem on the archive data file.

    The function gets the global campaign and does not assume that is given correctly. It then finds the working
    directory root where the files are. The global campaign for the photometry type this then created. The code will
    then search for a matching file and from that file work out the pysdo campaign. I.e. C7B etc. The code will then
    run Richard's sysrem code by calling the archive_search.run_sysrem function

    :type photometry_info: archive_constants.Photometry_info
    :param camera: Camera string e.g. KCAM_US_T01D01
    :param field: Field string e.g. 002000+450000
    :param campaign: Campaign string e.g. C7
    :param work_dir: Base working directory should be set to os.path.join(qes_path, archive_constants.archive_work) if
                     using the default
    :param camera_id: The database camera id, can be loaded from the configuration files
    :param code_dir: Location of the archive code
    :param photometry_info: The photometry type of the run, types defined in archive_constants
    :param job_id: This is given to the function when it is started as a job by archive_jobs. It is used by the
                   decorator archive_jobs.register_job to update the database with when the job starts and finishes.
    :param fname: Tells the code the underscore of the preceding file. See module text above.
    """
    campaign_global = archive_util.get_global_campaign(campaign)

    work_root = archive_util.create_work_root(field=field, campaign=campaign_global, camera=camera,
                                              photometry_info=photometry_info, work_dir=work_dir)

    campaign_global_phot = photometry_info.campaign_letter + campaign_global[1:]
    fits_file, campaign_phot = archive_search.find_file(work_root=work_root, field=field, camera_id=camera_id,
                                                        campaign=campaign_global_phot, tag=None, fname=fname)

    print("Using fits file {file}".format(file=fits_file))
    print("Campaign = {campaign}".format(campaign=campaign_phot))
    sysrem_file = archive_search.run_sysrem(input_file=fits_file, work_root=work_root, code_dir=code_dir,
                                            field=field, camera_id=camera_id, campaign=campaign_phot)
    print("{file} created".format(file=sysrem_file))


if __name__ == '__main__':

    try:
        import configparser
    except ImportError:
        import ConfigParser as configparser

    import argparse

    try:
        _qes_path = os.environ['QES_PATH']
    except NameError as n_error:
        print("Environmental variable not set: %s" % n_error)
        sys.exit(2)

    _work_dir_default = os.path.join(_qes_path, archive_constants.archive_work)
    _archive_code_default = os.path.join(_qes_path, archive_constants.archive_code)

    parser = argparse.ArgumentParser(description='Runs sysrem on the archive fetch file')
    parser.add_argument('camera', metavar='CAMERA', help='Camera to ingest (e.g. KCAM_US_T01D01)')
    parser.add_argument('field', metavar='FIELD', help='Field to ingest (e.g. 002000+350000)')
    parser.add_argument('campaign', metavar='CAMPAIGN', help='Campaign to ingest (e.g. C7)')
    parser.add_argument('photometry', metavar='TYPE', choices=['dia', 'aperture'],
                        help='Photometry type (dia or aperture)')
    parser.add_argument("--work_dir", default=_work_dir_default,
                        help="Work directory of ingest Files (default: {dir})".format(dir=_work_dir_default))
    parser.add_argument("--archive_code", default=_archive_code_default,
                        help="Directory of archive code (default: {dir})".format(dir=_archive_code_default))
    parser.add_argument("--job_id", default=None, type=int, help="Job id if running on the cluster")
    parser.add_argument("--fname", default=None, help="File name from last programme in the pipeline default ''"
                                                      "(will search "
                                                      "field_camera_id_campaign[A-Z].fits by default or if fname "
                                                      "included field_camera_id_campaign[A-Z]_fname.fits)")
    args = parser.parse_args()

    _alsubai_control_path = os.path.join(_qes_path, 'qes_pipeline', 'Alsubai_Control')
    _config_cam_campaign = configparser.ConfigParser()

    if not _config_cam_campaign.read(os.path.join(_alsubai_control_path,
                                                  'cfg', args.campaign, 'config.' + args.camera + '.cfg')):
        print('ERROR - The configuration file is missing for the camera: ' +
              os.path.join('cfg', args.campaign, 'config.' + args.camera + '.cfg'))
        sys.exit(1)

    sysrem(camera=args.camera, field=args.field, campaign=args.campaign, work_dir=args.work_dir,
           camera_id=_config_cam_campaign.get('main', 'camID'), code_dir=args.archive_code,
           photometry_info=getattr(archive_constants.photometry, args.photometry), job_id=args.job_id, fname=args.fname)
