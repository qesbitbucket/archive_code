#!/usr/bin/env python
"""
This module contains the routines for calling Richard's arhive code like orion and mcmc. It can use be used to run a
search on a fetched archive file. I.e. will run sysrem, orion, mcmc and then update. However this has been superseded
by the use of archive_jobs and also archive_search does not give you the same flexibility as running the programs like
archive_orion separately. That is it is only really useful for default searches.
"""
from __future__ import print_function
import os
import sys
import glob
import subprocess
import numpy
from astropy.io import fits
try:
    import qesutil
except ImportError:
    import qes_pipeline.qesutil as qesutil
import archive_constants
import archive_util

__author__ = 'Neil Parley'


@qesutil.check_nearly_all_param_for_none(can_be_none=["tag"])
def search(camera=None, field=None, campaign=None, work_dir=None, code_dir=None, orion_dir=None, mcmc_dir=None,
           camera_id=None, photometry_info=None, tag=None):
    """
    This routine runs sysrem, orion, mcmc and then the routines to update the database tables on an fetched archive file
    It is probably better to use archive_jobs but the code is left here in case it is useful.

    :param camera: Camera string e.g. KCAM_US_T01D01
    :param field: Field string e.g. 002000+450000
    :param campaign: Campaign string e.g. C7
    :param work_dir: Base working directory should be set to os.path.join(qes_path, archive_constants.archive_work) if
                     using the default
    :param code_dir: Location of the archive code
    :param orion_dir: Location of the orion code
    :param mcmc_dir: Location of the mcmc code
    :param camera_id: The database camera id, can be loaded from the configuration files
    :param photometry_info: The photometry type of the run, types defined in archive_constants
    :param tag: tag to use in the archive for the run, default is TAMUZ / TAMTFA set to DOHA for a doha run
    """

    campaign_global = archive_util.get_global_campaign(campaign)

    work_root = archive_util.create_work_root(field=field, campaign=campaign_global, camera=camera,
                                              photometry_info=photometry_info, work_dir=work_dir)

    campaign_global_phot = photometry_info.campaign_letter + campaign_global[1:]

    fits_file, campaign_phot = find_file(work_root=work_root, field=field, camera_id=camera_id,
                                         campaign=campaign_global_phot, tag=None)

    print("Using fits file {file}".format(file=fits_file))
    print("Campaign = {campaign}".format(campaign=campaign_phot))

    sysrem_file = run_sysrem(input_file=fits_file, work_root=work_root, code_dir=code_dir,
                             field=field, camera_id=camera_id, campaign=campaign_phot)

    tamuz_file = run_orion(input_file=sysrem_file, field=field, camera_id=camera_id, campaign=campaign_phot,
                           work_root=work_root, orion_dir=orion_dir, tfa=False, tag=tag)

    tamtfa_file = run_orion(input_file=sysrem_file, field=field, camera_id=camera_id, campaign=campaign_phot,
                            work_root=work_root, orion_dir=orion_dir, tfa=True, tag=tag)

    update_mcmc(input_file=tamuz_file, work_root=work_root, mcmc_dir=mcmc_dir)
    update_mcmc(input_file=tamtfa_file, work_root=work_root, mcmc_dir=mcmc_dir)
    run_clump(input_file=tamuz_file, work_root=work_root, orion_dir=orion_dir)
    run_clump(input_file=tamtfa_file, work_root=work_root, orion_dir=orion_dir)
    run_load(input_file=tamuz_file, work_root=work_root, orion_dir=orion_dir)
    run_load(input_file=tamtfa_file, work_root=work_root, orion_dir=orion_dir)
    print("Finished archive ingest")


def get_tag(tag=None, tfa=False):
    """
    Return the tag to be used by the orion / mcmc code. If tag is not given it will return the default tags of tamtfa or
    tamuz.

    :param tag: The user defined tag e.g. Doha
    :param tfa: True for using tfa, false for not using tfa
    :return: The tag to be used for orion or mcmc

    >>> get_tag(tag=None, tfa=True)
    'tamtfa'
    >>> get_tag(tag=None, tfa=False)
    'tamuz'
    >>> get_tag(tag="Doha", tfa=False)
    'doha'
    >>> get_tag(tag="Doha", tfa=True)
    'dohatfa'

    """
    if tfa:
        if tag is None:
            tag = "tamtfa"
        elif tag == 'new_tamtfa' or tag == 'NEW_TAMTFA':
            tag = tag.lower()
       	elif tag == 'new_multi_tamtfa' or tag == 'NEW_MULTI_TAMTFA':
       	    tag = tag.lower()
        else:
            tag = tag.lower() + "_tamtfa"
    else:
        if tag is None:
            tag = "tamuz"
       	elif tag == 'new_tamuz' or tag == 'NEW_TAMUZ':
       	    tag = tag.lower() 
        elif tag == 'new_multi_tamuz' or tag == 'NEW_MULTI_TAMUZ':
            tag = tag.lower() 
        elif tag == 'new_multi_nolim' or tag == 'NEW_MULTI_NOLIM':
            tag = tag.lower()
        elif tag == 'steliostest':
            tag = tag.lower()
        elif tag in ['artemis_single','artemis_multi','ARTEMIS_SINGLE','ARTEMIS_MULTI']:
            tag = tag.lower()
        else:
            tag = tag.lower() + "_tamuz"

    return tag


@qesutil.check_nearly_all_param_for_none(can_be_none=["tag", "fname"])
def find_file(work_root=None, field=None, camera_id=None, campaign=None, tag=None, fname=None):
    """
    Finds the fits file in the working directory of the field that matches the tag / fname etc. From the file it also
    gets the campaign version. Raises an IOError if it finds more than one matching file or no matching files.

    :param work_root: The working directory to look for the file in
    :param field: Field name (e.g. 021037+424400)
    :param camera_id: The database camera id, can be loaded from the configuration files
    :param campaign: Campaign string e.g. C7 (this routine finds the campaign version from the file e.g. C7B)
    :param tag: Tag for example Tamuz. Can be None is tag is not used at this stage
    :param fname: Type of file e.g. sysrem, orion, doha etc. Can be none if fname is not part of the file.
    :raises: IOError if no files match or more than one files match
    :return: String of the matching fits file and the campaign version string
    """

    if tag is None:
        tag = ""
    else:
        tag = "_{tag}".format(tag=tag)

    if fname is None:
        fname = ""
    else:
        fname = "_{fname}".format(fname=fname)

    search_string = os.path.join(work_root, "{field}_{camera_id}_{campaign}[A-Z]{fname}{tag}.fits".
                                 format(field=field, camera_id=camera_id, campaign=campaign, tag=tag, fname=fname))

    search_string_alt = os.path.join(work_root, "{field}_{camera_id}_{campaign}{fname}{tag}.fits".
                                     format(field=field, camera_id=camera_id, campaign=campaign, tag=tag,
                                            fname=fname))

    files = glob.glob(search_string)

    if len(files) > 1:
        raise IOError("Too many fits files match pattern: {search_string}".format(search_string=search_string))

    if len(files) == 0:
        print("No fits files match pattern: {search_string} assuming this is a multi run".format(search_string=search_string))
        files = glob.glob(search_string_alt)
        if len(files) > 1:
            raise IOError("Too many fits files match pattern: {search_string}".format(search_string=search_string_alt))
        if len(files) == 0:
            raise IOError("No fits files match pattern: {search_string}".format(search_string=search_string_alt))

    fits_file = os.path.basename(files[0])
    campaign_phot = fits_file.split("_")[2].split(".")[0]

    return fits_file, campaign_phot


@qesutil.check_all_param_for_none
def run_sysrem(input_file=None, work_root=None, code_dir=None, over_write=False, field=None, camera_id=None,
               campaign=None):
    """
    This routine calls the sysrem program apsysrem on the input fits file

    :param input_file: The file to be used as input for apsysrem
    :param work_root: The location of the fits file
    :param code_dir: The location of the archive code apsysrem
    :param over_write: If an sysrem output file is already found for input, then True will rerun sysrem and override
                       the output file. False will return and not rerun sysrem
    :param field: Field string e.g. 002000+450000
    :param camera_id: The database camera id, can be loaded from the configuration files
    :param campaign: Full campaign version i.e. C7A or C7B
    :return: String of the output fits file from sysrem
    :raises: subprocess.CalledProcessError is the call to apsysrem fails
    """

    print("Running sysrem")

    sysrem_root = os.path.join(code_dir, "apsysrem")
    output_file = "{field}_{camera_id}_{campaign}_sysrem.fits".format(field=field, camera_id=camera_id,
                                                                      campaign=campaign)

    if os.path.exists(os.path.join(work_root, output_file)) and not over_write:
        print("SYSREM FILE ALREADY EXISTS")
        return output_file

    sysrem_commands = [sysrem_root, "-i", os.path.join(work_root, input_file),
                       "-o", os.path.join(work_root, output_file)]

    child = subprocess.Popen(sysrem_commands, cwd=work_root)
    return_code = child.wait()
    if return_code:
        raise subprocess.CalledProcessError(return_code, cmd=sysrem_commands)
    return output_file


@qesutil.check_nearly_all_param_for_none(can_be_none=["tag"])
def run_orion(input_file=None, field=None, camera_id=None, campaign=None, work_root=None, orion_dir=None, tfa=False,
              over_write=False, parallel=False, tag=None, max_period=30):
    """
    This routine runs the orion code on an input fits file. The routine will run the code with or without the tfa
    detrending switch depending on if tfa is True or False.

    :param input_file: The input fits file to run orion on
    :param field: Field string e.g. 002000+450000
    :param camera_id: The database camera id, can be loaded from the configuration files
    :param campaign: Full campaign version i.e. C7A or C7B
    :param work_root: Location of the input and output file
    :param orion_dir: Location of the orion code
    :param tfa: True to use tfa detrending, if not False
    :param over_write: If over_write is False then orion will not be rerun if the output file already exists in
                       work_root. If it set to True then orion will be rereun and the file overwritten.
    :param parallel: Set to true if two versions of orion are being run on the same FITs file. I.e. with and without
                     tfa. Orion creates temp files in the working directory and parallel makes a working directory
                     with the tag to keep the separate.
    :param tag: Tag used for the orion run, e.g. Doha. Can be left as None and it will use the default tag.
    :param max_period: The maximum period which orion will run up to, in hours.
    :return: The output file name as a string is return.
    :raises: subprocess.CalledProcessError is raised if the orion program fails
    """

    print("Running orion")

    orion_root = os.path.join(orion_dir, "orion")

    if tfa:
        if tag is None:
            tag = "TAMTFA"
        else:
            tag = tag.upper() + "TFA"

        output_file = "{field}_{camera_id}_{campaign}_orion_{tag}.fits".format(field=field, camera_id=camera_id,
                                                                               campaign=campaign, tag=tag.lower())
        orion_commands = [orion_root, "--tfa", "--numstds", "900", "--tag", tag, "--field", field,
                          "--camera", camera_id, "--season", campaign, "--maxperiod", str(max_period), "--outfile",
                          os.path.join(work_root, output_file), os.path.join(work_root, input_file)]
    else:
        if tag is None:
            tag = "TAMUZ"
        else:
            tag = tag.upper()

        output_file = "{field}_{camera_id}_{campaign}_orion_{tag}.fits".format(field=field, camera_id=camera_id,
                                                                               campaign=campaign, tag=tag.lower())
        orion_commands = [orion_root, "--numstds", "900", "--tag", tag, "--field", field, "--camera", camera_id,
                          "--season", campaign, "--maxperiod", str(max_period), "--outfile", os.path.join(work_root, output_file),
                          os.path.join(work_root, input_file)]

    if os.path.exists(os.path.join(work_root, output_file)) and not over_write:
        print("ORION FILE ALREADY EXISTS")
        return output_file

    if parallel:
        work_root = os.path.join(work_root, tag)
        if not os.path.exists(work_root):
            os.makedirs(work_root)

    child = subprocess.Popen(orion_commands, cwd=work_root)
    return_code = child.wait()
    if return_code:
        raise subprocess.CalledProcessError(return_code, cmd=orion_commands)

    return output_file


@qesutil.check_nearly_all_param_for_none(can_be_none=["tag"])
def update_mcmc(input_file=None, work_root=None, mcmc_dir=None, sub_dir=None):
    """
    Runs mcmc on an input orion file to add the mcmc parametres to the file. The routine loops over all the rows in the
    fits file and then calls run_mcmc with the object id and peak for that row. run_mcmc will then run the mcmc code on
    that row and update the fits file.

    :param input_file: The input orion fits file
    :param work_root: The location of the input file
    :param mcmc_dir: The directory of the mcmc code
    :param sub_dir: Used to create a sub directory to store the temporary files created by the mcmc code. Will be added
                    to the work route sent to run_mcmc
    """
    fits_file = os.path.join(work_root, input_file)
    hdu = fits.open(fits_file)
    candidates = hdu['CANDIDATES'].data
    mcmc_zero = numpy.where(candidates['MCMC_PERIOD'] == 0.0)
    candidates = candidates[mcmc_zero]
    total_candidates_to_update = len(candidates)
    hdu.close()

    if sub_dir is not None:
        work_root = os.path.join(work_root, sub_dir)
        if not os.path.exists(work_root):
            os.makedirs(work_root)

    for i, candidate in enumerate(candidates):
        print("****** DOING {i} of {total} ROWS ******".format(i=i, total=total_candidates_to_update))
        object_id = candidate['OBJ_ID']
        peak = str(candidate['RANK'])
        run_mcmc(input_file=fits_file, work_root=work_root, mcmc_dir=mcmc_dir, obj_id=object_id, peak=peak)


@qesutil.check_all_param_for_none
def run_mcmc(input_file=None, work_root=None, mcmc_dir=None, obj_id=None, peak=None):
    """
    This routine runs mcmc on a row of an input fits file. A row is define by an object id and a peak number. Orion
    may find multiply peaks for the same object.

    :param input_file: The input fits file from the orion run (full path)
    :param work_root: Working directory where any temporary files from the mcmc code will be saved,
    :param mcmc_dir: Directory of the mcmc code
    :param obj_id: Object id being updated
    :param peak: Which peak is being updated
    :raises: subprocess.CalledProcessError if the call to the mcmc program fails
    """
    mcmc_root = os.path.join(mcmc_dir, "mcmc_orion")
    mcmc_commands = [mcmc_root, input_file, obj_id, peak]
    child = subprocess.Popen(mcmc_commands, cwd=work_root)
    return_code = child.wait()
    if return_code:
        raise subprocess.CalledProcessError(return_code, cmd=mcmc_commands)


@qesutil.check_all_param_for_none
def run_mcmc_script(input_file=None, work_root=None, orion_dir=None):
    """
    Calls run_script for the ormcmc program

    :param input_file: input fits file
    :param work_root: location of the fits file / working directory
    :param orion_dir: location of the orion code
    """
    run_script(script="ormcmc", input_file=input_file, work_root=work_root, orion_dir=orion_dir)

    return


@qesutil.check_all_param_for_none
def run_clump(input_file=None, work_root=None, orion_dir=None):
    """
    Calls run_script for the orclump program

    :param input_file: input fits file
    :param work_root: location of the fits file / working directory
    :param orion_dir: location of the orion code
    """
    run_script(script="orclump", input_file=input_file, work_root=work_root, orion_dir=orion_dir)

    return


@qesutil.check_all_param_for_none
def run_load(input_file=None, work_root=None, orion_dir=None):
    """
    Calls run_script for the aporload program

    :param input_file: input fits file
    :param work_root: location of the fits file / working directory
    :param orion_dir: location of the orion code
    """
    run_script(script="aporload", input_file=input_file, work_root=work_root, orion_dir=orion_dir)

    return


@qesutil.check_all_param_for_none
def run_script(script=None, input_file=None, work_root=None, orion_dir=None):
    """
    Routine to run the orion scripts on an input file

    :param script: script name
    :param input_file: input fits file
    :param work_root: location of the fits file / working directory
    :param orion_dir: location of the orion code
    :raises: subprocess.CalledProcessError if the script fails
    """
    print("Running {script}".format(script=script))

    script_root = os.path.join(orion_dir, script)

    script_commands = [script_root, os.path.join(work_root, input_file)]

    child = subprocess.Popen(script_commands, cwd=work_root)

    return_code = child.wait()

    if return_code:
        raise subprocess.CalledProcessError(return_code, cmd=script_commands)

    return

if __name__ == '__main__':

    try:
        import configparser
    except ImportError:
        import ConfigParser as configparser

    import argparse

    try:
        _qes_path = os.environ['QES_PATH']
    except NameError as n_error:
        print("Environmental variable not set: %s" % n_error)
        sys.exit(2)

    _work_dir_default = os.path.join(_qes_path, archive_constants.archive_work)
    _archive_code_default = os.path.join(_qes_path, archive_constants.archive_code)
    _orion_code_default = os.path.join(_qes_path, archive_constants.orion_code)
    _mcmc_code_defaualt = os.path.join(_qes_path, archive_constants.mcmc_code)

    parser = argparse.ArgumentParser(description='Runs the sysrem and orion searches')
    parser.add_argument('camera', metavar='CAMERA', help='Camera to ingest (e.g. KCAM_US_T01D01)')
    parser.add_argument('field', metavar='FIELD', help='Field to ingest (e.g. 002000+350000)')
    parser.add_argument('campaign', metavar='CAMPAIGN', help='Campaign to ingest (e.g. C7)')
    parser.add_argument('photometry', metavar='TYPE', choices=['dia', 'aperture'],
                        help='Photometry type (dia or aperture)')
    parser.add_argument("--work_dir", default=_work_dir_default,
                        help="Work directory of ingest Files (default: {dir})".format(dir=_work_dir_default))
    parser.add_argument("--archive_code", default=_archive_code_default,
                        help="Directory of archive code (default: {dir})".format(dir=_archive_code_default))
    parser.add_argument("--orion_code", default=_orion_code_default,
                        help="Directory of orion code (default: {dir})".format(dir=_orion_code_default))
    parser.add_argument("--mcmc_code", default=_mcmc_code_defaualt,
                        help="Directory of mcmc code (default: {dir})".format(dir=_mcmc_code_defaualt))
    args = parser.parse_args()

    _alsubai_control_path = os.path.join(_qes_path, 'qes_pipeline', 'Alsubai_Control')
    _config_cam_campaign = configparser.ConfigParser()

    if not _config_cam_campaign.read(os.path.join(_alsubai_control_path,
                                                  'cfg', args.campaign, 'config.' + args.camera + '.cfg')):
        print('ERROR - The configuration file is missing for the camera: ' +
              os.path.join('cfg', args.campaign, 'config.' + args.camera + '.cfg'))
        sys.exit(1)

    search(camera=args.camera, field=args.field, campaign=args.campaign, work_dir=args.work_dir,
           code_dir=args.archive_code, orion_dir=args.orion_code, mcmc_dir=args.mcmc_code,
           camera_id=_config_cam_campaign.get('main', 'camID'),
           photometry_info=getattr(archive_constants.photometry, args.photometry))
