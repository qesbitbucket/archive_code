#!/usr/bin/env python
"""
This module runs the archive jobs on the super computer. The default chain of jobs that will be run
are:

Ingest -> stats -> Fetch -> sysrem -> orion_tfa -> orion -> mcmc_tfa -> mcmc -> update_tfa -> ...
... update -> clean

The code in this module will handle on the job chaining so that one pbs job will be run after it's
dependent job is run. Pipeline stages can be defined using the ArchivePipelineProgram class.

To run all the pipeline stages for a field you can run this run with:

./archive_jobs.py KCAM_US_T01D01 002000+450000 C7 dia

as with archive_fetch if you do not want to create a new field letter you need to run with the
--rerun option (see archive_fetch docs). You can also override the tag option. See archive_orion
for more information about the tag variable.

./archive_jobs.py KCAM_US_T01D01 002000+450000 C7 dia --rerun --tag TEST

You can run just one stage of the archive pipeline using the --run_only command and then adding the
name (from ArchivePipelineProgram.name) of the stage. For example:

./archive_jobs.py KCAM_US_T01D01 002000+450000 C7 dia --run_only fetch

Similarly you can run part of the pipeline with the --start_with and --end_with commands. E.g.

./archive_jobs.py KCAM_US_T01D01 002000+450000 C7 dia --start_with ingest --end_with sysrem
"""
from __future__ import print_function
from functools import wraps
import os
import sys
import time
import signal

try:
    import qesutil
    import Alsubai_Control.pbs as pbs
except ImportError:
    import qes_pipeline.qesutil as qesutil
    import qes_pipeline.Alsubai_Control.pbs as pbs
import archive_constants
import archive_util
import archive_db

__author__ = 'Neil Parley'


class JobKilled(Exception):
    """
    Exception to be raised if a job is killed
    """
    pass


def clean_orphaned_jobs(orphaned_jobs):
    """
    If a job fails or is killed half way through a pipeline chain delete all the other pbs jobs in
    that chain which would never be able to run.

    :param orphaned_jobs: List of jobs to delete
    """
    for orphaned_job in orphaned_jobs:
        pbs.delete_job(orphaned_job.qsub_id)


def register_job(f):
    """
    Decorator to wrape archive functions which might be called in stand a lone or as pbs jobs. If
    the code is being called from the terminal and job_id is None then just return the function that
    the code is decorating.

    If there is a job id that the code is being run as a PBS job. Add a signal handler to catch the
    linux sigterm and sigint codes. The program will already have been added to the database when
    it was submitted to the pbs system (and hence how we know it's job_id), and it will be in a
    queue state. Because this code is running it is now got to the front of the queue so up date the
    database to say that the code is running.

    Then run the function that is being decorated in a try except wrapper to catch if the JobKilled
    or any other exception is raised. If JobKilled is raised then the process was terminated by the
    pbs system or the user. End the job in the database setting the status to killed and fail the
    rest of the archive pipeline as it will never run. Clean up all the orphaned jobs in the chain.
    If another exception was raised that something has gone wrong with the function that we are
    decorating. In this case end the job in the database setting the status to failed and again fail
    the rest of the pipeline and clean up any jobs left orphaned.

    If the job finishes with out error then update the database to show that the job is finished,
    also update the database to set the next job in the chain to queued.

    TFA variables here are used so that we don't fail the TFA chain if the non TFA chain has an
    exception. That is only fail the code that are children of the dependent. But we don't have the
    class information in this function, only the information we can get from the database.

    :param f: Function that is being decorated
    :return: :raise JobKilled: Exception that is called if the job is killed
    """
    @wraps(f)
    def wrapper(*args, **kwargs):
        if kwargs["job_id"] is None:
            return f(*args, **kwargs)
        else:

            def signal_term_handler(*_):
                """
                If a job is killed then print "Got killed" and raise the JobKilled exception
                """
                print('GOT KILLED')
                raise JobKilled

            signal.signal(signal.SIGTERM, signal_term_handler)
            signal.signal(signal.SIGINT, signal_term_handler)

            db = archive_db.ArchiveJobs(user=archive_constants.db_settings.user,
                                        password=archive_constants.db_settings.password,
                                        host=archive_constants.db_settings.host,
                                        db=archive_constants.db_settings.database)

            job_id = kwargs["job_id"]

            if "tfa" in kwargs:
                tfa = kwargs["tfa"]
            else:
                tfa = None

            db.end_job_in_queue(job_id=job_id)

            try:
                time.sleep(1)
                f(*args, **kwargs)
            except JobKilled:
                db.end_job(job_id=job_id, status=pbs.status.killed)
                db.fail_rest_of_pipeline(job_id=job_id, tfa=tfa)
                orphaned_jobs = db.get_rest_of_pipeline_ids(job_id=job_id, tfa=tfa)
                clean_orphaned_jobs(orphaned_jobs)
                raise
            except:
                db.end_job(job_id=job_id, status=pbs.status.failed)
                db.fail_rest_of_pipeline(job_id=job_id, tfa=tfa)
                orphaned_jobs = db.get_rest_of_pipeline_ids(job_id=job_id, tfa=tfa)
                clean_orphaned_jobs(orphaned_jobs)
                raise

            db.end_job(job_id=job_id, status=pbs.status.finish)
            db.queue_next_in_pipeline(job_id=job_id, tfa=tfa)

    return wrapper


class DependencyError(Exception):
    """
    Raised if the dependent job has not been submitted
    """
    pass


class ArchivePipelineProgram:
    @qesutil.check_nearly_all_param_for_none(can_be_none=["dependency", "out_name", "tfa",
                                                          "extra_commands"])
    def __init__(self, name=None, script=None, tfa=None, dependency=None, cpus=None, memory=None,
                 nodes=None, length=None, tag=False, data_dir=False, work_dir=False,
                 archive_code=False,
                 orion_code=False, mcmc_code=False, extra_commands=None, single_field=True,
                 out_name=None, rerun=False):
        """
        This class is used to make archive programs which are formed together to make pipelines.
        This mecanism is used so that in future if different or extra programs are used in the
        archiving searches then they can be added as new archive programs with out having to edit
        lots of code. See the stages set up below for examples.

        :param name: The name of the archive program (used as the name for the stage when running)
        :param script: The script that should be run. Should be in the same directory
        :param tfa: If the script should be run with the --tfa command line option. None means the
                    script does not use the --tfa option. False means the script excepts the option
                    but it should not be set and True means the script excepts the option and it
                    should be set.
        :param dependency: A list of pipeline programs which this pipeline program depends on.
        :param cpus: The number of CPUs that the script should use on the cluster
        :param memory: The amount of memory in GB that the script should use on the cluster
        :param nodes: The number of nodes to run the script on. (More than 1 node has not been
                      tested)
        :param length: The wall time of the script in hours
        :param tag: If the script accepts the --tag command line option. False means don't set the
                    tag option. True means set the tag option
        :param data_dir: If the script accepts the --data_dir command line option. False means don't
                         set, True means set.
        :param work_dir: If the script accepts the --work_dir command line option. False means don't
                         set, True means set.
        :param archive_code: If the script accepts the --archive_code command line option. False
                             means don't set, True means set.
        :param orion_code: If the script accepts the --orion_code command line option. False means
                           don't set, True means set.
        :param mcmc_code: If the script accepts the --mcmc_code command line option. False means
                          don't set. True means set.
        :param extra_commands: Any extra commands that are not part of the standard API that the
                               script needs. As a list of commands.
        :param single_field: If the script is working on a single field so expects to be sent the
                             camera, fieldname, campaign etc. Default True. If false any field info
                             will need to be sent via extra commands.
        :param out_name: A name to distigush the fis file made by the script. E.g. sysrem. Might be
                         the same name as another script if the script just updates the file. Also
                         if None if the script does not make a file. Is used for dependencies.
        :param rerun: If the --rerun command is accepted by the script. False don't set the command,
                      true set the command.
        """
        self.name = name
        self.script = script
        self.dependency = dependency
        self.cpus = cpus
        self.memory = memory
        self.nodes = nodes
        self.length = length
        self.tfa = tfa
        self.tag = tag
        self.data_dir = data_dir
        self.work_dir = work_dir
        self.archive_code = archive_code
        self.orion_code = orion_code
        self.mcmc_code = mcmc_code
        self.extra_commands = extra_commands
        self.single_field = single_field
        self.out_name = out_name
        self.rerun = rerun

# Pipeline stages

_INGEST_STAGE = ArchivePipelineProgram(name="ingest", script="archive_ingest.py", tfa=None,
                                       dependency=None, cpus=1, memory=4, nodes=1, length=100,
                                       archive_code=True, data_dir=True, work_dir=True,
                                       out_name=None)

_STATS_STAGE = ArchivePipelineProgram(name="stats", script="archive_stats.py", tfa=None,
                                      dependency=[_INGEST_STAGE], cpus=1, memory=4, nodes=1,
                                      length=100,
                                      archive_code=True, data_dir=True, work_dir=True,
                                      out_name=None)

_FETCH_STAGE = ArchivePipelineProgram(name="fetch", script="archive_fetch.py", tfa=None,
                                      dependency=[_STATS_STAGE], cpus=1, memory=4, nodes=1,
                                      length=100,
                                      work_dir=True, archive_code=True, out_name=None, rerun=True)

_SYSREM_STAGE = ArchivePipelineProgram(name="sysrem", script="archive_sysrem.py", tfa=None,
                                       dependency=[_FETCH_STAGE], cpus=4, memory=32, nodes=1,
                                       length=100,
                                       work_dir=True, archive_code=True, out_name="sysrem")

_ORION_TFA_STAGE = ArchivePipelineProgram(name="orion_tfa", script="archive_orion.py",
                                          tfa=True, dependency=[_SYSREM_STAGE],
                                          cpus=4, memory=32, nodes=1, length=100,
                                          work_dir=True, orion_code=True, out_name="orion",
                                          tag=True)

_ORION_STAGE = ArchivePipelineProgram(name="orion", script="archive_orion.py", tfa=False,
                                      dependency=[_SYSREM_STAGE], cpus=4, memory=32, nodes=1,
                                      length=100,
                                      work_dir=True, orion_code=True, out_name="orion", tag=True)

_MCMC_TFA_STAGE = ArchivePipelineProgram(name="mcmc_tfa", script="archive_mcmc.py",
                                         tfa=True, dependency=[_ORION_TFA_STAGE],
                                         cpus=4, memory=32, nodes=1, length=100,
                                         work_dir=True, mcmc_code=True, out_name="orion", tag=True)

_MCMC_STAGE = ArchivePipelineProgram(name="mcmc", script="archive_mcmc.py", tfa=False,
                                     dependency=[_ORION_STAGE], cpus=4, memory=32, nodes=1,
                                     length=100,
                                     work_dir=True, mcmc_code=True, out_name="orion", tag=True)

_UPDATE_TFA_STAGE = ArchivePipelineProgram(name="update_tfa", script="archive_update.py", tfa=True,
                                           dependency=[_MCMC_TFA_STAGE],
                                           cpus=1, memory=4, nodes=1, length=100,
                                           work_dir=True, orion_code=True, out_name=None, tag=True)

_UPDATE_STAGE = ArchivePipelineProgram(name="update", script="archive_update.py", tfa=False,
                                       dependency=[_MCMC_STAGE],
                                       cpus=1, memory=4, nodes=1, length=100,
                                       work_dir=True, orion_code=True, out_name=None, tag=True)

_CLEAN_STAGE = ArchivePipelineProgram(name="clean", script="archive_clean.py", tfa=None,
                                      dependency=[_UPDATE_STAGE, _UPDATE_TFA_STAGE],
                                      cpus=1, memory=4, nodes=1, length=5, work_dir=True)

# Default archive pipeline

archive_pipeline = (
    _INGEST_STAGE, _STATS_STAGE, _FETCH_STAGE, _SYSREM_STAGE, _ORION_TFA_STAGE, _ORION_STAGE,
    _MCMC_TFA_STAGE, _MCMC_STAGE, _UPDATE_TFA_STAGE, _UPDATE_STAGE, _CLEAN_STAGE)

# Archive names are just by the command line arguments code to identify the choices available for
# the start_with, end_with and run_only commands

archive_pipeline_names = [_stage.name for _stage in archive_pipeline]


class ArchivePipeline:
    @qesutil.check_nearly_all_param_for_none(
        ["pbs_server", "db_server", "tag", "data_dir", "work_dir", "_pipeline",
         "archive_code", "orion_code", "mcmc_code", "start_with", "end_with",
         "dependency"])
    def __init__(self, photometry=None, camera=None, camera_id=None, field=None, campaign=None,
                 qes_path=None, tag=None,
                 code_dir=archive_constants.archive_manager, username=None, pbs_server=None,
                 db_server=None,
                 data_dir=None, work_dir=None, archive_code=None, orion_code=None, mcmc_code=None,
                 exclude_tfa=False,
                 start_with=None, end_with=None, _pipeline=None, rerun=False, dependency=None,
                 only_tfa=False):

        """
        Set up the ArchivePipeline object. For the parameters supplied by the user. Sets up all the
        default properties. Then goes through the pipeline and creates an ArchivePipelineStage
        object for each stage that matches the requirements.

        :param photometry: The photometry to be used form archive_constants.photometry
        :param camera: The name name, e.g. KCAM_US_T01D01
        :param camera_id: The database camera id, e.g. 401
        :param field: The field e.g 234000+350000
        :param campaign: The campaign e.g. C7
        :param qes_path: The root path for all the qes files and code
        :param tag: The tag to set for the oripn runs is different from default
        :param code_dir: Set if the code directory is not the default one
        :param username: The username of the person running the jobs for the pbs system
        :param pbs_server: Can be set to override the default PBS server object
        :param db_server: Can be set to override the default database connection
        :param data_dir: Set if the data directory is not the default
        :param work_dir: Set if the work directory if not the default
        :param archive_code: Set if the archive code location is no the default
        :param orion_code: Set if the orion code location is not the default
        :param mcmc_code: Set if the mcmc_code location is not the default
        :param exclude_tfa: Don't run stages that have tfa set to True
        :param start_with: Start the pipeline with this stage
        :param end_with: End the pipeline with this stage
        :param _pipeline: Pipeline to use instead of the default pipeline
        :param rerun: True if rerun variable should be set
        :param dependency: A list of dependent pipelines that must finished before this pipeline
        :param only_tfa: Don't run stages that have tfa set to False
        :raise ValueError: If both only_tfa and exclude_tfa are set to true
        """
        self.photometry = photometry
        self.camera_id = camera_id
        self.camera = camera
        self.field = field
        self.campaign = campaign
        self.qes_path = qes_path
        self.code_dir = code_dir

        archive_jobs_dir = os.path.join(self.qes_path, archive_constants.archive_job_logs, camera)
        self.output_location = archive_util.create_work_root(field=field, camera=camera,
                                                             campaign=campaign,
                                                             photometry_info=photometry,
                                                             work_dir=archive_jobs_dir)
        self.username = username
        self.exclude_tfa = exclude_tfa
        self.only_tfa = only_tfa

        if only_tfa is True and exclude_tfa is True:
            raise ValueError("only_tfa and exclude_tfa can't both be true")

        self.pipeline_id = None
        self.pbs_job = None
        self.out_name = None

        if _pipeline is None:
            self._pipeline = archive_pipeline
        else:
            self._pipeline = _pipeline

        if pbs_server:
            self.pbs_server = pbs_server
        else:
            self.pbs_server = pbs.PBS()

        if db_server:
            self.db = db_server
        else:
            self.db = archive_db.ArchiveJobs(user=archive_constants.db_settings.user,
                                             password=archive_constants.db_settings.password,
                                             host=archive_constants.db_settings.host,
                                             db=archive_constants.db_settings.database)

        self.tfa_commands = ["--tfa"]

        if rerun:
            self.rerun_commands = ["--rerun"]
        else:
            self.rerun_commands = []

        if tag is not None:
            self.tag_commands = ["--tag", tag]
        else:
            self.tag_commands = []

        if data_dir is not None:
            self.data_dir_commands = ["--data_dir", data_dir]
        else:
            self.data_dir_commands = []

        if work_dir is not None:
            self.work_dir_commands = ["--work_dir", work_dir]
        else:
            self.work_dir_commands = []

        if archive_code is not None:
            self.archive_code_commands = ["--archive_code", archive_code]
        else:
            self.archive_code_commands = []

        if orion_code is not None:
            self.orion_code_commands = ["--orion_code", orion_code]
        else:
            self.orion_code_commands = []

        if mcmc_code is not None:
            self.mcmc_code_commands = ["--mcmc_code", orion_code]
        else:
            self.mcmc_code_commands = []

        self.stages = {}
        previous_stage = None

        #  Stages is a dictionary of the ArchivePipelineStage objects. Each stage has a next
        #  property which points it to the next stage. When .next is None we have got to the end
        #  of the pipeline. We can change the start and end of the pipeline by changing the
        #  start_stage and by setting the stage where next is None.

        for stage_id, stage_info in enumerate(self._pipeline):
            if exclude_tfa and stage_info.tfa:
                continue
            if only_tfa and stage_info.tfa is False:
                continue
            self.stages[stage_info.name] = ArchivePipelineStage(stage_id, self, stage_info)
            if previous_stage is not None:
                previous_stage.next = self.stages[stage_info.name]
            previous_stage = self.stages[stage_info.name]

        if start_with is not None:
            self.start_stage = self.stages[start_with]
            if self.start_stage.dependency is not None:
                self.start_stage.dependency = dependency
        else:
            self.start_stage = self.stages[self._pipeline[0].name]
            self.start_stage.dependency = dependency

        if end_with is not None:
            self.stages[end_with].next = None

    @property
    def pipeline(self):
        """
        Return the pipeline as a generator of ArchivePipelineStage objects. That is you can use
        this as:
        for stage in myArchivePipelineObject.pipeline
        """
        current_stage = self.start_stage
        while current_stage is not None:
            yield current_stage
            current_stage = current_stage.next

    def submit_job(self, pipeline_stage=None, dry_run=False, ignore_dependency=False):
        """
        Used to submit a pipeline stage as a pbs job.

        If there are no dependencies then the pbs status will be set to queued. If there is a
        dependencies then the pbs status will be set to held. Loop through all the dependencies for
        the pipeline stage making sure the dependencies have been submitted and have a qsub id. If
        they don't raise a DependencyError.

        Submit the job to the database and get back a job_id to use in the pbs job submission. Add
        the pbs job id to the pbd command. Create a pbs job object with the information from the
        stage. Submit the job and get back the qsub id. Then update the database with this qsub id.

        :param pipeline_stage: The pipeline stage to submit (ArchivePipelineStage object)
        :param dry_run: If dry run is true, don't actually submit the stage
        :param ignore_dependency: If ignore dependency is True doesn't take into account of the
                                  dependency of the stage
        :raise ValueError: Raises ValueError if job_id can't be got from the database because there
                           has been more than 99999999999999 jobs.
        """
        if pipeline_stage.dependency is None or ignore_dependency is True:
            dependents = None
            pbs_status = pbs.status.queued
        else:
            dependents = []
            pbs_status = pbs.status.held
            for dependent in pipeline_stage.dependency:
                if dependent.pbs_job is None:
                    raise DependencyError("Dependent stage {name} for stage {stage} has not been "
                                          "submitted and has no qsub id"
                                          .format(name=dependent.name, stage=pipeline_stage.name))
                else:
                    dependents.append(dependent.pbs_job.id)

        session, job_obj = self.db.save_job_get_id(field=self.field, campaign=self.campaign,
                                                   camera=self.camera, tfa=pipeline_stage.tfa,
                                                   photometry=self.photometry.name,
                                                   stage=pipeline_stage.stage_id,
                                                   name=pipeline_stage.name,
                                                   username=self.username, status=pbs_status,
                                                   pipeline_id=self.pipeline_id)

        job_id = job_obj.id
        if job_id < 99999999999999:
            job_name = "A{job_id}".format(job_id=job_id)
            self.pipeline_id = job_obj.pipeline
            pipeline_stage.job_id = job_id
            pipeline_stage.job_name = job_name
        else:
            session.rollback()
            session.close()
            raise ValueError("Job ID is too large and we thought this day would never come")

        try:
            if pipeline_stage.fname:
                pipeline_stage.command += " --fname {fname}".format(fname=pipeline_stage.fname)
            pipeline_stage.command += " --job_id {id}".format(id=job_id)
            pbs_job = pbs.Job(self.qes_path, pipeline_stage.command, name=job_name,
                              cpus=pipeline_stage.cpus,
                              nodes=pipeline_stage.nodes, length=pipeline_stage.length,
                              memory=pipeline_stage.memory,
                              out_location=self.output_location)

            self.pbs_server.submit_job(pbs_job, dry_run=dry_run, depend=dependents)
            pipeline_stage.pbs_job = pbs_job
            self.pbs_job = pbs_job
            job_obj.qsub_id = pbs_job.id
        except:
            session.rollback()
            session.close()
            raise

        self.db.commit_saved_job(session)

    def submit_pipeline(self, dry_run=False, _out=sys.stdout):
        """
        Used to submit the whole pipeline to the pbs system. Loops over each pipeline stage in the
        pipeline property and sends that to submit job.

        :param dry_run: If this is a dry and not to actually submit the jobs
        :param _out: If the output should go somewhere other than stdout
        """
        for stage in self.pipeline:

#            print(stage.command)

            self.submit_job(pipeline_stage=stage, dry_run=dry_run)
            print("Submitted stage {stage}, job: {job}, "
                  "Qsub id: {qsub}".format(stage=stage.name, job=stage.job_name,
                                           qsub=stage.pbs_job.id), file=_out)

    def _print_pipeline(self):
        """
        Can be used to print out the pipeline stages for an archive pipeline object
        """
        pipeline_strings = []
        for stage in self.pipeline:
            pipeline_strings.append(stage.name)
            if stage.next is not None:
                pipeline_strings.append("->")
        return ' '.join(pipeline_strings)


class ArchivePipelineStage:
    @qesutil.check_all_param_for_none
    def __init__(self, stage_id=None, pipeline_info=None, stage_info=None):
        """
        Class that definds the peipline stages inside the archivePipeline objects.

        Define the properties of the pipeline stage. Dependencies for the stage are built using the
        dependency variable from the pipeline stage object taking into account any filtering that
        has been set up in the create of the ArchivePipelineObject. I.e. to exclude tfa stages etc.

        The object then defines the command line command that needs to be run for the stage taking
        into account the variables defind in the pipeline stage and the options set in the
        archivePipelineObject

        :param stage_id: The number of the stage
        :param pipeline_info: The archivePipelineObject parent object
        :param stage_info: The ArchivePipelineProgram object for the stage
        """
        self.stage_id = stage_id
        self.name = stage_info.name
        self.script = stage_info.script

        if stage_info.dependency is None:
            self.dependency = None
        else:
            self.dependency = [pipeline_info.stages[stage.name] for stage in stage_info.dependency
                               if not ((stage.tfa and pipeline_info.exclude_tfa)
                               or (stage.tfa is False and pipeline_info.only_tfa))] or None

        self.out_name = stage_info.out_name
        self.fname = None
        if self.dependency is not None:
            self.fname = self.dependency[0].out_name

        self.cpus = stage_info.cpus
        self.memory = stage_info.memory
        self.nodes = stage_info.nodes
        self.length = stage_info.length
        self.tfa = stage_info.tfa

        self.command = os.path.join(pipeline_info.qes_path, pipeline_info.code_dir, self.script)
        if stage_info.single_field:
            self.command += " {camera} {field} {campaign} {type}".format(
                camera=pipeline_info.camera,
                field=pipeline_info.field,
                campaign=pipeline_info.campaign,
                type=pipeline_info.photometry.name)
        extra_commands = []
        if stage_info.tfa:
            extra_commands += pipeline_info.tfa_commands
        if stage_info.rerun:
            extra_commands += pipeline_info.rerun_commands
        if stage_info.tag:
            extra_commands += pipeline_info.tag_commands
        if stage_info.data_dir:
            extra_commands += pipeline_info.data_dir_commands
        if stage_info.work_dir:
            extra_commands += pipeline_info.work_dir_commands
        if stage_info.archive_code:
            extra_commands += pipeline_info.archive_code_commands
        if stage_info.orion_code:
            extra_commands += pipeline_info.orion_code_commands
        if stage_info.mcmc_code:
            extra_commands += pipeline_info.mcmc_code_commands
        if stage_info.extra_commands:
            extra_commands += stage_info.extra_commands

        if extra_commands:
            for extra_command in extra_commands:
                self.command += " " + extra_command

        self.pbs_job = None
        self.job_id = None
        self.job_name = None
        self.next = None


@qesutil.check_nearly_all_param_for_none(
    can_be_none=["run_only", "start_with", "end_with", "data_dir", "work_dir",
                 "code_dir", "archive_code", "orion_code", "mcmc_code",
                 "pbs_server", "tag", "fname"])
def submit_archive_job(camera=None, camera_id=None, field=None, campaign=None, photometry=None,
                       tag=None,
                       qes_path=None, username=None, code_dir=archive_constants.archive_manager,
                       data_dir=None,
                       work_dir=None, archive_code=None, orion_code=None, mcmc_code=None,
                       exclude_tfa=False,
                       run_only=None, start_with=None, end_with=None, rerun=False, pbs_server=None,
                       fname=None):
    """
    This routine is called by the main program when archive jobs is run from the command line. It
    sets up the ArchivePipelineObject from the command line variables given. The routine does some
    checking that the command line arguments make sense.

    :param camera: The camera name
    :param camera_id: The camera id
    :param field: The field name
    :param campaign: The campaign string
    :param photometry: The photometry structure
    :param tag: The tag to use if not using the default
    :param qes_path: The QES path if not using the default
    :param username: The username of the user running the code
    :param code_dir: The code directory if not running the default
    :param data_dir: The data directory if not running the default
    :param work_dir: The working directory if not using the default
    :param archive_code: The archive code location if not using the default
    :param orion_code: The orion code location of not using the default
    :param mcmc_code: The mcmc code directry if not using the default
    :param exclude_tfa: If tfa stages should be excluded
    :param run_only: If only one stage should be run
    :param start_with: The stage to start with
    :param end_with: The stage to end with
    :param rerun: If run only should be set so that a new sub campaign is not create
    :param pbs_server: The pbs server to use if not using the default
    :param fname: The fname to use if not using the default
    :raise ValueError: If there is an error in one of the values given
    """
    if run_only and (start_with or end_with):
        raise ValueError("Can't have run_only and start with or end with set")

    pipeline = ArchivePipeline(photometry=photometry, camera=camera, username=username,
                               camera_id=camera_id, tag=tag,
                               field=field, campaign=campaign, qes_path=qes_path, code_dir=code_dir,
                               data_dir=data_dir,
                               work_dir=work_dir, archive_code=archive_code, orion_code=orion_code,
                               mcmc_code=mcmc_code,
                               exclude_tfa=exclude_tfa, start_with=start_with, end_with=end_with,
                               rerun=rerun,
                               pbs_server=pbs_server)

    if end_with is not None:
        end_id = pipeline.stages[end_with].stage_id
    else:
        end_id = len(pipeline.stages)

    if start_with is not None:
        start_id = pipeline.stages[start_with].stage_id
        if fname is not None:
            pipeline.stages[start_with].fname = fname
    else:
        start_id = 0

    if end_id < start_id:
        raise ValueError("Start_with is after end_with in the pipeline")

    if run_only:
        try:
            _ = pipeline.stages[run_only].stage_id
        except KeyError:
            raise ValueError(
                "Stage does not exist (are you trying to run a TFA stage with -exclude_tfa set")

        pipeline.submit_job(pipeline.stages[run_only], ignore_dependency=True)
        print("Submitted stage {stage}, "
              "job: {job}, Qsub id: {qsub}".format(stage=run_only,
                                                   job=pipeline.stages[run_only].job_name,
                                                   qsub=pipeline.stages[run_only].pbs_job.id))
    else:
        pipeline.submit_pipeline()


if __name__ == '__main__':

    try:
        import configparser
    except ImportError:
        import ConfigParser as configparser

    import argparse

    try:
        _qes_path = os.environ['QES_PATH']
        _username = os.environ['USER']
    except NameError as n_error:
        print("Environmental variable not set: %s" % n_error)
        sys.exit(2)

    _data_dir_default = os.path.join(_qes_path, "qes_data")
    _work_dir_default = os.path.join(_qes_path, archive_constants.archive_work)
    _archive_code_default = os.path.join(_qes_path, archive_constants.archive_code)
    _mcmc_code_defaualt = os.path.join(_qes_path, archive_constants.mcmc_code)
    _orion_code_default = os.path.join(_qes_path, archive_constants.orion_code)

    parser = argparse.ArgumentParser(description='Runs an archive job')
    parser.add_argument('camera', metavar='CAMERA', help='Camera to ingest (e.g. KCAM_US_T01D01)')
    parser.add_argument('field', metavar='FIELD', help='Field to ingest (e.g. 002000+350000)')
    parser.add_argument('campaign', metavar='CAMPAIGN', help='Campaign to ingest (e.g. C7)')
    parser.add_argument('photometry', metavar='TYPE', choices=['dia', 'aperture'],
                        help='Photometry type (dia or aperture)')
    parser.add_argument("--data_dir", default=None,
                        help="Root directory of qes Files (default: {dir})".format(
                            dir=_data_dir_default))
    parser.add_argument("--work_dir", default=None,
                        help="Work directory of ingest Files (default: {dir})".format(
                            dir=_work_dir_default))
    parser.add_argument("--archive_code", default=None,
                        help="Directory of archive code (default: {dir})".format(
                            dir=_archive_code_default))
    parser.add_argument("--mcmc_code", default=None,
                        help="Directory of mcmc code (default: {dir})".format(
                            dir=_mcmc_code_defaualt))
    parser.add_argument("--orion_code", default=None,
                        help="Directory of orion code (default: {dir})".format(
                            dir=_orion_code_default))
    parser.add_argument("--tag", default=None, help="Override the archive TAMUZ tag")
    parser.add_argument("--run_only", default=None, choices=archive_pipeline_names,
                        help="Only run one stage, dependencies will be turned off "
                             "(default all stages are run)")
    parser.add_argument("--start_with", default=None, choices=archive_pipeline_names,
                        help="Pipeline Stage to start the pipeline "
                             "(default {name})".format(name=archive_pipeline_names[0]))
    parser.add_argument("--end_with", default=None, choices=archive_pipeline_names,
                        help="Pipeline Stage to end the pipeline "
                             "(default {name})".format(name=archive_pipeline_names[-1]))
    parser.add_argument("--fname", default=None,
                        help="Text after the underscore for the first file to be read "
                             "by the first stage of the "
                             "pipeline. Eg. field_camera_id_campaign[A-Z]_{fname}.fits")
    parser.add_argument("--exclude_tfa", help="Don't run the tfa stages (default False)",
                        action="store_true")
    parser.add_argument("--rerun", help="Don't create a new field / season", action="store_true")
    c_args = parser.parse_args()

    _alsubai_control_path = os.path.join(_qes_path, 'qes_pipeline', 'Alsubai_Control')
    _config_cam_campaign = configparser.ConfigParser()
    _campaign = archive_util.get_global_campaign(c_args.campaign)

    if not _config_cam_campaign.read(os.path.join(_alsubai_control_path,
                                                  'cfg', _campaign,
                                                  'config.' + c_args.camera + '.cfg')):
        print('ERROR - The configuration file is missing for the camera: ' +
              os.path.join('cfg', _campaign, 'config.' + c_args.camera + '.cfg'))
        sys.exit(1)

    submit_archive_job(camera=c_args.camera, camera_id=_config_cam_campaign.get('main', 'camID'),
                       field=c_args.field,
                       campaign=_campaign,
                       photometry=getattr(archive_constants.photometry, c_args.photometry),
                       qes_path=_qes_path, username=_username, data_dir=c_args.data_dir,
                       tag=c_args.tag,
                       work_dir=c_args.work_dir, archive_code=c_args.archive_code,
                       orion_code=c_args.orion_code,
                       mcmc_code=c_args.mcmc_code, exclude_tfa=c_args.exclude_tfa,
                       run_only=c_args.run_only,
                       start_with=c_args.start_with, end_with=c_args.end_with, rerun=c_args.rerun,
                       fname=c_args.fname)
