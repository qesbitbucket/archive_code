#!/usr/bin/env python
"""
This module runs Richards orion code. The function that actually starts the orion code is archive_search.run_orion.
The normal calling procedure for this module would be:

./archive_orion.py KCAM_US_T01D01 002000+450000 C7 dia

which would run orion on the dia file for field 002000+450000 / camera KCAM_US_T01D01. By default the script will search
for a file that matches the pattern {field}_{camera_id}_{campaign}_sysrem.fits. However, if --fname value is given it
will look for the file {field}_{camera_id}_{campaign}_value.fits instead.

--work_dir is the root directory where the working archive directories are. The code will look in a sub directory
of for example 002000+450000_KCAM_US_T01D01_C7_dia for the files.

--orion_code can be used to over ride the location of the orion code although if the code moves permanently the
value in the archive_constants file should be changed.

--tfa if the tfa flag is set then the orion code will first perform tfa detrending of the data before doing the BLS
search.

--tag by default the TAMUZ tag is created in the archive for orion searches for done without tfa and for tfa searches
the tamfta search is used. Tag can be used to over ride this. For example setting --tag DOHA will create archive
eateries with the name DOHA when --tfa is not set and DOHATFA when it is set.

--job_id is used for when the code is being run by the archive_jobs.py code and should not be used if the code is
being used from the command line
"""
from __future__ import print_function
__author__ = 'Neil Parley'
import os
import sys
try:
    import qesutil
except ImportError:
    import qes_pipeline.qesutil as qesutil
import archive_constants
import archive_util
import archive_search
import archive_jobs


@archive_jobs.register_job
@qesutil.check_nearly_all_param_for_none(can_be_none=["tag", "fname", "job_id"])
def orion(camera=None, field=None, campaign=None, work_dir=None, camera_id=None, tfa=False, orion_dir=None,
          photometry_info=None, tag=None, fname="sysrem", job_id=None):
    """
    Runs orion on the archive data file.

    The function gets the global campaign and does not assume that is given correctly. It then finds the working
    directory root where the files are. The global campaign for the photometry type this then created. The code will
    then search for a matching file and from that file work out the pysdo campaign. I.e. C7B etc. The code will then
    run Richard's orion code by calling the archive_search.run_orion function

    :param camera: Camera string e.g. KCAM_US_T01D01
    :param field: Field string e.g. 002000+450000
    :param campaign: Campaign string e.g. C7
    :param work_dir: Base working directory should be set to os.path.join(qes_path, archive_constants.archive_work) if
                     using the default
    :param camera_id: The database camera id, can be loaded from the configuration files
    :param tfa: True to use TFA detrending else false
    :param orion_dir: Directory of the orion_code e.g. os.path.join(_qes_path, archive_constants.orion_code)
    :param photometry_info: The photometry type of the run, types defined in archive_constants
    :param tag: tag to use in the archive for the run, default is TAMUZ / TAMTFA set to DOHA for a doha run
    :param fname: defaults to sysrem to look for a sysrem file but can be overridden if a different detrending has been
                  used. (Tells the code the underscore of the preceding file. see above)
    :param job_id: This is given to the function when it is started as a job by archive_jobs. It is used by the
                   decorator archive_jobs.register_job to update the database with when the job starts and finishes.
    """
    campaign_global = archive_util.get_global_campaign(campaign)

    work_root = archive_util.create_work_root(field=field, campaign=campaign_global, camera=camera,
                                              photometry_info=photometry_info, work_dir=work_dir)

    campaign_global_phot = photometry_info.campaign_letter + campaign_global[1:]

    fits_file, campaign_phot = archive_search.find_file(work_root=work_root, field=field, camera_id=camera_id,
                                                        campaign=campaign_global_phot, tag=None, fname=fname)

    out_file = archive_search.run_orion(input_file=fits_file, field=field, camera_id=camera_id,
                                        campaign=campaign_phot, work_root=work_root,
                                        orion_dir=orion_dir, tfa=tfa, over_write=True, parallel=True, tag=tag)

    print("{file} created".format(file=out_file))


if __name__ == '__main__':

    try:
        import configparser
    except ImportError:
        import ConfigParser as configparser

    import argparse

    try:
        _qes_path = os.environ['QES_PATH']
    except NameError as n_error:
        print("Environmental variable not set: %s" % n_error)
        sys.exit(2)

    _work_dir_default = os.path.join(_qes_path, archive_constants.archive_work)
    _archive_code_default = os.path.join(_qes_path, archive_constants.archive_code)
    _orion_code_default = os.path.join(_qes_path, archive_constants.orion_code)

    parser = argparse.ArgumentParser(description='Runs orion on the sysrem file')
    parser.add_argument('camera', metavar='CAMERA', help='Camera to ingest (e.g. KCAM_US_T01D01)')
    parser.add_argument('field', metavar='FIELD', help='Field to ingest (e.g. 002000+350000)')
    parser.add_argument('campaign', metavar='CAMPAIGN', help='Campaign to ingest (e.g. C7)')
    parser.add_argument('photometry', metavar='TYPE', choices=['dia', 'aperture'],
                        help='Photometry type (dia or aperture)')
    parser.add_argument("--tfa", help="Run on a tfa file (default is false)", action="store_true")
    parser.add_argument("--work_dir", default=_work_dir_default,
                        help="Work directory of ingest Files (default: {dir})".format(dir=_work_dir_default))
    parser.add_argument("--orion_code", default=_orion_code_default,
                        help="Directory of orion code (default: {dir})".format(dir=_orion_code_default))
    parser.add_argument("--tag", default=None, help="Override the archive TAMUZ tag")
    parser.add_argument("--job_id", default=None, type=int, help="Job id if running on the cluster")
    parser.add_argument("--fname", default="sysrem", help="File name from last programme in the pipeline default sysrem"
                                                          "(will search "
                                                          "field_camera_id_campaign[A-Z]_fname.fits)")
    args = parser.parse_args()

    _alsubai_control_path = os.path.join(_qes_path, 'qes_pipeline', 'Alsubai_Control')
    _config_cam_campaign = configparser.ConfigParser()

    if not _config_cam_campaign.read(os.path.join(_alsubai_control_path,
                                                  'cfg', args.campaign, 'config.' + args.camera + '.cfg')):
        print('ERROR - The configuration file is missing for the camera: ' +
              os.path.join('cfg', args.campaign, 'config.' + args.camera + '.cfg'))
        sys.exit(1)

    orion(camera=args.camera, field=args.field, campaign=args.campaign, work_dir=args.work_dir, job_id=args.job_id,
          camera_id=_config_cam_campaign.get('main', 'camID'), tfa=args.tfa, orion_dir=args.orion_code,
          photometry_info=getattr(archive_constants.photometry, args.photometry), tag=args.tag, fname=args.fname)
