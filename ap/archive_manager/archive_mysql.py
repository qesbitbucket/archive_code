"""
This module contains all the mysql routine for interacting with the archive database.
"""
__author__ = 'Neil Parley'
import contextlib
import MySQLdb
import archive_constants
try:
    import qesutil
except ImportError:
    import qes_pipeline.qesutil as qesutil


@contextlib.contextmanager
def open_db_connection(host=archive_constants.db_settings.host,
                       user=archive_constants.db_settings.user,
                       passwd=archive_constants.db_settings.password,
                       db=archive_constants.db_settings.database):
    """
    Creates a context manager to open and close a mysql database connection to the database. Host, user, passwd and db
    can be given but they default to the values set in archive_constants.db_settings

    :param host: Database host name
    :param user: Database user name
    :param passwd: Database password
    :param db: Database name
    :returns: contextmanager
    """
    db_con = MySQLdb.connect(host=host, user=user, passwd=passwd, db=db)
    try:
        yield db_con
    finally:
        db_con.commit()
        db_con.close()


def field_load(dump_file):
    """
    Routine to load fields into the database

    :param dump_file: Input data file
    :return: number of rows affected
    """
    sql = "LOAD DATA CONCURRENT LOCAL INFILE '{file}' IGNORE INTO TABLE fieldnames FIELDS TERMINATED BY ',' " \
          "OPTIONALLY ENCLOSED BY '\"' (obj_id, field, camera_id, campaign)".format(file=dump_file)
    return execute_sql(sql)


def image_load(dump_file):
    """
    Routine to load image data into the database

    :param dump_file: Input data file
    :return: number of rows affected
    """
    sql = "LOAD DATA LOCAL INFILE '{file}' REPLACE INTO TABLE imagelist FIELDS TERMINATED BY ',' " \
          "OPTIONALLY ENCLOSED BY '\"' (image_id, tstart, tmid, night, field, camera_id, mount_id, fileleaf, " \
          "exptime, ambtemp, rdnoise, filter, gain, campaign, crval1, crpix1, crval2, crpix2, cd1_1, cd1_2, " \
          "cd2_1, cd2_2, pv2_1, pv2_3, zpfit, zmag, fwhm1, fwhm2, astrms, centaz, centalt, jdmid, refstars, " \
          "sfits, a, b, c, d, shift_x, shift_y, det, c0, c1, c2, c3, c4, c5, c6, c7, c8, c9, c10, c11, " \
          "moffset, jkoffset, xoffset, yoffset, catrms, computer, pipevers, blurfwhm, refname1, refname2, " \
          "refname3, refname4)".format(file=dump_file)
    return execute_sql(sql)


def title_load(dump_file):
    """
    Routine to load titlefile data into the database

    :param dump_file: Input data file
    :return: number of rows affected
    """
    sql = "LOAD DATA LOCAL INFILE '{file}' REPLACE INTO TABLE tilefiles FIELDS TERMINATED BY ',' " \
          "OPTIONALLY ENCLOSED BY '\"' (field, camera_id, campaign, night, filename, obs_start, " \
          "obs_stop, filesize) SET ingest_date=NOW()".format(file=dump_file)
    return execute_sql(sql)


def calc_stats_load(dump_file):
    """
    Routine to load stats data into the database

    :param dump_file: Input data file
    :return: number of rows affected
    """
    sql = "LOAD DATA LOCAL INFILE '{file}' REPLACE INTO TABLE photsummary FIELDS TERMINATED BY ',' " \
          "OPTIONALLY ENCLOSED BY '\"' (obj_id, npts, obs_start, obs_stop, obs_interval, flux_mean, flux_chisq, " \
          "flux_skew, flux_kurtosis, flux_rms, mag_mean, blend_fraction)".format(file=dump_file)
    return execute_sql(sql)


def nomad_load(dump_file):
    """
    Routine to load nomad data into the database

    :param dump_file: Input data file
    :return: number of rows affected
    """
    sql = "LOAD DATA CONCURRENT LOCAL INFILE '{file}' IGNORE INTO TABLE nomad FIELDS TERMINATED BY ',' " \
          "OPTIONALLY ENCLOSED BY '\"' (obj_id, bmag, vmag, rmag, jmag, hmag, kmag, mu_ra, mu_dec, mu_ra_err, " \
          "mu_dec_err, dilution_r, dilution_v)".format(file=dump_file)
    return execute_sql(sql)


@qesutil.check_nearly_all_param_for_none(can_be_none=["tag"])
def delete_from(camera_id=None, field=None, campaign=None, tag=None, table=None):
    """
    Routine for deleting data from a database table for a corresponding cameram field, campaign

    :param camera_id: The database camera id, can be loaded from the configuration files
    :param field: Field string e.g. 002000+450000
    :param campaign: Photometry Campaign version string e.g. C7A or A7B
    :param tag: Orion run tag, e.g. TAMUZ or DOHA etc
    :param table: table name where data will be deleted
    :return: number of rows affected
    """
    sql = "delete from {table} where field='{field}' and camera_id='{camera_id}' " \
          "and campaign='{campaign}'".format(table=table, camera_id=camera_id, field=field, campaign=campaign)
    if tag:
        sql += " and tag='{tag}'".format(tag=tag)
    print("Deleting from {table}".format(table=table))
    return execute_sql(sql)


@qesutil.check_all_param_for_none
def delete_from_meta_fields(camera_id=None, field=None, campaign=None, tag=None):
    """
    Routine for deleting a campaign version from the metafields table

    :param camera_id: The database camera id, can be loaded from the configuration files
    :param field: Field string e.g. 002000+450000
    :param campaign: Photometry Campaign version string e.g. C7A or A7B to be deleted
    :param tag: Orion run tag, e.g. TAMUZ or DOHA etc
    :return: number of rows affected
    """
    sql = "delete from orion_metafields where meta_field='{field}' and meta_camera='{camera_id}' and tag='{tag}' " \
          "and meta_campaign='{campaign}'".format(camera_id=camera_id, field=field, campaign=campaign, tag=tag)
    print("Deleting from meta_fields")
    return execute_sql(sql)


@qesutil.check_all_param_for_none
def update_star_count(field=None, camera_id=None, campaign=None):
    """
    Routine to update the starcount table when a field is ingested with new data

    :param field: Field string e.g. 002000+450000
    :param camera_id: The database camera id, can be loaded from the configuration files
    :param campaign: Photometry Campaign version string e.g. C7A or A7B
    :return: number of rows affected
    """
    sql = "replace into fieldstarcount SELECT field, camera_id, substring(campaign,3,2) as campaign, COUNT(*) FROM " \
          "fieldnames where field='{field}' and camera_id={camera_id} and campaign like '%{campaign}%' " \
          "GROUP BY field, camera_id, fieldnames.campaign".format(camera_id=camera_id, field=field, campaign=campaign)
    return execute_sql(sql)


@qesutil.check_all_param_for_none
def get_new_nomad(field=None, camera_id=None, campaign=None):
    """
    Routine for finding all the stars for a given field, camera, campaign that don't have any nomad catalogue
    information.

    :param field: Field string e.g. 002000+450000
    :param camera_id: The database camera id, can be loaded from the configuration files
    :param campaign: Photometry Campaign version string e.g. C7A or A7B
    :return: mysqldb results record of the stars which don't have nomad id
    """
    with open_db_connection() as db:
        sql = "select obj_id, ra, declination from photsummary join catalogue using (obj_id)  join fieldnames " \
              "using (obj_id) left join nomad " \
              "using (obj_id) where nomad.obj_id is null and camera_id='{camera_id}' and field='{field}' " \
              "and campaign like '%{campaign}%'".format(camera_id=camera_id, field=field, campaign=campaign)
        cursor = db.cursor()
        try:
            cursor.execute(sql)
            result = cursor.fetchall()
            return result
        except MySQLdb.Error:
            db.rollback()
            raise


def execute_sql(sql):
    """
    Helper routine for executing the mysql from the other functions in this module. Takes the mysql and returns the
    number of rolls affect.

    :param sql: MySQL to be run on the database
    :return: The number of affected rolls
    """
    with open_db_connection() as db:
        cursor = db.cursor()
        try:
            rows = cursor.execute(sql)
            db.commit()
            return rows
        except MySQLdb.Error:
            db.rollback()
            raise