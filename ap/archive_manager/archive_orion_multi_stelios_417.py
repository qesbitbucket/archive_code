#!/usr/bin/env python
import os
import sys
import subprocess
from collections import namedtuple
try:
    import qesutil
except ImportError:
    import qes_pipeline.qesutil as qesutil
import archive_constants
import archive_util
import archive_jobs
import archive_search
__author__ = 'Neil Parley'

MultiOrion = namedtuple('MultiOrion', 'field_name, campaign, work_root, camera, camera_id, '
                                      'photometry_info, input_files')


@qesutil.check_all_param_for_none
def get_info(fields=None, work_dir=None, fname="srm", create=True):
    main_field_name = None
    main_campaign = None
    # main_work_root = None
    main_camera_id = None
    main_camera = None
    main_photometry_info = None
    input_files = []

    for field in fields:
        camera = field[0]
        field_name = field[1]
        campaign = field[2]
        photometry_info = field[3]
        camera_id = field[4]

        campaign_global = archive_util.get_global_campaign(campaign)

        if main_field_name is None:
            main_field_name = field_name
        if main_campaign is None:
            main_campaign = campaign_global
        if main_camera_id is None:
            main_camera_id = camera_id
        if main_camera is None:
            main_camera = camera
        if main_photometry_info is None:
            main_photometry_info = photometry_info

        #  Across multi campaigns
        if main_campaign not in (campaign_global, None, "CX"):
            main_campaign = "CX"

        #  Across multi cameras
        if main_camera_id not in (camera_id, None, '410'):
            main_camera_id = '410'
            main_camera = camera[0:-1] + '0'

    if main_camera_id == '400':
        main_camera_id = '410'
    elif main_camera_id == '401':
        main_camera_id = '411'
    elif main_camera_id == '402':
        main_camera_id = '412'
    elif main_camera_id == '403':
        main_camera_id = '413'
    elif main_camera_id == '404':
        main_camera_id = '414'
    elif main_camera_id == '405':
        main_camera_id = '415'
    elif main_camera_id == '406':
        main_camera_id = '416'

    some_index = 1
    for field in fields:
        if some_index == 1:
            first_camera_id = field[4]
            some_index = 0

    if main_camera_id == '410' and first_camera_id in ['406','416']:
        main_camera_id = '417'
        main_camera = main_camera[0:-1] + '7'
        main_field_name = main_field_name[0:-1] + '1'

    work_root_base = archive_util.create_work_root(field=main_field_name, campaign=main_campaign,
                                                   camera=main_camera,
                                                   photometry_info=main_photometry_info,
                                                   work_dir='', create=create)

    if os.path.basename(work_dir) == work_root_base:
        main_work_root = work_dir
    else:
        main_work_root = os.path.join(work_dir, work_root_base)
    #print 'Main work root',main_work_root
    if create:
        for field in fields:
            camera = field[0]
            field_name = field[1]
            campaign = field[2]
            photometry_info = field[3]
            camera_id = field[4]
            campaign_global = archive_util.get_global_campaign(campaign)

            work_root = archive_util.create_work_root(field=field_name, campaign=campaign_global,
                                                      camera=camera,
                                                      photometry_info=photometry_info,
                                                      work_dir=main_work_root, create=create)

            campaign_global_phot = photometry_info.campaign_letter + campaign_global[1:]
            #print 'Work root',work_root
            try:
                fits_file, campaign_phot = archive_search.find_file(work_root=work_root,
                                                                    field=field_name,
                                                                    camera_id=camera_id,
                                                                    campaign=campaign_global_phot,
                                                                    tag=None, fname=fname)
                input_files.append(os.path.join(work_root, fits_file))
                #print input_files
            except:
                continue

    #IT WAS HERE BEFORE, BUT NOT ANYMORE

    return MultiOrion(field_name=main_field_name, campaign=main_campaign, camera=main_camera,
                      camera_id=main_camera_id, work_root=main_work_root,
                      photometry_info=main_photometry_info,
                      input_files=input_files)


@archive_jobs.register_job
@qesutil.check_nearly_all_param_for_none(can_be_none=["tag", "fname", "job_id"])
def orion_multi(fields=None, work_dir=None, tfa=False, multi_only=False, orion_dir=None,
                tag="NEW_MULTI", fname="srm",
                job_id=None, max_period=30):

    multi_orion = get_info(fields=fields, work_dir=work_dir, fname=fname)

    print("Running orion")

    #orion_root = os.path.join(orion_dir, "orion_nolimit")
    orion_root = os.path.join(orion_dir, "orion")

    if tfa:
        tag = tag.upper() + "_TAMTFA"
        output_file = "{field}_{camera_id}_{campaign}_orion_{tag}.fits" \
            .format(field=multi_orion.field_name, camera_id=multi_orion.camera_id,
                    campaign=multi_orion.campaign, tag=tag.lower())
        orion_commands = [orion_root, "--tfa"]

    else:
        tag = tag.upper() + "_TAMUZ" 
        output_file = "{field}_{camera_id}_{campaign}_orion_{tag}.fits" \
            .format(field=multi_orion.field_name, camera_id=multi_orion.camera_id,
                    campaign=multi_orion.campaign, tag=tag.lower())
        orion_commands = [orion_root]

    orion_commands += ["--numstds", "900", "--tag", tag,
                       "--field", multi_orion.field_name, "--camera", multi_orion.camera_id,
                       "--season", multi_orion.campaign, "--maxperiod", str(max_period)]
    if multi_only:
        orion_commands.append("--multionly")

    orion_commands += ["--outfile", os.path.join(multi_orion.work_root,
                                                 output_file)] + multi_orion.input_files
    print 'Commands:',orion_commands
    child = subprocess.Popen(orion_commands, cwd=multi_orion.work_root)
    return_code = child.wait()
    if return_code:
        raise subprocess.CalledProcessError(return_code, cmd=orion_commands)


if __name__ == '__main__':

    try:
        import configparser
    except ImportError:
        import ConfigParser as configparser

    import argparse

    try:
        _qes_path = os.environ['QES_PATH']
    except NameError as n_error:
        print("Environmental variable not set: %s" % n_error)
        sys.exit(2)

    _work_dir_default = os.path.join(_qes_path, archive_constants.archive_work)
    _archive_code_default = os.path.join(_qes_path, archive_constants.archive_code)
    _orion_code_default = os.path.join(_qes_path, archive_constants.orion_code)

    parser = argparse.ArgumentParser(description='Runs an archive job')
    parser.add_argument('--field', action='append', nargs=4, required=True,
                        metavar=('CAMERA', 'FIELD', 'CAMPAIGN', 'TYPE'),
                        help="Add as many fields as you wish to combine, each --field needs:\n "
                             "Camera to ingest (e.g. KCAM_US_T01D01), "
                             "Field to ingest (e.g. 002000+350000), "
                             "Campaign to ingest (e.g. C7), "
                             "Photometry type (dia or aperture)")

    parser.add_argument("--work_dir", default=_work_dir_default,
                        help="Work directory of ingest Files (default: {dir})"
                        .format(dir=_work_dir_default))
    parser.add_argument("--archive_code", default=_archive_code_default,
                        help="Directory of archive code (default: {dir})"
                        .format(dir=_archive_code_default))
    parser.add_argument("--orion_code", default=_orion_code_default,
                        help="Directory of orion code (default: {dir})"
                        .format(dir=_orion_code_default))
    parser.add_argument("--tfa", help="Run tfa (default False)",
                        action="store_true")
    parser.add_argument("--multionly", help="Only stars in all the fields (default False)",
                        action="store_true")
    parser.add_argument("--tag", default="NEW_MULTI", help="Override the archive TAMUZ tag")
    parser.add_argument("--job_id", default=None, type=int, help="Job id if running on the cluster")

    c_args = parser.parse_args()

    if len(c_args.field) < 2:
        print("Error: You need to provide at least two different fields to archive_multi_jobs")
        sys.exit(1)

    _alsubai_control_path = os.path.join(_qes_path, 'qes_pipeline', 'Alsubai_Control')

    for f in c_args.field:

        if f[3] not in ('dia', 'aperture'):
            print("Error: Type must be dia or aperture")
        else:
            f[3] = getattr(archive_constants.photometry, f[3])

        _config_cam_campaign = configparser.ConfigParser()
        _campaign = archive_util.get_global_campaign(f[2])

        if not _config_cam_campaign.read(os.path.join(_alsubai_control_path, 'cfg', _campaign,
                                                      'config.' + f[0] + '.cfg')):
            print('ERROR - The configuration file is missing for the camera: ' +
                  os.path.join('cfg', _campaign, 'config.' + f[0] + '.cfg'))
            sys.exit(1)

        _camera_id = _config_cam_campaign.get('main', 'camID')
        f.append(_camera_id)
    #print 'We are here and the tag is', c_args.tag
    orion_multi(fields=c_args.field, work_dir=c_args.work_dir, tfa=c_args.tfa,
                multi_only=c_args.multionly, tag=c_args.tag,
                orion_dir=c_args.orion_code, job_id=c_args.job_id)
