__author__ = 'Neil Parley'

import pyfits
import numpy


def normalise(orion_file):
    """

    Read in hdu of fits file
    Read in period data in period variable
    Read in periodogram data in variable
    Create a array of all the different boxes sizes and then create a master_periodograms variable
    to hold a periodogram per box size
    Loop through the periodograms and sum up the periodograms for each box size
    Loop through the master_periodograms and remove a 3rd order polyfit
    For each periodogram also fit and remove a 3rd order polyfit then divide by master
    update the fits file

    :param orion_file: File name of orion file to fix
    """
    hdu = pyfits.open(orion_file, mode='update')
    periods = hdu['periods'].data[0][0]
    periodogram_data = hdu['periodograms'].data

    box_sizes = tuple(set(periodogram_data['box_size']))
    array_length = periodogram_data['chisq'].shape[1]

    master_periodograms = numpy.zeros((len(box_sizes), array_length))
    ratio_chisq_array = [[] for _ in xrange(len(box_sizes))]
    used = numpy.zeros(len(box_sizes), dtype=int)
    mean_chisq_ratio = numpy.zeros(len(box_sizes))
    std_chisq_ratio = numpy.zeros(len(box_sizes))

    for periodogram in periodogram_data:
        box_size = periodogram['box_size']
        index = box_sizes.index(box_size)
        min_chisq = periodogram['chisq'].min()
        median_chisq = numpy.median(periodogram['chisq'])
        ratio = min_chisq / median_chisq
        ratio_chisq_array[index].append(ratio)

    for box_size in box_sizes:
        index = box_sizes.index(box_size)
        mean_chisq_ratio[index] = numpy.median(ratio_chisq_array[index])
        std_chisq_ratio[index] = numpy.std(ratio_chisq_array[index])
        print("Box size {box_size}, Median: {med}, std: {std}".format(box_size=box_size,
                                                                      med=mean_chisq_ratio[index],
                                                                      std=std_chisq_ratio[index]))

    for periodogram in periodogram_data:
        box_size = periodogram['box_size']
        index = box_sizes.index(box_size)
        min_chisq = periodogram['chisq'].min()
        median_chisq = numpy.median(periodogram['chisq'])
        ratio = min_chisq / median_chisq
        if (mean_chisq_ratio[index] + std_chisq_ratio[index]) > ratio:
            master_periodograms[index] += periodogram['chisq']
            used[index] += 1

    for box_size in box_sizes:
        index = box_sizes.index(box_size)
        print("Used {used} out of {total}".format(used=used[index],
                                                  total=len(ratio_chisq_array[index])))

    for master_periodogram in master_periodograms:
        coeff = numpy.polyfit(periods, master_periodogram, 3)
        model = numpy.poly1d(coeff)
        master_periodogram /= model(periods)

    for periodogram in periodogram_data:
        box_size = periodogram['box_size']
        index = box_sizes.index(box_size)
        master_periodogram = master_periodograms[index]
        coeff = numpy.polyfit(periods, periodogram['chisq'], 3)
        model = numpy.poly1d(coeff)

        periodogram['chisq'] /= model(periods)
        periodogram['chisq'] /= master_periodogram
        periodogram['chisq'] *= -1.0

    hdu.flush()

    return