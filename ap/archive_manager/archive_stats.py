#!/usr/bin/env python
"""
This module runs the stats to update the database tables after new data is ingested into the archive. The normal
calling procedure would be:

./archive_stats.py KCAM_US_T01D01 002000+450000 C7 dia

--data_dir can be used to overload the default directory of the qes data. However if the data directory has been moved
permanently this should be updated in archive_constants.py instead

--archive_code can be used to overload the location of the archive code although if the code moves permanently the
value in the archive_constants file should be changed.

--job_id is used for when the code is being run by the archive_jobs.py code and should not be used if the code is
being used from the command line

--work_dir is the root directory where the working archive directories are. The code will look in a sub directory
of for example 002000+450000_KCAM_US_T01D01_C7_dia for the files.
"""
from __future__ import print_function
import sys
import os
import subprocess
import errno
try:
    import qesutil
except ImportError:
    import qes_pipeline.qesutil as qesutil
import archive_jobs
import archive_constants
import archive_util
import archive_mysql
import archive_db
__author__ = 'Neil Parley'


@archive_jobs.register_job
@qesutil.check_nearly_all_param_for_none(can_be_none=["job_id"])
def stats(camera=None, data_dir=None, field=None, campaign=None, work_dir=None, code_dir=None, camera_id=None,
          photometry_info=None, job_id=None):
    """
    Main function for running the stats programs on data that has just been ingested by the pipeline.

    The function gets the global campaign and then the working directory using the camera / field / campaign and
    photometry type. It then runs calc_stats which calls the programs responsible for calculating the photometry
    stats (i.e mean etc). It then calls diff list laod which updates the database table with the location of any new
    difference image png files. It then calls nomad_fetch which updates the database with all new nomad stars that have
    been detected and are not already in the database. It then calls make_acel which creates accelerator files for the
    archive sky title files.

    :type photometry_info: archive_constants.Photometry_info
    :param camera: Camera string e.g. KCAM_US_T01D01
    :param data_dir: The location of the QES data root (e.g. $QES_PATH/qes_data
    :param field: Field string e.g. 002000+450000
    :param campaign: Campaign string e.g. C7
    :param work_dir: Base working directory should be set to os.path.join(qes_path, archive_constants.archive_work) if
                     using the default
    :param code_dir: Location of the archive code
    :param camera_id: The database camera id, can be loaded from the configuration files
    :param photometry_info: The photometry type of the run, types defined in archive_constants
    :param job_id: This is given to the function when it is started as a job by archive_jobs. It is used by the
                   decorator archive_jobs.register_job to update the database with when the job starts and finishes.
    """
    campaign_global = archive_util.get_global_campaign(campaign)

    work_root = archive_util.create_work_root(field=field, campaign=campaign_global, camera=camera,
                                              photometry_info=photometry_info, work_dir=work_dir)

    data_root = os.path.join(data_dir, camera, field, campaign_global)

    campaign_global_phot = photometry_info.campaign_letter + campaign_global[1:]

    calc_stats(field=field, campaign=campaign_global_phot, camera_id=camera_id, code_dir=code_dir, work_root=work_root)
    print("Loading photsummary")
    archive_mysql.calc_stats_load(os.path.join(work_root, "photsummary.dump"))

    if photometry_info == archive_constants.dia:
        diff_image_count = diff_list_load(camera_id=camera_id, field=field, campaign=campaign_global,
                                          data_root=data_root)
        print("Added {count} diff images".format(count=diff_image_count))

    nomad_count = nomad_fetch(camera_id=camera_id, field=field, campaign=campaign_global_phot, work_root=work_root,
                              code_dir=code_dir)
    if nomad_count is None:
        print("No stars need updating with new nomad info")
    else:
        archive_mysql.nomad_load(os.path.join(work_root, "nomad.dump"))
        print("Tried to update {count} stars with new nomad info".format(count=nomad_count))

    make_accel(field=field, campaign=campaign_global_phot, camera_id=camera_id, code_dir=code_dir, work_root=work_root)


@qesutil.check_all_param_for_none
def make_accel(field=None, campaign=None, camera_id=None, code_dir=None, work_root=None):
    """
    Calls the apmakeaccel code to create the sky tile accelerator files

    :param field: Field string e.g. 002000+450000
    :param campaign: Campaign string e.g. C7
    :param camera_id: The database camera id, can be loaded from the configuration files e.g. 411
    :param code_dir: Location of the archive code
    :param work_root: Working directory of the archive files for this field
    :raise subprocess.CalledProcessError: Raised if apmakeaccel does not execute successfully.
    """
    print("Running make accel")

    make_accel_root = os.path.join(code_dir, "apmakeaccel")

    make_accel_commands = [make_accel_root, "--field", field, "--camera", camera_id, "--campaign", campaign]

    child = subprocess.Popen(make_accel_commands, cwd=work_root)
    return_code = child.wait()
    if return_code:
        raise subprocess.CalledProcessError(return_code, cmd=make_accel_commands)


@qesutil.check_all_param_for_none
def calc_stats(field=None, campaign=None, camera_id=None, code_dir=None, work_root=None):
    """
    Calls apcalstats to calculate the stats i.e. mean etc for the new data that has just been ingested. Creates a file
    called photsummary.dump. To load the results into the data the function
    archive_mysql.calc_stats_load(os.path.join(work_root, "photsummary.dump")) has to be called. This is done in stats()
    after calling this function

    :param field: Field string e.g. 002000+450000
    :param campaign: Campaign string e.g. C7
    :param camera_id: The database camera id, can be loaded from the configuration files e.g. 411
    :param code_dir: Location of the archive code
    :param work_root: Working directory of the archive files for this field
    :raise subprocess.CalledProcessError: Raised if apcalcstats does not execute successfully.
    """
    print("Running calc stats")

    calc_stats_root = os.path.join(code_dir, "apcalcstats_threads")

    calc_stats_commands = [calc_stats_root, "-f", field, "-c", camera_id, "-s", campaign]

    child = subprocess.Popen(calc_stats_commands, cwd=work_root)
    return_code = child.wait()
    if return_code:
        raise subprocess.CalledProcessError(return_code, cmd=calc_stats_commands)


@qesutil.check_all_param_for_none
def diff_list_load(camera_id=None, field=None, campaign=None, data_root=None):
    """
    Update the diff list table with new difference images for the field being ingested. Gets a list of new images with
    no difference image in the database. For each of those images search to see if the difference file is on disk. If
    the file is on disk add the difference image to the database.

    :param camera_id: The database camera id, can be loaded from the configuration files e.g. 411
    :param field: Field string e.g. 002000+450000
    :param campaign: Campaign string e.g. C7
    :param data_root: Root of the QES data
    :return: Number of difference images added
    """

    diff_list = archive_db.DiffList(user=archive_constants.db_settings.user,
                                    password=archive_constants.db_settings.password,
                                    host=archive_constants.db_settings.host,
                                    db=archive_constants.db_settings.database)

    new_diff_images = diff_list.get_new_diff_images(field=field, camera_id=camera_id, campaign=campaign)
    png_image_path = os.path.join(data_root, "pngImages")
    diff_image_counter = 0

    for new_diff_image in new_diff_images:
        file_name = new_diff_image.fileleaf
        file_name = file_name.replace("_lc.fits", "")
        file_path = os.path.join(png_image_path, file_name)
        if os.path.exists(file_path+".res.png"):
            diff_list.add_diff_image(image_id=new_diff_image.image_id, file_path=file_path)
            diff_image_counter += 1

    return diff_image_counter


@qesutil.check_all_param_for_none
def nomad_fetch(camera_id=None, field=None, campaign=None, work_root=None, code_dir=None):
    """
    Fetch any new stars from the nomad catalogue. Get a list of stars with no nomad ids from the database then call
    apgetnomad which fetches the catalogue enteries from the online catalogue. Loading of the nomad catalogue which is
    downloaded for the new stars is done by nomad_load in archive_fetch

    :param camera_id: The database camera id, can be loaded from the configuration files e.g. 411
    :param field: Field string e.g. 002000+450000
    :param campaign: Campaign string e.g. C7
    :param work_root: Working directory of the archive files for this field
    :param code_dir: Location of the archive code
    :return:
    """

    print("Running nomad fetch")

    star_list = archive_mysql.get_new_nomad(field=field, camera_id=camera_id, campaign=campaign)
    if star_list is None:
        return None

    nomad_fetch_root = os.path.join(code_dir, "apgetnomad")

    child = subprocess.Popen(nomad_fetch_root, stdin=subprocess.PIPE, cwd=work_root)

    for star in star_list:
        try:
            child.stdin.write("{id} {ra:15.11f} {dec:15.11f}\n".format(id=star[0], ra=star[1], dec=star[2]))
        except IOError as e:
            if e.errno == errno.EPIPE or e.errno == errno.EINVAL:
                break
            else:
                raise

    child.stdin.close()
    return_code = child.wait()
    if return_code:
        raise subprocess.CalledProcessError(return_code, cmd=nomad_fetch_root)

    return len(star_list)


if __name__ == '__main__':

    try:
        import configparser
    except ImportError:
        import ConfigParser as configparser

    import argparse

    try:
        _qes_path = os.environ['QES_PATH']
    except NameError as n_error:
        print("Environmental variable not set: %s" % n_error)
        sys.exit(2)

    _data_dir_default = os.path.join(_qes_path, "qes_data")
    _work_dir_default = os.path.join(_qes_path, archive_constants.archive_work)
    _archive_code_default = os.path.join(_qes_path, archive_constants.archive_code)

    parser = argparse.ArgumentParser(description='Runs the stats commands on the archive ingest')
    parser.add_argument('camera', metavar='CAMERA', help='Camera to ingest (e.g. KCAM_US_T01D01)')
    parser.add_argument('field', metavar='FIELD', help='Field to ingest (e.g. 002000+350000)')
    parser.add_argument('campaign', metavar='CAMPAIGN', help='Campaign to ingest (e.g. C7)')
    parser.add_argument('photometry', metavar='TYPE', choices=['dia', 'aperture'],
                        help='Photometry type (dia or aperture)')
    parser.add_argument("--data_dir", default=_data_dir_default,
                        help="Root directory of qes Files (default: {dir})".format(dir=_data_dir_default))
    parser.add_argument("--work_dir", default=_work_dir_default,
                        help="Work directory of ingest Files (default: {dir})".format(dir=_work_dir_default))
    parser.add_argument("--archive_code", default=_archive_code_default,
                        help="Directory of archive code (default: {dir})".format(dir=_archive_code_default))
    parser.add_argument("--job_id", default=None, type=int, help="Job id if running on the cluster")
    args = parser.parse_args()

    _alsubai_control_path = os.path.join(_qes_path, 'qes_pipeline', 'Alsubai_Control')
    _config_cam_campaign = configparser.ConfigParser()

    if not _config_cam_campaign.read(os.path.join(_alsubai_control_path,
                                                  'cfg', args.campaign, 'config.' + args.camera + '.cfg')):
        print('ERROR - The configuration file is missing for the camera: ' +
              os.path.join('cfg', args.campaign, 'config.' + args.camera + '.cfg'))
        sys.exit(1)

    stats(camera=args.camera, field=args.field, campaign=args.campaign, data_dir=args.data_dir, work_dir=args.work_dir,
          code_dir=args.archive_code, camera_id=_config_cam_campaign.get('main', 'camID'),
          photometry_info=getattr(archive_constants.photometry, args.photometry), job_id=args.job_id)
