__author__ = 'Neil Parley'

import unittest
import time
try:
    from cStringIO import StringIO
except ImportError:
    from io import StringIO
import archive_db
import archive_jobs
import archive_constants
import Alsubai_Control.pbs as pbs
import Alsubai_Control.pbs_test as pbs_test


_ingest_submit_string = """#!/bin/bash
#PBS -W umask=0007
#PBS -N A1
#PBS -q workq
#PBS -l ncpus=1
#PBS -l mem=4gb
#PBS -l walltime=20:00:00
#PBS -o /qes_path/qes_data/archive_logs/KCAM_US_T01D01/002000+350000_KCAM_US_T01D01_C7_dia/A1.out
#PBS -e /qes_path/qes_data/archive_logs/KCAM_US_T01D01/002000+350000_KCAM_US_T01D01_C7_dia/A1.err
. /qes_path/.bashrc
/qes_path/archive_code/ap/archive_manager/archive_ingest.py KCAM_US_T01D01 002000+350000 C7 dia --job_id 1"""

_stats_submit_string = """#!/bin/bash
#PBS -W umask=0007
#PBS -W depend=afterok:dry_run
#PBS -N A2
#PBS -q workq
#PBS -l ncpus=1
#PBS -l mem=4gb
#PBS -l walltime=20:00:00
#PBS -o /qes_path/qes_data/archive_logs/KCAM_US_T01D01/002000+350000_KCAM_US_T01D01_C7_dia/A2.out
#PBS -e /qes_path/qes_data/archive_logs/KCAM_US_T01D01/002000+350000_KCAM_US_T01D01_C7_dia/A2.err
. /qes_path/.bashrc
/qes_path/archive_code/ap/archive_manager/archive_stats.py KCAM_US_T01D01 002000+350000 C7 dia --job_id 2"""

_stats_submit_string_start_with = """#!/bin/bash
#PBS -W umask=0007
#PBS -N A1
#PBS -q workq
#PBS -l ncpus=1
#PBS -l mem=4gb
#PBS -l walltime=20:00:00
#PBS -o /qes_path/qes_data/archive_logs/KCAM_US_T01D01/002000+350000_KCAM_US_T01D01_C7_dia/A1.out
#PBS -e /qes_path/qes_data/archive_logs/KCAM_US_T01D01/002000+350000_KCAM_US_T01D01_C7_dia/A1.err
. /qes_path/.bashrc
/qes_path/archive_code/ap/archive_manager/archive_stats.py KCAM_US_T01D01 002000+350000 C7 dia --job_id 1"""

_mcmc_submit_string = """#!/bin/bash
#PBS -W umask=0007
#PBS -N A1
#PBS -q workq
#PBS -l ncpus=16
#PBS -l mem=32gb
#PBS -l walltime=46:00:00
#PBS -o /qes_path/qes_data/archive_logs/KCAM_US_T01D01/002000+350000_KCAM_US_T01D01_C7_dia/A1.out
#PBS -e /qes_path/qes_data/archive_logs/KCAM_US_T01D01/002000+350000_KCAM_US_T01D01_C7_dia/A1.err
. /qes_path/.bashrc
/qes_path/archive_code/ap/archive_manager/archive_mcmc.py KCAM_US_T01D01 002000+350000 C7 dia --tfa --fname orion --job_id 1"""


def database_connection():
    return archive_db.ArchiveJobs(user="pipeline", password="pipeline", host="localhost", db="unit_test")


def tare_down():
    dh = database_connection()
    connection = dh.engine.connect()
    connection.execute("truncate table archive_jobs;")
    connection.close()


class ArchiveJobTests(unittest.TestCase):
    def test_submit_one_job(self):
        db_server = database_connection()
        pbs_server = pbs.PBS(queue_text=pbs_test._pbsqueues_output, load_text=pbs_test._pbsload_out, _test=True)
        pipeline = archive_jobs.ArchivePipeline(photometry=archive_constants.dia, camera="KCAM_US_T01D01",
                                                username="np40", pbs_server=pbs_server, db_server=db_server,
                                                camera_id=411, field="002000+350000", campaign="C7",
                                                qes_path="/qes_path")

        pipeline.submit_job(pipeline.stages["ingest"], dry_run=True)
        ingest_stage_submit_string = pipeline.stages["ingest"].pbs_job.submit_string
        tare_down()

        self.assertEqual(ingest_stage_submit_string, _ingest_submit_string)

    def test_submit_a_job(self):
        db_server = database_connection()
        pbs_server = pbs.PBS(queue_text=pbs_test._pbsqueues_output, load_text=pbs_test._pbsload_out, _test=True)
        pipeline = archive_jobs.ArchivePipeline(photometry=archive_constants.dia, camera="KCAM_US_T01D02",
                                                username="np40", pbs_server=pbs_server, db_server=db_server,
                                                camera_id=411, field="021037+324400", campaign="C7",
                                                qes_path="/panfs/vol/qeeri/qes")

        pipeline.submit_job(pipeline.stages["ingest"], dry_run=True, ignore_dependency=True)
        pipeline.submit_job(pipeline.stages["stats"], dry_run=True, ignore_dependency=True)
        pipeline.submit_job(pipeline.stages["fetch"], dry_run=True, ignore_dependency=True)
        pipeline.submit_job(pipeline.stages["sysrem"], dry_run=True, ignore_dependency=True)
        pipeline.submit_job(pipeline.stages["orion"], dry_run=True, ignore_dependency=True)
        pipeline.submit_job(pipeline.stages["orion_tfa"], dry_run=True, ignore_dependency=True)
        pipeline.submit_job(pipeline.stages["mcmc"], dry_run=True, ignore_dependency=True)
        pipeline.submit_job(pipeline.stages["mcmc_tfa"], dry_run=True, ignore_dependency=True)
        pipeline.submit_job(pipeline.stages["update"], dry_run=True, ignore_dependency=True)
        pipeline.submit_job(pipeline.stages["update_tfa"], dry_run=True, ignore_dependency=True)
        # print pipeline.stages["update_tfa"].pbs_job.submit_string
        tare_down()

    def test_submit_pipeline_1(self):
        db_server = database_connection()
        pbs_server = pbs.PBS(queue_text=pbs_test._pbsqueues_output, load_text=pbs_test._pbsload_out, _test=True)
        pipeline = archive_jobs.ArchivePipeline(photometry=archive_constants.dia, camera="KCAM_US_T01D02",
                                                username="np40", pbs_server=pbs_server, db_server=db_server,
                                                camera_id=411, field="021037+324400", campaign="C7",
                                                qes_path="/panfs/vol/qeeri/qes")
        out = StringIO()
        pipeline.submit_pipeline(dry_run=True, _out=out)
        time.sleep(1)
        pipeline.db.end_job_in_queue(job_id=1)
        job = pipeline.db.get_job(job_id=1)
        self.assertGreaterEqual(job.in_queue, 1)
        tare_down()

    def test_submit_pipeline_2(self):
        db_server = database_connection()
        pbs_server = pbs.PBS(queue_text=pbs_test._pbsqueues_output, load_text=pbs_test._pbsload_out, _test=True)
        pipeline = archive_jobs.ArchivePipeline(photometry=archive_constants.dia, camera="KCAM_US_T01D02",
                                                username="np40", pbs_server=pbs_server, db_server=db_server,
                                                camera_id=411, field="021037+324400", campaign="C7",
                                                qes_path="/panfs/vol/qeeri/qes")
        out = StringIO()
        pipeline.submit_pipeline(dry_run=True, _out=out)
        pipeline.db.end_job_in_queue(job_id=1)
        pipeline.db.end_job(job_id=1, status=pbs.status.finish)
        job = pipeline.db.get_job(job_id=1)
        pipeline.db.queue_next_in_pipeline(job_id=1, tfa=None)
        job2 = pipeline.db.get_job(job_id=2)
        self.assertEqual((job.status, job2.status), ('Finished', 'Queued'))
        tare_down()

    def test_submit_pipeline_3(self):
        db_server = database_connection()
        pbs_server = pbs.PBS(queue_text=pbs_test._pbsqueues_output, load_text=pbs_test._pbsload_out, _test=True)
        pipeline = archive_jobs.ArchivePipeline(photometry=archive_constants.dia, camera="KCAM_US_T01D02",
                                                username="np40", pbs_server=pbs_server, db_server=db_server,
                                                camera_id=411, field="021037+324400", campaign="C7",
                                                qes_path="/panfs/vol/qeeri/qes")
        out = StringIO()
        pipeline.submit_pipeline(dry_run=True, _out=out)

        pipeline.db.end_job_in_queue(job_id=1)
        pipeline.db.end_job(job_id=1, status=pbs.status.finish)
        pipeline.db.queue_next_in_pipeline(job_id=1, tfa=None)

        pipeline.db.end_job_in_queue(job_id=2)
        pipeline.db.end_job(job_id=2, status=pbs.status.finish)
        pipeline.db.queue_next_in_pipeline(job_id=2, tfa=None)

        pipeline.db.end_job_in_queue(job_id=3)
        pipeline.db.end_job(job_id=3, status=pbs.status.finish)
        pipeline.db.queue_next_in_pipeline(job_id=3, tfa=None)

        pipeline.db.end_job_in_queue(job_id=4)
        pipeline.db.end_job(job_id=4, status=pbs.status.finish)
        pipeline.db.queue_next_in_pipeline(job_id=4, tfa=None)

        job5 = pipeline.db.get_job(job_id=5)
        job6 = pipeline.db.get_job(job_id=6)

        self.assertEqual((job5.status, job6.status), ('Queued', 'Queued'))
        tare_down()

    def test_submit_pipeline_4(self):
        db_server = database_connection()
        pbs_server = pbs.PBS(queue_text=pbs_test._pbsqueues_output, load_text=pbs_test._pbsload_out, _test=True)
        pipeline = archive_jobs.ArchivePipeline(photometry=archive_constants.dia, camera="KCAM_US_T01D02",
                                                username="np40", pbs_server=pbs_server, db_server=db_server,
                                                camera_id=411, field="021037+324400", campaign="C7",
                                                qes_path="/panfs/vol/qeeri/qes")
        out = StringIO()
        pipeline.submit_pipeline(dry_run=True, _out=out)

        pipeline.db.end_job_in_queue(job_id=1)
        pipeline.db.end_job(job_id=1, status=pbs.status.finish)
        pipeline.db.queue_next_in_pipeline(job_id=1, tfa=None)

        pipeline.db.end_job_in_queue(job_id=2)
        pipeline.db.end_job(job_id=2, status=pbs.status.finish)
        pipeline.db.queue_next_in_pipeline(job_id=2, tfa=None)

        pipeline.db.end_job_in_queue(job_id=3)
        pipeline.db.end_job(job_id=3, status=pbs.status.finish)
        pipeline.db.queue_next_in_pipeline(job_id=3, tfa=None)

        pipeline.db.end_job_in_queue(job_id=4)
        pipeline.db.end_job(job_id=4, status=pbs.status.finish)
        pipeline.db.queue_next_in_pipeline(job_id=4, tfa=None)

        pipeline.db.end_job_in_queue(job_id=5)
        pipeline.db.end_job_in_queue(job_id=6)
        pipeline.db.end_job(job_id=5, status=pbs.status.finish)
        pipeline.db.queue_next_in_pipeline(job_id=5, tfa=True)

        job7 = pipeline.db.get_job(job_id=7)
        job6 = pipeline.db.get_job(job_id=6)

        self.assertEqual((job7.status, job6.status), ('Queued', 'Running'))
        tare_down()

    def test_submit_pipeline_5(self):
        db_server = database_connection()
        pbs_server = pbs.PBS(queue_text=pbs_test._pbsqueues_output, load_text=pbs_test._pbsload_out, _test=True)
        pipeline = archive_jobs.ArchivePipeline(photometry=archive_constants.dia, camera="KCAM_US_T01D02",
                                                username="np40", pbs_server=pbs_server, db_server=db_server,
                                                camera_id=411, field="021037+324400", campaign="C7",
                                                qes_path="/panfs/vol/qeeri/qes")
        out = StringIO()
        pipeline.submit_pipeline(dry_run=True, _out=out)

        pipeline.db.end_job_in_queue(job_id=1)
        pipeline.db.end_job(job_id=1, status=pbs.status.finish)
        pipeline.db.queue_next_in_pipeline(job_id=1, tfa=None)

        pipeline.db.end_job_in_queue(job_id=2)
        pipeline.db.end_job(job_id=2, status=pbs.status.finish)
        pipeline.db.queue_next_in_pipeline(job_id=2, tfa=None)

        pipeline.db.end_job_in_queue(job_id=3)
        pipeline.db.end_job(job_id=3, status=pbs.status.finish)
        pipeline.db.queue_next_in_pipeline(job_id=3, tfa=None)

        pipeline.db.end_job_in_queue(job_id=4)
        pipeline.db.end_job(job_id=4, status=pbs.status.failed)
        pipeline.db.fail_rest_of_pipeline(job_id=4, tfa=None)

        job5 = pipeline.db.get_job(job_id=5)
        job6 = pipeline.db.get_job(job_id=6)

        self.assertEqual((job5.status, job6.status), ('Failed', 'Failed'))
        tare_down()

    def test_submit_pipeline_6(self):
        db_server = database_connection()
        pbs_server = pbs.PBS(queue_text=pbs_test._pbsqueues_output, load_text=pbs_test._pbsload_out, _test=True)
        pipeline = archive_jobs.ArchivePipeline(photometry=archive_constants.dia, camera="KCAM_US_T01D02",
                                                username="np40", pbs_server=pbs_server, db_server=db_server,
                                                camera_id=411, field="021037+324400", campaign="C7",
                                                qes_path="/panfs/vol/qeeri/qes")
        out = StringIO()
        pipeline.submit_pipeline(dry_run=True, _out=out)

        pipeline.db.end_job_in_queue(job_id=1)
        pipeline.db.end_job(job_id=1, status=pbs.status.finish)
        pipeline.db.queue_next_in_pipeline(job_id=1, tfa=None)

        pipeline.db.end_job_in_queue(job_id=2)
        pipeline.db.end_job(job_id=2, status=pbs.status.finish)
        pipeline.db.queue_next_in_pipeline(job_id=2, tfa=None)

        pipeline.db.end_job_in_queue(job_id=3)
        pipeline.db.end_job(job_id=3, status=pbs.status.finish)
        pipeline.db.queue_next_in_pipeline(job_id=3, tfa=None)

        pipeline.db.end_job_in_queue(job_id=4)
        pipeline.db.end_job(job_id=4, status=pbs.status.finish)
        pipeline.db.queue_next_in_pipeline(job_id=4, tfa=None)

        pipeline.db.end_job_in_queue(job_id=5)
        pipeline.db.end_job_in_queue(job_id=6)
        pipeline.db.end_job(job_id=5, status=pbs.status.finish)
        pipeline.db.queue_next_in_pipeline(job_id=5, tfa=True)

        pipeline.db.end_job(job_id=6, status=pbs.status.failed)
        pipeline.db.fail_rest_of_pipeline(job_id=6, tfa=False)

        job7 = pipeline.db.get_job(job_id=7)
        job6 = pipeline.db.get_job(job_id=8)

        self.assertEqual((job7.status, job6.status), ('Queued', 'Failed'))
        tare_down()

    def test_submit_pipeline_7(self):
        db_server = database_connection()
        pbs_server = pbs.PBS(queue_text=pbs_test._pbsqueues_output, load_text=pbs_test._pbsload_out, _test=True)
        pipeline = archive_jobs.ArchivePipeline(photometry=archive_constants.dia, camera="KCAM_US_T01D02",
                                                username="np40", pbs_server=pbs_server, db_server=db_server,
                                                camera_id=411, field="021037+324400", campaign="C7",
                                                qes_path="/panfs/vol/qeeri/qes")
        out = StringIO()
        pipeline.submit_pipeline(dry_run=True, _out=out)

        pipeline.db.end_job_in_queue(job_id=1)
        pipeline.db.end_job(job_id=1, status=pbs.status.finish)
        pipeline.db.queue_next_in_pipeline(job_id=1, tfa=None)

        pipeline.db.end_job_in_queue(job_id=2)
        pipeline.db.end_job(job_id=2, status=pbs.status.finish)
        pipeline.db.queue_next_in_pipeline(job_id=2, tfa=None)

        pipeline.db.end_job_in_queue(job_id=3)
        pipeline.db.end_job(job_id=3, status=pbs.status.finish)
        pipeline.db.queue_next_in_pipeline(job_id=3, tfa=None)

        pipeline.db.end_job_in_queue(job_id=4)
        pipeline.db.end_job(job_id=4, status=pbs.status.finish)
        pipeline.db.queue_next_in_pipeline(job_id=4, tfa=None)

        pipeline.db.end_job_in_queue(job_id=5)
        pipeline.db.end_job(job_id=5, status=pbs.status.finish)
        pipeline.db.queue_next_in_pipeline(job_id=5, tfa=True)

        pipeline.db.end_job_in_queue(job_id=6)
        pipeline.db.end_job(job_id=6, status=pbs.status.finish)
        pipeline.db.queue_next_in_pipeline(job_id=6, tfa=False)

        pipeline.db.end_job_in_queue(job_id=7)
        pipeline.db.end_job(job_id=7, status=pbs.status.finish)
        pipeline.db.queue_next_in_pipeline(job_id=7, tfa=True)

        pipeline.db.end_job_in_queue(job_id=8)
        pipeline.db.end_job(job_id=8, status=pbs.status.finish)
        pipeline.db.queue_next_in_pipeline(job_id=8, tfa=False)

        pipeline.db.end_job_in_queue(job_id=9)
        pipeline.db.end_job(job_id=9, status=pbs.status.finish)
        pipeline.db.queue_next_in_pipeline(job_id=9, tfa=True)

        pipeline.db.end_job_in_queue(job_id=10)
        pipeline.db.end_job(job_id=10, status=pbs.status.finish)
        pipeline.db.queue_next_in_pipeline(job_id=10, tfa=False)

        tare_down()

    def test_submit_two_jobs(self):
        db_server = database_connection()
        pbs_server = pbs.PBS(queue_text=pbs_test._pbsqueues_output, load_text=pbs_test._pbsload_out, _test=True)
        pipeline = archive_jobs.ArchivePipeline(photometry=archive_constants.dia, camera="KCAM_US_T01D01",
                                                username="np40", pbs_server=pbs_server, db_server=db_server,
                                                camera_id=411, field="002000+350000", campaign="C7",
                                                qes_path="/qes_path")

        pipeline.submit_job(pipeline.stages["ingest"], dry_run=True)
        pipeline.submit_job(pipeline.stages["stats"], dry_run=True)

        stats_stage_submit_string = pipeline.stages["stats"].pbs_job.submit_string
        tare_down()

        self.assertEqual(stats_stage_submit_string, _stats_submit_string)


    def test_submit_start_with(self):
        db_server = database_connection()
        pbs_server = pbs.PBS(queue_text=pbs_test._pbsqueues_output, load_text=pbs_test._pbsload_out, _test=True)
        pipeline = archive_jobs.ArchivePipeline(photometry=archive_constants.dia, camera="KCAM_US_T01D01",
                                                username="np40", pbs_server=pbs_server, db_server=db_server,
                                                camera_id=411, field="002000+350000", campaign="C7",
                                                qes_path="/qes_path", start_with='stats')

        pipeline.submit_job(pipeline.stages["stats"], dry_run=True)

        stats_stage_submit_string = pipeline.stages["stats"].pbs_job.submit_string

        tare_down()
        self.assertEqual(stats_stage_submit_string, _stats_submit_string_start_with)

    def test_submit_wrong_order(self):
        db_server = database_connection()
        pbs_server = pbs.PBS(queue_text=pbs_test._pbsqueues_output, load_text=pbs_test._pbsload_out, _test=True)
        pipeline = archive_jobs.ArchivePipeline(photometry=archive_constants.dia, camera="KCAM_US_T01D01",
                                                username="np40", pbs_server=pbs_server, db_server=db_server,
                                                camera_id=411, field="002000+350000", campaign="C7",
                                                qes_path="/qes_path")

        self.assertRaises(archive_jobs.DependencyError, pipeline.submit_job, pipeline.stages["stats"], dry_run=True)
        tare_down()

    def test_submit_one_ignore_dependency(self):
        db_server = database_connection()
        pbs_server = pbs.PBS(queue_text=pbs_test._pbsqueues_output, load_text=pbs_test._pbsload_out, _test=True)
        pipeline = archive_jobs.ArchivePipeline(photometry=archive_constants.dia, camera="KCAM_US_T01D01",
                                                username="np40", pbs_server=pbs_server, db_server=db_server,
                                                camera_id=411, field="002000+350000", campaign="C7",
                                                qes_path="/qes_path")

        pipeline.submit_job(pipeline.stages["mcmc_tfa"], dry_run=True, ignore_dependency=True)
        mcmc_stage_submit_string = pipeline.stages["mcmc_tfa"].pbs_job.submit_string
        tare_down()

        self.assertEqual(mcmc_stage_submit_string, _mcmc_submit_string)

    def test_print_pipeline(self):
        db_server = database_connection()
        pbs_server = pbs.PBS(queue_text=pbs_test._pbsqueues_output, load_text=pbs_test._pbsload_out, _test=True)
        pipeline = archive_jobs.ArchivePipeline(photometry=archive_constants.dia, camera="KCAM_US_T01D01",
                                                username="np40", pbs_server=pbs_server, db_server=db_server,
                                                camera_id=411, field="002000+350000", campaign="C7",
                                                qes_path="/qes_path")

        expected_pipeline = "ingest -> stats -> fetch -> sysrem -> orion_tfa -> orion -> mcmc_tfa -> mcmc -> " \
                            "update_tfa -> update -> clean"
        self.assertEqual(pipeline._print_pipeline(), expected_pipeline)


    def test_print_pipeline2(self):
        db_server = database_connection()
        pbs_server = pbs.PBS(queue_text=pbs_test._pbsqueues_output, load_text=pbs_test._pbsload_out, _test=True)
        pipeline = archive_jobs.ArchivePipeline(photometry=archive_constants.dia, camera="KCAM_US_T01D01",
                                                username="np40", pbs_server=pbs_server, db_server=db_server,
                                                camera_id=411, field="002000+350000", campaign="C7",
                                                qes_path="/qes_path", start_with='orion', end_with='update',
                                                exclude_tfa=True)

        expected_pipeline = "orion -> mcmc -> update"
        self.assertEqual(pipeline._print_pipeline(), expected_pipeline)

if __name__ == '__main__':
    unittest.main()
