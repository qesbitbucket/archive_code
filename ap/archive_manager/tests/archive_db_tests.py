__author__ = 'Neil Parley'
import datetime
import unittest
import archive_db

entry_before = ('002000+350000', 411, 'A7', datetime.date(2012, 9, 21), datetime.date(2014, 7, 10), 3644L,
                137L, 'no', 2.44901, 0.150708, 'real')


def database_connection():
    return archive_db.FieldSeason(user="pipeline", password="pipeline", host="localhost", db="unit_test")


def database_connection2():
    return archive_db.DiffList(user="pipeline", password="pipeline", host="localhost", db="unit_test")


class DBTests(unittest.TestCase):
    def test01_stats(self):
        field_season = database_connection()
        stats = field_season.get_image_stats_for_field(field="002000+350000", camera_id=411, campaign="C7")
        self.assertEqual(stats, ('002000+350000', 411, 'C7', datetime.date(2012, 9, 21),
                                 datetime.date(2014, 7, 12), 5968L, 197L, 2.43, 0.15))

    def test01_letter(self):
        field_season = database_connection()
        letter = field_season.get_field_season_version_letter(field="002000+350000", camera_id=411, base_campaign="C7")
        self.assertEqual(letter, 'A')

    def test01_letter2(self):
        field_season = database_connection()
        letter = field_season.get_field_season_version_letter(field="002000+350000", camera_id=411, base_campaign="A7")
        self.assertEqual(letter, 'B')

    def test01_count(self):
        field_season = database_connection()
        new = field_season.field_season_new(field="002000+350000", camera_id=411, campaign="C7")
        self.assertEqual(new, True)

    def test01_count2(self):
        field_season = database_connection()
        new = field_season.field_season_new(field="002000+350000", camera_id=411, campaign="A7")
        self.assertEqual(new, False)

    def test01_get_field_season(self):
        field_season = database_connection()
        entry = field_season.get_field_season(field="002000+350000", camera_id=411, campaign="A7")
        self.assertEqual(entry, entry_before)

    def test01_get_max_field_season(self):
        field_season = database_connection()
        entry = field_season.get_current_max_field_season(field="234000+350000", camera_id=411, base_campaign="C7")
        self.assertEqual(entry, "C7B")

    def test02_update_field_season(self):
        field_season = database_connection()
        stats = field_season.get_image_stats_for_field(field="002000+350000", camera_id=411, campaign="A7")
        result = field_season.update_field_season(field="002000+350000", camera_id=411, campaign="A7",
                                                  first_night=stats.first_night, last_night=stats.last_night,
                                                  nimage=stats.nimage, nnight=stats.nnight, complete='no',
                                                  psf_fwhm_med=stats.psf_fwhm_med, psf_fwhm_iqr=stats.psf_fwhm_iqr)
        entry = field_season.get_field_season(field="002000+350000", camera_id=411, campaign="A7")
        entry_expected = ('002000+350000', 411, 'A7', datetime.date(2012, 9, 21), datetime.date(2014, 7, 12),
                          6366L, 197L, 'no', 2.44, 0.15, 'real')
        self.assertEqual((entry, result), (entry_expected, True))

    def test02_add_pseudo_field_season(self):
        field_season = database_connection()
        field = "002000+350000"
        camera_id = 411
        campaign = "A7"
        base_campaign = campaign

        stats = field_season.get_image_stats_for_field(field=field, camera_id=camera_id, campaign=campaign)
        letter = field_season.get_field_season_version_letter(field=field, camera_id=camera_id, base_campaign=campaign)

        campaign += letter
        result = field_season.add_field_season_pseudo(field=field, camera_id=camera_id, campaign=campaign,
                                                      base_campaign=base_campaign, first_night=stats.first_night,
                                                      last_night=stats.last_night, nimage=stats.nimage,
                                                      nnight=stats.nnight, complete='no',
                                                      psf_fwhm_med=stats.psf_fwhm_med, psf_fwhm_iqr=stats.psf_fwhm_iqr)
        entry = field_season.get_field_season_pseudo(field=field, camera_id=camera_id, campaign=campaign)
        entry_expected = ('002000+350000', 411, 'A7B', 'A7', datetime.date(2012, 9, 21), datetime.date(2014, 7, 12),
                          6366L, 197L, 'no', 2.44, 0.15)
        self.assertEqual((entry, result), (entry_expected, True))

    def test02_add_field_season(self):
        field_season = database_connection()
        field = "002000+350000"
        camera_id = 411
        campaign = "C7"

        stats = field_season.get_image_stats_for_field(field=field, camera_id=camera_id, campaign=campaign)
        result = field_season.add_field_season(field=field, camera_id=camera_id, campaign=campaign,
                                               first_night=stats.first_night, last_night=stats.last_night,
                                               nimage=stats.nimage, nnight=stats.nnight, complete='no',
                                               psf_fwhm_med=stats.psf_fwhm_med, psf_fwhm_iqr=stats.psf_fwhm_iqr,
                                               season_type='real')
        entry = field_season.get_field_season(field=field, camera_id=camera_id, campaign=campaign)
        entry_expected = ('002000+350000', 411, 'C7', datetime.date(2012, 9, 21), datetime.date(2014, 7, 12), 5968L,
                          197L, 'no', 2.43, 0.15, 'real')
        self.assertEqual((entry, result), (entry_expected, True))

    def test09_reset_db(self):
        field_season = database_connection()
        result = field_season.update_field_season(*entry_before)
        result2 = field_season.delete_field_season_pseudo(field="002000+350000", camera_id=411, campaign="A7B")
        result3 = field_season.delete_field_season(field="002000+350000", camera_id=411, campaign="C7")
        self.assertEqual((result, result2, result3), (True, True, True))

    def test01_diff_image_new(self):
        diff_images = database_connection2()
        diff_image_list = diff_images.get_new_diff_images(field="002000+350000", camera_id=411, campaign="C7")
        self.assertEqual(((411201209210224320L, 'KCAM_US_T01D01.20120921.8079_lc.fits', None), 5968),
                         (diff_image_list[0], len(diff_image_list)))

if __name__ == '__main__':
    unittest.main()
