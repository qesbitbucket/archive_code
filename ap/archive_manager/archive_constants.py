"""
This module contains constants for the archive management code

Photometry_types - different types of photometry in the archive (defines named tuple)
Photometry_info - defines named tuple for photometry information
DB_settings - defines named tuple for DB settings

dia, aperture - holds information on the dia and aperture photometry respectively
photometry - hold the two photometry tuples in another tuple

archive_job_logs - Location of the archive log files (standard out and standard error from jobs)
transit_searches - Location where the orion output fits files are stored
archive_work - default main working directory for the archive code
archive_code - default directory of the archive code
archive_manager - location of the archive manager code
orion_code - location of the orion code
mcmc_code - location of the mcmc code

db_settings - settings to connect to the mysql archive database
"""
__author__ = 'Neil Parley'
import collections
import os

Photometry_types = collections.namedtuple('photometry_types', 'dia aperture')
Photometry_info = collections.namedtuple('photometry_info', 'name campaign_letter directory')
DB_settings = collections.namedtuple('db_settings', 'host user password database')

dia = Photometry_info(name="dia", campaign_letter='C', directory="lcfits")
aperture = Photometry_info(name="aperture", campaign_letter='A', directory="phot")
photometry = Photometry_types(dia=dia, aperture=aperture)

archive_job_logs = os.path.join("qes_data", "archive_logs")
transit_searches = os.path.join("qes_data", "transit_searches", "orion")
archive_work = os.path.join("archive", "work")
archive_code = os.path.join("archive_code", "ap")
archive_manager = os.path.join(archive_code, "archive_manager")
orion_code = os.path.join("archive_code", "orion")
mcmc_code = os.path.join("archive_code", "orion", "mcmc")

db_settings = DB_settings(host='raad-bak2', user='archive', password='bECr4$ru', database='ap')