#!/usr/bin/env python
"""
This module runs the mcmc code after an orion run to populate the mcmc columns in the fits files which are then read
into the database by archive_update. The normal way to run this file is:

    ./archive_mcmc.py KCAM_US_T01D01 002000+450000 C7 dia

which will run mcmc for camera KCAM_US_T01D01, field 002000+450000, campaign C7 and the dia photometry type. By default
the script will search for a file that matches the pattern {field}_{camera_id}_{campaign}_sysrem.fits. However, if
--fname value is given it will look for the file {field}_{camera_id}_{campaign}_value.fits instead.

--tfa needs: to be set if the orion run also had the tfa flag set

--tag needs: to be set if the orion run also had a non default tag set

--mcmc_code: can be used to override the location of the mcmc code. Default location can be changed in
             archive_constants.mcmc_code

--job_id is used for when the code is being run by the archive_jobs.py code and should not be used if the code is
being used from the command line

--work_dir is the root directory where the working archive directories are. The code will look in a sub directory
of for example 002000+450000_KCAM_US_T01D01_C7_dia for the files.

"""
from __future__ import print_function
import os
import sys
import archive_constants
import archive_util
import archive_search
import archive_jobs
try:
    import qesutil
except ImportError:
    import qes_pipeline.qesutil as qesutil

__author__ = 'Neil Parley'


@archive_jobs.register_job
@qesutil.check_nearly_all_param_for_none(can_be_none=["tag", "mcmc", "job_id"])
def mcmc(camera=None, field=None, campaign=None, work_dir=None, mcmc_dir=None, camera_id=None,
         photometry_info=None, tfa=False, tag=None, fname="orion", job_id=None):
    """
    Routine for setting up the parameters needed to call the mcmc code.

    The global campaign is calculated (C7 not C7A etc) and the working route is computed. The tag is then set and the
    code searches for an orion file that matches. Finally archive_search.update_mcmc is ran to run the mcmc code.

    :param camera: Camera string e.g. KCAM_US_T01D01
    :param field: Field string e.g. 002000+450000
    :param campaign: Campaign string e.g. C7
    :param work_dir: Base working directory should be set to os.path.join(qes_path, archive_constants.archive_work) if
                     using the default
    :param mcmc_dir: Directory where the mcmc code is located
    :param camera_id: The database camera id, can be loaded from the configuration files
    :param photometry_info: The photometry type of the run, types defined in archive_constants
    :param tfa: True to use TFA detrending else false (needs to be the same as the orion run)
    :param tag: tag to use in the archive for the run, default is TAMUZ / TAMTFA set to DOHA for a doha run (needs to
                be set the same as the orion run)
    :param fname: defaults to sysrem to look for a sysrem file but can be overridden if a different detrending has been
                  used. (Tells the code the underscore of the preceding file. see above)
    :param job_id: This is given to the function when it is started as a job by archive_jobs. It is used by the
                   decorator archive_jobs.register_job to update the database with when the job starts and finishes.
    """

    campaign_global = archive_util.get_global_campaign(campaign)

    work_root = archive_util.create_work_root(field=field, campaign=campaign_global, camera=camera,
                                              photometry_info=photometry_info, work_dir=work_dir)

    campaign_global_phot = photometry_info.campaign_letter + campaign_global[1:]

    tag = archive_search.get_tag(tag=tag, tfa=tfa)

    fits_file, campaign_phot = archive_search.find_file(work_root=work_root, field=field, camera_id=camera_id,
                                                        campaign=campaign_global_phot, tag=tag, fname=fname)

    archive_search.update_mcmc(input_file=fits_file, work_root=work_root, mcmc_dir=mcmc_dir, sub_dir=tag)


if __name__ == '__main__':

    try:
        import configparser
    except ImportError:
        import ConfigParser as configparser

    import argparse

    try:
        _qes_path = os.environ['QES_PATH']
    except NameError as n_error:
        print("Environmental variable not set: %s" % n_error)
        sys.exit(2)

    _work_dir_default = os.path.join(_qes_path, archive_constants.archive_work)
    _archive_code_default = os.path.join(_qes_path, archive_constants.archive_code)
    _mcmc_code_defaualt = os.path.join(_qes_path, archive_constants.mcmc_code)

    parser = argparse.ArgumentParser(description='Runs mcmc on the orion file')
    parser.add_argument('camera', metavar='CAMERA', help='Camera to ingest (e.g. KCAM_US_T01D01)')
    parser.add_argument('field', metavar='FIELD', help='Field to ingest (e.g. 002000+350000)')
    parser.add_argument('campaign', metavar='CAMPAIGN', help='Campaign to ingest (e.g. C7)')
    parser.add_argument('photometry', metavar='TYPE', choices=['dia', 'aperture'],
                        help='Photometry type (dia or aperture)')
    parser.add_argument("--tfa", help="Run on a tfa file (default is false)", action="store_true")
    parser.add_argument("--work_dir", default=_work_dir_default,
                        help="Work directory of ingest Files (default: {dir})".format(dir=_work_dir_default))
    parser.add_argument("--mcmc_code", default=_mcmc_code_defaualt,
                        help="Directory of mcmc code (default: {dir})".format(dir=_mcmc_code_defaualt))
    parser.add_argument("--tag", default=None, help="Override the archive TAMUZ tag")
    parser.add_argument("--job_id", default=None, type=int, help="Job id if running on the cluster")
    parser.add_argument("--fname", default="orion", help="File name from last programme in the pipeline default orion"
                                                         "(will search "
                                                         "field_camera_id_campaign[A-Z]_fname_tag.fits)")
    args = parser.parse_args()

    temporary_camera_name = args.camera
    if temporary_camera_name != 'KCAM_US_T01D08':
        args.camera = 'KCAM_US_T01D08'

    if args.camera == 'KCAM_US_T01D08':
        temporary_field_name = args.field
        new_field_name_ste = temporary_field_name[0:-1]+'2'
        args.field = new_field_name_ste

    print(args)

    _alsubai_control_path = os.path.join(_qes_path, 'qes_pipeline', 'Alsubai_Control')
    _config_cam_campaign = configparser.ConfigParser()

    if not _config_cam_campaign.read(os.path.join(_alsubai_control_path,
                                                  'cfg', args.campaign, 'config.' + args.camera + '.cfg')):
        print('ERROR - The configuration file is missing for the camera: ' +
              os.path.join('cfg', args.campaign, 'config.' + args.camera + '.cfg'))
        sys.exit(1)

    mcmc(camera=args.camera, field=args.field, campaign=args.campaign, work_dir=args.work_dir, mcmc_dir=args.mcmc_code,
         camera_id=_config_cam_campaign.get('main', 'camID'), tfa=args.tfa, job_id=args.job_id,
         photometry_info=getattr(archive_constants.photometry, args.photometry), tag=args.tag, fname=args.fname)
