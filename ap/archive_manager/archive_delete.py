#!/usr/bin/env python
"""
The module is used to delete a data from the archive. It can be used to delete an orion run and to delete a campaign
version. Calling this code like:

./archive_delete 411 002000+450000 C7B --tag TAMUZ

will delete the tamauz the tamuz run data for field 002000+450000, camera 411 and campaign C7B. Similarly running:

./archive_delete 411 002000+450000 C7B --delete_campaign

will delete the C7B campaign for the field 002000+450000 and camera 411, so that the next call to
fetch (with out rerun) for 411 002000+450000 will use C7B instead of C7C.

Both --tag and --delete_campaign can be used in the same call.
"""
from __future__ import print_function
import os
import sys
import errno
try:
    import qesutil
except ImportError:
    import qes_pipeline.qesutil as qesutil
import archive_constants
import archive_mysql
__author__ = 'Neil Parley'


@qesutil.check_nearly_all_param_for_none(can_be_none=["tag"])
def delete_archive_run(camera=None, field=None, campaign=None, tag=None, delete_campaign=False, qes_path=None):
    """
    Function used to help delete data from the archive. If tag is not None then orion runs corresponding to that tag
    are deleted. If delete_campaign equals True then the campaign version is deleted from the fieldseason table.

    If tag is set. Warn user what is going to happen and make sure they are ok with it. If they are delete the fits
    file and the database entries for the orion run.

    If delete_campaign is true then warn the user what is going to happen. If they are ok with it then delete the
    campaign from the two fieldsummary tables. You probably should know what you are doing to do this.

    :param camera: This is the archive camera id - not the pipeline camera id, eg. 411
    :param field: The field e.g. 002000+450000
    :param campaign: The campaign version, e.g. C7B
    :param tag: If not None delete the orion run with the corresponding tag
    :param delete_campaign: True or False, if True will delete the campaign version from fieldseason table
    :param qes_path: The qes base path on the super computer, i.e. the QES_PATH environmental variable.
    """

    if tag is None and not delete_campaign:
        print("Nothing to do. Neither tag or delete_campaign is set")
        return

    if tag:

        tag_path = os.path.join(qes_path, archive_constants.transit_searches, tag.upper())
        file_path = os.path.join(tag_path, "{field}_{camera_id}".format(field=field, camera_id=camera))
        file_name = "orion_{tag}_{field}_{camera_id}_{campaign}.fits".format(tag=tag.upper(), field=field,
                                                                             camera_id=camera, campaign=campaign)

        test = raw_input("This will delete file {f_name} type y to continue? [n]:".format(f_name=file_name))
        if test != "y":
            return

        try:
            os.remove(os.path.join(file_path, file_name))
            print("Removed: {file}".format(file=file_name))
        except OSError as ex:
            if ex.errno == errno.ENOENT:
                print("File already deleted".format(path=file_name))
            else:
                raise

        try:
            os.rmdir(file_path)
            print("Removed {path}".format(path=file_path))
        except OSError as ex:
            if ex.errno == errno.ENOTEMPTY:
                print("{path} is not empty".format(path=file_path))
            else:
                raise

        try:
            os.rmdir(tag_path)
            print("Removed {path}".format(path=tag_path))
        except OSError as ex:
            if ex.errno == errno.ENOTEMPTY:
                print("{path} is not empty".format(path=tag_path))
            else:
                raise

        archive_mysql.delete_from(camera_id=camera, field=field, campaign=campaign, tag=tag, table="hunter_runs")
        archive_mysql.delete_from(camera_id=camera, field=field, campaign=campaign, tag=tag, table="hunterdet")
        archive_mysql.delete_from(camera_id=camera, field=field, campaign=campaign, tag=tag, table="hunterdet_mcmc")
        archive_mysql.delete_from(camera_id=camera, field=field, campaign=campaign, tag=tag, table="hunterdet_peaks")
        archive_mysql.delete_from(camera_id=camera, field=field, campaign=campaign, tag=tag, table="hunterdet_obj")
        archive_mysql.delete_from_meta_fields(camera_id=camera, field=field, campaign=campaign, tag=tag)

    if delete_campaign:

        test = raw_input("This will delete the campaign from the fieldseason table are you sure?"
                         " (only do this is you made it by mistake) [n]:")
        if test != "y":
            return

        archive_mysql.delete_from(camera_id=camera, field=field, campaign=campaign, table="fieldseason_pseudo")
        archive_mysql.delete_from(camera_id=camera, field=field, campaign=campaign, table="fieldseason")

if __name__ == '__main__':

    try:
        _qes_path = os.environ['QES_PATH']
    except NameError as n_error:
        print("Environmental variable not set: %s" % n_error)
        sys.exit(2)

    import argparse

    parser = argparse.ArgumentParser(description='Deletes a camera / field / campaign from the archive')
    parser.add_argument('camera_id', metavar='CAMERA', help='Camera id (e.g. 411)')
    parser.add_argument('field', metavar='FIELD', help='Field (e.g. 002000+350000)')
    parser.add_argument('campaign', metavar='CAMPAIGN', help='Campaign (e.g. C7A)')
    parser.add_argument("--tag", default=None, help="Tag (e.g. TAMUZ)")
    parser.add_argument("--delete_campaign", help="Delete the whole campaign not just the tag", action="store_true")
    args = parser.parse_args()

    delete_archive_run(camera=args.camera_id, field=args.field, campaign=args.campaign, tag=args.tag,
                       delete_campaign=args.delete_campaign, qes_path=_qes_path)
