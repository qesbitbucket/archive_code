.. archive_manager documentation master file, created by
   sphinx-quickstart on Wed Dec 10 10:08:06 2014.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to archive_manager's documentation!
===========================================

Contents:

.. toctree::
   :maxdepth: 2

   modules.rst

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

