#!/usr/bin/env python
"""
This module will scrape the webpage:

http://pipeline.qes.qa/status/index.php?nights=10

to find all combinations of cameras+fields+campaigns that
can be combined. It writes out the list into:

combinations.txt

It works by considering each KCAM_US_T01D01 field, and finding
all KCAMS2-5 within the field. Then it does the same for each
KCAM_US_T01D06 field, finding all KCAMS1-5 within the field.
Finally it finds all KCAM_US_T01D06 and KCAM_US_T01D01 overlaps.
"""

from __future__ import print_function
import requests
from astropy import units as u
from astropy.coordinates import SkyCoord
__author__ = 'Dan Bramich'

# Function for converting RA and Dec string
def radec2float(radec_str):
    ra_hh, ra_mm, ra_ss = float(radec_str[0:2]), float(radec_str[2:4]), float(radec_str[4:6])
    dec_dd, dec_mm, dec_ss = float(radec_str[7:9]), float(radec_str[9:11]), float(radec_str[11:13])
    ra = (360.0/24.0)*(ra_hh + (ra_mm/60.0) + (ra_ss/3600.0))
    dec = float(radec_str[6] + '1')*(dec_dd + (dec_mm/60.0) + (dec_ss/3600.0))
    return ra, dec

# Main code
print('')
print('Querying webpage...')
result = requests.get('http://pipeline.qes.qa/status/index.php?nights=10', auth=('pipeline', 'alsubai_project'))

print('')
print('Extracting data...')
line_list = result.text.split('<td>')
for i,line in enumerate(line_list):
    line = line.replace('\n', '')
    line = line.replace('\t', '')
    line = line.replace('&nbsp;', '')
    line = line.replace('</td>', '')
    line = line.replace(' ', '')
    line_list[i] = line

cam_field_camp = []
for i,line in enumerate(line_list):
    if line_list[i] == 'KCAM_US_T01D01':
        tmp_str = str(line_list[i+1])
        ra, dec = radec2float(tmp_str)
        cam_field_camp.append(('KCAM_US_T01D01', tmp_str, str(line_list[i+2]), ra, dec))
    if line_list[i] == 'KCAM_US_T01D02':
        tmp_str = str(line_list[i+1])
        ra, dec = radec2float(tmp_str)
        cam_field_camp.append(('KCAM_US_T01D02', tmp_str, str(line_list[i+2]), ra, dec))
    if line_list[i] == 'KCAM_US_T01D03':
        tmp_str = str(line_list[i+1])
        ra, dec = radec2float(tmp_str)
        cam_field_camp.append(('KCAM_US_T01D03', tmp_str, str(line_list[i+2]), ra, dec))
    if line_list[i] == 'KCAM_US_T01D04':
        tmp_str = str(line_list[i+1])
        ra, dec = radec2float(tmp_str)
        cam_field_camp.append(('KCAM_US_T01D04', tmp_str, str(line_list[i+2]), ra, dec))
    if line_list[i] == 'KCAM_US_T01D05':
        tmp_str = str(line_list[i+1])
        ra, dec = radec2float(tmp_str)
        cam_field_camp.append(('KCAM_US_T01D05', tmp_str, str(line_list[i+2]), ra, dec))
    if line_list[i] == 'KCAM_US_T01D06':
        tmp_str = str(line_list[i+1])
        ra, dec = radec2float(tmp_str)
        cam_field_camp.append(('KCAM_US_T01D06', tmp_str, str(line_list[i+2]), ra, dec))

print('')
print('Making associations for cameras...')
outfile = open('combinations.txt', mode='w')
for i,item in enumerate(cam_field_camp):

    new_assoc = []
    print('')
    print('Current: ', item)

    if item[0] == 'KCAM_US_T01D01':
        skyc1 = SkyCoord(ra = item[3]*u.degree, dec = item[4]*u.degree)
        comp_list = cam_field_camp[i:]

        for comp_item in comp_list:
            if comp_item[0] == 'KCAM_US_T01D06': continue
            skyc2 = SkyCoord(ra = comp_item[3]*u.degree, dec = comp_item[4]*u.degree)
            dist = skyc1.separation(skyc2)
            if dist.degree < 8.0:
                new_assoc.append(comp_item)

        print('Writing out the command...')
        cmd_str = './archive_multi_jobs.py --tfa'
        for assoc_item in new_assoc:
            cmd_str = cmd_str + ' --field ' + assoc_item[0] + ' ' + assoc_item[1] + ' ' + assoc_item[2] + ' dia'
        cmd_str = cmd_str + '\n'
        print(cmd_str)
        outfile.write(cmd_str)

    if item[0] == 'KCAM_US_T01D06':
        skyc1 = SkyCoord(ra = item[3]*u.degree, dec = item[4]*u.degree)
        comp_list = cam_field_camp[i:] + cam_field_camp[0:(i-1)]

        for comp_item in comp_list:
#            if comp_item[0] == 'KCAM_US_T01D02': continue
#            if comp_item[0] == 'KCAM_US_T01D03': continue
#            if comp_item[0] == 'KCAM_US_T01D04': continue
#            if comp_item[0] == 'KCAM_US_T01D05': continue
            skyc2 = SkyCoord(ra = comp_item[3]*u.degree, dec = comp_item[4]*u.degree)
            dist = skyc1.separation(skyc2)
            if dist.degree < 8.0:
                new_assoc.append(comp_item)

        print('Writing out the command...')
        cmd_str = './archive_multi_jobs.py --tfa'
        for assoc_item in new_assoc:
            cmd_str = cmd_str + ' --field ' + assoc_item[0] + ' ' + assoc_item[1] + ' ' + assoc_item[2] + ' dia'
        cmd_str = cmd_str + '\n'
        print(cmd_str)
        outfile.write(cmd_str)

    if item[0] == 'KCAM_US_T01D06':
        skyc1 = SkyCoord(ra = item[3]*u.degree, dec = item[4]*u.degree)
        comp_list = cam_field_camp[i:] + cam_field_camp[0:(i-1)]

        for comp_item in comp_list:
            if comp_item[0] == 'KCAM_US_T01D02': continue
            if comp_item[0] == 'KCAM_US_T01D03': continue
            if comp_item[0] == 'KCAM_US_T01D04': continue
            if comp_item[0] == 'KCAM_US_T01D05': continue
            skyc2 = SkyCoord(ra = comp_item[3]*u.degree, dec = comp_item[4]*u.degree)
            dist = skyc1.separation(skyc2)
            if dist.degree < 8.0:
                new_assoc.append(comp_item)

        print('Writing out the command...')
        cmd_str = './archive_multi_jobs.py --tfa --multionly'
        for assoc_item in new_assoc:
            cmd_str = cmd_str + ' --field ' + assoc_item[0] + ' ' + assoc_item[1] + ' ' + assoc_item[2] + ' dia'
        cmd_str = cmd_str + '\n'
        print(cmd_str)
        outfile.write(cmd_str)

#    if (item[0] == 'KCAM_US_T01D02') or (item[0] == 'KCAM_US_T01D03') or (item[0] == 'KCAM_US_T01D04') or (item[0] == 'KCAM_US_T01D05'):
#        skyc1 = SkyCoord(ra = item[3]*u.degree, dec = item[4]*u.degree)
#        comp_list = cam_field_camp[i:] + cam_field_camp[0:(i-1)]
#
#        for comp_item in comp_list:
#            if comp_item[0] == 'KCAM_US_T01D01': continue
#            if comp_item[0] == 'KCAM_US_T01D06': continue
#            skyc2 = SkyCoord(ra = comp_item[3]*u.degree, dec = comp_item[4]*u.degree)
#            dist = skyc1.separation(skyc2)
#            if dist.degree < 3.6:
#                new_assoc.append(comp_item)
#
#        print('Writing out the command...')
#        cmd_str = './archive_multi_jobs.py --tfa'
#        for assoc_item in new_assoc:
#            cmd_str = cmd_str + ' --field ' + assoc_item[0] + ' ' + assoc_item[1] + ' ' + assoc_item[2] + ' dia'
#        cmd_str = cmd_str + '\n'
#        print(cmd_str)
#        outfile.write(cmd_str)



outfile.close()
