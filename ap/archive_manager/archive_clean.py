#!/usr/bin/env python
"""
The module deletes the working directory used for all the temporary archive files. It is run once the new archive data
has been uploaded to the website / database.

--work_dir is the root directory where the working archive directories are. The code will look in a sub directory
of for example 002000+450000_KCAM_US_T01D01_C7_dia and will remove this directory.

--job_id is used for when the code is being run by the archive_jobs.py code and should not be used if the code is
being used from the command line
"""
from __future__ import print_function
import os
import sys
import shutil
try:
    import qesutil
except ImportError:
    import qes_pipeline.qesutil as qesutil
import archive_constants
import archive_util
import archive_jobs
__author__ = 'Neil Parley'


@archive_jobs.register_job
@qesutil.check_nearly_all_param_for_none(can_be_none=["job_id"])
def clean(camera=None, field=None, campaign=None, work_dir=None, photometry_info=None, job_id=None):
    """
    Function that actual does the cleaning

    :param camera: camera string e.g. KCAM_US_T01D01
    :param field: Field string e.g. 002000+450000
    :param campaign: Campaign string e.g. C7
    :param work_dir: Base working directory should be set to os.path.join(qes_path, archive_constants.archive_work) if
                     using the default
    :param photometry_info: The photometry type of the run, types defined in archive_constants
    :param job_id: This is given to the function when it is started as a job by archive_jobs. It is used by the
                   decorator archive_jobs.register_job to update the database with when the job starts and finishes.
    """

    campaign_global = archive_util.get_global_campaign(campaign)

    work_root = archive_util.create_work_root(field=field, campaign=campaign_global, camera=camera,
                                              photometry_info=photometry_info, work_dir=work_dir)

    print("Removing {root}".format(root=work_root))

    try:
        shutil.rmtree(work_root)
    except OSError:
        pass

if __name__ == '__main__':

    try:
        import configparser
    except ImportError:
        import ConfigParser as configparser

    import argparse

    try:
        _qes_path = os.environ['QES_PATH']
    except NameError as n_error:
        print("Environmental variable not set: %s" % n_error)
        sys.exit(2)

    _work_dir_default = os.path.join(_qes_path, archive_constants.archive_work)
    _orion_code_default = os.path.join(_qes_path, archive_constants.orion_code)

    parser = argparse.ArgumentParser(description='Removes the work directory')
    parser.add_argument('camera', metavar='CAMERA', help='Camera to ingest (e.g. KCAM_US_T01D01)')
    parser.add_argument('field', metavar='FIELD', help='Field to ingest (e.g. 002000+350000)')
    parser.add_argument('campaign', metavar='CAMPAIGN', help='Campaign to ingest (e.g. C7)')
    parser.add_argument('photometry', metavar='TYPE', choices=['dia', 'aperture'],
                        help='Photometry type (dia or aperture)')
    parser.add_argument("--work_dir", default=_work_dir_default,
                        help="Work directory of ingest Files (default: {dir})".format(dir=_work_dir_default))
    parser.add_argument("--job_id", default=None, type=int, help="Job id if running on the cluster")
    args = parser.parse_args()

    _alsubai_control_path = os.path.join(_qes_path, 'qes_pipeline', 'Alsubai_Control')
    _config_cam_campaign = configparser.ConfigParser()

    if not _config_cam_campaign.read(os.path.join(_alsubai_control_path,
                                                  'cfg', args.campaign, 'config.' + args.camera + '.cfg')):
        print('ERROR - The configuration file is missing for the camera: ' +
              os.path.join('cfg', args.campaign, 'config.' + args.camera + '.cfg'))
        sys.exit(1)

    clean(camera=args.camera, field=args.field, campaign=args.campaign, work_dir=args.work_dir,
          photometry_info=getattr(archive_constants.photometry, args.photometry), job_id=args.job_id)
