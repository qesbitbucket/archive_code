#!/usr/bin/env python
"""
This module updates the archive web page with the result of the orion / mcmc run. I.e. the fits file is created that
the web page usese to make the graphs and the fits file is read into MySQL tables that the web page uses to display
results. The module also runs the clumb index code which is used by the web page to determine if there are systematic
periods

This program can be run from the command line to update the web page for a field run. For example to update a field's
dia run you could type:

    ./archive_update.py KCAM_US_T01D01 002000+450000 C7 dia

If you are updating a file that has a none default tag and used tfa you could use:

    ./archive_update.py KCAM_US_T01D01 002000+450000 C7 dia --tag test --tfa

The work_dir switch is the location where the code will look for a file so your might have the file in a non default
place so you might run the command like:

    ./archive_update.py KCAM_US_T01D01 002000+450000 C7 dia --work_dir=$HOME/my_reduction

--orion_code can be used to overload the location of the orion code. However if the location of the orion code changes
permanently it is probably better to change the location of the variable in archive_constants.py

--job_id is used for when the code is being run by the archive_jobs.py code and should not be used if the code is
being used from the command line

--fname can be used if the script before was not orion i.e. a fname of newbls would change the search file from
002000+450000_411_C7A_orion_tamuz.fits to 002000+450000_411_C7A_newbls_tamuz.fits
"""
from __future__ import print_function
import os
import sys
try:
    import qesutil
except ImportError:
    import qes_pipeline.qesutil as qesutil
import archive_constants
import archive_util
import archive_search
import archive_jobs
__author__ = 'Neil Parley'


@archive_jobs.register_job
@qesutil.check_nearly_all_param_for_none(can_be_none=["tag", "fname", "job_id"])
def update(camera=None, field=None, campaign=None, work_dir=None, orion_dir=None, camera_id=None, photometry_info=None,
           tag=None, tfa=None, fname="orion", job_id=None):
    """
    Main function used for updating the database after an orion run with the results. All the parameters other than
    tag, fname and job_id need to be given.

    The function gets the global campaign and does not assume that is given correctly. It then finds the working
    directory root where the files are. The global campaign for the photometry type this then created. The code will
    then search for a matching file and from that file work out the pysdo campaign. I.e. C7B etc. The code will then
    run Richards clumb index code and then Richards update code via the python calling functions.

    :type camera: str
    :param camera: Camera string e.g. KCAM_US_T01D01
    :param field: Field string e.g. 002000+450000
    :param campaign: Campaign string e.g. C7
    :param work_dir: Base working directory should be set to os.path.join(qes_path, archive_constants.archive_work) if
                     using the default
    :param orion_dir: Directory of the orion code. Should be set to
                      os.path.join(qes_path, archive_constants.orion_code) if using the default
    :param camera_id: The database camera id, can be loaded from the configuration files
    :param photometry_info: The photometry type of the run, types defined in archive_constants
    :param tag: Used to override the tag used for the reduction on the web site, e.g. default sysrem tag is TAMUZ. If
                None the default tag will be used.
    :param tfa: True if tfa has been run else false
    :param fname: Tells the code the underscore of the preceding file. For example by default update will be run after
                  mcmc. MCMC does not create a new file it just updates the orion file so this code will be searching
                  for a file with {field}_{camid}_{campaign}_orion.fits if fname is give then this overrides the orion
                  string.
    :param job_id: This is given to the function when it is started as a job by archive_jobs. It is used by the
                   decorator archive_jobs.register_job to update the database with when the job starts and finishes.
    """
    campaign_global = archive_util.get_global_campaign(campaign)

    work_root = archive_util.create_work_root(field=field, campaign=campaign_global, camera=camera,
                                              photometry_info=photometry_info, work_dir=work_dir)

    campaign_global_phot = photometry_info.campaign_letter + campaign_global[1:]

    tag = archive_search.get_tag(tag=tag, tfa=tfa)

    fits_file, campaign_phot = archive_search.find_file(work_root=work_root, field=field, camera_id=camera_id,
                                                        campaign=campaign_global_phot, tag=tag, fname=fname)

    archive_search.run_clump(input_file=fits_file, work_root=work_root, orion_dir=orion_dir)
    archive_search.run_load(input_file=fits_file, work_root=work_root, orion_dir=orion_dir)
    print("Finished archive ingest")


if __name__ == '__main__':

    try:
        import configparser
    except ImportError:
        import ConfigParser as configparser

    import argparse

    try:
        _qes_path = os.environ['QES_PATH']
    except NameError as n_error:
        print("Environmental variable not set: %s" % n_error)
        sys.exit(2)


    _work_dir_default = os.path.join(_qes_path, archive_constants.archive_work)
    _orion_code_default = os.path.join(_qes_path, archive_constants.orion_code)

    parser = argparse.ArgumentParser(description='Runs clumb index and archive load commands')
    parser.add_argument('camera', metavar='CAMERA', help='Camera to ingest (e.g. KCAM_US_T01D01)')
    parser.add_argument('field', metavar='FIELD', help='Field to ingest (e.g. 002000+350000)')
    parser.add_argument('campaign', metavar='CAMPAIGN', help='Campaign to ingest (e.g. C7)')
    parser.add_argument('photometry', metavar='TYPE', choices=['dia', 'aperture'],
                        help='Photometry type (dia or aperture)')
    parser.add_argument("--tfa", help="Run on a tfa file (default is false)", action="store_true")
    parser.add_argument("--work_dir", default=_work_dir_default,
                        help="Work directory of ingest Files (default: {dir})".format(dir=_work_dir_default))
    parser.add_argument("--orion_code", default=_orion_code_default,
                        help="Directory of orion code (default: {dir})".format(dir=_orion_code_default))
    parser.add_argument("--tag", default=None, help="Override the archive TAMUZ tag")
    parser.add_argument("--job_id", default=None, type=int, help="Job id if running on the cluster")
    parser.add_argument("--fname", default="orion", help="File name from last programme in the pipeline default orion"
                                                         "(will search "
                                                         "field_camera_id_campaign[A-Z]_fname_tag.fits)")
    args = parser.parse_args()


    temporary_camera_name = args.camera
    if temporary_camera_name != 'KCAM_US_T01D08':
        args.camera = 'KCAM_US_T01D08'

    if args.camera == 'KCAM_US_T01D08':
        temporary_field_name = args.field
        new_field_name_ste = temporary_field_name[0:-1]+'2'
        args.field = new_field_name_ste

    print(args)

    _alsubai_control_path = os.path.join(_qes_path, 'qes_pipeline', 'Alsubai_Control')
    _config_cam_campaign = configparser.ConfigParser()

    if not _config_cam_campaign.read(os.path.join(_alsubai_control_path,
                                                  'cfg', args.campaign, 'config.' + args.camera + '.cfg')):
        print('ERROR - The configuration file is missing for the camera: ' +
              os.path.join('cfg', args.campaign, 'config.' + args.camera + '.cfg'))
        sys.exit(1)

    update(camera=args.camera, field=args.field, campaign=args.campaign, work_dir=args.work_dir, job_id=args.job_id,
           orion_dir=args.orion_code, camera_id=_config_cam_campaign.get('main', 'camID'), tfa=args.tfa,
           photometry_info=getattr(archive_constants.photometry, args.photometry), tag=args.tag, fname=args.fname)
