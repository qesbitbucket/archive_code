"""
Module to do general utility functions for the archive software
"""
__author__ = 'Neil Parley'
import os
try:
    import qesutil
except ImportError:
    import qes_pipeline.qesutil as qesutil


@qesutil.check_all_param_for_none
def create_work_root(field=None, camera=None, campaign=None, photometry_info=None, work_dir=None,
                     create=True):
    """
    Creates the working directory when given the root directory and the field camera etc. The working directory that is
    created is {work_dir}/{field}_{camera}_{campaign}_{phot_name}
    e.g. /work_dir/021037+424400_KCAM_US_TO1D02_C7_dia
    If the directory already exists the function will not create an error

    :param field: Field name (e.g. 021037+424400)
    :type field: str
    :param camera: Camera name (e.g KCAM_US_TO1D02)
    :type camera: str
    :param campaign: Campaign string (e.g C7)
    :type campaign: str
    :param photometry_info: Photometry object (e.g. archive_constants.dia)
    :type photometry_info: archive_constants.Photometry_info
    :param work_dir: Root working directory (e.g $QES_PATH/archive/work)
    :type work_dir: str
    :return: str of the working directory that has been created
    """
    field_string = "{field}_{camera}_{campaign}_{phot_name}"\
        .format(field=field, camera=camera, campaign=campaign, phot_name=photometry_info.name)
    work_root = os.path.join(work_dir, field_string)
    if create:
        try:
            os.makedirs(work_root)
        except OSError:
            pass
    return work_root


def get_global_campaign(campaign):
    """
    Returns the global campaign. That is the C campaign with no letters. The code should work even if the user gives
    the aperture campaign i.e. A10 or the pysdo campaign i.e. C7A

    :param campaign: Campaign in theory should be C7A but designed to work with incorrect import
    :return: Returns the global campaign
    :type campaign: str
    :rtype: str


    >>> get_global_campaign("C7")
    'C7'
    >>> get_global_campaign("C7A")
    'C7'
    >>> get_global_campaign("C10")
    'C10'
    >>> get_global_campaign("C10B")
    'C10'
    >>> get_global_campaign("A10")
    'C10'
    >>> get_global_campaign("C")
    Traceback (most recent call last):
    ...
    ValueError: Incorrect Campaign
    """

    if campaign == 'CX':
        return campaign

    campaign_number = ''.join([letter for letter in campaign if letter.isdigit()])
    if campaign_number:
        return 'C{0}'.format(campaign_number)
    else:
        raise ValueError("Incorrect Campaign")

