#!/usr/bin/python
import os
import sys
import glob
import subprocess
import MySQLdb
import contextlib
from functools import wraps

__author__ = 'Neil Parley'


@contextlib.contextmanager
def open_db_connection(host='raad-bak2', user='archive', passwd='bECr4$ru', db='ap'):
    db_con = MySQLdb.connect(host=host, user=user, passwd=passwd, db=db)
    try:
        yield db_con
    finally:
        db_con.commit()
        db_con.close()


def check_all_param_for_none(f):
    """
    Decorator to test to see if all the parameters have been supplied when all the parameters are required not to be
    None. Assumes all parameters have keywords. In Python 3 this would not be needed.
    :param f: Function
    :return: Function or raise ValueError if None is in one of the arguments
    """
    @wraps(f)
    def check_args(*args, **kwargs):
        values = list(f.__defaults__)
        keys = list(f.__code__.co_varnames)[0:len(values)]
        function_name = f.__name__

        for i, arg in enumerate(args[1:]):
            values[i] = arg

        arg_dict = dict(zip(keys, values))
        arg_dict.update(kwargs)
        for key, value in arg_dict.iteritems():
            if value is None:
                raise ValueError("{key} value must be supplied to {func_name}".format(key=key, func_name=function_name))
        return f(*args, **kwargs)
    return check_args


@check_all_param_for_none
def ap_ingest_one(field=None, campaign=None, data_dir=None, work_dir=None, code_dir=None, camera=None, cam_id=None):

    field_string = "{field}_{camera}_{campaign}".format(field=field, camera=camera, campaign=campaign)
    work_root = os.path.join(work_dir, field_string)
    try:
        os.makedirs(work_root)
    except OSError:
        pass

    data_root = os.path.join(data_dir, camera, field, campaign)

    print("Searching in {dir}".format(dir=data_root))
    search_night_string = data_root+"/20*"
    nights = glob.glob(search_night_string)
    print("Found {num_nights} nights for field".format(num_nights=len(nights)))

    total = [0, 0]
    phot_names = ["DIA", "AP"]
    paths = ["lcfits", "phot"]
    filter_name = "K2"

    for night in nights:
        print("---- Doing night {night} ----".format(night=os.path.basename(night)))
        for i, path in enumerate(paths):
            if phot_names[i] == "AP":
                phot_cam = campaign.replace("C", "A")
            else:
                phot_cam = campaign

            local_path = os.path.join(data_root, night, path, filter_name)
            search_string = local_path+"/*_lc.fits"
            lc_files = glob.glob(search_string)
            total[i] += len(lc_files)
            print("{files} files in {name} directory for {night}".format(files=len(lc_files), name=phot_names[i],
                                                                         night=night))
            if len(lc_files) > 0:
                ap_ingest(camera_id=cam_id, field=field, campaign=phot_cam, night=os.path.basename(night),
                          work_root=work_root, sub_dir=local_path, code_dir=code_dir)

    print("---- Finished ----")
    for i, name in enumerate(phot_names):
        print("Total {name} data points expected ~{total}".format(name=name, total=total[i]))


@check_all_param_for_none
def ap_ingest(camera_id=None, field=None, campaign=None, night=None, work_root=None, sub_dir=None, code_dir=None):
    print("Running scripts for {dir}".format(dir=sub_dir))

    ap_commands = ("apbackout", "apprepingest", "apfileimport")

    ap_args = {"apbackout": ["--field", field, "--camera", camera_id, "--campaign", campaign, "--night", night],
               "apprepingest": ["--indir={sub_dir}".format(sub_dir=sub_dir),
                                "--outdir={work_dir}".format(work_dir=work_root)],
               "apfileimport": [work_root]}

    for command in ap_commands:
        print("Running {command}".format(command=command))
        command_array = [os.path.join(code_dir, command)] + ap_args[command]
        child = subprocess.Popen(command_array, cwd=work_root)
        return_code = child.wait()
        if return_code:
            raise subprocess.CalledProcessError(return_code, cmd=command_array)

    mysql_calls = ((ap_field_load, "Fieldnames", "fieldname.dump"),
                   (ap_image_load, "Imagelist", "imagelist.dump"),
                   (ap_title_load, "Titlefiles", "tilefiles.dump"))

    for mysql_call in mysql_calls:
        mysql_func, table_name, mysql_file = mysql_call
        mysql_file = os.path.join(work_root, mysql_file)
        if os.path.exists(mysql_file):
            print("Updated {table_name} table table - effected {rows}".
                  format(table_name=table_name, rows=mysql_func(mysql_file)))
            os.remove(mysql_file)


def ap_field_load(dump_file):
    sql = "LOAD DATA CONCURRENT LOCAL INFILE '{file}' IGNORE INTO TABLE fieldnames FIELDS TERMINATED BY ',' " \
          "OPTIONALLY ENCLOSED BY '\"' (obj_id, field, camera_id, campaign)".format(file=dump_file)
    return execute_sql(sql)


def ap_image_load(dump_file):
    sql = "LOAD DATA LOCAL INFILE '{file}' REPLACE INTO TABLE imagelist FIELDS TERMINATED BY ',' " \
          "OPTIONALLY ENCLOSED BY '\"' (image_id, tstart, tmid, night, field, camera_id, mount_id, fileleaf, " \
          "exptime, ambtemp, rdnoise, filter, gain, campaign, crval1, crpix1, crval2, crpix2, cd1_1, cd1_2, " \
          "cd2_1, cd2_2, pv2_1, pv2_3, zpfit, zmag, fwhm1, fwhm2, astrms, centaz, centalt, jdmid, refstars, " \
          "sfits, a, b, c, d, shift_x, shift_y, det, c0, c1, c2, c3, c4, c5, c6, c7, c8, c9, c10, c11, " \
          "moffset, jkoffset, xoffset, yoffset, catrms, computer, pipevers, blurfwhm, refname1, refname2, " \
          "refname3, refname4)".format(file=dump_file)
    return execute_sql(sql)


def ap_title_load(dump_file):
    sql = "LOAD DATA LOCAL INFILE '{file}' REPLACE INTO TABLE tilefiles FIELDS TERMINATED BY ',' " \
          "OPTIONALLY ENCLOSED BY '\"' (field, camera_id, campaign, night, filename, obs_start, " \
          "obs_stop, filesize) SET ingest_date=NOW()".format(file=dump_file)
    return execute_sql(sql)


def execute_sql(sql):
    with open_db_connection() as db:
        cursor = db.cursor()
        try:
            rows = cursor.execute(sql)
            db.commit()
            return rows
        except MySQLdb.Error:
            db.rollback()
            raise


if __name__ == '__main__':

    try:
        import configparser
    except ImportError:
        import ConfigParser as configparser

    import argparse

    try:
        _qes_path = os.environ['QES_PATH']
    except NameError as n_error:
        print("Environmental variable not set: %s" % n_error)
        sys.exit(2)

    _data_dir_default = os.path.join(_qes_path, "qes_data")
    _work_dir_default = os.path.join(_qes_path, "archive", "work")
    _archive_code_default = os.path.join(_qes_path, "archive_code", "ap")

    parser = argparse.ArgumentParser(description='Runs the apingest for one camera field campaign')
    parser.add_argument('camera', metavar='CAMERA', help='Camera to ingest (e.g. KCAM_US_T01D01)')
    parser.add_argument('field', metavar='FIELD', help='Field to ingest (e.g. 002000+350000)')
    parser.add_argument('campaign', metavar='CAMPAIGN', type=str, help='Campaign to ingest (e.g. C7)')
    parser.add_argument("-d", "--data_dir", default=_data_dir_default,
                        help="Root directory of qes Files (default: {dir})".format(dir=_data_dir_default))
    parser.add_argument("-w", "--work_dir", default=_work_dir_default,
                        help="Work directory of ingest Files (default: {dir})".format(dir=_work_dir_default))
    parser.add_argument("-a", "--archive_code", default=_archive_code_default,
                        help="Directory of archive code (default: {dir})".format(dir=_archive_code_default))
    args = parser.parse_args()

    _alsubai_control_path = os.path.join(_qes_path, 'qes_pipeline', 'Alsubai_Control')
    _config_cam_campaign = configparser.ConfigParser()

    if not _config_cam_campaign.read(os.path.join(_alsubai_control_path,
                                                  'cfg', args.campaign, 'config.' + args.camera + '.cfg')):
        print('ERROR - The configuration file is missing for the camera: ' +
              os.path.join('cfg', args.campaign, 'config.' + args.camera + '.cfg'))
        sys.exit(1)

    ap_ingest_one(camera=args.camera, field=args.field, campaign=args.campaign, data_dir=args.data_dir,
                  work_dir=args.work_dir, code_dir=args.archive_code,
                  cam_id=_config_cam_campaign.get('main', 'camID'))
