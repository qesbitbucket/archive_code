#ifndef _FITSUTILS_H_
#define _FITSUTILS_H_

#ifdef _CPLUSPLUS
extern "C" {
#endif

int fits_check_error(const char *msg);

#ifdef _CPLUSPLUS
};
#endif

#endif
