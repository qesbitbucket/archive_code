#ifndef _DBUTILS_H_
#define _DBUTILS_H_

#include "mysql++.h"

using namespace mysqlpp;

Connection* dbConnect(void);

#endif
