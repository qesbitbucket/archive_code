#include <mysql++.h>
#include <values.h>
//#include <mathimf.h>

#include <iostream>
#include <iomanip>
#include <string>
#include <sstream>
#include <valarray>

#include "fitsio.h"
//#include "slalib.h"

#define ISMAIN
#include "types.h"

using namespace std;
using namespace mysqlpp;

const int minPts=0;
const double minInterval=0.0;

const double DTOR=1.74532925199433E-02;
const double RTOD=57.295779513082;

#define sqr(_a_) ((_a_)*(_a_))

// Use concatenated tile files?
#define USE_CONCAT 0

typedef struct {
  long int first, last;
} Interval;

class Lightcurve {
 public:
  int numPts, cntr;
  valarray<ImageID> image_id;
  valarray<int> hjd;
  valarray<float> flux;
  valarray<float> fluxErr;
  valarray<unsigned short int> ccdX, ccdY;

  //
  Lightcurve(int size) {
    image_id.resize(size);
    hjd.resize(size);
    flux.resize(size);
    fluxErr.resize(size);
    ccdX.resize(size);
    ccdY.resize(size);
  }
};

class SkyTileFile {
 private:
  string fileName;
  int status;
  fitsfile *fptr;
  map<ObjectID, Interval> index;
  bool isOpen;
  int flux_col_in, fluxerr_col_in, imageid_col_in, ccdx_col_in,
    ccdy_col_in, time_col_in, bkg_col_in;

 public:
  SkyTileFile(string name);
  ~SkyTileFile();

  void open(void);
  void close(void);
  void fetch(ObjectID obj, Lightcurve& lc);
};

class SkyTile {
 private:
  vector<SkyTileFile*> files;
  bool isOpen;

 public:
  SkyTile(string field, string camera, string season);
  ~SkyTile();

  //
  void open(void);
  void close(void);
  void fetch(ObjectID obj, Lightcurve& lc);
};

typedef struct {
  ImageID id;
  double ra, dec, lst, latitude, jdmid;
  float opaxra, opaxdec;
  float wxTemp, moonPhase, moonAlt, moonDist, skyBkg;
  float medFWHMmaj, medFWHMmin, zeroPoint, extinctCoeff;
  int hiCnt, loCnt, tMid, night;
  std::string tStart, fieldName;
  int year, month, day, hour, minute, second, site;
  int cntFWHM;
} Image;

typedef struct  {
  ObjectID id;
  short int ucac3_zone;
  int ucac3_id, npts, nptsReal, nomad_match;
  double ra, declination;
  float flux_mean, blend_fraction, flux_chisq, flux_skew, flux_kurtosis,
    pwrMean, pwrMax, mag_mean, mag_rms;
  float intvl_flux_rms, intvl_flux_wmean, intvl_flux_chisq;
  short int ccdMinX, ccdMaxX, ccdMinY, ccdMaxY;
  float intvl_ccdMeanX, intvl_ccdMeanY;
  float vmag, bmag, jmag, hmag, kmag, mu_ra, mu_dec, mu_ra_err,
    mu_dec_err, dilution_r, dilution_v;
  int cntFWHM;
} Object;

// Constructor for the sky-tile file class
//
SkyTileFile::SkyTileFile(std::string name)
{
  fileName=name;
  isOpen=false;
  fptr=NULL;
  status=0;
}


SkyTileFile::~SkyTileFile(void)
{
  if(isOpen) close();
}


void SkyTileFile::close(void)
{
  if(fptr) fits_close_file(fptr, &status);
  isOpen=false;
  fptr=NULL;
  index.clear();
}


void SkyTileFile::open(void)
{
  //
  if(fptr || isOpen) {
    cerr << "Warning: sky-tile file " << fileName << " is already open"
	 << endl;
    return;
  }

  // Open the file
  fits_open_file(&fptr, fileName.c_str(), READONLY, &status);

  // Find the INDEX extension
  fits_movnam_hdu(fptr, BINARY_TBL, "INDEX", 0, &status);
  if(status) {
    cout << "FITS error " << status << " opening file " << fileName << endl;
    exit(1);
  }

  // 
  long int nrows;
  fits_get_num_rows(fptr, &nrows, &status);

  //
  int idcol, firstcol, lastcol;
  fits_get_colnum(fptr, CASEINSEN, "OBJ_ID", &idcol, &status);
  fits_get_colnum(fptr, CASEINSEN, "FIRST_ROW", &firstcol, &status);
  fits_get_colnum(fptr, CASEINSEN, "LAST_ROW", &lastcol, &status);

  //
  int first[nrows], last[nrows];
  char *obj_id[nrows], *nullstr="\0";
  for(int idx=0; idx<nrows; idx++)
    obj_id[idx]=new char[27];

  // Read the index
  fits_read_col_str(fptr, idcol, 1, 1, nrows, nullstr, obj_id,
		    NULL, &status);
  fits_read_col_int(fptr, firstcol, 1, 1, nrows, 0, first, NULL, &status);
  fits_read_col_int(fptr, lastcol, 1, 1, nrows, 0, last, NULL, &status);
  
  for(int idx=0; idx<nrows; idx++) {
    string id=obj_id[idx];
    index[id].first=first[idx];
    index[id].last=last[idx];
  }

  //
  for(int idx=0; idx<nrows; idx++)
    delete obj_id[idx];

  // Seek to the photometry table
  fits_movnam_hdu(fptr, BINARY_TBL, "PHOTOMETRY", 0, &status);

  // Enumerate the columns in the input file
  fits_get_colnum(fptr, CASEINSEN, "TIME", &time_col_in, &status);
  fits_get_colnum(fptr, CASEINSEN, "FLUX2", &flux_col_in, &status);
  fits_get_colnum(fptr, CASEINSEN, "FLUX2_ERR", &fluxerr_col_in, &status);
  fits_get_colnum(fptr, CASEINSEN, "IMAGE_ID", &imageid_col_in, &status);
  fits_get_colnum(fptr, CASEINSEN, "CCDX", &ccdx_col_in, &status);
  fits_get_colnum(fptr, CASEINSEN, "CCDY", &ccdy_col_in, &status);

  // Check the FITS status
  if(status) {
    cerr << "FITS error " << status << " attempting to open "
	 << fileName << endl;
  }
  else isOpen=true;
}



// Fetch a lightcurve segment
//
void SkyTileFile::fetch(ObjectID obj_id, Lightcurve& lc)
{
  map<ObjectID, Interval>::iterator loc=index.find(obj_id);
  if(loc==index.end()) return;

  int first=index[obj_id].first, last=index[obj_id].last;
  int nseg=last-first+1;
  if(lc.cntr+nseg>lc.flux.size())
    cerr << "Warning lightcurve buffer overflow" << endl;
  if(lc.cntr<0)
    cerr << "lc.cntr<0" << endl;

  //
  fits_read_col_lnglng(fptr, imageid_col_in, first, 1, nseg, 0,
		       (LONGLONG*)&lc.image_id[lc.cntr], NULL, &status);
  fits_read_col_int(fptr, time_col_in, first, 1, nseg, 0,
		    &lc.hjd[lc.cntr], NULL, &status);
  fits_read_col_flt(fptr, flux_col_in, first, 1, nseg, 0,
		    &lc.flux[lc.cntr], NULL, &status);
  fits_read_col_flt(fptr, fluxerr_col_in, first, 1, nseg, 0,
		    &lc.fluxErr[lc.cntr], NULL, &status);
  fits_read_col_usht(fptr, ccdx_col_in, first, 1, nseg, 0,
		    &lc.ccdX[lc.cntr], NULL, &status);
  fits_read_col_usht(fptr, ccdy_col_in, first, 1, nseg, 0,
		    &lc.ccdY[lc.cntr], NULL, &status);

  // Check the FITS status
  if(status) cerr << "FITS error " << status << " reading "
		  << fileName << endl;
  //
  lc.cntr+=nseg;
  lc.numPts+=nseg;
}




// Constructor
//
// This queries the database to fetch a list of the tile files, but doesn't
// actually open them
//
SkyTile::SkyTile(string field, string camera, string season)
{
  // Connect to the database
  Connection con;
  if(!con.connect("ap", "raad-bak2", "archive", "bECr4$ru")) {
    cerr << "Cannot connect to database server" << endl;
    exit(1);
  }

  // Fetch the list of files associated with this FCC
  Query query=con.query();
  query << "SELECT CONCAT(directory, '/', filename) AS path FROM tiledir LEFT JOIN tilefiles USING (field, camera_id, campaign) "
	<< "WHERE field='" << field<< "' AND camera_id=" << camera << " AND campaign='" << season << "' ORDER BY night";
  StoreQueryResult res=query.store();  
  for(int idx=0; idx<res.size(); idx++) {
    Row row=res.at(idx);
    files.push_back(new SkyTileFile((string)row["path"]));
  }

  // Close database connection
  con.disconnect();

  //
  isOpen=false;
}


// Destructor
//
SkyTile::~SkyTile(void)
{
  if(isOpen) close();

  //
  files.clear();
}


// Open a sky-tile.
//
// This involves:
//   - opening the tile files
//   - reading the internal indexes for those tile files
//
void SkyTile::open(void)
{
  if(isOpen) return;

  //
  for(vector<SkyTileFile*>::iterator it=files.begin();
      it!=files.end(); it++)
    (*it)->open();

  //
  isOpen=true;
}



// Close a sky-tile, freeing up the associated cache
//
void SkyTile::close(void)
{
  if(!isOpen) return;

  //
  for(vector<SkyTileFile*>::iterator it=files.begin();
      it!=files.end(); it++)
    (*it)->close();
  files.clear();

  //
  isOpen=false;
}


// Fetch a lightcurve
//
void SkyTile::fetch(ObjectID obj_id, Lightcurve& lc)
{
  // Make sure the tile is open
  open();

  // Initialise the internal counters in the lightcurve
  lc.numPts=0;
  lc.cntr=0;

  //
  for(vector<SkyTileFile*>::iterator it=files.begin();
      it!=files.end(); it++)
    (*it)->fetch(obj_id, lc);
}


//
//
int main(int argc, char *argv[])
{
  //
  if(argc!=4) {
    cerr << "Usage: apfetch <field> <camera> <campaign>" << endl;
    return EXIT_FAILURE;
  }
  ostringstream oss;
  string field=argv[1], camera=argv[2], season=argv[3];
  string season_main = season.substr(0, 2);
  oss << field << "_" << camera << "_" << season << ".fits";
  string outfile=oss.str();

  // Connect to the database
  Connection con(true);
  if(!con.connect("ap", "raad-bak2", "archive", "bECr4$ru")) {
    cerr << "Cannot connect to database server" << endl;
    exit(1);
  }
  Query query=con.query();
  StoreQueryResult res;

  //
  int cameraID=atoi(camera.c_str());
  cout << "Extracting field " << field << "_" << camera << "_" << season_main << " as " << season << endl;
  cout << "Outfile = " << outfile << endl;

  // Fetch the list of images associated with these fields
  query << "SELECT image_id, exptime, tstart, "
	<< "TIMESTAMPDIFF(SECOND, '2004-01-01 00:00:00', tstart) as texp, "
	<< "field, DATE_FORMAT(night, '%Y%m%d') as night, jdmid "
	<< "FROM imagelist WHERE field='" << field << "' "
	<< "AND camera_id=" << camera << " AND campaign='" << season_main << "' "
	<< "ORDER BY image_id";

  res=query.store();

  // Create a map from image_id to index into the regularised light-curve
  // arrays
  Row row;
  map<ImageID, int> imageIDToPos, imageHiCnt;
  vector<Image*> imageList;
  vector<float> imgMedFWHM, zpList, extCoeffList;
  int minNight=29999999, maxNight=0;
  for(int idx=0; idx<res.size(); idx++) {
    row=res.at(idx);

    //
    try {
      Image *img=new Image;
      img->id=(unsigned long int)row["image_id"];
      img->hiCnt=img->loCnt=0;
      img->tMid=row["texp"]+row["exptime"]/2;
      img->tStart=(string)row["tstart"];
      img->fieldName=(string)row["field"];
      int night=img->night=(int)row["night"];
      if(night<minNight) minNight=night;
      if(night>maxNight) maxNight=night;
      img->jdmid=(double)row["jdmid"];

      //
      imageIDToPos[img->id]=idx;
      imageList.push_back(img);
    } catch(mysqlpp::BadConversion) {
      cout << "Skipping Image bad conversion, maybe exptime was wrong" << endl;
      continue;
    }
  }
  int numImages=imageList.size();
  cout << "Found " << numImages << " images" << endl;

  //
  if(!numImages) {
    cout << "No images found for this field/camera" << endl;
    return EXIT_FAILURE;
  }

  // Now fetch the list of catalogued objects in the field
  query << "SELECT catalogue.obj_id, ucac3_zone, ucac3_id, ra, "
	<< "declination, npts, flux_mean, flux_chisq, flux_skew, flux_kurtosis, "
	<< "sky_tile, "
	<< "ucac3_mag, 2mass_jmag, 2mass_hmag, 2mass_kmag, "
	<< "ucac3_pm_ra_cosdec, ucac3_pm_dec, ucac3_pm_ra_cosdec_err, ucac3_pm_dec_err, "
	<< "rmag, bmag, vmag, jmag, hmag, kmag, "
	<< "mu_ra, mu_dec, mu_ra_err, mu_dec_err, "
	<< "nomad.dilution_r, nomad.dilution_v "
	<< "FROM catalogue JOIN photsummary USING (obj_id) "
	<< "JOIN fieldnames USING (obj_id) LEFT JOIN nomad USING (obj_id) "
	<< "WHERE field='" << field << "' " << "AND  campaign like '%"<<season_main<<"%' "
	<< "AND camera_id=" << camera << " AND npts>=" << minPts << " "
	<< "ORDER BY catalogue.obj_id";

  UseQueryResult resuse=query.use();

  //
  vector<Object*> objectList;
  map<ObjectID, Object*> catalogue;
  int maxPoints=0, cnt=0;
  while(row=resuse.fetch_row()) {
    cnt++;

    //
    Object *obj=new Object;
    obj->id=(string)row["obj_id"];
    obj->ra=(double)row["ra"];
    obj->declination=(double)row["declination"];
    obj->ucac3_zone=(short int)row["ucac3_zone"];
    obj->ucac3_id=(int)row["ucac3_id"];
    obj->npts=(int)row["npts"];
    obj->flux_mean=(row["flux_mean"].is_null())?
      FLT_NULL:(float)row["flux_mean"];
    obj->flux_chisq=(row["flux_chisq"].is_null())?
      FLT_NULL:(float)row["flux_chisq"];
    obj->flux_skew=(row["flux_skew"].is_null())?
      FLT_NULL:(float)row["flux_skew"];
    obj->flux_kurtosis=(row["flux_kurtosis"].is_null())?
      FLT_NULL:(float)row["flux_kurtosis"];
//      obj->blend_fraction=(float)row["blend_fraction"];
//    obj->sky_tile=(string)row["sky_tile"];
    obj->ccdMinX=obj->ccdMinY=32767;
    obj->ccdMaxX=obj->ccdMaxY=0;
    obj->vmag=(row["ucac3_mag"].is_null())?FLT_NULL:(float)row["ucac3_mag"];
    obj->bmag=(row["bmag"].is_null())?FLT_NULL:(float)row["bmag"];
    obj->jmag=(row["jmag"].is_null())?FLT_NULL:(float)row["jmag"];
    obj->hmag=(row["hmag"].is_null())?FLT_NULL:(float)row["hmag"];
    obj->kmag=(row["kmag"].is_null())?FLT_NULL:(float)row["kmag"];
    obj->mu_ra=(row["mu_ra"].is_null())?FLT_NULL:(float)row["mu_ra"];
    obj->mu_dec=(row["mu_dec"].is_null())?FLT_NULL:(float)row["mu_dec"];
    obj->mu_ra_err=(row["mu_ra_err"].is_null())?FLT_NULL:(float)row["mu_ra_err"];
    obj->mu_dec_err=(row["mu_dec_err"].is_null())?FLT_NULL:(float)row["mu_dec_err"];
    obj->dilution_v=(row["dilution_v"].is_null())?FLT_NULL:(float)row["dilution_v"];
    obj->dilution_r=(row["dilution_r"].is_null())?FLT_NULL:(float)row["dilution_r"];
    obj->nomad_match=1; //!(row["nomad_id"].is_null());

    //
    if(obj->npts>maxPoints) maxPoints=obj->npts;

    //
    objectList.push_back(obj);
    catalogue[obj->id]=obj;
  }
  int numObjects=objectList.size();
  cout << "Found " << numObjects << " objects" << endl;

  // Close the connection
  con.disconnect();

  // Create intermediate files
  enum {HJD_TMP=0, FLUX_TMP, FLUXERR_TMP, CCDX_TMP, CCDY_TMP,
	NUM_TMPFILES};
  fitsfile *ftptr[NUM_TMPFILES];
  int status=0;
  for(int idx=0; idx<NUM_TMPFILES; idx++) {
    stringstream fss;
    fss << "tmp" << idx << ".fits";
    unlink(fss.str().c_str());
    fits_create_file(&ftptr[idx], fss.str().c_str(), &status);
    cout << "Creating temporary file " << fss.str() << endl;
  }
  if(status) {
    cerr << "FITS error " << status << " creating temporary files" << endl;
    exit(1);
  }

  // Create the primary arrays in the temporary files
  long naxes[2]={numImages, numObjects};
  cout << "Creating temporary arrays " << numImages << "x"
       << numObjects << endl;
  int imgtype[]={LONG_IMG, FLOAT_IMG, FLOAT_IMG, USHORT_IMG, USHORT_IMG,
  };
  char *imgname[]={"HJD", "FLUX", "FLUXERR", "CCDX", "CCDY"};
  for(int idx=0; idx<NUM_TMPFILES; idx++) {
    fits_write_imghdr(ftptr[idx], 8, 0, NULL, &status);
    fits_create_img(ftptr[idx], imgtype[idx], 2, naxes, &status);
    fits_movabs_hdu(ftptr[idx], 2, NULL, &status);
    fits_write_key(ftptr[idx], TSTRING, "EXTNAME", imgname[idx], "", &status);
    if(imgtype[idx]==LONG_IMG)
      fits_write_key(ftptr[idx], TINT, "BLANK", &INT_NULL, "", &status);
    if(imgtype[idx]==BYTE_IMG)
      fits_write_key(ftptr[idx], TBYTE, "BLANK", &BYT_NULL, "", &status);
    if(imgtype[idx]==SHORT_IMG)
      fits_write_key(ftptr[idx], TSHORT, "BLANK", &SHT_NULL, "", &status);
  }

  // Loop over each covered sky-tile
  Lightcurve lc(maxPoints*10);
  vector<Object*> outCat;
  int rowidx=1, rejcnt=0, brightCnt=0, sigrejcnt=0;
  vector<float> fracRMS, redChisq, fitFlux, fitRMS;
  SkyTile *s=new SkyTile(field, camera, season_main);
  for(int oidx=0; oidx<objectList.size(); oidx++) {
    Object *obj=objectList[oidx];
    s->fetch(obj->id, lc);

    //
    int hjd[numImages];
    float flux[numImages], fluxErr[numImages], mag[numImages], magWght[numImages];
    signed short int ccdX[numImages], ccdY[numImages];
    for(int idx=0; idx<numImages; idx++) {
      hjd[idx]=INT_NULL;
      flux[idx]=FLT_NULL;
      fluxErr[idx]=FLT_NULL;
      ccdX[idx]=USHT_NULL;
      ccdY[idx]=USHT_NULL;
      mag[idx]=FLT_NULL;
      magWght[idx]=FLT_NULL;
    }
    
    //
    int nptsReal=0;
    float fluxTot=0.0, fluxMax=-FLT_MAX, fluxMin=FLT_MAX;
    double magMean=0.0, magM2=0.0, mm=0.0, mm2=0.0, mw=0.0;
    int minHJD=INT_MAX, maxHJD=INT_MIN;
    ImageID imgMax=0, imgMin=0;
    obj->cntFWHM=0;
    for(int idx=0; idx<lc.numPts; idx++) {
      // Check camera ID
      int cid=lc.image_id[idx]/1000000000000000L;
      if(cid!=cameraID) continue;
      if(lc.fluxErr[idx]<=0.0 || lc.flux[idx]>10000.0) continue;
      if((lc.flux[idx]/lc.fluxErr[idx])>1000.0)  {
	sigrejcnt++;
	continue;
      }
      
      // Map the point into the regularised vectors
      map<ImageID, int>::iterator iit=imageIDToPos.find(lc.image_id[idx]);
      if(iit!=imageIDToPos.end()) {
	int ridx=iit->second;
	
	//
	hjd[ridx]=lc.hjd[idx];
	flux[ridx]=lc.flux[idx];
	fluxErr[ridx]=lc.fluxErr[idx];
	ccdX[ridx]=lc.ccdX[idx];
	ccdY[ridx]=lc.ccdY[idx];

	//
	if(hjd[ridx]<minHJD) minHJD=hjd[ridx];
	if(hjd[ridx]>maxHJD) maxHJD=hjd[ridx];

	//
	if(ccdX[ridx]<obj->ccdMinX) obj->ccdMinX=ccdX[ridx];
	if(ccdX[ridx]>obj->ccdMaxX) obj->ccdMaxX=ccdX[ridx];
	if(ccdY[ridx]<obj->ccdMinY) obj->ccdMinY=ccdY[ridx];
	if(ccdY[ridx]>obj->ccdMaxY) obj->ccdMaxY=ccdY[ridx];

	//
	fluxTot+=lc.flux[idx];
	float mag=15.0-2.5*log10(lc.flux[idx]);
	float magErr=2.5*(lc.fluxErr[idx]/lc.flux[idx])*log10(M_E);
	float magVar=magErr*magErr;
	float magWght=1.0/magVar;
	nptsReal++;
	mm+=mag*magWght;
	mm2+=mag*mag*magWght;
	mw+=magWght;
      }
    }

    // Write the regularised light-curve to the output file
    if(nptsReal>minPts && (maxHJD-minHJD)>minInterval) {
      // Adjust the recorded object properties
      obj->npts=nptsReal;
      obj->flux_mean=fluxTot/nptsReal;
      obj->mag_mean=mm/mw;
      obj->mag_rms=sqrt((mm2-mm*mm/mw)/mw);

      // Add to the output catalogue
      outCat.push_back(obj);

      // Write the rows to the temporary arrays
      long fpixel[]={1, rowidx}, nelements=numImages;
      fits_write_pixnull(ftptr[HJD_TMP], TINT, fpixel,
			 nelements, hjd, &INT_NULL, &status);
      fits_write_pixnull(ftptr[FLUX_TMP], TFLOAT, fpixel,
			 nelements, flux, &FLT_NULL, &status);
      fits_write_pixnull(ftptr[FLUXERR_TMP], TFLOAT, fpixel,
			 nelements, fluxErr, &FLT_NULL, &status);
      fits_write_pix(ftptr[CCDX_TMP], TUSHORT, fpixel, nelements, ccdX, &status);
      fits_write_pix(ftptr[CCDY_TMP], TUSHORT, fpixel, nelements, ccdY, &status);

      //
      rowidx++;
//      cout << rowidx << endl;
    } else 
      rejcnt++;
  }
//  cout << rowidx << endl;
//  cout << "status=" << status << endl;
  cout << "Rejected " << sigrejcnt << " points for having too small error bar" << endl;
  delete s;

  // Resize the arrays in the temporary files
  cout << "Truncating temporary arrays to " << naxes[0] << "x" << rowidx-1 << endl;
  naxes[1]=rowidx-1;
  long tnaxes[2];
  for(int idx=0; idx<NUM_TMPFILES; idx++) {
    int bitpix;
    fits_get_img_type(ftptr[idx], &bitpix, &status);
    fits_resize_img(ftptr[idx], bitpix, 2, naxes, &status);
    fits_flush_file(ftptr[idx], &status);
    fits_get_img_size(ftptr[idx], 2, tnaxes, &status);
    cout << "New size is " << tnaxes[0] << "x" << tnaxes[1] << endl;
  }
  //  cout << "status=" << status << endl;

  // Open output file
  fitsfile *ofptr;
  unlink(outfile.c_str());
  fits_create_file(&ofptr, outfile.c_str(), &status);
  fits_write_imghdr(ofptr, 8, 0, NULL, &status);

  // Write the field and camera to the header
  fits_write_key_str(ofptr, "FIELD", (char*)field.c_str(), "Field name", &status);
  fits_write_key_str(ofptr, "CAMERA_ID", (char*)camera.c_str(), "Camera ID",
		 &status);
  fits_write_key_str(ofptr, "SEASON", (char*)season.c_str(), "Campaign", &status);
  fits_write_key_str(ofptr, "TSTART", (char*)(imageList.front()->tStart.c_str()),
		     "Time of first observation", &status);
  fits_write_key_str(ofptr, "TSTOP", (char*)(imageList.back()->tStart.c_str()),
		     "Time of last observation", &status);
  fits_write_key_log(ofptr, "HASNOMAD", 1, "Catalogue table contains NOMAD cross-correlation",
		     &status);
  fits_write_key_str(ofptr, "DETREND", "NONE", "De-trending applied", &status);
  fits_write_key_str(ofptr, "PROJECT", "ALSUBAI", "Project ID", &status);
  fits_write_key_lng(ofptr, "NFIRST", minNight, "First night", &status);
  fits_write_key_lng(ofptr, "NLAST", maxNight, "Last night", &status);
  //  cout << "status=" << status << endl;

  // Write the catalogue
  char *cat_names[]={"OBJ_ID", "FLUX_MEAN", "NPTS", "RA", "DEC",
		     "UCAC3_ZONE", "UCAC3_ID", "FLUX_CHISQ",
		     "FLUX_SKEW", "FLUX_KURTOSIS", "MINCCDX", "MAXCCDX",
		     "MINCCDY", "MAXCCDY", "ORGIDX", "PWR_MEAN", "PWR_MAX",
		     "BMAG", "VMAG", "JMAG", "HMAG", "KMAG", "MU_RA", "MU_DEC",
		     "MU_RA_ERR", "MU_DEC_ERR", "DILUTION_V", "DILUTION_R",
		     "MATCH_FLAG", "MAG_MEAN", "MAG_RMS"};
  char *cat_types[]={"26A", "1E", "1J", "1D", "1D", "1I", "1J", "1E", "1E",
		     "1E", "1I", "1I", "1I", "1I", "1J", "1E", "1E", "1E",
		     "1E", "1E", "1E", "1E", "1E", "1E", "1E", "1E", "1E", "1E", "1I",
		     "1E", "1E"};
  char *cat_units[]={"", "", "", "deg", "deg", "", "", "", "", "", "",
		     "", "", "", "", "", "", "", "", "", "", "", "", "", "",
		     "", "", "", "", "", "", ""};
  cout << "Writing catalogue" << endl;
  fits_create_tbl(ofptr, BINARY_TBL, outCat.size(),
		  sizeof(cat_names)/sizeof(char*),
		  cat_names, cat_types, cat_units, "CATALOGUE", &status);
  for(int idx=0; idx<outCat.size(); idx++) {
    Object *obj=outCat[idx];
    char *ptr=(char*)obj->id.c_str(), **pptr=&ptr;
    float val;
    fits_write_col_str(ofptr, 1, idx+1, 1, 1, pptr, &status);
    fits_write_col_flt(ofptr, 2, idx+1, 1, 1, &obj->flux_mean, &status);
    fits_write_col_int(ofptr, 3, idx+1, 1, 1, &obj->npts, &status);
    fits_write_col_dbl(ofptr, 4, idx+1, 1, 1, &obj->ra, &status);
    fits_write_col_dbl(ofptr, 5, idx+1, 1, 1, &obj->declination, &status);
    fits_write_col_sht(ofptr, 6, idx+1, 1, 1, &obj->ucac3_zone, &status);
    fits_write_col_int(ofptr, 7, idx+1, 1, 1, &obj->ucac3_id, &status);
    fits_write_col_flt(ofptr, 8, idx+1, 1, 1, &obj->flux_chisq, &status);
    fits_write_col_flt(ofptr, 9, idx+1, 1, 1, &obj->flux_skew, &status);
    fits_write_col_flt(ofptr, 10, idx+1, 1, 1, &obj->flux_kurtosis, &status);
    fits_write_col_sht(ofptr, 11, idx+1, 1, 1, &obj->ccdMinX, &status);
    fits_write_col_sht(ofptr, 12, idx+1, 1, 1, &obj->ccdMaxX, &status);
    fits_write_col_sht(ofptr, 13, idx+1, 1, 1, &obj->ccdMinY, &status);
    fits_write_col_sht(ofptr, 14, idx+1, 1, 1, &obj->ccdMaxY, &status);
    int ival=idx+1;
    fits_write_col_int(ofptr, 15, idx+1, 1, 1, &ival, &status);
    fits_write_col_flt(ofptr, 16, idx+1, 1, 1, &obj->pwrMean, &status);
    fits_write_col_flt(ofptr, 17, idx+1, 1, 1, &obj->pwrMax, &status);
    fits_write_col_flt(ofptr, 18, idx+1, 1, 1, &obj->bmag, &status);
    fits_write_col_flt(ofptr, 19, idx+1, 1, 1, &obj->vmag, &status);
    fits_write_col_flt(ofptr, 20, idx+1, 1, 1, &obj->jmag, &status);
    fits_write_col_flt(ofptr, 21, idx+1, 1, 1, &obj->hmag, &status);
    fits_write_col_flt(ofptr, 22, idx+1, 1, 1, &obj->kmag, &status);
    fits_write_col_flt(ofptr, 23, idx+1, 1, 1, &obj->mu_ra, &status);
    fits_write_col_flt(ofptr, 24, idx+1, 1, 1, &obj->mu_dec, &status);
    fits_write_col_flt(ofptr, 25, idx+1, 1, 1, &obj->mu_ra_err, &status);
    fits_write_col_flt(ofptr, 26, idx+1, 1, 1, &obj->mu_dec_err, &status);
    fits_write_col_flt(ofptr, 27, idx+1, 1, 1, &obj->dilution_v, &status);
    fits_write_col_flt(ofptr, 28, idx+1, 1, 1, &obj->dilution_r, &status);
    fits_write_col_int(ofptr, 29, idx+1, 1, 1, &obj->nomad_match, &status);
    fits_write_col_flt(ofptr, 30, idx+1, 1, 1, &obj->mag_mean, &status);
    fits_write_col_flt(ofptr, 31, idx+1, 1, 1, &obj->mag_rms, &status);
  }
  //  cout << "status=" << status << endl;

  // Write the image list
  char *img_names[]={"IMAGEID", "TMID", "FIELD", "NIGHT", "JDMID"};
  char *img_types[]={"18A", "1J", "12A", "1J", "1D"};
  char *img_units[]={"", "", "", "", ""};
  cout << "Writing image list" << endl;
  fits_create_tbl(ofptr, BINARY_TBL, imageList.size(),
		  sizeof(img_names)/sizeof(char*),
		  img_names, img_types, img_units, "IMAGELIST", &status);
  for(int idx=0; idx<imageList.size(); idx++) {
    Image *img=imageList[idx];
    char imgid[19], *pptr=(char*)&imgid;
    snprintf(imgid, 19, "%18lld", img->id);
    fits_write_col_str(ofptr, 1, idx+1, 1, 1, &pptr, &status);
    fits_write_col_int(ofptr, 2, idx+1, 1, 1, &img->tMid, &status);
    char *iptr=(char*)img->fieldName.c_str();
    fits_write_col_str(ofptr, 3, idx+1, 1, 1, &iptr, &status);
    fits_write_col_int(ofptr, 4, idx+1, 1, 1, &img->night, &status);
    fits_write_col_dbl(ofptr, 5, idx+1, 1, 1, &img->jdmid, &status);
  }
  //  cout << "status=" << status << endl;

  // Copy the contents of the temporary files into the output file
  for(int idx=0; idx<NUM_TMPFILES; idx++) {
    cout << "Copying temporary file " << idx  << endl;
    fits_copy_hdu(ftptr[idx], ofptr, 0, &status);
    //    cout << "status=" << status << endl;
  }

  // Close and remove the temporary files
  for(int idx=0; idx<NUM_TMPFILES; idx++)
    fits_delete_file(ftptr[idx], &status);
  //  cout << "status=" << status << endl;

  // Close the output file
  fits_close_file(ofptr, &status);
//  cout << "status=" << status << endl;
}
