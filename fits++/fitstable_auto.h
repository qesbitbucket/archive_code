void readColumnNull(string name, int *data, int nullVal, rowNum firstRow=0, rowNum numRows=0);
void readColumnNull(string name, valarray<int>& data, int nullVal, rowNum firstRow=0, rowNum numRows=0);
void readColumnNull(string name, vector<int>& data, int nullVal, rowNum firstRow=0, rowNum numRows=0);
void readColumnNull(string name, int& data, int nullVal, rowNum rowIdx);

void readColumn(string name, int *data, rowNum firstRow=0, rowNum numRows=0)
{readColumnNull(name, data, 0, firstRow, numRows);};
void readColumn(string name, valarray<int>& data, rowNum firstRow=0, rowNum numRows=0)
{readColumnNull(name, data, 0, firstRow, numRows);};
void readColumn(string name, vector<int>& data, rowNum firstRow=0, rowNum numRows=0)
{readColumnNull(name, data, 0, firstRow, numRows);};
void readColumn(string name, int& data, rowNum row)
{readColumnNull(name, data, 0, row);}

void readVectorColumnNull(string name, int *data, int nullVal, rowNum firstRow=0, rowNum numRows=0);
void readVectorColumnNull(string name, valarray<int>& data, int nullVal, rowNum firstRow=0, rowNum numRows=0);
void readVectorColumnNull(string name, vector<int>& data, int nullVal, rowNum firstRow=0, rowNum numRows=0);

void readVectorColumn(string name, int *data, rowNum firstRow=0, rowNum numRows=0)
{readVectorColumnNull(name, data, 0, firstRow, numRows);};
void readVectorColumn(string name, valarray<int>& data, rowNum firstRow=0, rowNum numRows=0)
{readVectorColumnNull(name, data, 0, firstRow, numRows);};
void readVectorColumn(string name, vector<int>& data, rowNum firstRow=0, rowNum numRows=0)
{readVectorColumnNull(name, data, 0, firstRow, numRows);};
void readVariableVectorColumn(string name, int *data, rowNum rowIdx, LONGLONG nels);
void readVariableVectorColumn(string name, valarray<int>& data, rowNum rowIdx);
void readVariableVectorColumn(string name, vector<int>& data, rowNum rowIdx);
void writeColumnNull(string name, int *data, int nullVal, rowNum firstRow=0, rowNum numRows=0);
void writeColumnNull(string name, valarray<int>& data, int nullVal, rowNum firstRow=0, rowNum numRows=0);
void writeColumnNull(string name, vector<int>& data, int nullVal, rowNum firstRow=0, rowNum numRows=0);
void writeColumnNull(string name, int data, int nullVal, rowNum firstRow);

void writeColumn(string name, int *data, rowNum firstRow=0, rowNum numRows=0);
void writeColumn(string name, valarray<int>& data, rowNum firstRow=0, rowNum numRows=0);
void writeColumn(string name, vector<int>& data, rowNum firstRow=0,rowNum numRows=0);
void writeColumn(string name, int data, rowNum firstRow);

void writeVectorColumnNull(string name, int *data, int nullVal, rowNum firstRow=0, rowNum numRows=0);
void writeVectorColumnNull(string name, valarray<int>& data, int nullVal, rowNum firstRow=0, rowNum numRows=0);
void writeVectorColumnNull(string name, vector<int>& data, int nullVal, rowNum firstRow=0, rowNum numRows=0);

void writeVectorColumn(string name, int *data, rowNum firstRow=0, rowNum numRows=0);
void writeVectorColumn(string name, valarray<int>& data, rowNum firstRow=0, rowNum numRows=0);
void writeVectorColumn(string name, vector<int>& data, rowNum firstRow=0, rowNum numRows=0);
void readColumnNull(string name, unsigned long *data, unsigned long nullVal, rowNum firstRow=0, rowNum numRows=0);
void readColumnNull(string name, valarray<unsigned long>& data, unsigned long nullVal, rowNum firstRow=0, rowNum numRows=0);
void readColumnNull(string name, vector<unsigned long>& data, unsigned long nullVal, rowNum firstRow=0, rowNum numRows=0);
void readColumnNull(string name, unsigned long& data, unsigned long nullVal, rowNum rowIdx);

void readColumn(string name, unsigned long *data, rowNum firstRow=0, rowNum numRows=0)
{readColumnNull(name, data, 0, firstRow, numRows);};
void readColumn(string name, valarray<unsigned long>& data, rowNum firstRow=0, rowNum numRows=0)
{readColumnNull(name, data, 0, firstRow, numRows);};
void readColumn(string name, vector<unsigned long>& data, rowNum firstRow=0, rowNum numRows=0)
{readColumnNull(name, data, 0, firstRow, numRows);};
void readColumn(string name, unsigned long& data, rowNum row)
{readColumnNull(name, data, 0, row);}

void readVectorColumnNull(string name, unsigned long *data, unsigned long nullVal, rowNum firstRow=0, rowNum numRows=0);
void readVectorColumnNull(string name, valarray<unsigned long>& data, unsigned long nullVal, rowNum firstRow=0, rowNum numRows=0);
void readVectorColumnNull(string name, vector<unsigned long>& data, unsigned long nullVal, rowNum firstRow=0, rowNum numRows=0);

void readVectorColumn(string name, unsigned long *data, rowNum firstRow=0, rowNum numRows=0)
{readVectorColumnNull(name, data, 0, firstRow, numRows);};
void readVectorColumn(string name, valarray<unsigned long>& data, rowNum firstRow=0, rowNum numRows=0)
{readVectorColumnNull(name, data, 0, firstRow, numRows);};
void readVectorColumn(string name, vector<unsigned long>& data, rowNum firstRow=0, rowNum numRows=0)
{readVectorColumnNull(name, data, 0, firstRow, numRows);};
void readVariableVectorColumn(string name, unsigned long *data, rowNum rowIdx, LONGLONG nels);
void readVariableVectorColumn(string name, valarray<unsigned long>& data, rowNum rowIdx);
void readVariableVectorColumn(string name, vector<unsigned long>& data, rowNum rowIdx);
void writeColumnNull(string name, unsigned long *data, unsigned long nullVal, rowNum firstRow=0, rowNum numRows=0);
void writeColumnNull(string name, valarray<unsigned long>& data, unsigned long nullVal, rowNum firstRow=0, rowNum numRows=0);
void writeColumnNull(string name, vector<unsigned long>& data, unsigned long nullVal, rowNum firstRow=0, rowNum numRows=0);
void writeColumnNull(string name, unsigned long data, unsigned long nullVal, rowNum firstRow);

void writeColumn(string name, unsigned long *data, rowNum firstRow=0, rowNum numRows=0);
void writeColumn(string name, valarray<unsigned long>& data, rowNum firstRow=0, rowNum numRows=0);
void writeColumn(string name, vector<unsigned long>& data, rowNum firstRow=0,rowNum numRows=0);
void writeColumn(string name, unsigned long data, rowNum firstRow);

void writeVectorColumnNull(string name, unsigned long *data, unsigned long nullVal, rowNum firstRow=0, rowNum numRows=0);
void writeVectorColumnNull(string name, valarray<unsigned long>& data, unsigned long nullVal, rowNum firstRow=0, rowNum numRows=0);
void writeVectorColumnNull(string name, vector<unsigned long>& data, unsigned long nullVal, rowNum firstRow=0, rowNum numRows=0);

void writeVectorColumn(string name, unsigned long *data, rowNum firstRow=0, rowNum numRows=0);
void writeVectorColumn(string name, valarray<unsigned long>& data, rowNum firstRow=0, rowNum numRows=0);
void writeVectorColumn(string name, vector<unsigned long>& data, rowNum firstRow=0, rowNum numRows=0);
void readColumnNull(string name, string *data, string nullVal, rowNum firstRow=0, rowNum numRows=0);
void readColumnNull(string name, valarray<string>& data, string nullVal, rowNum firstRow=0, rowNum numRows=0);
void readColumnNull(string name, vector<string>& data, string nullVal, rowNum firstRow=0, rowNum numRows=0);
void readColumnNull(string name, string& data, string nullVal, rowNum rowIdx);

void readColumn(string name, string *data, rowNum firstRow=0, rowNum numRows=0)
{readColumnNull(name, data, "", firstRow, numRows);};
void readColumn(string name, valarray<string>& data, rowNum firstRow=0, rowNum numRows=0)
{readColumnNull(name, data, "", firstRow, numRows);};
void readColumn(string name, vector<string>& data, rowNum firstRow=0, rowNum numRows=0)
{readColumnNull(name, data, "", firstRow, numRows);};
void readColumn(string name, string& data, rowNum row)
{readColumnNull(name, data, "", row);}

void readVectorColumnNull(string name, string *data, string nullVal, rowNum firstRow=0, rowNum numRows=0);
void readVectorColumnNull(string name, valarray<string>& data, string nullVal, rowNum firstRow=0, rowNum numRows=0);
void readVectorColumnNull(string name, vector<string>& data, string nullVal, rowNum firstRow=0, rowNum numRows=0);

void readVectorColumn(string name, string *data, rowNum firstRow=0, rowNum numRows=0)
{readVectorColumnNull(name, data, "", firstRow, numRows);};
void readVectorColumn(string name, valarray<string>& data, rowNum firstRow=0, rowNum numRows=0)
{readVectorColumnNull(name, data, "", firstRow, numRows);};
void readVectorColumn(string name, vector<string>& data, rowNum firstRow=0, rowNum numRows=0)
{readVectorColumnNull(name, data, "", firstRow, numRows);};
void writeColumnNull(string name, string *data, string nullVal, rowNum firstRow=0, rowNum numRows=0);
void writeColumnNull(string name, valarray<string>& data, string nullVal, rowNum firstRow=0, rowNum numRows=0);
void writeColumnNull(string name, vector<string>& data, string nullVal, rowNum firstRow=0, rowNum numRows=0);
void writeColumnNull(string name, string data, string nullVal, rowNum firstRow);

void writeColumn(string name, string *data, rowNum firstRow=0, rowNum numRows=0);
void writeColumn(string name, valarray<string>& data, rowNum firstRow=0, rowNum numRows=0);
void writeColumn(string name, vector<string>& data, rowNum firstRow=0,rowNum numRows=0);
void writeColumn(string name, string data, rowNum firstRow);

void writeVectorColumnNull(string name, string *data, string nullVal, rowNum firstRow=0, rowNum numRows=0);
void writeVectorColumnNull(string name, valarray<string>& data, string nullVal, rowNum firstRow=0, rowNum numRows=0);
void writeVectorColumnNull(string name, vector<string>& data, string nullVal, rowNum firstRow=0, rowNum numRows=0);

void writeVectorColumn(string name, string *data, rowNum firstRow=0, rowNum numRows=0);
void writeVectorColumn(string name, valarray<string>& data, rowNum firstRow=0, rowNum numRows=0);
void writeVectorColumn(string name, vector<string>& data, rowNum firstRow=0, rowNum numRows=0);
void readColumnNull(string name, unsigned int *data, unsigned int nullVal, rowNum firstRow=0, rowNum numRows=0);
void readColumnNull(string name, valarray<unsigned int>& data, unsigned int nullVal, rowNum firstRow=0, rowNum numRows=0);
void readColumnNull(string name, vector<unsigned int>& data, unsigned int nullVal, rowNum firstRow=0, rowNum numRows=0);
void readColumnNull(string name, unsigned int& data, unsigned int nullVal, rowNum rowIdx);

void readColumn(string name, unsigned int *data, rowNum firstRow=0, rowNum numRows=0)
{readColumnNull(name, data, 0, firstRow, numRows);};
void readColumn(string name, valarray<unsigned int>& data, rowNum firstRow=0, rowNum numRows=0)
{readColumnNull(name, data, 0, firstRow, numRows);};
void readColumn(string name, vector<unsigned int>& data, rowNum firstRow=0, rowNum numRows=0)
{readColumnNull(name, data, 0, firstRow, numRows);};
void readColumn(string name, unsigned int& data, rowNum row)
{readColumnNull(name, data, 0, row);}

void readVectorColumnNull(string name, unsigned int *data, unsigned int nullVal, rowNum firstRow=0, rowNum numRows=0);
void readVectorColumnNull(string name, valarray<unsigned int>& data, unsigned int nullVal, rowNum firstRow=0, rowNum numRows=0);
void readVectorColumnNull(string name, vector<unsigned int>& data, unsigned int nullVal, rowNum firstRow=0, rowNum numRows=0);

void readVectorColumn(string name, unsigned int *data, rowNum firstRow=0, rowNum numRows=0)
{readVectorColumnNull(name, data, 0, firstRow, numRows);};
void readVectorColumn(string name, valarray<unsigned int>& data, rowNum firstRow=0, rowNum numRows=0)
{readVectorColumnNull(name, data, 0, firstRow, numRows);};
void readVectorColumn(string name, vector<unsigned int>& data, rowNum firstRow=0, rowNum numRows=0)
{readVectorColumnNull(name, data, 0, firstRow, numRows);};
void readVariableVectorColumn(string name, unsigned int *data, rowNum rowIdx, LONGLONG nels);
void readVariableVectorColumn(string name, valarray<unsigned int>& data, rowNum rowIdx);
void readVariableVectorColumn(string name, vector<unsigned int>& data, rowNum rowIdx);
void writeColumnNull(string name, unsigned int *data, unsigned int nullVal, rowNum firstRow=0, rowNum numRows=0);
void writeColumnNull(string name, valarray<unsigned int>& data, unsigned int nullVal, rowNum firstRow=0, rowNum numRows=0);
void writeColumnNull(string name, vector<unsigned int>& data, unsigned int nullVal, rowNum firstRow=0, rowNum numRows=0);
void writeColumnNull(string name, unsigned int data, unsigned int nullVal, rowNum firstRow);

void writeColumn(string name, unsigned int *data, rowNum firstRow=0, rowNum numRows=0);
void writeColumn(string name, valarray<unsigned int>& data, rowNum firstRow=0, rowNum numRows=0);
void writeColumn(string name, vector<unsigned int>& data, rowNum firstRow=0,rowNum numRows=0);
void writeColumn(string name, unsigned int data, rowNum firstRow);

void writeVectorColumnNull(string name, unsigned int *data, unsigned int nullVal, rowNum firstRow=0, rowNum numRows=0);
void writeVectorColumnNull(string name, valarray<unsigned int>& data, unsigned int nullVal, rowNum firstRow=0, rowNum numRows=0);
void writeVectorColumnNull(string name, vector<unsigned int>& data, unsigned int nullVal, rowNum firstRow=0, rowNum numRows=0);

void writeVectorColumn(string name, unsigned int *data, rowNum firstRow=0, rowNum numRows=0);
void writeVectorColumn(string name, valarray<unsigned int>& data, rowNum firstRow=0, rowNum numRows=0);
void writeVectorColumn(string name, vector<unsigned int>& data, rowNum firstRow=0, rowNum numRows=0);
void readColumnNull(string name, unsigned char *data, unsigned char nullVal, rowNum firstRow=0, rowNum numRows=0);
void readColumnNull(string name, valarray<unsigned char>& data, unsigned char nullVal, rowNum firstRow=0, rowNum numRows=0);
void readColumnNull(string name, vector<unsigned char>& data, unsigned char nullVal, rowNum firstRow=0, rowNum numRows=0);
void readColumnNull(string name, unsigned char& data, unsigned char nullVal, rowNum rowIdx);

void readColumn(string name, unsigned char *data, rowNum firstRow=0, rowNum numRows=0)
{readColumnNull(name, data, 0, firstRow, numRows);};
void readColumn(string name, valarray<unsigned char>& data, rowNum firstRow=0, rowNum numRows=0)
{readColumnNull(name, data, 0, firstRow, numRows);};
void readColumn(string name, vector<unsigned char>& data, rowNum firstRow=0, rowNum numRows=0)
{readColumnNull(name, data, 0, firstRow, numRows);};
void readColumn(string name, unsigned char& data, rowNum row)
{readColumnNull(name, data, 0, row);}

void readVectorColumnNull(string name, unsigned char *data, unsigned char nullVal, rowNum firstRow=0, rowNum numRows=0);
void readVectorColumnNull(string name, valarray<unsigned char>& data, unsigned char nullVal, rowNum firstRow=0, rowNum numRows=0);
void readVectorColumnNull(string name, vector<unsigned char>& data, unsigned char nullVal, rowNum firstRow=0, rowNum numRows=0);

void readVectorColumn(string name, unsigned char *data, rowNum firstRow=0, rowNum numRows=0)
{readVectorColumnNull(name, data, 0, firstRow, numRows);};
void readVectorColumn(string name, valarray<unsigned char>& data, rowNum firstRow=0, rowNum numRows=0)
{readVectorColumnNull(name, data, 0, firstRow, numRows);};
void readVectorColumn(string name, vector<unsigned char>& data, rowNum firstRow=0, rowNum numRows=0)
{readVectorColumnNull(name, data, 0, firstRow, numRows);};
void readVariableVectorColumn(string name, unsigned char *data, rowNum rowIdx, LONGLONG nels);
void readVariableVectorColumn(string name, valarray<unsigned char>& data, rowNum rowIdx);
void readVariableVectorColumn(string name, vector<unsigned char>& data, rowNum rowIdx);
void writeColumnNull(string name, unsigned char *data, unsigned char nullVal, rowNum firstRow=0, rowNum numRows=0);
void writeColumnNull(string name, valarray<unsigned char>& data, unsigned char nullVal, rowNum firstRow=0, rowNum numRows=0);
void writeColumnNull(string name, vector<unsigned char>& data, unsigned char nullVal, rowNum firstRow=0, rowNum numRows=0);
void writeColumnNull(string name, unsigned char data, unsigned char nullVal, rowNum firstRow);

void writeColumn(string name, unsigned char *data, rowNum firstRow=0, rowNum numRows=0);
void writeColumn(string name, valarray<unsigned char>& data, rowNum firstRow=0, rowNum numRows=0);
void writeColumn(string name, vector<unsigned char>& data, rowNum firstRow=0,rowNum numRows=0);
void writeColumn(string name, unsigned char data, rowNum firstRow);

void writeVectorColumnNull(string name, unsigned char *data, unsigned char nullVal, rowNum firstRow=0, rowNum numRows=0);
void writeVectorColumnNull(string name, valarray<unsigned char>& data, unsigned char nullVal, rowNum firstRow=0, rowNum numRows=0);
void writeVectorColumnNull(string name, vector<unsigned char>& data, unsigned char nullVal, rowNum firstRow=0, rowNum numRows=0);

void writeVectorColumn(string name, unsigned char *data, rowNum firstRow=0, rowNum numRows=0);
void writeVectorColumn(string name, valarray<unsigned char>& data, rowNum firstRow=0, rowNum numRows=0);
void writeVectorColumn(string name, vector<unsigned char>& data, rowNum firstRow=0, rowNum numRows=0);
void readColumnNull(string name, double *data, double nullVal, rowNum firstRow=0, rowNum numRows=0);
void readColumnNull(string name, valarray<double>& data, double nullVal, rowNum firstRow=0, rowNum numRows=0);
void readColumnNull(string name, vector<double>& data, double nullVal, rowNum firstRow=0, rowNum numRows=0);
void readColumnNull(string name, double& data, double nullVal, rowNum rowIdx);

void readColumn(string name, double *data, rowNum firstRow=0, rowNum numRows=0)
{readColumnNull(name, data, 0.0, firstRow, numRows);};
void readColumn(string name, valarray<double>& data, rowNum firstRow=0, rowNum numRows=0)
{readColumnNull(name, data, 0.0, firstRow, numRows);};
void readColumn(string name, vector<double>& data, rowNum firstRow=0, rowNum numRows=0)
{readColumnNull(name, data, 0.0, firstRow, numRows);};
void readColumn(string name, double& data, rowNum row)
{readColumnNull(name, data, 0.0, row);}

void readVectorColumnNull(string name, double *data, double nullVal, rowNum firstRow=0, rowNum numRows=0);
void readVectorColumnNull(string name, valarray<double>& data, double nullVal, rowNum firstRow=0, rowNum numRows=0);
void readVectorColumnNull(string name, vector<double>& data, double nullVal, rowNum firstRow=0, rowNum numRows=0);

void readVectorColumn(string name, double *data, rowNum firstRow=0, rowNum numRows=0)
{readVectorColumnNull(name, data, 0.0, firstRow, numRows);};
void readVectorColumn(string name, valarray<double>& data, rowNum firstRow=0, rowNum numRows=0)
{readVectorColumnNull(name, data, 0.0, firstRow, numRows);};
void readVectorColumn(string name, vector<double>& data, rowNum firstRow=0, rowNum numRows=0)
{readVectorColumnNull(name, data, 0.0, firstRow, numRows);};
void readVariableVectorColumn(string name, double *data, rowNum rowIdx, LONGLONG nels);
void readVariableVectorColumn(string name, valarray<double>& data, rowNum rowIdx);
void readVariableVectorColumn(string name, vector<double>& data, rowNum rowIdx);
void writeColumnNull(string name, double *data, double nullVal, rowNum firstRow=0, rowNum numRows=0);
void writeColumnNull(string name, valarray<double>& data, double nullVal, rowNum firstRow=0, rowNum numRows=0);
void writeColumnNull(string name, vector<double>& data, double nullVal, rowNum firstRow=0, rowNum numRows=0);
void writeColumnNull(string name, double data, double nullVal, rowNum firstRow);

void writeColumn(string name, double *data, rowNum firstRow=0, rowNum numRows=0);
void writeColumn(string name, valarray<double>& data, rowNum firstRow=0, rowNum numRows=0);
void writeColumn(string name, vector<double>& data, rowNum firstRow=0,rowNum numRows=0);
void writeColumn(string name, double data, rowNum firstRow);

void writeVectorColumnNull(string name, double *data, double nullVal, rowNum firstRow=0, rowNum numRows=0);
void writeVectorColumnNull(string name, valarray<double>& data, double nullVal, rowNum firstRow=0, rowNum numRows=0);
void writeVectorColumnNull(string name, vector<double>& data, double nullVal, rowNum firstRow=0, rowNum numRows=0);

void writeVectorColumn(string name, double *data, rowNum firstRow=0, rowNum numRows=0);
void writeVectorColumn(string name, valarray<double>& data, rowNum firstRow=0, rowNum numRows=0);
void writeVectorColumn(string name, vector<double>& data, rowNum firstRow=0, rowNum numRows=0);
void readColumnNull(string name, unsigned short int *data, unsigned short int nullVal, rowNum firstRow=0, rowNum numRows=0);
void readColumnNull(string name, valarray<unsigned short int>& data, unsigned short int nullVal, rowNum firstRow=0, rowNum numRows=0);
void readColumnNull(string name, vector<unsigned short int>& data, unsigned short int nullVal, rowNum firstRow=0, rowNum numRows=0);
void readColumnNull(string name, unsigned short int& data, unsigned short int nullVal, rowNum rowIdx);

void readColumn(string name, unsigned short int *data, rowNum firstRow=0, rowNum numRows=0)
{readColumnNull(name, data, 0, firstRow, numRows);};
void readColumn(string name, valarray<unsigned short int>& data, rowNum firstRow=0, rowNum numRows=0)
{readColumnNull(name, data, 0, firstRow, numRows);};
void readColumn(string name, vector<unsigned short int>& data, rowNum firstRow=0, rowNum numRows=0)
{readColumnNull(name, data, 0, firstRow, numRows);};
void readColumn(string name, unsigned short int& data, rowNum row)
{readColumnNull(name, data, 0, row);}

void readVectorColumnNull(string name, unsigned short int *data, unsigned short int nullVal, rowNum firstRow=0, rowNum numRows=0);
void readVectorColumnNull(string name, valarray<unsigned short int>& data, unsigned short int nullVal, rowNum firstRow=0, rowNum numRows=0);
void readVectorColumnNull(string name, vector<unsigned short int>& data, unsigned short int nullVal, rowNum firstRow=0, rowNum numRows=0);

void readVectorColumn(string name, unsigned short int *data, rowNum firstRow=0, rowNum numRows=0)
{readVectorColumnNull(name, data, 0, firstRow, numRows);};
void readVectorColumn(string name, valarray<unsigned short int>& data, rowNum firstRow=0, rowNum numRows=0)
{readVectorColumnNull(name, data, 0, firstRow, numRows);};
void readVectorColumn(string name, vector<unsigned short int>& data, rowNum firstRow=0, rowNum numRows=0)
{readVectorColumnNull(name, data, 0, firstRow, numRows);};
void readVariableVectorColumn(string name, unsigned short int *data, rowNum rowIdx, LONGLONG nels);
void readVariableVectorColumn(string name, valarray<unsigned short int>& data, rowNum rowIdx);
void readVariableVectorColumn(string name, vector<unsigned short int>& data, rowNum rowIdx);
void writeColumnNull(string name, unsigned short int *data, unsigned short int nullVal, rowNum firstRow=0, rowNum numRows=0);
void writeColumnNull(string name, valarray<unsigned short int>& data, unsigned short int nullVal, rowNum firstRow=0, rowNum numRows=0);
void writeColumnNull(string name, vector<unsigned short int>& data, unsigned short int nullVal, rowNum firstRow=0, rowNum numRows=0);
void writeColumnNull(string name, unsigned short int data, unsigned short int nullVal, rowNum firstRow);

void writeColumn(string name, unsigned short int *data, rowNum firstRow=0, rowNum numRows=0);
void writeColumn(string name, valarray<unsigned short int>& data, rowNum firstRow=0, rowNum numRows=0);
void writeColumn(string name, vector<unsigned short int>& data, rowNum firstRow=0,rowNum numRows=0);
void writeColumn(string name, unsigned short int data, rowNum firstRow);

void writeVectorColumnNull(string name, unsigned short int *data, unsigned short int nullVal, rowNum firstRow=0, rowNum numRows=0);
void writeVectorColumnNull(string name, valarray<unsigned short int>& data, unsigned short int nullVal, rowNum firstRow=0, rowNum numRows=0);
void writeVectorColumnNull(string name, vector<unsigned short int>& data, unsigned short int nullVal, rowNum firstRow=0, rowNum numRows=0);

void writeVectorColumn(string name, unsigned short int *data, rowNum firstRow=0, rowNum numRows=0);
void writeVectorColumn(string name, valarray<unsigned short int>& data, rowNum firstRow=0, rowNum numRows=0);
void writeVectorColumn(string name, vector<unsigned short int>& data, rowNum firstRow=0, rowNum numRows=0);
void readColumnNull(string name, float *data, float nullVal, rowNum firstRow=0, rowNum numRows=0);
void readColumnNull(string name, valarray<float>& data, float nullVal, rowNum firstRow=0, rowNum numRows=0);
void readColumnNull(string name, vector<float>& data, float nullVal, rowNum firstRow=0, rowNum numRows=0);
void readColumnNull(string name, float& data, float nullVal, rowNum rowIdx);

void readColumn(string name, float *data, rowNum firstRow=0, rowNum numRows=0)
{readColumnNull(name, data, 0.0f, firstRow, numRows);};
void readColumn(string name, valarray<float>& data, rowNum firstRow=0, rowNum numRows=0)
{readColumnNull(name, data, 0.0f, firstRow, numRows);};
void readColumn(string name, vector<float>& data, rowNum firstRow=0, rowNum numRows=0)
{readColumnNull(name, data, 0.0f, firstRow, numRows);};
void readColumn(string name, float& data, rowNum row)
{readColumnNull(name, data, 0.0f, row);}

void readVectorColumnNull(string name, float *data, float nullVal, rowNum firstRow=0, rowNum numRows=0);
void readVectorColumnNull(string name, valarray<float>& data, float nullVal, rowNum firstRow=0, rowNum numRows=0);
void readVectorColumnNull(string name, vector<float>& data, float nullVal, rowNum firstRow=0, rowNum numRows=0);

void readVectorColumn(string name, float *data, rowNum firstRow=0, rowNum numRows=0)
{readVectorColumnNull(name, data, 0.0f, firstRow, numRows);};
void readVectorColumn(string name, valarray<float>& data, rowNum firstRow=0, rowNum numRows=0)
{readVectorColumnNull(name, data, 0.0f, firstRow, numRows);};
void readVectorColumn(string name, vector<float>& data, rowNum firstRow=0, rowNum numRows=0)
{readVectorColumnNull(name, data, 0.0f, firstRow, numRows);};
void readVariableVectorColumn(string name, float *data, rowNum rowIdx, LONGLONG nels);
void readVariableVectorColumn(string name, valarray<float>& data, rowNum rowIdx);
void readVariableVectorColumn(string name, vector<float>& data, rowNum rowIdx);
void writeColumnNull(string name, float *data, float nullVal, rowNum firstRow=0, rowNum numRows=0);
void writeColumnNull(string name, valarray<float>& data, float nullVal, rowNum firstRow=0, rowNum numRows=0);
void writeColumnNull(string name, vector<float>& data, float nullVal, rowNum firstRow=0, rowNum numRows=0);
void writeColumnNull(string name, float data, float nullVal, rowNum firstRow);

void writeColumn(string name, float *data, rowNum firstRow=0, rowNum numRows=0);
void writeColumn(string name, valarray<float>& data, rowNum firstRow=0, rowNum numRows=0);
void writeColumn(string name, vector<float>& data, rowNum firstRow=0,rowNum numRows=0);
void writeColumn(string name, float data, rowNum firstRow);

void writeVectorColumnNull(string name, float *data, float nullVal, rowNum firstRow=0, rowNum numRows=0);
void writeVectorColumnNull(string name, valarray<float>& data, float nullVal, rowNum firstRow=0, rowNum numRows=0);
void writeVectorColumnNull(string name, vector<float>& data, float nullVal, rowNum firstRow=0, rowNum numRows=0);

void writeVectorColumn(string name, float *data, rowNum firstRow=0, rowNum numRows=0);
void writeVectorColumn(string name, valarray<float>& data, rowNum firstRow=0, rowNum numRows=0);
void writeVectorColumn(string name, vector<float>& data, rowNum firstRow=0, rowNum numRows=0);
void readColumnNull(string name, LONGLONG *data, LONGLONG nullVal, rowNum firstRow=0, rowNum numRows=0);
void readColumnNull(string name, valarray<LONGLONG>& data, LONGLONG nullVal, rowNum firstRow=0, rowNum numRows=0);
void readColumnNull(string name, vector<LONGLONG>& data, LONGLONG nullVal, rowNum firstRow=0, rowNum numRows=0);
void readColumnNull(string name, LONGLONG& data, LONGLONG nullVal, rowNum rowIdx);

void readColumn(string name, LONGLONG *data, rowNum firstRow=0, rowNum numRows=0)
{readColumnNull(name, data, 0, firstRow, numRows);};
void readColumn(string name, valarray<LONGLONG>& data, rowNum firstRow=0, rowNum numRows=0)
{readColumnNull(name, data, 0, firstRow, numRows);};
void readColumn(string name, vector<LONGLONG>& data, rowNum firstRow=0, rowNum numRows=0)
{readColumnNull(name, data, 0, firstRow, numRows);};
void readColumn(string name, LONGLONG& data, rowNum row)
{readColumnNull(name, data, 0, row);}

void readVectorColumnNull(string name, LONGLONG *data, LONGLONG nullVal, rowNum firstRow=0, rowNum numRows=0);
void readVectorColumnNull(string name, valarray<LONGLONG>& data, LONGLONG nullVal, rowNum firstRow=0, rowNum numRows=0);
void readVectorColumnNull(string name, vector<LONGLONG>& data, LONGLONG nullVal, rowNum firstRow=0, rowNum numRows=0);

void readVectorColumn(string name, LONGLONG *data, rowNum firstRow=0, rowNum numRows=0)
{readVectorColumnNull(name, data, 0, firstRow, numRows);};
void readVectorColumn(string name, valarray<LONGLONG>& data, rowNum firstRow=0, rowNum numRows=0)
{readVectorColumnNull(name, data, 0, firstRow, numRows);};
void readVectorColumn(string name, vector<LONGLONG>& data, rowNum firstRow=0, rowNum numRows=0)
{readVectorColumnNull(name, data, 0, firstRow, numRows);};
void readVariableVectorColumn(string name, LONGLONG *data, rowNum rowIdx, LONGLONG nels);
void readVariableVectorColumn(string name, valarray<LONGLONG>& data, rowNum rowIdx);
void readVariableVectorColumn(string name, vector<LONGLONG>& data, rowNum rowIdx);
void writeColumnNull(string name, LONGLONG *data, LONGLONG nullVal, rowNum firstRow=0, rowNum numRows=0);
void writeColumnNull(string name, valarray<LONGLONG>& data, LONGLONG nullVal, rowNum firstRow=0, rowNum numRows=0);
void writeColumnNull(string name, vector<LONGLONG>& data, LONGLONG nullVal, rowNum firstRow=0, rowNum numRows=0);
void writeColumnNull(string name, LONGLONG data, LONGLONG nullVal, rowNum firstRow);

void writeColumn(string name, LONGLONG *data, rowNum firstRow=0, rowNum numRows=0);
void writeColumn(string name, valarray<LONGLONG>& data, rowNum firstRow=0, rowNum numRows=0);
void writeColumn(string name, vector<LONGLONG>& data, rowNum firstRow=0,rowNum numRows=0);
void writeColumn(string name, LONGLONG data, rowNum firstRow);

void writeVectorColumnNull(string name, LONGLONG *data, LONGLONG nullVal, rowNum firstRow=0, rowNum numRows=0);
void writeVectorColumnNull(string name, valarray<LONGLONG>& data, LONGLONG nullVal, rowNum firstRow=0, rowNum numRows=0);
void writeVectorColumnNull(string name, vector<LONGLONG>& data, LONGLONG nullVal, rowNum firstRow=0, rowNum numRows=0);

void writeVectorColumn(string name, LONGLONG *data, rowNum firstRow=0, rowNum numRows=0);
void writeVectorColumn(string name, valarray<LONGLONG>& data, rowNum firstRow=0, rowNum numRows=0);
void writeVectorColumn(string name, vector<LONGLONG>& data, rowNum firstRow=0, rowNum numRows=0);
void readColumnNull(string name, long *data, long nullVal, rowNum firstRow=0, rowNum numRows=0);
void readColumnNull(string name, valarray<long>& data, long nullVal, rowNum firstRow=0, rowNum numRows=0);
void readColumnNull(string name, vector<long>& data, long nullVal, rowNum firstRow=0, rowNum numRows=0);
void readColumnNull(string name, long& data, long nullVal, rowNum rowIdx);

void readColumn(string name, long *data, rowNum firstRow=0, rowNum numRows=0)
{readColumnNull(name, data, 0, firstRow, numRows);};
void readColumn(string name, valarray<long>& data, rowNum firstRow=0, rowNum numRows=0)
{readColumnNull(name, data, 0, firstRow, numRows);};
void readColumn(string name, vector<long>& data, rowNum firstRow=0, rowNum numRows=0)
{readColumnNull(name, data, 0, firstRow, numRows);};
void readColumn(string name, long& data, rowNum row)
{readColumnNull(name, data, 0, row);}

void readVectorColumnNull(string name, long *data, long nullVal, rowNum firstRow=0, rowNum numRows=0);
void readVectorColumnNull(string name, valarray<long>& data, long nullVal, rowNum firstRow=0, rowNum numRows=0);
void readVectorColumnNull(string name, vector<long>& data, long nullVal, rowNum firstRow=0, rowNum numRows=0);

void readVectorColumn(string name, long *data, rowNum firstRow=0, rowNum numRows=0)
{readVectorColumnNull(name, data, 0, firstRow, numRows);};
void readVectorColumn(string name, valarray<long>& data, rowNum firstRow=0, rowNum numRows=0)
{readVectorColumnNull(name, data, 0, firstRow, numRows);};
void readVectorColumn(string name, vector<long>& data, rowNum firstRow=0, rowNum numRows=0)
{readVectorColumnNull(name, data, 0, firstRow, numRows);};
void readVariableVectorColumn(string name, long *data, rowNum rowIdx, LONGLONG nels);
void readVariableVectorColumn(string name, valarray<long>& data, rowNum rowIdx);
void readVariableVectorColumn(string name, vector<long>& data, rowNum rowIdx);
void writeColumnNull(string name, long *data, long nullVal, rowNum firstRow=0, rowNum numRows=0);
void writeColumnNull(string name, valarray<long>& data, long nullVal, rowNum firstRow=0, rowNum numRows=0);
void writeColumnNull(string name, vector<long>& data, long nullVal, rowNum firstRow=0, rowNum numRows=0);
void writeColumnNull(string name, long data, long nullVal, rowNum firstRow);

void writeColumn(string name, long *data, rowNum firstRow=0, rowNum numRows=0);
void writeColumn(string name, valarray<long>& data, rowNum firstRow=0, rowNum numRows=0);
void writeColumn(string name, vector<long>& data, rowNum firstRow=0,rowNum numRows=0);
void writeColumn(string name, long data, rowNum firstRow);

void writeVectorColumnNull(string name, long *data, long nullVal, rowNum firstRow=0, rowNum numRows=0);
void writeVectorColumnNull(string name, valarray<long>& data, long nullVal, rowNum firstRow=0, rowNum numRows=0);
void writeVectorColumnNull(string name, vector<long>& data, long nullVal, rowNum firstRow=0, rowNum numRows=0);

void writeVectorColumn(string name, long *data, rowNum firstRow=0, rowNum numRows=0);
void writeVectorColumn(string name, valarray<long>& data, rowNum firstRow=0, rowNum numRows=0);
void writeVectorColumn(string name, vector<long>& data, rowNum firstRow=0, rowNum numRows=0);
void readColumnNull(string name, short int *data, short int nullVal, rowNum firstRow=0, rowNum numRows=0);
void readColumnNull(string name, valarray<short int>& data, short int nullVal, rowNum firstRow=0, rowNum numRows=0);
void readColumnNull(string name, vector<short int>& data, short int nullVal, rowNum firstRow=0, rowNum numRows=0);
void readColumnNull(string name, short int& data, short int nullVal, rowNum rowIdx);

void readColumn(string name, short int *data, rowNum firstRow=0, rowNum numRows=0)
{readColumnNull(name, data, 0, firstRow, numRows);};
void readColumn(string name, valarray<short int>& data, rowNum firstRow=0, rowNum numRows=0)
{readColumnNull(name, data, 0, firstRow, numRows);};
void readColumn(string name, vector<short int>& data, rowNum firstRow=0, rowNum numRows=0)
{readColumnNull(name, data, 0, firstRow, numRows);};
void readColumn(string name, short int& data, rowNum row)
{readColumnNull(name, data, 0, row);}

void readVectorColumnNull(string name, short int *data, short int nullVal, rowNum firstRow=0, rowNum numRows=0);
void readVectorColumnNull(string name, valarray<short int>& data, short int nullVal, rowNum firstRow=0, rowNum numRows=0);
void readVectorColumnNull(string name, vector<short int>& data, short int nullVal, rowNum firstRow=0, rowNum numRows=0);

void readVectorColumn(string name, short int *data, rowNum firstRow=0, rowNum numRows=0)
{readVectorColumnNull(name, data, 0, firstRow, numRows);};
void readVectorColumn(string name, valarray<short int>& data, rowNum firstRow=0, rowNum numRows=0)
{readVectorColumnNull(name, data, 0, firstRow, numRows);};
void readVectorColumn(string name, vector<short int>& data, rowNum firstRow=0, rowNum numRows=0)
{readVectorColumnNull(name, data, 0, firstRow, numRows);};
void readVariableVectorColumn(string name, short int *data, rowNum rowIdx, LONGLONG nels);
void readVariableVectorColumn(string name, valarray<short int>& data, rowNum rowIdx);
void readVariableVectorColumn(string name, vector<short int>& data, rowNum rowIdx);
void writeColumnNull(string name, short int *data, short int nullVal, rowNum firstRow=0, rowNum numRows=0);
void writeColumnNull(string name, valarray<short int>& data, short int nullVal, rowNum firstRow=0, rowNum numRows=0);
void writeColumnNull(string name, vector<short int>& data, short int nullVal, rowNum firstRow=0, rowNum numRows=0);
void writeColumnNull(string name, short int data, short int nullVal, rowNum firstRow);

void writeColumn(string name, short int *data, rowNum firstRow=0, rowNum numRows=0);
void writeColumn(string name, valarray<short int>& data, rowNum firstRow=0, rowNum numRows=0);
void writeColumn(string name, vector<short int>& data, rowNum firstRow=0,rowNum numRows=0);
void writeColumn(string name, short int data, rowNum firstRow);

void writeVectorColumnNull(string name, short int *data, short int nullVal, rowNum firstRow=0, rowNum numRows=0);
void writeVectorColumnNull(string name, valarray<short int>& data, short int nullVal, rowNum firstRow=0, rowNum numRows=0);
void writeVectorColumnNull(string name, vector<short int>& data, short int nullVal, rowNum firstRow=0, rowNum numRows=0);

void writeVectorColumn(string name, short int *data, rowNum firstRow=0, rowNum numRows=0);
void writeVectorColumn(string name, valarray<short int>& data, rowNum firstRow=0, rowNum numRows=0);
void writeVectorColumn(string name, vector<short int>& data, rowNum firstRow=0, rowNum numRows=0);
