// fits++.h
//
// rgw

#ifndef _FITSPLUSPLUS_H_
#define _FITSPLUSPLUS_H_

#include <string>
#include <vector>
#include <valarray>
#include <exception>

using namespace std;

#include "fitsio.h"

// Use a namespace
namespace fitspp {

  //
  typedef enum {readOnly=READONLY, readWrite=READWRITE} accessMode;
  typedef enum {imageHDU=IMAGE_HDU, binaryTable=BINARY_TBL,
		asciiTable=ASCII_TBL} hduType;
  typedef enum {int8Img=BYTE_IMG, int16Img=SHORT_IMG, int32Img=LONG_IMG,
		int64Img=LONGLONG_IMG, floatImg=FLOAT_IMG,
		unsignedInt32Img=USHORT_IMG, unsignedInt64Img=ULONG_IMG,
		doubleImg=DOUBLE_IMG} imgDataType;
  typedef int colNum;
  typedef LONGLONG rowNum;

  //
  const bool verbose=false;

  //
  class FITSFile;
  class FITSHDU;

  // Exception class
  class FITSException {
  private:
  protected:
    friend class FITSFile;
    friend class FITSTable;
    friend class FITSHDULoc;
    friend class FITSKeyword;
    friend class FITSHDU;
    friend class FITSTableDef;
    friend class FITSImage;
    friend class FITSArray;
    FITSException(int errcode, string comment="");
  public:
    string fitsMessage, userMessage;
    int status;
  };

  // FITS HDU locator
  class FITSHDULoc {
  private:
  protected:
    friend class FITSFile;
    friend class FITSTable;
    friend class FITSHDU;
    FITSFile *parent;
    string name;
    int num, type;
    FITSHDULoc(FITSFile* parent, string name, hduType type, int num) :
      parent(parent), name(name), type(type), num(num) {};
  public:
    fitsfile *filePtr(void);
    void makeCurrent(void);
  };

  // FITS keyword
  class FITSKeyword {
  private:
    FITSHDULoc *loc;
    string name;
  protected:
    friend class FITSHDU;
    FITSKeyword(FITSHDULoc *loc, string name) : loc(loc), name(name) {};
  public:
    operator int();
    operator long();
    operator float();
    operator double();
    operator bool();
    operator string();
  };
  
  // FITS table definition
  typedef struct {
    string name, type, unit;
  } FITSColDef;

  class FITSTableDef {
  protected:
    vector<FITSColDef> columnDefs;
    friend class FITSFile;
    friend class FITSTable;
    FITSTableDef(string colList);
  };

  // FITS HDU reference
  class FITSHDU {
  private:
  protected:
    friend class FITSFile;
    FITSHDULoc *loc;
    FITSHDU(FITSHDULoc *loc) : loc(loc) {};
    FITSHDULoc *copyHDU(FITSFile *dest, hduType type, string name="");
  public:
    FITSKeyword readKeyword(string name);
    string readKeywordComment(string name);
    void writeKeyword(string name, long value, string comment="");
    void writeKeyword(string name, unsigned long value, string comment="")
      {writeKeyword(name, (long)value, comment);};
    void writeKeyword(string name, int value, string comment="")
      {writeKeyword(name, (long)value, comment);};
    void writeKeyword(string name, unsigned int value, string comment="")
      {writeKeyword(name, (long)value, comment);};
    void writeKeyword(string name, short int value, string comment="")
      {writeKeyword(name, (long)value, comment);};
    void writeKeyword(string name, unsigned short int value, string comment="")
      {writeKeyword(name, (long)value, comment);};
    void writeKeyword(string name, char value, string comment="")
      {writeKeyword(name, (long)value, comment);};
    void writeKeyword(string name, unsigned char value, string comment="")
      {writeKeyword(name, (long)value, comment);};
    void writeKeyword(string name, float value, string comment="", int decimals=-9);
    void writeKeyword(string name, double value, string comment="", int decimals=-17);
    void writeKeyword(string name, string value, string comment="");


    bool hasKeyword(string name);
    void copyKeyword(string name, FITSHDU* other);
    void copyKeywords(FITSHDU* other);
    void readHeaderString(string& hdr, int& nkeys);
  };

  // FITS table HDU
  class FITSTable : public FITSHDU {
  private:
  protected:
    FITSTable(FITSHDULoc* loc) : FITSHDU(loc) {};
    colNum findColumn(string name);
    void rowsToRead(rowNum& firstRow, rowNum& numRows);
    long getColumnRepeat(string name);
    LONGLONG getVariableColumnLength(string name, rowNum rowIdx);
  public:
    long getNumRows(void);
    void addColumns(string def, bool force=false);
    void removeColumn(string name);
    void setColumnNullValue(string name, long value);
    bool hasColumn(string name);
    string getTableDef(void);

#include "fitstable_auto.h"
  };

  // FITS binary table HDU
  class FITSBinaryTable : public FITSTable {
  private:
  protected:
    FITSBinaryTable(FITSHDULoc* loc) : FITSTable(loc) {};
    friend class FITSFile;
  public:
    FITSBinaryTable *copyTable(FITSFile *dest, string name)
      {return new FITSBinaryTable(copyHDU(dest, binaryTable, name));};
  };

  // FITS ASCII table HDU
  class FITSASCIITable : public FITSTable {
  private:
  protected:
    FITSASCIITable(FITSHDULoc* loc) : FITSTable(loc) {};
    friend class FITSFile;
  public:
    FITSASCIITable *copyTable(FITSFile *dest, string name)
      {return new FITSASCIITable(copyHDU(dest, asciiTable, name));};
  };

  // Helper routines to promote valarrays & vectors of shorter integers to long
  // (useful for some of the overloaded members of FITSImage and FITSFile)
  inline valarray<long> intToLongValarray(valarray<int>& in)
    {
      valarray<long> out(in.size());
      for(int idx=0; idx<in.size(); idx++)
	out[idx]=in[idx];
      return out;
    }
  inline vector<long> intToLongVector(vector<int>& in)
    {
      vector<long> out(in.size());
      for(int idx=0; idx<in.size(); idx++)
	out[idx]=in[idx];
      return out;
    }

  //
#include "fitsarray.h"

  // FITS image HDU
  class FITSImage : public FITSHDU {
  private:
  protected:
    FITSImage(FITSHDULoc* loc) : FITSHDU(loc) {};
    friend class FITSFile;
  public:
    FITSImage *copyImage(FITSFile *dest, string name)
      {return new FITSImage(copyHDU(dest, imageHDU, name));};
    void getImageSize(valarray<int>& size);
    void getImageSize(valarray<long>& size);
    imgDataType getImageType(void);
    imgDataType getImageEquivalentType(void);
    void resizeImage(valarray<long> size);
    void resizeImage(valarray<int> size)
      {resizeImage(intToLongValarray(size));};

#include "fitsimage_auto.h"
  };

  // Class to represent an open FITS file
  class FITSFile {
  private:
    FITSFile();
    vector<FITSHDULoc*> hduList;
    string name;
  protected:
    fitsfile *fptr;
    FITSHDULoc* findHDU(string name);
    FITSHDULoc* findHDU(int idx);
    FITSHDU* primaryHDU(void);
    friend class FITSHDU;
    friend class FITSTable;
    friend class FITSHDULoc;
  public:
    ~FITSFile();
    static FITSFile* openFile(string filename, accessMode mode=readOnly);
    static FITSFile* createFile(string filename);
    static bool exists(string filename);
    void closeFile(void);
    FITSImage* addImage(string name, imgDataType type, int naxes, long axes[]);
    FITSImage* addImage(string name, imgDataType type, int naxes, int axes[]);
    FITSImage* addImage(string name, imgDataType type, valarray<long> axes)
      {return addImage(name, type, axes.size(), &axes[0]);};
    FITSImage* addImage(string name, imgDataType type, valarray<int> axes)
      {return addImage(name, type, intToLongValarray(axes));};
    FITSImage* addImage(string name, imgDataType type, vector<long> axes)
      {return addImage(name, type, axes.size(), &axes[0]);};
    FITSImage* addImage(string name, imgDataType type, vector<int> axes)
       {return addImage(name, type, intToLongVector(axes));};
    void addEmptyPrimary(void);
    FITSImage* findImage(string name);
    FITSImage* findPrimary(void)
      {return findImage("_PRIMARY");};
    FITSBinaryTable* addBinaryTable(string name, string def, long numRows=0);
    FITSBinaryTable* findBinaryTable(string name);
    FITSBinaryTable* findBinaryTable(int idx);
    bool hasTable(string tableName)
    {return hasHDU(tableName);};
    bool hasImage(string imageName)
    {return hasHDU(imageName);};
    bool hasHDU(string hduName);
    void copyPrimaryKeywords(FITSFile* other)
    {return copyKeywords(other->primaryHDU());};
    void copyBinaryTable(FITSBinaryTable* srctbl, string name);
    void copyBinaryTable(FITSFile *srcFile, string srcName, string destname="");
    
    // Methods to read/write keywords to the primary header
    FITSKeyword readKeyword(string name)
      {return primaryHDU()->readKeyword(name);};
    string readKeywordComment(string name)
      {return primaryHDU()->readKeywordComment(name);};
    void writeKeyword(string name, long value, string comment="")
      {primaryHDU()->writeKeyword(name, value, comment);};
    void writeKeyword(string name, unsigned long value, string comment="")
      {primaryHDU()->writeKeyword(name, (long)value, comment);};
    void writeKeyword(string name, int value, string comment="")
      {primaryHDU()->writeKeyword(name, (long)value, comment);};
    void writeKeyword(string name, unsigned int value, string comment="")
      {primaryHDU()->writeKeyword(name, (long)value, comment);};
    void writeKeyword(string name, short int value, string comment="")
      {primaryHDU()->writeKeyword(name, (long)value, comment);};
    void writeKeyword(string name, unsigned short int value, string comment="")
      {primaryHDU()->writeKeyword(name, (long)value, comment);};
    void writeKeyword(string name, char value, string comment="")
      {primaryHDU()->writeKeyword(name, (long)value, comment);};
    void writeKeyword(string name, unsigned char value, string comment="")
      {primaryHDU()->writeKeyword(name, (long)value, comment);};
    void writeKeyword(string name, float value, string comment="", int decimals=-9)
      {primaryHDU()->writeKeyword(name, value, comment, decimals);};
    void writeKeyword(string name, double value, string comment="", int decimals=-17)
      {primaryHDU()->writeKeyword(name, value, comment, decimals);};
    void writeKeyword(string name, string value, string comment="")
      {primaryHDU()->writeKeyword(name, value, comment);};
    void copyKeyword(string name, FITSHDU* other)
      {primaryHDU()->copyKeyword(name, other);};
    void copyKeyword(string name, FITSFile* other)
      {primaryHDU()->copyKeyword(name, other->primaryHDU());};
    void copyKeywords(FITSHDU* other)
      {primaryHDU()->copyKeywords(other);};
    void copyKeywords(FITSFile* other)
    {primaryHDU()->copyKeywords(other->primaryHDU());};
    bool hasKeyword(string name)
    {return primaryHDU()->hasKeyword(name);};
  };

};

#endif

