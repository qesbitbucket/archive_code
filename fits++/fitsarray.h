#ifndef _FITSARRAY_H_
#define _FITSARRAY_H_

#include <valarray>

template<class T> class FITSArray2D {
protected:
  friend class FITSImage;
  std::valarray<T> data;
  std::valarray<long> size;

public:
  FITSArray2D() {init(0, 0);}
  FITSArray2D(long ix, long iy) {init(ix, iy);}
  FITSArray2D(std::valarray<long> sz) {init(sz[0], sz[1]);}
  ~FITSArray2D() {init(0, 0);}

  // Set all array elements to a given value
  void set(T value) {
    data=value;
  }

  // Initialiser
  inline void init(long ix, long iy) {
    size.resize(2);
    size[0]=ix; size[1]=iy;
    data.resize(size[0]*size[1]);
  }
  inline void init(long ix, long iy, float val) { 
    init(ix, iy);
    data=val;
  }
 
  // Operator overloading
  inline T& operator()(long i, long j) {
    return data[i + size[0]*j];
  }
  inline T& operator[](long i) {
    return data[i];
  }
  inline void operator=(T val) {
    data=val;
  }

  // Resize array
  inline void resize(long w, long h) {
    init(w, h);
  }
  inline void resize(valarray<long> sz) {
    init(sz[0], sz[1]);
  }

  // Fetch the size of the array
  inline valarray<long> axes(void) {
    std::valarray<long> sz(2);
    sz=size;
    return sz;
  }
  inline long nels(void) {
    return data.size();
  }
  inline long width(void) {
    return size[0];
  }
  inline long height(void) {
    return size[1];
  }
};

#endif
