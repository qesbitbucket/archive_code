#!/usr/bin/perl
#
# Create code for the various data-type-dependent that read and write
# columns in tables.
#
# The standard C++ template functionality isn't suitable for this, as the
# name of the underlying CFITSIO routine which needs to be called depends
# on the data type of the column data to be written.
#
# Copyright Richard West 2009

use English;
use strict qw(vars refs subs);

my %typemap=('float' => 'flt', 'double' => 'dbl',
	     'unsigned char' => 'byt',
	     'short int' => 'sht', 'unsigned short int' => 'usht',
	     'int' => 'int', 'unsigned int' => 'uint',
	     'long' => 'lng', 'unsigned long' => 'ulng',
	     'LONGLONG' => 'lnglng',
	     );
my %nullmap=('float' => '0.0f', 'double' => '0.0',
	     'unsigned char' => '0',
	     'short int' => '0', 'unsigned short int' => '0',
	     'int' => '0', 'unsigned int' => '0',
	     'long' => '0', 'unsigned long' => '0',
	     'LONGLONG' => 0,
	     );
my %codemap=('float' => 'TFLOAT', 'double' => 'TDOUBLE',
	     'unsigned char' => 'TBYTE',
	     'short int' => 'TSHORT', 'unsigned short int' => 'TUSHORT',
	     'int' => 'TINT', 'unsigned int' => 'TUINT',
	     'long' => 'TLONG', 'unsigned long' => 'TULONG',
	     'LONGLONG' => 'TLONGLONG',
	     );

open(CFILE, ">fitsimage_auto.C");
open(HFILE, ">fitsimage_auto.h");

# Preamble 
print CFILE "

";

#
foreach my $type (keys %typemap) {
    my $shttype=$typemap{$type};
    my $nullval=$nullmap{$type};
    my $code=$codemap{$type};

# Code to write full images and slices
    print CFILE "
  // Write a whole image
  void FITSImage::writeImageNull(long nels, $type *data, $type nullVal)
  {
    loc->makeCurrent();
    int status=0;
    fits_write_imgnull_${shttype}(loc->filePtr(), 0, 1, nels, data, nullVal, &status);
    if(status) throw new FITSException(status, \"writing image data\");
  }

  void FITSImage::writeImage(long nels, $type *data)
  {
    loc->makeCurrent();
    int status=0;
    fits_write_img_${shttype}(loc->filePtr(), 0, 1, nels, data, &status);
    if(status) throw new FITSException(status, \"writing image data\");
  }

  // Write a slice of an image
  void FITSImage::writeImageSliceNull(long nels, $type* data, long* fpixel, $type nullVal)
  {
    loc->makeCurrent();
    int status=0;
    fits_write_pixnull(loc->filePtr(), $code, fpixel, nels, data, &nullVal, &status);
    if(status) throw new FITSException(status, \"writing image data\");
  }

";
    print HFILE "
    void writeImageNull(long nels, $type* data, $type nullVal);
    void writeImageNull(valarray<$type>& data, $type nullVal)
      {writeImageNull(data.size(), &data[0], nullVal);};
    void writeImageNull(vector<$type>& data, $type nullVal)
      {writeImageNull(data.size(), &data[0], nullVal);};
    void writeImage(long nels, $type* data);
    void writeImage(valarray<$type>& data)
      {writeImage(data.size(), &data[0]);};
    void writeImage(vector<$type>& data)
      {writeImage(data.size(), &data[0]);};
    void writeImage(FITSArray2D<$type>& arr)
      {writeImage(arr.data.size(), &arr.data[0]);};
    void writeImage(FITSArray2D<$type> *arr)
      {writeImage(arr->data.size(), &arr->data[0]);};

    void writeImageSliceNull(long nels, $type* data, long* fpixel, $type nullVal);
    void writeImageSliceNull(valarray<$type>& data, valarray<long> fpixel, $type nullVal)
      {writeImageSliceNull(data.size(), &data[0], &fpixel[0], nullVal);};
    void writeImageSliceNull(valarray<$type>& data, valarray<int> fpixel, $type nullVal)
      {writeImageSliceNull(data, intToLongValarray(fpixel), nullVal);};
    void writeImageSliceNull(FITSArray2D<$type>& arr, valarray<int> fpixel, $type nullVal)
      {writeImageSliceNull(arr.data, intToLongValarray(fpixel), nullVal);};
    void writeImageSliceNull(FITSArray2D<$type> *arr, valarray<int> fpixel, $type nullVal)
      {writeImageSliceNull(arr->data, intToLongValarray(fpixel), nullVal);};

    void writeImageSlice($type* data, long* fpixel, long *lpixel);
    void writeImageSlice(valarray<$type>& data, valarray<long> fpixel)
      {writeImageSliceNull(data.size(), &data[0], &fpixel[0], $nullval);};
    void writeImageSlice(valarray<$type>& data, valarray<int> fpixel)
      {writeImageSlice(data, intToLongValarray(fpixel));};
    void writeImageSlice(FITSArray2D<$type>& arr, valarray<int> fpixel)
      {writeImageSlice(arr.data, intToLongValarray(fpixel));};
    void writeImageSlice(FITSArray2D<$type>* arr, valarray<int> fpixel)
      {writeImageSlice(arr->data, intToLongValarray(fpixel));};
";

    # Code to read images and slices
    print CFILE "

  // Read a whole image
  void FITSImage::readImageNull(long nels, $type *data, $type nullVal)
  {
    loc->makeCurrent();
    int status=0, dummy;
    fits_read_img_${shttype}(loc->filePtr(), 0, 1, nels, nullVal, data, &dummy,
		      &status);
    if(status) throw new FITSException(status, \"reading image data\");
  }

  void FITSImage::readImageNull(valarray<$type>& data, $type nullVal)
  {
    loc->makeCurrent();
    valarray<long> axes;
    getImageSize(axes);
    long nels=1;
    for(int idx=0; idx<axes.size(); idx++)
      nels*=axes[idx];
    data.resize(nels);
    readImageNull(nels, &data[0], nullVal);
  }

  void FITSImage::readImageNull(FITSArray2D<$type>& arr, $type nullVal)
  {
    loc->makeCurrent();
    valarray<long> axes;
    getImageSize(axes);
    arr.resize(axes);
    readImageNull(arr.data.size(), &arr.data[0], nullVal);
  }

  void FITSImage::readImageNull(FITSArray2D<$type> *arr, $type nullVal)
  {
    loc->makeCurrent();
    valarray<long> axes;
    getImageSize(axes);
    arr->resize(axes);
    readImageNull(arr->data.size(), &arr->data[0], nullVal);
  }

  void FITSImage::readImageNull(vector<$type>& data, $type nullVal)
  {
    loc->makeCurrent();
    valarray<long> axes;
    getImageSize(axes);
    long nels=1;
    for(int idx=0; idx<axes.size(); idx++)
      nels*=axes[idx];
    data.resize(nels);
    readImageNull(nels, &data[0], nullVal);
  }

  // Read an image slice
  void FITSImage::readImageSliceNull(long nels, $type *data, long *fpixel, $type nullVal)
  {
    loc->makeCurrent();
    int status=0, dummy;
    fits_read_pix(loc->filePtr(), $code, fpixel, nels, &nullVal, data, &dummy, &status);
    if(status) throw new FITSException(status, \"reading image subset\");
  }
    
  void FITSImage::readImageSliceNull(long nels, valarray<$type>& data, valarray<long> fpixel,
				     $type nullVal)
  {
    loc->makeCurrent();
    data.resize(nels);
    readImageSliceNull(nels, &data[0], &fpixel[0], nullVal);
  }

  void FITSImage::readImageSliceNull(long nels, vector<$type>& data, valarray<long> fpixel,
				     $type nullVal)
  {
    loc->makeCurrent();
    data.resize(nels);
    readImageSliceNull(nels, &data[0], &fpixel[0], nullVal);
  }


";
    print HFILE "

    void readImageNull(long nels, $type *data, $type nullVal);
    void readImage(long nels, $type *data) {readImageNull(nels, data, $nullval);};
    void readImageNull(valarray<$type>& data, $type nullVal);
    void readImage(valarray<$type>& data) {readImageNull(data, $nullval);};
    void readImageNull(vector<$type>& data, $type nullVal);
    void readImage(vector<$type>& data) {readImageNull(data, $nullval);};
    void readImageNull(FITSArray2D<$type>& arr, $type nullVal);
    void readImage(FITSArray2D<$type>& arr) {readImageNull(arr, $nullval);};
    void readImageNull(FITSArray2D<$type> *arr, $type nullVal);
    void readImage(FITSArray2D<$type> *arr) {readImageNull(arr, $nullval);};

    void readImageSliceNull(long nels, $type *data, long *fpixel, $type nullVal);
    void readImageSliceNull(long nels, valarray<$type>& data, valarray<long> fpixel, $type nullVal);
    void readImageSliceNull(long nels, vector<$type>& data, valarray<long> fpixel, $type nullVal);

    void readImageSlice(long nels, $type *data, long *fpixel)
      {readImageSliceNull(nels, data, fpixel, $nullval);};
    void readImageSlice(long nels, valarray<$type>& data, valarray<long> fpixel)
      {readImageSliceNull(nels, data, fpixel, $nullval);};
    void readImageSlice(long nels, vector<$type>& data, valarray<long> fpixel)
      {readImageSliceNull(nels, data, fpixel, $nullval);};

";


}

#
close(CFILE);
close(HFILE);
