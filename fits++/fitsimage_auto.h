
    void writeImageNull(long nels, int* data, int nullVal);
    void writeImageNull(valarray<int>& data, int nullVal)
      {writeImageNull(data.size(), &data[0], nullVal);};
    void writeImageNull(vector<int>& data, int nullVal)
      {writeImageNull(data.size(), &data[0], nullVal);};
    void writeImage(long nels, int* data);
    void writeImage(valarray<int>& data)
      {writeImage(data.size(), &data[0]);};
    void writeImage(vector<int>& data)
      {writeImage(data.size(), &data[0]);};
    void writeImage(FITSArray2D<int>& arr)
      {writeImage(arr.data.size(), &arr.data[0]);};
    void writeImage(FITSArray2D<int> *arr)
      {writeImage(arr->data.size(), &arr->data[0]);};

    void writeImageSliceNull(long nels, int* data, long* fpixel, int nullVal);
    void writeImageSliceNull(valarray<int>& data, valarray<long> fpixel, int nullVal)
      {writeImageSliceNull(data.size(), &data[0], &fpixel[0], nullVal);};
    void writeImageSliceNull(valarray<int>& data, valarray<int> fpixel, int nullVal)
      {writeImageSliceNull(data, intToLongValarray(fpixel), nullVal);};
    void writeImageSliceNull(FITSArray2D<int>& arr, valarray<int> fpixel, int nullVal)
      {writeImageSliceNull(arr.data, intToLongValarray(fpixel), nullVal);};
    void writeImageSliceNull(FITSArray2D<int> *arr, valarray<int> fpixel, int nullVal)
      {writeImageSliceNull(arr->data, intToLongValarray(fpixel), nullVal);};

    void writeImageSlice(int* data, long* fpixel, long *lpixel);
    void writeImageSlice(valarray<int>& data, valarray<long> fpixel)
      {writeImageSliceNull(data.size(), &data[0], &fpixel[0], 0);};
    void writeImageSlice(valarray<int>& data, valarray<int> fpixel)
      {writeImageSlice(data, intToLongValarray(fpixel));};
    void writeImageSlice(FITSArray2D<int>& arr, valarray<int> fpixel)
      {writeImageSlice(arr.data, intToLongValarray(fpixel));};
    void writeImageSlice(FITSArray2D<int>* arr, valarray<int> fpixel)
      {writeImageSlice(arr->data, intToLongValarray(fpixel));};


    void readImageNull(long nels, int *data, int nullVal);
    void readImage(long nels, int *data) {readImageNull(nels, data, 0);};
    void readImageNull(valarray<int>& data, int nullVal);
    void readImage(valarray<int>& data) {readImageNull(data, 0);};
    void readImageNull(vector<int>& data, int nullVal);
    void readImage(vector<int>& data) {readImageNull(data, 0);};
    void readImageNull(FITSArray2D<int>& arr, int nullVal);
    void readImage(FITSArray2D<int>& arr) {readImageNull(arr, 0);};
    void readImageNull(FITSArray2D<int> *arr, int nullVal);
    void readImage(FITSArray2D<int> *arr) {readImageNull(arr, 0);};

    void readImageSliceNull(long nels, int *data, long *fpixel, int nullVal);
    void readImageSliceNull(long nels, valarray<int>& data, valarray<long> fpixel, int nullVal);
    void readImageSliceNull(long nels, vector<int>& data, valarray<long> fpixel, int nullVal);

    void readImageSlice(long nels, int *data, long *fpixel)
      {readImageSliceNull(nels, data, fpixel, 0);};
    void readImageSlice(long nels, valarray<int>& data, valarray<long> fpixel)
      {readImageSliceNull(nels, data, fpixel, 0);};
    void readImageSlice(long nels, vector<int>& data, valarray<long> fpixel)
      {readImageSliceNull(nels, data, fpixel, 0);};


    void writeImageNull(long nels, unsigned long* data, unsigned long nullVal);
    void writeImageNull(valarray<unsigned long>& data, unsigned long nullVal)
      {writeImageNull(data.size(), &data[0], nullVal);};
    void writeImageNull(vector<unsigned long>& data, unsigned long nullVal)
      {writeImageNull(data.size(), &data[0], nullVal);};
    void writeImage(long nels, unsigned long* data);
    void writeImage(valarray<unsigned long>& data)
      {writeImage(data.size(), &data[0]);};
    void writeImage(vector<unsigned long>& data)
      {writeImage(data.size(), &data[0]);};
    void writeImage(FITSArray2D<unsigned long>& arr)
      {writeImage(arr.data.size(), &arr.data[0]);};
    void writeImage(FITSArray2D<unsigned long> *arr)
      {writeImage(arr->data.size(), &arr->data[0]);};

    void writeImageSliceNull(long nels, unsigned long* data, long* fpixel, unsigned long nullVal);
    void writeImageSliceNull(valarray<unsigned long>& data, valarray<long> fpixel, unsigned long nullVal)
      {writeImageSliceNull(data.size(), &data[0], &fpixel[0], nullVal);};
    void writeImageSliceNull(valarray<unsigned long>& data, valarray<int> fpixel, unsigned long nullVal)
      {writeImageSliceNull(data, intToLongValarray(fpixel), nullVal);};
    void writeImageSliceNull(FITSArray2D<unsigned long>& arr, valarray<int> fpixel, unsigned long nullVal)
      {writeImageSliceNull(arr.data, intToLongValarray(fpixel), nullVal);};
    void writeImageSliceNull(FITSArray2D<unsigned long> *arr, valarray<int> fpixel, unsigned long nullVal)
      {writeImageSliceNull(arr->data, intToLongValarray(fpixel), nullVal);};

    void writeImageSlice(unsigned long* data, long* fpixel, long *lpixel);
    void writeImageSlice(valarray<unsigned long>& data, valarray<long> fpixel)
      {writeImageSliceNull(data.size(), &data[0], &fpixel[0], 0);};
    void writeImageSlice(valarray<unsigned long>& data, valarray<int> fpixel)
      {writeImageSlice(data, intToLongValarray(fpixel));};
    void writeImageSlice(FITSArray2D<unsigned long>& arr, valarray<int> fpixel)
      {writeImageSlice(arr.data, intToLongValarray(fpixel));};
    void writeImageSlice(FITSArray2D<unsigned long>* arr, valarray<int> fpixel)
      {writeImageSlice(arr->data, intToLongValarray(fpixel));};


    void readImageNull(long nels, unsigned long *data, unsigned long nullVal);
    void readImage(long nels, unsigned long *data) {readImageNull(nels, data, 0);};
    void readImageNull(valarray<unsigned long>& data, unsigned long nullVal);
    void readImage(valarray<unsigned long>& data) {readImageNull(data, 0);};
    void readImageNull(vector<unsigned long>& data, unsigned long nullVal);
    void readImage(vector<unsigned long>& data) {readImageNull(data, 0);};
    void readImageNull(FITSArray2D<unsigned long>& arr, unsigned long nullVal);
    void readImage(FITSArray2D<unsigned long>& arr) {readImageNull(arr, 0);};
    void readImageNull(FITSArray2D<unsigned long> *arr, unsigned long nullVal);
    void readImage(FITSArray2D<unsigned long> *arr) {readImageNull(arr, 0);};

    void readImageSliceNull(long nels, unsigned long *data, long *fpixel, unsigned long nullVal);
    void readImageSliceNull(long nels, valarray<unsigned long>& data, valarray<long> fpixel, unsigned long nullVal);
    void readImageSliceNull(long nels, vector<unsigned long>& data, valarray<long> fpixel, unsigned long nullVal);

    void readImageSlice(long nels, unsigned long *data, long *fpixel)
      {readImageSliceNull(nels, data, fpixel, 0);};
    void readImageSlice(long nels, valarray<unsigned long>& data, valarray<long> fpixel)
      {readImageSliceNull(nels, data, fpixel, 0);};
    void readImageSlice(long nels, vector<unsigned long>& data, valarray<long> fpixel)
      {readImageSliceNull(nels, data, fpixel, 0);};


    void writeImageNull(long nels, unsigned int* data, unsigned int nullVal);
    void writeImageNull(valarray<unsigned int>& data, unsigned int nullVal)
      {writeImageNull(data.size(), &data[0], nullVal);};
    void writeImageNull(vector<unsigned int>& data, unsigned int nullVal)
      {writeImageNull(data.size(), &data[0], nullVal);};
    void writeImage(long nels, unsigned int* data);
    void writeImage(valarray<unsigned int>& data)
      {writeImage(data.size(), &data[0]);};
    void writeImage(vector<unsigned int>& data)
      {writeImage(data.size(), &data[0]);};
    void writeImage(FITSArray2D<unsigned int>& arr)
      {writeImage(arr.data.size(), &arr.data[0]);};
    void writeImage(FITSArray2D<unsigned int> *arr)
      {writeImage(arr->data.size(), &arr->data[0]);};

    void writeImageSliceNull(long nels, unsigned int* data, long* fpixel, unsigned int nullVal);
    void writeImageSliceNull(valarray<unsigned int>& data, valarray<long> fpixel, unsigned int nullVal)
      {writeImageSliceNull(data.size(), &data[0], &fpixel[0], nullVal);};
    void writeImageSliceNull(valarray<unsigned int>& data, valarray<int> fpixel, unsigned int nullVal)
      {writeImageSliceNull(data, intToLongValarray(fpixel), nullVal);};
    void writeImageSliceNull(FITSArray2D<unsigned int>& arr, valarray<int> fpixel, unsigned int nullVal)
      {writeImageSliceNull(arr.data, intToLongValarray(fpixel), nullVal);};
    void writeImageSliceNull(FITSArray2D<unsigned int> *arr, valarray<int> fpixel, unsigned int nullVal)
      {writeImageSliceNull(arr->data, intToLongValarray(fpixel), nullVal);};

    void writeImageSlice(unsigned int* data, long* fpixel, long *lpixel);
    void writeImageSlice(valarray<unsigned int>& data, valarray<long> fpixel)
      {writeImageSliceNull(data.size(), &data[0], &fpixel[0], 0);};
    void writeImageSlice(valarray<unsigned int>& data, valarray<int> fpixel)
      {writeImageSlice(data, intToLongValarray(fpixel));};
    void writeImageSlice(FITSArray2D<unsigned int>& arr, valarray<int> fpixel)
      {writeImageSlice(arr.data, intToLongValarray(fpixel));};
    void writeImageSlice(FITSArray2D<unsigned int>* arr, valarray<int> fpixel)
      {writeImageSlice(arr->data, intToLongValarray(fpixel));};


    void readImageNull(long nels, unsigned int *data, unsigned int nullVal);
    void readImage(long nels, unsigned int *data) {readImageNull(nels, data, 0);};
    void readImageNull(valarray<unsigned int>& data, unsigned int nullVal);
    void readImage(valarray<unsigned int>& data) {readImageNull(data, 0);};
    void readImageNull(vector<unsigned int>& data, unsigned int nullVal);
    void readImage(vector<unsigned int>& data) {readImageNull(data, 0);};
    void readImageNull(FITSArray2D<unsigned int>& arr, unsigned int nullVal);
    void readImage(FITSArray2D<unsigned int>& arr) {readImageNull(arr, 0);};
    void readImageNull(FITSArray2D<unsigned int> *arr, unsigned int nullVal);
    void readImage(FITSArray2D<unsigned int> *arr) {readImageNull(arr, 0);};

    void readImageSliceNull(long nels, unsigned int *data, long *fpixel, unsigned int nullVal);
    void readImageSliceNull(long nels, valarray<unsigned int>& data, valarray<long> fpixel, unsigned int nullVal);
    void readImageSliceNull(long nels, vector<unsigned int>& data, valarray<long> fpixel, unsigned int nullVal);

    void readImageSlice(long nels, unsigned int *data, long *fpixel)
      {readImageSliceNull(nels, data, fpixel, 0);};
    void readImageSlice(long nels, valarray<unsigned int>& data, valarray<long> fpixel)
      {readImageSliceNull(nels, data, fpixel, 0);};
    void readImageSlice(long nels, vector<unsigned int>& data, valarray<long> fpixel)
      {readImageSliceNull(nels, data, fpixel, 0);};


    void writeImageNull(long nels, unsigned char* data, unsigned char nullVal);
    void writeImageNull(valarray<unsigned char>& data, unsigned char nullVal)
      {writeImageNull(data.size(), &data[0], nullVal);};
    void writeImageNull(vector<unsigned char>& data, unsigned char nullVal)
      {writeImageNull(data.size(), &data[0], nullVal);};
    void writeImage(long nels, unsigned char* data);
    void writeImage(valarray<unsigned char>& data)
      {writeImage(data.size(), &data[0]);};
    void writeImage(vector<unsigned char>& data)
      {writeImage(data.size(), &data[0]);};
    void writeImage(FITSArray2D<unsigned char>& arr)
      {writeImage(arr.data.size(), &arr.data[0]);};
    void writeImage(FITSArray2D<unsigned char> *arr)
      {writeImage(arr->data.size(), &arr->data[0]);};

    void writeImageSliceNull(long nels, unsigned char* data, long* fpixel, unsigned char nullVal);
    void writeImageSliceNull(valarray<unsigned char>& data, valarray<long> fpixel, unsigned char nullVal)
      {writeImageSliceNull(data.size(), &data[0], &fpixel[0], nullVal);};
    void writeImageSliceNull(valarray<unsigned char>& data, valarray<int> fpixel, unsigned char nullVal)
      {writeImageSliceNull(data, intToLongValarray(fpixel), nullVal);};
    void writeImageSliceNull(FITSArray2D<unsigned char>& arr, valarray<int> fpixel, unsigned char nullVal)
      {writeImageSliceNull(arr.data, intToLongValarray(fpixel), nullVal);};
    void writeImageSliceNull(FITSArray2D<unsigned char> *arr, valarray<int> fpixel, unsigned char nullVal)
      {writeImageSliceNull(arr->data, intToLongValarray(fpixel), nullVal);};

    void writeImageSlice(unsigned char* data, long* fpixel, long *lpixel);
    void writeImageSlice(valarray<unsigned char>& data, valarray<long> fpixel)
      {writeImageSliceNull(data.size(), &data[0], &fpixel[0], 0);};
    void writeImageSlice(valarray<unsigned char>& data, valarray<int> fpixel)
      {writeImageSlice(data, intToLongValarray(fpixel));};
    void writeImageSlice(FITSArray2D<unsigned char>& arr, valarray<int> fpixel)
      {writeImageSlice(arr.data, intToLongValarray(fpixel));};
    void writeImageSlice(FITSArray2D<unsigned char>* arr, valarray<int> fpixel)
      {writeImageSlice(arr->data, intToLongValarray(fpixel));};


    void readImageNull(long nels, unsigned char *data, unsigned char nullVal);
    void readImage(long nels, unsigned char *data) {readImageNull(nels, data, 0);};
    void readImageNull(valarray<unsigned char>& data, unsigned char nullVal);
    void readImage(valarray<unsigned char>& data) {readImageNull(data, 0);};
    void readImageNull(vector<unsigned char>& data, unsigned char nullVal);
    void readImage(vector<unsigned char>& data) {readImageNull(data, 0);};
    void readImageNull(FITSArray2D<unsigned char>& arr, unsigned char nullVal);
    void readImage(FITSArray2D<unsigned char>& arr) {readImageNull(arr, 0);};
    void readImageNull(FITSArray2D<unsigned char> *arr, unsigned char nullVal);
    void readImage(FITSArray2D<unsigned char> *arr) {readImageNull(arr, 0);};

    void readImageSliceNull(long nels, unsigned char *data, long *fpixel, unsigned char nullVal);
    void readImageSliceNull(long nels, valarray<unsigned char>& data, valarray<long> fpixel, unsigned char nullVal);
    void readImageSliceNull(long nels, vector<unsigned char>& data, valarray<long> fpixel, unsigned char nullVal);

    void readImageSlice(long nels, unsigned char *data, long *fpixel)
      {readImageSliceNull(nels, data, fpixel, 0);};
    void readImageSlice(long nels, valarray<unsigned char>& data, valarray<long> fpixel)
      {readImageSliceNull(nels, data, fpixel, 0);};
    void readImageSlice(long nels, vector<unsigned char>& data, valarray<long> fpixel)
      {readImageSliceNull(nels, data, fpixel, 0);};


    void writeImageNull(long nels, double* data, double nullVal);
    void writeImageNull(valarray<double>& data, double nullVal)
      {writeImageNull(data.size(), &data[0], nullVal);};
    void writeImageNull(vector<double>& data, double nullVal)
      {writeImageNull(data.size(), &data[0], nullVal);};
    void writeImage(long nels, double* data);
    void writeImage(valarray<double>& data)
      {writeImage(data.size(), &data[0]);};
    void writeImage(vector<double>& data)
      {writeImage(data.size(), &data[0]);};
    void writeImage(FITSArray2D<double>& arr)
      {writeImage(arr.data.size(), &arr.data[0]);};
    void writeImage(FITSArray2D<double> *arr)
      {writeImage(arr->data.size(), &arr->data[0]);};

    void writeImageSliceNull(long nels, double* data, long* fpixel, double nullVal);
    void writeImageSliceNull(valarray<double>& data, valarray<long> fpixel, double nullVal)
      {writeImageSliceNull(data.size(), &data[0], &fpixel[0], nullVal);};
    void writeImageSliceNull(valarray<double>& data, valarray<int> fpixel, double nullVal)
      {writeImageSliceNull(data, intToLongValarray(fpixel), nullVal);};
    void writeImageSliceNull(FITSArray2D<double>& arr, valarray<int> fpixel, double nullVal)
      {writeImageSliceNull(arr.data, intToLongValarray(fpixel), nullVal);};
    void writeImageSliceNull(FITSArray2D<double> *arr, valarray<int> fpixel, double nullVal)
      {writeImageSliceNull(arr->data, intToLongValarray(fpixel), nullVal);};

    void writeImageSlice(double* data, long* fpixel, long *lpixel);
    void writeImageSlice(valarray<double>& data, valarray<long> fpixel)
      {writeImageSliceNull(data.size(), &data[0], &fpixel[0], 0.0);};
    void writeImageSlice(valarray<double>& data, valarray<int> fpixel)
      {writeImageSlice(data, intToLongValarray(fpixel));};
    void writeImageSlice(FITSArray2D<double>& arr, valarray<int> fpixel)
      {writeImageSlice(arr.data, intToLongValarray(fpixel));};
    void writeImageSlice(FITSArray2D<double>* arr, valarray<int> fpixel)
      {writeImageSlice(arr->data, intToLongValarray(fpixel));};


    void readImageNull(long nels, double *data, double nullVal);
    void readImage(long nels, double *data) {readImageNull(nels, data, 0.0);};
    void readImageNull(valarray<double>& data, double nullVal);
    void readImage(valarray<double>& data) {readImageNull(data, 0.0);};
    void readImageNull(vector<double>& data, double nullVal);
    void readImage(vector<double>& data) {readImageNull(data, 0.0);};
    void readImageNull(FITSArray2D<double>& arr, double nullVal);
    void readImage(FITSArray2D<double>& arr) {readImageNull(arr, 0.0);};
    void readImageNull(FITSArray2D<double> *arr, double nullVal);
    void readImage(FITSArray2D<double> *arr) {readImageNull(arr, 0.0);};

    void readImageSliceNull(long nels, double *data, long *fpixel, double nullVal);
    void readImageSliceNull(long nels, valarray<double>& data, valarray<long> fpixel, double nullVal);
    void readImageSliceNull(long nels, vector<double>& data, valarray<long> fpixel, double nullVal);

    void readImageSlice(long nels, double *data, long *fpixel)
      {readImageSliceNull(nels, data, fpixel, 0.0);};
    void readImageSlice(long nels, valarray<double>& data, valarray<long> fpixel)
      {readImageSliceNull(nels, data, fpixel, 0.0);};
    void readImageSlice(long nels, vector<double>& data, valarray<long> fpixel)
      {readImageSliceNull(nels, data, fpixel, 0.0);};


    void writeImageNull(long nels, unsigned short int* data, unsigned short int nullVal);
    void writeImageNull(valarray<unsigned short int>& data, unsigned short int nullVal)
      {writeImageNull(data.size(), &data[0], nullVal);};
    void writeImageNull(vector<unsigned short int>& data, unsigned short int nullVal)
      {writeImageNull(data.size(), &data[0], nullVal);};
    void writeImage(long nels, unsigned short int* data);
    void writeImage(valarray<unsigned short int>& data)
      {writeImage(data.size(), &data[0]);};
    void writeImage(vector<unsigned short int>& data)
      {writeImage(data.size(), &data[0]);};
    void writeImage(FITSArray2D<unsigned short int>& arr)
      {writeImage(arr.data.size(), &arr.data[0]);};
    void writeImage(FITSArray2D<unsigned short int> *arr)
      {writeImage(arr->data.size(), &arr->data[0]);};

    void writeImageSliceNull(long nels, unsigned short int* data, long* fpixel, unsigned short int nullVal);
    void writeImageSliceNull(valarray<unsigned short int>& data, valarray<long> fpixel, unsigned short int nullVal)
      {writeImageSliceNull(data.size(), &data[0], &fpixel[0], nullVal);};
    void writeImageSliceNull(valarray<unsigned short int>& data, valarray<int> fpixel, unsigned short int nullVal)
      {writeImageSliceNull(data, intToLongValarray(fpixel), nullVal);};
    void writeImageSliceNull(FITSArray2D<unsigned short int>& arr, valarray<int> fpixel, unsigned short int nullVal)
      {writeImageSliceNull(arr.data, intToLongValarray(fpixel), nullVal);};
    void writeImageSliceNull(FITSArray2D<unsigned short int> *arr, valarray<int> fpixel, unsigned short int nullVal)
      {writeImageSliceNull(arr->data, intToLongValarray(fpixel), nullVal);};

    void writeImageSlice(unsigned short int* data, long* fpixel, long *lpixel);
    void writeImageSlice(valarray<unsigned short int>& data, valarray<long> fpixel)
      {writeImageSliceNull(data.size(), &data[0], &fpixel[0], 0);};
    void writeImageSlice(valarray<unsigned short int>& data, valarray<int> fpixel)
      {writeImageSlice(data, intToLongValarray(fpixel));};
    void writeImageSlice(FITSArray2D<unsigned short int>& arr, valarray<int> fpixel)
      {writeImageSlice(arr.data, intToLongValarray(fpixel));};
    void writeImageSlice(FITSArray2D<unsigned short int>* arr, valarray<int> fpixel)
      {writeImageSlice(arr->data, intToLongValarray(fpixel));};


    void readImageNull(long nels, unsigned short int *data, unsigned short int nullVal);
    void readImage(long nels, unsigned short int *data) {readImageNull(nels, data, 0);};
    void readImageNull(valarray<unsigned short int>& data, unsigned short int nullVal);
    void readImage(valarray<unsigned short int>& data) {readImageNull(data, 0);};
    void readImageNull(vector<unsigned short int>& data, unsigned short int nullVal);
    void readImage(vector<unsigned short int>& data) {readImageNull(data, 0);};
    void readImageNull(FITSArray2D<unsigned short int>& arr, unsigned short int nullVal);
    void readImage(FITSArray2D<unsigned short int>& arr) {readImageNull(arr, 0);};
    void readImageNull(FITSArray2D<unsigned short int> *arr, unsigned short int nullVal);
    void readImage(FITSArray2D<unsigned short int> *arr) {readImageNull(arr, 0);};

    void readImageSliceNull(long nels, unsigned short int *data, long *fpixel, unsigned short int nullVal);
    void readImageSliceNull(long nels, valarray<unsigned short int>& data, valarray<long> fpixel, unsigned short int nullVal);
    void readImageSliceNull(long nels, vector<unsigned short int>& data, valarray<long> fpixel, unsigned short int nullVal);

    void readImageSlice(long nels, unsigned short int *data, long *fpixel)
      {readImageSliceNull(nels, data, fpixel, 0);};
    void readImageSlice(long nels, valarray<unsigned short int>& data, valarray<long> fpixel)
      {readImageSliceNull(nels, data, fpixel, 0);};
    void readImageSlice(long nels, vector<unsigned short int>& data, valarray<long> fpixel)
      {readImageSliceNull(nels, data, fpixel, 0);};


    void writeImageNull(long nels, float* data, float nullVal);
    void writeImageNull(valarray<float>& data, float nullVal)
      {writeImageNull(data.size(), &data[0], nullVal);};
    void writeImageNull(vector<float>& data, float nullVal)
      {writeImageNull(data.size(), &data[0], nullVal);};
    void writeImage(long nels, float* data);
    void writeImage(valarray<float>& data)
      {writeImage(data.size(), &data[0]);};
    void writeImage(vector<float>& data)
      {writeImage(data.size(), &data[0]);};
    void writeImage(FITSArray2D<float>& arr)
      {writeImage(arr.data.size(), &arr.data[0]);};
    void writeImage(FITSArray2D<float> *arr)
      {writeImage(arr->data.size(), &arr->data[0]);};

    void writeImageSliceNull(long nels, float* data, long* fpixel, float nullVal);
    void writeImageSliceNull(valarray<float>& data, valarray<long> fpixel, float nullVal)
      {writeImageSliceNull(data.size(), &data[0], &fpixel[0], nullVal);};
    void writeImageSliceNull(valarray<float>& data, valarray<int> fpixel, float nullVal)
      {writeImageSliceNull(data, intToLongValarray(fpixel), nullVal);};
    void writeImageSliceNull(FITSArray2D<float>& arr, valarray<int> fpixel, float nullVal)
      {writeImageSliceNull(arr.data, intToLongValarray(fpixel), nullVal);};
    void writeImageSliceNull(FITSArray2D<float> *arr, valarray<int> fpixel, float nullVal)
      {writeImageSliceNull(arr->data, intToLongValarray(fpixel), nullVal);};

    void writeImageSlice(float* data, long* fpixel, long *lpixel);
    void writeImageSlice(valarray<float>& data, valarray<long> fpixel)
      {writeImageSliceNull(data.size(), &data[0], &fpixel[0], 0.0f);};
    void writeImageSlice(valarray<float>& data, valarray<int> fpixel)
      {writeImageSlice(data, intToLongValarray(fpixel));};
    void writeImageSlice(FITSArray2D<float>& arr, valarray<int> fpixel)
      {writeImageSlice(arr.data, intToLongValarray(fpixel));};
    void writeImageSlice(FITSArray2D<float>* arr, valarray<int> fpixel)
      {writeImageSlice(arr->data, intToLongValarray(fpixel));};


    void readImageNull(long nels, float *data, float nullVal);
    void readImage(long nels, float *data) {readImageNull(nels, data, 0.0f);};
    void readImageNull(valarray<float>& data, float nullVal);
    void readImage(valarray<float>& data) {readImageNull(data, 0.0f);};
    void readImageNull(vector<float>& data, float nullVal);
    void readImage(vector<float>& data) {readImageNull(data, 0.0f);};
    void readImageNull(FITSArray2D<float>& arr, float nullVal);
    void readImage(FITSArray2D<float>& arr) {readImageNull(arr, 0.0f);};
    void readImageNull(FITSArray2D<float> *arr, float nullVal);
    void readImage(FITSArray2D<float> *arr) {readImageNull(arr, 0.0f);};

    void readImageSliceNull(long nels, float *data, long *fpixel, float nullVal);
    void readImageSliceNull(long nels, valarray<float>& data, valarray<long> fpixel, float nullVal);
    void readImageSliceNull(long nels, vector<float>& data, valarray<long> fpixel, float nullVal);

    void readImageSlice(long nels, float *data, long *fpixel)
      {readImageSliceNull(nels, data, fpixel, 0.0f);};
    void readImageSlice(long nels, valarray<float>& data, valarray<long> fpixel)
      {readImageSliceNull(nels, data, fpixel, 0.0f);};
    void readImageSlice(long nels, vector<float>& data, valarray<long> fpixel)
      {readImageSliceNull(nels, data, fpixel, 0.0f);};


    void writeImageNull(long nels, LONGLONG* data, LONGLONG nullVal);
    void writeImageNull(valarray<LONGLONG>& data, LONGLONG nullVal)
      {writeImageNull(data.size(), &data[0], nullVal);};
    void writeImageNull(vector<LONGLONG>& data, LONGLONG nullVal)
      {writeImageNull(data.size(), &data[0], nullVal);};
    void writeImage(long nels, LONGLONG* data);
    void writeImage(valarray<LONGLONG>& data)
      {writeImage(data.size(), &data[0]);};
    void writeImage(vector<LONGLONG>& data)
      {writeImage(data.size(), &data[0]);};
    void writeImage(FITSArray2D<LONGLONG>& arr)
      {writeImage(arr.data.size(), &arr.data[0]);};
    void writeImage(FITSArray2D<LONGLONG> *arr)
      {writeImage(arr->data.size(), &arr->data[0]);};

    void writeImageSliceNull(long nels, LONGLONG* data, long* fpixel, LONGLONG nullVal);
    void writeImageSliceNull(valarray<LONGLONG>& data, valarray<long> fpixel, LONGLONG nullVal)
      {writeImageSliceNull(data.size(), &data[0], &fpixel[0], nullVal);};
    void writeImageSliceNull(valarray<LONGLONG>& data, valarray<int> fpixel, LONGLONG nullVal)
      {writeImageSliceNull(data, intToLongValarray(fpixel), nullVal);};
    void writeImageSliceNull(FITSArray2D<LONGLONG>& arr, valarray<int> fpixel, LONGLONG nullVal)
      {writeImageSliceNull(arr.data, intToLongValarray(fpixel), nullVal);};
    void writeImageSliceNull(FITSArray2D<LONGLONG> *arr, valarray<int> fpixel, LONGLONG nullVal)
      {writeImageSliceNull(arr->data, intToLongValarray(fpixel), nullVal);};

    void writeImageSlice(LONGLONG* data, long* fpixel, long *lpixel);
    void writeImageSlice(valarray<LONGLONG>& data, valarray<long> fpixel)
      {writeImageSliceNull(data.size(), &data[0], &fpixel[0], 0);};
    void writeImageSlice(valarray<LONGLONG>& data, valarray<int> fpixel)
      {writeImageSlice(data, intToLongValarray(fpixel));};
    void writeImageSlice(FITSArray2D<LONGLONG>& arr, valarray<int> fpixel)
      {writeImageSlice(arr.data, intToLongValarray(fpixel));};
    void writeImageSlice(FITSArray2D<LONGLONG>* arr, valarray<int> fpixel)
      {writeImageSlice(arr->data, intToLongValarray(fpixel));};


    void readImageNull(long nels, LONGLONG *data, LONGLONG nullVal);
    void readImage(long nels, LONGLONG *data) {readImageNull(nels, data, 0);};
    void readImageNull(valarray<LONGLONG>& data, LONGLONG nullVal);
    void readImage(valarray<LONGLONG>& data) {readImageNull(data, 0);};
    void readImageNull(vector<LONGLONG>& data, LONGLONG nullVal);
    void readImage(vector<LONGLONG>& data) {readImageNull(data, 0);};
    void readImageNull(FITSArray2D<LONGLONG>& arr, LONGLONG nullVal);
    void readImage(FITSArray2D<LONGLONG>& arr) {readImageNull(arr, 0);};
    void readImageNull(FITSArray2D<LONGLONG> *arr, LONGLONG nullVal);
    void readImage(FITSArray2D<LONGLONG> *arr) {readImageNull(arr, 0);};

    void readImageSliceNull(long nels, LONGLONG *data, long *fpixel, LONGLONG nullVal);
    void readImageSliceNull(long nels, valarray<LONGLONG>& data, valarray<long> fpixel, LONGLONG nullVal);
    void readImageSliceNull(long nels, vector<LONGLONG>& data, valarray<long> fpixel, LONGLONG nullVal);

    void readImageSlice(long nels, LONGLONG *data, long *fpixel)
      {readImageSliceNull(nels, data, fpixel, 0);};
    void readImageSlice(long nels, valarray<LONGLONG>& data, valarray<long> fpixel)
      {readImageSliceNull(nels, data, fpixel, 0);};
    void readImageSlice(long nels, vector<LONGLONG>& data, valarray<long> fpixel)
      {readImageSliceNull(nels, data, fpixel, 0);};


    void writeImageNull(long nels, long* data, long nullVal);
    void writeImageNull(valarray<long>& data, long nullVal)
      {writeImageNull(data.size(), &data[0], nullVal);};
    void writeImageNull(vector<long>& data, long nullVal)
      {writeImageNull(data.size(), &data[0], nullVal);};
    void writeImage(long nels, long* data);
    void writeImage(valarray<long>& data)
      {writeImage(data.size(), &data[0]);};
    void writeImage(vector<long>& data)
      {writeImage(data.size(), &data[0]);};
    void writeImage(FITSArray2D<long>& arr)
      {writeImage(arr.data.size(), &arr.data[0]);};
    void writeImage(FITSArray2D<long> *arr)
      {writeImage(arr->data.size(), &arr->data[0]);};

    void writeImageSliceNull(long nels, long* data, long* fpixel, long nullVal);
    void writeImageSliceNull(valarray<long>& data, valarray<long> fpixel, long nullVal)
      {writeImageSliceNull(data.size(), &data[0], &fpixel[0], nullVal);};
    void writeImageSliceNull(valarray<long>& data, valarray<int> fpixel, long nullVal)
      {writeImageSliceNull(data, intToLongValarray(fpixel), nullVal);};
    void writeImageSliceNull(FITSArray2D<long>& arr, valarray<int> fpixel, long nullVal)
      {writeImageSliceNull(arr.data, intToLongValarray(fpixel), nullVal);};
    void writeImageSliceNull(FITSArray2D<long> *arr, valarray<int> fpixel, long nullVal)
      {writeImageSliceNull(arr->data, intToLongValarray(fpixel), nullVal);};

    void writeImageSlice(long* data, long* fpixel, long *lpixel);
    void writeImageSlice(valarray<long>& data, valarray<long> fpixel)
      {writeImageSliceNull(data.size(), &data[0], &fpixel[0], 0);};
    void writeImageSlice(valarray<long>& data, valarray<int> fpixel)
      {writeImageSlice(data, intToLongValarray(fpixel));};
    void writeImageSlice(FITSArray2D<long>& arr, valarray<int> fpixel)
      {writeImageSlice(arr.data, intToLongValarray(fpixel));};
    void writeImageSlice(FITSArray2D<long>* arr, valarray<int> fpixel)
      {writeImageSlice(arr->data, intToLongValarray(fpixel));};


    void readImageNull(long nels, long *data, long nullVal);
    void readImage(long nels, long *data) {readImageNull(nels, data, 0);};
    void readImageNull(valarray<long>& data, long nullVal);
    void readImage(valarray<long>& data) {readImageNull(data, 0);};
    void readImageNull(vector<long>& data, long nullVal);
    void readImage(vector<long>& data) {readImageNull(data, 0);};
    void readImageNull(FITSArray2D<long>& arr, long nullVal);
    void readImage(FITSArray2D<long>& arr) {readImageNull(arr, 0);};
    void readImageNull(FITSArray2D<long> *arr, long nullVal);
    void readImage(FITSArray2D<long> *arr) {readImageNull(arr, 0);};

    void readImageSliceNull(long nels, long *data, long *fpixel, long nullVal);
    void readImageSliceNull(long nels, valarray<long>& data, valarray<long> fpixel, long nullVal);
    void readImageSliceNull(long nels, vector<long>& data, valarray<long> fpixel, long nullVal);

    void readImageSlice(long nels, long *data, long *fpixel)
      {readImageSliceNull(nels, data, fpixel, 0);};
    void readImageSlice(long nels, valarray<long>& data, valarray<long> fpixel)
      {readImageSliceNull(nels, data, fpixel, 0);};
    void readImageSlice(long nels, vector<long>& data, valarray<long> fpixel)
      {readImageSliceNull(nels, data, fpixel, 0);};


    void writeImageNull(long nels, short int* data, short int nullVal);
    void writeImageNull(valarray<short int>& data, short int nullVal)
      {writeImageNull(data.size(), &data[0], nullVal);};
    void writeImageNull(vector<short int>& data, short int nullVal)
      {writeImageNull(data.size(), &data[0], nullVal);};
    void writeImage(long nels, short int* data);
    void writeImage(valarray<short int>& data)
      {writeImage(data.size(), &data[0]);};
    void writeImage(vector<short int>& data)
      {writeImage(data.size(), &data[0]);};
    void writeImage(FITSArray2D<short int>& arr)
      {writeImage(arr.data.size(), &arr.data[0]);};
    void writeImage(FITSArray2D<short int> *arr)
      {writeImage(arr->data.size(), &arr->data[0]);};

    void writeImageSliceNull(long nels, short int* data, long* fpixel, short int nullVal);
    void writeImageSliceNull(valarray<short int>& data, valarray<long> fpixel, short int nullVal)
      {writeImageSliceNull(data.size(), &data[0], &fpixel[0], nullVal);};
    void writeImageSliceNull(valarray<short int>& data, valarray<int> fpixel, short int nullVal)
      {writeImageSliceNull(data, intToLongValarray(fpixel), nullVal);};
    void writeImageSliceNull(FITSArray2D<short int>& arr, valarray<int> fpixel, short int nullVal)
      {writeImageSliceNull(arr.data, intToLongValarray(fpixel), nullVal);};
    void writeImageSliceNull(FITSArray2D<short int> *arr, valarray<int> fpixel, short int nullVal)
      {writeImageSliceNull(arr->data, intToLongValarray(fpixel), nullVal);};

    void writeImageSlice(short int* data, long* fpixel, long *lpixel);
    void writeImageSlice(valarray<short int>& data, valarray<long> fpixel)
      {writeImageSliceNull(data.size(), &data[0], &fpixel[0], 0);};
    void writeImageSlice(valarray<short int>& data, valarray<int> fpixel)
      {writeImageSlice(data, intToLongValarray(fpixel));};
    void writeImageSlice(FITSArray2D<short int>& arr, valarray<int> fpixel)
      {writeImageSlice(arr.data, intToLongValarray(fpixel));};
    void writeImageSlice(FITSArray2D<short int>* arr, valarray<int> fpixel)
      {writeImageSlice(arr->data, intToLongValarray(fpixel));};


    void readImageNull(long nels, short int *data, short int nullVal);
    void readImage(long nels, short int *data) {readImageNull(nels, data, 0);};
    void readImageNull(valarray<short int>& data, short int nullVal);
    void readImage(valarray<short int>& data) {readImageNull(data, 0);};
    void readImageNull(vector<short int>& data, short int nullVal);
    void readImage(vector<short int>& data) {readImageNull(data, 0);};
    void readImageNull(FITSArray2D<short int>& arr, short int nullVal);
    void readImage(FITSArray2D<short int>& arr) {readImageNull(arr, 0);};
    void readImageNull(FITSArray2D<short int> *arr, short int nullVal);
    void readImage(FITSArray2D<short int> *arr) {readImageNull(arr, 0);};

    void readImageSliceNull(long nels, short int *data, long *fpixel, short int nullVal);
    void readImageSliceNull(long nels, valarray<short int>& data, valarray<long> fpixel, short int nullVal);
    void readImageSliceNull(long nels, vector<short int>& data, valarray<long> fpixel, short int nullVal);

    void readImageSlice(long nels, short int *data, long *fpixel)
      {readImageSliceNull(nels, data, fpixel, 0);};
    void readImageSlice(long nels, valarray<short int>& data, valarray<long> fpixel)
      {readImageSliceNull(nels, data, fpixel, 0);};
    void readImageSlice(long nels, vector<short int>& data, valarray<long> fpixel)
      {readImageSliceNull(nels, data, fpixel, 0);};

