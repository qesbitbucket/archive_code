#include "fitsio.h"

#include "fits++.h"

namespace fitspp {

#if 0
  template<class T> void readPrimaryImage(string file, valarray<T>& data)
  {
    FITSFile *f=openFile(file);
    FITSImage *pimg=f->findPrimaryImage();
    pimg->readImage(data);
    f->closeFile();
  }
#endif

};
