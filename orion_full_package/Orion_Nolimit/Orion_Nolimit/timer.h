#ifndef _TIMER_H_
#define _TIMER_H_

#include <string>
#include <map>
#include <vector>

typedef struct {
  int startCnt, stopCnt;
  double elapsedTime, cpuTime;
} Instrument;

class Timer {
  map<string, Instrument*> instList;
  vector<string> idList;
 private:
  double get_elapsed(void);
  double get_cpu(void);
 public:
  Timer();
  ~Timer();
  void start(string id);
  void stop(string id);
  void print(void);
};

#ifdef ISMAIN
Timer timer;
#else
extern Timer timer;
#endif

#endif
