#ifndef _PDGRAMFILE_H_
#define _PDGRAMFILE_H_

#include <vector>

class PDGramFile {
  string filename;
  fitsfile *fptr;
  int status;
 public:
  PDGramFile(const char *filename, vector<double> samplePeriod,
	     int minBoxSize, int maxBoxSize, float boxSize);
  ~PDGramFile(void);
  void close(void);
};


#endif
