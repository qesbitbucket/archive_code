#ifndef _UTOBJECT_H_
#define _UTOBJECT_H_

#include <valarray>
#include <vector>

#include "types.h"
#include "candpeak.h"

using namespace std;

typedef struct {
  double refTime, period;
  float zeroPoint;
  int numTerms;
  valarray<float> sinCoeff, cosCoeff;
} HarmonicCoeffs;

typedef struct {
  float fluxMean, magMean;
  float merit, starSigmaXS;
  int numValidPts, rowIdx, numPostClipPts;
  HarmonicCoeffs *hfCoeffs;
} UTObjSegStats;

class UTObject {

 public:
  ObjectID id;
  double ra, dec;
  float fluxMean, magMean, magRMS, merit, starSigmaXS;
  valarray<float> flux, fluxErr, rawMag, corrMag, magVar, weight, model;
  valarray<int> hjd;
  vector<UTObjSegStats*> segStats;
  int calStdIdx, numValidPts, numCands, objFlag;
  int fakeEpoch, numPtsUsed, rowIdx, catIdx;
  double fakePeriod, fakeWidth, fakeDepth;
  int fakeCandMatch, fakePeakMatch;
  float fakePeriodMult;
  float bMag, vMag, rMag, jMag, hMag, kMag, muRA, muDec, muRA_err, muDec_err,
    dilutionV, dilutionR;
  bool isStd, badStd, hasFake, hasHarmonicCoeffs;

 public:
  void computeMagnitudes(void);
  void computeStatistics(void);
  float computeMedianMag(void);
  float computeMeanMag(float *imSigmaXS=NULL);
  float computeRMSMag(float *imSigmaXS=NULL);
  int sigmaClipMag(float threshold, float *imSigmaXS=NULL);
  float computeMagResiduals(void);
  void freeMemory(void);
  void fitEllipse(CandPeak *p);

 public:
//  void readLightcurve(fitsfile *ifptr, int numImages, int *status);
//  void writeLightcurve(fitsfile *ofptr, int *status);

};

enum {OBJFLAG_INSUFFICIENT_POINTS=1,
      OBJFLAG_LOW_FLUX=2,
      OBJFLAG_IS_STANDARD=4,
      OBJFLAG_SINGLE_SEGMENT=8,
      OBJFLAG_MISSING_HARM_COEFFS=16,
      OBJFLAG_OROF_CUT=32,
};

#endif
