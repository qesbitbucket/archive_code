#ifndef _CANDPEAK_H_
#define _CANDPEAK_H_

#include "types.h"
#include "psample.h"

class CandPeak {
 public:
  int epoch;
  double period, width, depth, deltaChisq, offset, chisqLocal, deltaChisqLocal;
  int numPtsInTransit, numTransits, numNightBounds, numPtsLocal, flags,
    boxSize, lcIdx, pgIdx, cndIdx;
  float ampEllipse, snEllipse, gapRatio, snAnti, sde, boxRMS;

 public:
  CandPeak() {
    numTransits=numNightBounds=0;
    flags=0;
    ampEllipse=FLT_NULL;
    snEllipse=FLT_NULL;
  };
  CandPeak(PSample *p) {
    period=p->period;
    epoch=p->epoch;
    width=p->width;
    depth=p->depth;
    deltaChisq=p->deltaChisq;
    boxSize=p->boxSize;
    numPtsInTransit=p->numPtsInTransit;
  };
};


#endif
