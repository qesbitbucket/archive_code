#!perl

# Configuration for orion

$_config = {
    ExePath => "/home/rgw/pandora/orion",
    LogDir::ngtshead => "/wasp/scratch/logfiles",
    QueueOptions => "-q default",
    Context => {
	FetchRoot::ngtshead => "/wasp/fetch",
	TamRoot::ngtshead => "/wasp/tamuz",
	OrionRoot::ngtshead => "/wasp/scratch/orion",
	OrionKPRoot::ngtshead => "/wasp/scratch/orion_kp",
	MetaFields::ngtshead => "/wasp/scratch/metafields",
	OverlapFields::ngtshead => "/wasp/scratch/overlaps",
    },
    Modules => {
	OrionOF => {
	    HostGroups => "ngtshead",
	    QueueOptions => "-pe parallel 24",
	    QueuedJobLimit => 4,
	    Priority => 4,
	},
	OrionOFBig => {
	    HostGroups => "ngtshead",
	    QueueOptions => "-pe parallel 24",
	    QueuedJobLimit => 4,
	    Priority => 4,
	},
	OrionSW => {
	    HostGroups => "ngtshead",
	    QueueOptions::ngtshead => "-pe parallel 24",
	    QueuedJobLimit => 4,
	    Priority => 2,
	},
	OrionMCMC => {
	    HostGroups => "ngtshead",
	    QueuedJobLimit => 4,
	    QueueOptions => "-q mcmc",
	},
	OrionLoad => {
	    HostGroups => "ngtshead",
	    QueueOptions => "-q huntingest",
	},
	OrionKP => {
	    HostGroups => "ngtshead",
	    QueueOptions => "-pe parallel 24",
	    QueuedJobLimit => 4,
	    Priority => 1,
	},
	OrionKPBig => {
	    HostGroups => "ngtshead",
	    QueueOptions => "-pe parallel 24",
	    QueuedJobLimit => 4,
	    Priority => 1,
	},
	OrionKPLoad => {
	    HostGroups => "ngtshead",
	    QueueOptions => "-q huntingest",
	    QueuedJobLimit => 4,
	},
    },
};

1;

