#!/usr/bin/perl
#
# calculate clump index
#
# Reads in cand.fits file and adds a column to cand.fits
# for the clump_index 
#

use English;
use strict qw(vars subs refs);

use Astro::FITS::CFITSIO qw(:constants :shortnames :longnames);

sub ClumpCalc {
    my ($file)=@ARG;

# PARAMETERS FOR BINNING
    my ($Ps, $Eps, $cut_index_min, $cut_index_max, $Xstep, $Pstep, $cut, $nulval);
    $Ps=0.02;
    $Eps=0.5;		#binsizes for period and epoch bins
    $cut_index_min=5;
    $cut_index_max = 20;
    $Xstep=30;$Pstep=0.05;
    $cut=1;
    $nulval=-99;

# OUTPUT file names
    my ($of, $of1, $of2, $of3, $of4, $smfile);
    $of='peri_chi_dist.out';
    $of1='chi.hist';
    $of2='chi_cut.hist';
    $of3='peri_chi_distri.hist';
    $of4='peri_chi_distri_cut.hist';

    $smfile='clump_index.sm';

    # TRY to open the input file
    my ($status, $comment, $hdutype, $nrows, $anynul);
    my $fptr=Astro::FITS::CFITSIO::open_file($file, READWRITE, $status);
    fitsio_err($status) if $status;

    # Read the minimum and maximum periods
    my ($Pmin, $Pmax, $field, $camera);
    $fptr->read_key_flt('PMIN', $Pmin, $comment, $status);
    $fptr->read_key_flt('PMAX', $Pmax, $comment, $status);
    $fptr->read_key_str('FIELD', $field, $comment, $status);
    $fptr->read_key_str('CAMERA', $camera, $comment, $status);
    fitsio_err($status) if $status;


    # MOVE to first table extension and get nrows
    $fptr->movnam_hdu(BINARY_TBL, "CANDIDATES", 0, $status);
    fitsio_err($status) if $status;
    $fptr->get_num_rows($nrows, $status);	
    printf "Reading $nrows candidates from candidate file \n";
    return unless $nrows;

    # READ PERIOD and EPOCH for all stars for peak_index=1
    my ($pcol, $ecol, $ccol, $rcol);
    $fptr->get_colnum(CASEINSEN, 'PERIOD', $pcol, $status);
    $fptr->get_colnum(CASEINSEN, 'EPOCH', $ecol, $status);
    $fptr->get_colnum(CASEINSEN, 'DELTA_CHISQ', $ccol, $status);
    $fptr->get_colnum(CASEINSEN, 'RANK', $rcol, $status);
    my(@period, @epoch, @dchisq, @orgidx);
    my $cnt=0;
    for(my $idx=1; $idx<=$nrows; $idx++) {
	my $rank=[];
	$fptr->read_col_lng($rcol, $idx, 1, 1, 0, $rank, $anynul, $status);
	if($$rank[0]==1) {
	    my $val=[];
	    $fptr->read_col_lng($pcol, $idx, 1, 1, 0, $val, $anynul, $status);
	    push(@period, $$val[0]/86400.0);
	    $fptr->read_col_lng($pcol, $idx, 1, 1, 0, $val, $anynul, $status);
	    push(@epoch, $$val[0]/86400.0);
	    $fptr->read_col_flt($ccol, $idx, 1, 1, $nulval, $val, $anynul, $status);
	    push(@dchisq, $$val[0]);
	    push(@orgidx, $idx);
	    $cnt++;
	}
    }
    fitsio_err($status) if $status;
    print "Found $cnt unique candidates\n";

    # BIN candidates in 2D period & epoch space
    # calculate clump index
    open(OF,">$of") || die "cannot open $of \n";

    my $val=int(0.01*$cnt);
    if ($val<$cut_index_min) { $val = $cut_index_min; }
    if ($val>$cut_index_max) { $val = $cut_index_max; }

    my (@Nbox, @Nperiod, @clump_index, $Ngood);
    $#Nbox=$cnt;
    $#Nperiod=$cnt;
    $#clump_index=$cnt;
    $Ngood=0;					#Num with clump_indx < cut
    for(my $ii=0; $ii<$cnt; $ii++) {
	$Nbox[$ii]=0;
	$Nperiod[$ii]=0;
	for(my $jj=0; $jj<$cnt; $jj++) {
	    if($period[$jj]>=($period[$ii]-$Ps/2.0) && $period[$jj]<=($period[$ii]+$Ps/2.0)) {
		$Nperiod[$ii]=$Nperiod[$ii]+1;
		if($epoch[$jj]>=($epoch[$ii]-$Eps/2.0) && $epoch[$jj]<=($epoch[$ii]+$Eps/2.0)) {
		    $Nbox[$ii] = $Nbox[$ii]+1;
		}
	    }
	}
	$clump_index[$ii] = $Nbox[$ii]/$val;
	if($clump_index[$ii] <= $cut){ $Ngood = $Ngood+1; }
	printf OF "%12.4f %16.4f %12.4f \n",$period[$ii],$epoch[$ii],$clump_index[$ii];
    }
    close(OF) || die "cannot close $of \n";

    printf "Adding clump_indx to candidate file \n";
    my ($xcol, $xtype, $xrpt, $xwidth);
    $fptr->get_colnum(CASEINSEN, "CLUMP_INDX", $xcol, $status);
    $fptr->get_coltype($xcol, $xtype, $xrpt, $xwidth, $status);
    if($xtype==TSHORT) {
	print "Changing CLUMP_INDX column data-type...\n";
	$fptr->delete_col($xcol, $status);
	$fptr->insert_col($xcol, "CLUMP_INDX", "1E", $status);
    }
    for(my $idx=0; $idx<$cnt; $idx++) {
	$fptr->write_col_flt($xcol, $orgidx[$idx], 1, 1, $clump_index[$idx], $status);
    }
    fitsio_err($status) if $status;

    # CLOSE inputfile
    $fptr->close_file($status) if defined($fptr);	
    fitsio_err($status) if ($status);
    unlink($of1);
    unlink($of2);

# Generate output data files for plotting histograms

# Create delta-chisq histogram output files
    my ($Xmax, $Xmin, $freq_c, $freq, $fname, $label, $command);
    $Xmax=0.0;
    $Xmin=-2000.;
    open(OF,">$of1") || die "cannot open $of1 \n";
    open(OF2,">$of2") || die "cannot open $of2 \n";
    while($Xmin < $Xmax)
    {
	$freq=0;
	$freq_c=0;
	for(0 .. $#dchisq)
	{
	    if ($dchisq[$_] >= ($Xmin-$Xstep/2) && $dchisq[$_] < ($Xmin+$Xstep/2)) { $freq = $freq+1; }
	    if ($dchisq[$_] >= ($Xmin-$Xstep/2) && $dchisq[$_] < ($Xmin+$Xstep/2) && $clump_index[$_] <= $cut) { $freq_c = $freq_c+1; }
	}
	printf OF " %12.2f %5d\n",$Xmin,$freq;
	printf OF2 " %12.2f %5d\n",$Xmin,$freq_c;
	$Xmin=$Xmin+$Xstep;
    }
    close(OF) || die "cannot close $of1 \n";
    close(OF2) || die "cannot close $of2 \n";

# Create period histogram output files
    unlink($of3);
    unlink($of4);
    open(OF,">$of3") || die "cannot open $of3 \n";
    open(OF2,">$of4") || die "cannot open $of4 \n";
    while($Pmin < $Pmax)
    {
	$freq=0;
	$freq_c=0;
	for(0 .. $#period)
	{
	    if ($period[$_] >= ($Pmin-$Pstep/2) && $period[$_] < ($Pmin+$Pstep/2)) { $freq = $freq+1; }
	    if ($period[$_] >= ($Pmin-$Pstep/2) && $period[$_] < ($Pmin+$Pstep/2) && $clump_index[$_] <= $cut) { $freq_c = $freq_c+1; }
	}
	printf OF " %12.2f %5d\n",$Pmin,$freq;
	printf OF2 " %12.2f %5d\n",$Pmin,$freq_c;
	$Pmin=$Pmin+$Pstep;
    }
    close(OF) || die "cannot close $of3 \n";
    close(OF2) || die "cannot close $of4 \n";

# Create sm file for plotting

    $label=$field."_".$camera;
    $fname=$field."_".$camera."_distri.ps";
    open(SM,">$smfile") || die "cannot open $smfile \n";
    printf SM  "    device postencap %30s \n",$fname;
    printf SM  "    ctype black \n";
    printf SM  "    ptype 10 3 \n";
    printf SM  "    expand 0.7 \n";
    printf SM  "    toplabel %16s \n",$label;
    printf SM  "";
    printf SM  "    data %16s \n",$of1;
    printf SM  "    read {x1 1 x2 2}\n";
    printf SM  "    window 2 2 1 2\n";
    printf SM  "    limits -2000 0 x2 \n";             #x-axis in chi^2 distribution will have only [-1000:0]
    printf SM  "    hist x1 x2 \n";
    printf SM  "    ctype red \n";
    printf SM  "    data %10s\n",$of2;
    printf SM  "    read {x1 1 x2 2}\n";
    printf SM  "    hist x1 x2 \n";
    printf SM  "    ctype black \n";
    printf SM  "    box  \n";
    printf SM  "    xlabel  Delta-Chisq\n";
    printf SM  "    ylabel N\n";
    printf SM  "";
    printf SM  "    data %16s\n",$of3;
    printf SM  "    read {x1 1 x2 2}\n";
    printf SM  "    window 2 2 2 2 \n";
    printf SM  "    limits x1 x2 \n";
    printf SM  "    hist x1 x2 \n";
    printf SM  "    ctype red \n";
    printf SM  "    data %16s\n",$of4;
    printf SM  "    read {x1 1 x2 2}\n";
    printf SM  "    hist x1 x2 \n";
    printf SM  "    ctype black \n";
    printf SM  "    box  \n";
    printf SM  "    xlabel  Period\n";
    printf SM  "    ylabel N\n";
    printf SM  "";
    printf SM  "    data %16s\n",$of;
    printf SM  "    read {x1 1 x2 2 x3 3}\n";
    printf SM  "    window 2 2 1 1\n";
    printf SM  "    limits x1 x2 \n";
    printf SM  "    points x1 x2 \n";
    printf SM  "    ctype red \n";
    printf SM  "    points x1 x2 if(x3< %8.2f)\n",$cut;
    printf SM  "    ctype black \n";
    printf SM  "    box  \n";
    printf SM  "    xlabel  Period\n";
    printf SM  "    ylabel Epoch\n";
    printf SM  "";
    printf SM  "    window 2 2 2 1 \n";
    printf SM  "    limits x1 x3 \n";
    printf SM  "    points x1 x3 \n";
    printf SM  "    relocate (10000 1500) putlabel 5 No of candidates before the cut = %5d \n",$#period;
    printf SM  "    ctype red \n";
    printf SM  "    relocate 0 %8.2f draw 6 %8.2f\n",$cut,$cut;
    printf SM  "    relocate (18000 500) putlabel 5 (red indicates the points after the cut on clumping index) \n";
    printf SM  "    relocate (25000 1500) putlabel 5 No of candidates after the cut = %5d \n",$Ngood;
    printf SM  "    ctype black \n";
    printf SM  "    box  \n";
    printf SM  "    xlabel  Period\n";
    printf SM  "    ylabel clumping index\n";
    close(SM) || die "cannot close $smfile \n";

    $command="/usr/local/bin/sm < $smfile ";
#    system($command);
}


#----------------------------------------------------------------------

sub fitsio_err {
    my $status = shift;
    my $errmsg = '';
    ffgerr($status, $errmsg);
    die( @_, ": $errmsg\n");
}   


1;
