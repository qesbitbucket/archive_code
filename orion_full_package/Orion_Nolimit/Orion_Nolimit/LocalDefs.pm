#!perl

use Sys::Hostname;
my $hostname=hostname();
$hostname="spectrum" if $hostname=~/comp\d{2}/;
$hostname="wasphead" if $hostname=~/wasp\d{2}/;
$hostname="ngts" if $hostname=~/ngts\d{2}|ngtsaux1/;

require "LocalDefs.$hostname";

1;
