#ifndef _MULTIENGINE_H_
#define _MULTIENGINE_H_

#include <vector>
#include <map>

#include "types.h"
#include "fitsio.h"
#include "utimage.h"
#include "utobject.h"
#include "matrix.h"
#include "outfile.h"

typedef struct {
  string filename;
  fitsfile *fptr, *lcptr, *pgptr;
} OutputFile;

class UTFile {
 public:
  string tag, filename, field, obsStart, obsStop, season, project;
  int cameraID;
  fitsfile *fptr;
  int status, tStart, tStop;
  long numObjects, numImages;
  vector<UTObject*> catalogue, defaultStdList, customStdList, *currentStdList;
  vector<UTImage*> images;
  matrix<MATRIX_T> *defaultMatrix, *customMatrix, *currentMatrix, *rawMatrix;
  int firstNight, lastNight;
  bool hasHarmonicFit;
};

class MultiEngine {
 public:
  int status, tStart, tStop;
  string obsStart, obsStop;
  int firstNight, lastNight;
  int numImages, numObjects, numSegments;
  vector<UTFile*> files;
  vector<UTImage*> masterImList;
  vector<UTRange*> segBounds, nightBounds;
  map<string, UTObject*> masterCatByName;
  vector<UTObject*> masterCat, allCalStandards;
  bool hasFakeTransits, hasTamuz;
  OutputFile *outFile;
  valarray<bool> dtMask;
  string tag, filename, field, season, project;
  int cameraID, numLCwritten;
  long numPtsWritten, maxPtsWritten;
  
 public:
  MultiEngine();
  ~MultiEngine();
  void addInputFile(string filename);
  void initialiseTFA(void);
  void readLightcurve(UTObject *obj);
  void detrend(UTObject *obj, bool debug=false);
  void reconstruct(UTObject *obj, CandPeak *p);
  void transitStats(UTObject *obj, CandPeak *p);
  void createOutputFile(string filename);
  void closeOutputFile(void);
  void refineTransit(UTObject *obj, CandPeak *p);
  int writeCandidate(UTObject *obj, CandPeak *peak, int rank);
  void createPeriodogramStore(vector<double> s);
  int writePeriodogram(UTObject *t, valarray<PSample>, int boxSize,
		       double grad, double itcpt, double rms);
  void createLightcurveStore(int numSearcha);
  int writeLightcurve(UTObject *t);
  void updateCatalogue(vector<UTObject*> cat);
  void adjustCustomMatrix(UTFile *s);
  void computeStarSigmaXS(UTObject*t);
};


#endif
