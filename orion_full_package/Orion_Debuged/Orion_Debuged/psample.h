#ifndef _PSAMPLE_H_
#define _PSAMPLE_H_

typedef struct {
  double deltaChisq, depth, depthVar, width, period;
  int numPtsInTransit, epoch, boxSize;
  float sde; //, rms;
} PSample;


#endif
