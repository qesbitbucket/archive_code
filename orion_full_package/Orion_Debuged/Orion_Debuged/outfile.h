#ifndef _OUTFILE_H_
#define _OUTFILE_H_

#include "utobject.h"
#include "candpeak.h"
#include "psample.h"

#include <vector>

class OutFile {
  string filename;
  fitsfile *fptr;
  int status;
  bool hasFake;
 public:
  OutFile(const char *filename, bool withFakeCols, vector<UTObject*> cat);
  ~OutFile(void);
  void close(void);
  void writeCandidate(UTObject *obj, CandPeak *peak, int rank);
  void createPeriodogramStore(vector<double> s);
  int writePeriodogram(UTObject *t, valarray<PSample>, int boxSize, double grad,
		       double itcpt, double rms);
  void createLightcurveStore(void);
  int writeLightcurve(UTObject *t);
  void updateCatalogue(vector<UTObject*> cat);
};


#endif
