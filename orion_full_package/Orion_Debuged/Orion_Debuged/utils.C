#include <cmath>
#include <iostream>
#include <iomanip>
#include <string>
#include <sstream>
#include <valarray>
#include <vector>
#include <algorithm>
#include <limits>
#include <fstream>
#include <sys/time.h>

#include "utils.h"

double get_time(void)
{
    struct timeval tval;
    gettimeofday(&tval, NULL);
    return tval.tv_sec+tval.tv_usec*1.0e-6;
}
