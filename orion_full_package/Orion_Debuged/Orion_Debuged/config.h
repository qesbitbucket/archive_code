#ifndef _CONFIG_H_
#define _CONFIG_H_

// Version string of code
#define CODE_VERSION "1.1"

// Threshold for sigma-clipping
#define SIGCLIP_THRESH 5.0

// To be considered as a cal star an object must be at least this many arcmin
// from any other catalogued object
#define TFA_CAL_MIN_NEAREST 10.0

// Calibration stars must have more valid points than the following percentile
// in the distribution of npts in a segment 
//
#define TFA_CAL_NPTS_PERCENTILE 0.7

// Reference JD for epochs, etc.
//
const double EPOCH_REF=0.0;

// Peaks detected in the periodogram only if their delta chisq is
// great than PDGRAM_RMS_PEAK_THRESH * the RMS of the periodogram
// about the fitted central trend line
//
//#define PDGRAM_SDE_PEAK_THRESH 7.0
//
// This is now a command-line option

#define BINWIDTH 0.75*3600
#define MINBOXSIZE 2
#define MAXBOXSIZE 5

enum {periodSampleOrion, periodSampleHunter};

class Config {
 public:
  int minPoints, minBoxSize, maxBoxSize, minSegPoints;
  float minFlux, periodMin, periodMax, binWidth, minStdFlux, minPeakSDE;
  bool useTFA, writePeriodogram, writeLightcurve, superSample,
    multiBox, writeASCII, downweightTFA, exclude1Day, ignoreSingle,
    useTamuzDW, sortByRow, dumpMatrix, verbose, useHarmonicFilter,
    harshCut;
  int maxTFAstandards, maxFitPeaks, firstObject, lastObject, maxDumpPeaks,
    dumpFold, periodSampling;

  Config() { // Default configuration
    minPoints=500;
    minPeakSDE=7.0;
    minSegPoints=500;
    minFlux=3.5;
    minStdFlux=10.0;
    periodMin=0.35*86400.0;
    periodMax=10.0*86400.0;
    maxTFAstandards=600;
    writePeriodogram=true;
    writeLightcurve=true;
    writeASCII=false;
    useTFA=false;
    downweightTFA=true;
    maxFitPeaks=10;
    maxDumpPeaks=5;
    exclude1Day=false;
    dumpFold=-1;
    useTamuzDW=true;
    periodSampling=periodSampleOrion;
    sortByRow=false;
    dumpMatrix=false;
    useHarmonicFilter=false;
    harshCut=false; // Used for draconian filtering of candidates from OROF runs

    // Don't change these, they just record compile-time options 
    minBoxSize=MINBOXSIZE;
    maxBoxSize=MAXBOXSIZE;
    binWidth=BINWIDTH;
  };
};

#ifdef ISMAIN
Config config;
#else
extern Config config;
#endif

#endif
