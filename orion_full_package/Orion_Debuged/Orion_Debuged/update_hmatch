#!/usr/local/bin/perl
#

use English;
use strict qw(vars subs refs);
use File::Path;
use lib qw(/home/rgw/pandora/orion);
use Cwd;

use DBI;
use Astro::FITS::CFITSIO qw(:longnames :constants);
use FITSUtils;

# Connect to the database
my $dbh=DBI->connect("dbi:mysql:wasp;host=waspdb;user=rgw");
die "Can't connect to waspdb: $DBI::errstr" unless $dbh;

# Extract pertinent sub-set of hunterdet_peaks
my $qry;
print "Extracting list of peaks in OR_TAMTFA runs...\n";
foreach my $tab ('wasptmp.matchtmp', 'wasptmp.matchtmp2') {
    $qry="CREATE TEMPORARY TABLE $tab (obj_id CHAR(26), field CHAR(11), camera_id smallint(3), ".
	"tag CHAR(12), period DOUBLE, season enum('UNKNOWN','2004','2005','2006','2007','2008','2009','2010',".
	"'2011','2012','2013','2014','2015','MULTI') DEFAULT 'UNKNOWN', INDEX(obj_id)) ".
	"SELECT obj_id, field, camera_id, tag, period, season FROM hunterdet_peaks WHERE tag LIKE '%OR_TAMTFA'";
    $dbh->do($qry) or die "Database error: $DBI::errstr";
}

# Create the multi-season/multi-field/multi-tag match tables
print "Updating hunterdet_allmatch...\n";
$dbh->do("TRUNCATE hunterdet_allmatch");
$qry="INSERT INTO hunterdet_allmatch ".
    "SELECT t1.obj_id, COUNT(*) AS cnt FROM wasptmp.matchtmp AS t1 INNER JOIN wasptmp.matchtmp2 AS t2 ".
    "USING (obj_id) WHERE (t1.tag<>t2.tag OR t1.field<>t2.field OR t1.camera_id<>t2.camera_id) ".
    "AND (t1.period/t2.period BETWEEN 0.99 AND 1.01 OR t1.period/t2.period BETWEEN 0.495 AND 0.505 ".
    "OR t1.period/t2.period BETWEEN 1.98 AND 2.02) GROUP BY obj_id";
$dbh->do($qry) or die "Database error: $DBI::errstr";

print "Updating hunterdet_seasonmatch...\n";
$dbh->do("TRUNCATE hunterdet_seasonmatch");
$qry="INSERT INTO hunterdet_seasonmatch ".
    "SELECT t1.obj_id, t1.season, COUNT(*) AS cnt FROM wasptmp.matchtmp AS t1 INNER JOIN wasptmp.matchtmp2 AS t2 ".
    "USING (obj_id) WHERE t1.season=t2.season AND t1.field<>t2.field AND t1.camera_id<>t2.camera_id ".
    "AND (t1.period/t2.period BETWEEN 0.99 AND 1.01 OR t1.period/t2.period BETWEEN 0.495 AND 0.505 ".
    "OR t1.period/t2.period BETWEEN 1.98 AND 2.02) GROUP BY obj_id, season";
$dbh->do($qry) or die "Database error: $DBI::errstr";

print "Updating hunterdet_tagmatch...\n";
$dbh->do("TRUNCATE hunterdet_tagmatch");
$qry="INSERT INTO hunterdet_tagmatch ".
    "SELECT t1.obj_id, t1.tag, COUNT(*) AS cnt FROM wasptmp.matchtmp AS t1 INNER JOIN wasptmp.matchtmp2 AS t2 ".
    "USING (obj_id) WHERE t1.tag=t2.tag AND t1.field<>t2.field AND t1.camera_id<>t2.camera_id ".
    "AND (t1.period/t2.period BETWEEN 0.99 AND 1.01 OR t1.period/t2.period BETWEEN 0.495 AND 0.505 ".
    "OR t1.period/t2.period BETWEEN 1.98 AND 2.02) GROUP BY obj_id, tag";
$dbh->do($qry) or die "Database error: $DBI::errstr";

# Update the hunterdet table from the view
print "Updating hunterdet table...\n";
my $qry="TRUNCATE hunterdet";
$dbh->do($qry) or die "Database error: $DBI::errstr";
$qry="INSERT INTO hunterdet SELECT * FROM hunterdet_view";
$dbh->do($qry) or die "Database error: $DBI::errstr";

# Close connection to database
$dbh->disconnect();
