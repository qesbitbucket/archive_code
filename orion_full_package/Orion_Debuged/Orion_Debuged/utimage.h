#ifndef _UTIMAGE_H_
#define _UTIMAGE_H_

#include "types.h"

class UTImage {
 public:
  ImageID imageID;
  int hiCount, loCount, night, tMid, numCalPts;
  float imSigmaXS, zeroPoint;
};


#endif
