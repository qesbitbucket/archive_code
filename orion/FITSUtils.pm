package FITSUtils;

use strict;
use vars qw($VERSION @ISA @EXPORT @EXPORT_OK %EXPORT_TAGS $AUTOLOAD);
use English;

require Exporter;
require DynaLoader;
require AutoLoader;

@ISA = qw(Exporter DynaLoader);

use Astro::FITS::CFITSIO qw(:longnames :constants);


#
sub enumerate_columns {
    my ($fptr, $status)=@ARG;

    #
    my ($ncols, $name, $col);
    $fptr->get_num_cols($ncols, $status);
    for(my $idx=1; $idx<$ncols; $idx++) {
	$fptr->read_key_str("TTYPE$idx", $name, undef, $status);
	$$col{uc $name}=$idx;
    }

    #
    return $col;
}

#
sub read_table {
    my ($fptr, $status)=@ARG;

    #
    my ($ncols, $name, $data, $type, $repeat, $width, $bval,
	$nrows, $anynul);
    $fptr->get_num_cols($ncols, $status);
    $fptr->get_num_rows($nrows, $status);
    for(my $idx=1; $idx<=$ncols; $idx++) {
	$fptr->read_key_str("TTYPE$idx", $name, undef, $status);
	$fptr->get_coltype($idx, $type, $repeat, $width, $status);
	next if $type<0;

	if($type==TSTRING) {
	    $bval='\0' x ($width+1);
	    $repeat=1;
	}
	$bval=0 if $type==TSHORT || $type==TINT || $type==TLONG ||
	    $type==TLOGICAL || $type==TBIT || $type==TBYTE;
	$bval=0.0 if $type==TFLOAT || $type==TDOUBLE;
	$bval=[($bval) x $repeat];
	$fptr->read_col($type, $idx, 1, 1, $nrows*$repeat, undef,
			$bval, $anynul, $status);
	if($repeat==1) {
	    $$data{uc $name}=$bval;
	} else {
	    my $vval=[];
	    for(my $r=0; $r<$nrows; $r++) {
		push(@$vval, [splice(@$bval, 0, $repeat)]);
	    }
	    $$data{uc $name}=$vval;
	}
    }

    #
    return $data;
}

#
sub read_table_row {
    my ($fptr, $rownum, $status)=@ARG;

    #
    my ($ncols, $name, $data, $type, $repeat, $width, $bval, $anynul);
    $fptr->get_num_cols($ncols, $status);
    for(my $idx=1; $idx<=$ncols; $idx++) {
	$fptr->read_key_str("TTYPE$idx", $name, undef, $status);
	$fptr->get_coltype($idx, $type, $repeat, $width, $status);
	next if $type<0;

	if($type==TSTRING) {
	    $bval='\0' x $width;
	    $repeat=1;
	}
	$bval=0 if $type==TSHORT || $type==TINT || $type==TLONG ||
	    $type==TLOGICAL || $type==TBIT || $type==TBYTE;
	$bval=0.0 if $type==TFLOAT || $type==TDOUBLE;
	$bval=[($bval) x $repeat];
	$fptr->read_col($type, $idx, $rownum, 1, $repeat, undef,
			$bval, $anynul, $status);
	if($repeat==1) {
	    $$data{uc $name}=$$bval[0];
	} else {
	    $$data{uc $name}=$bval;
	}
    }

    #
    return $data;
}

1;
