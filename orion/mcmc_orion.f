C+      
        program mcmctransit
        
c  ***  This is Andrew Cameron's parameter-fitting code for
c       transiting exoplanets. 

c  ***  This version reads 
c  ***  Important: please read the blurb at line 540

c  ***  Recent History:

c       v1.3 2007-09-04 ACC at St A 
c       Calculate and report opposition flux ratio for unit albedo,
c       synchronisation timescales for star and planet, Log g for
c       star and planet, planet density in jovian units.
        
c       v1.4 2007-09-05 ACC at St A 
c       Reduce initial guesstimate of transit epoch uncertainty from
c       5 min to 2 min when setting starting parameters.
c       This reduces the likelihood of jumping too far during burn-in.
                
c       v1.5 2007-09-18 ACC at St A 
c       Report planet/star blackbody equilibrium temperature ratio.

c       v1.6 2008-10-08 RGW
c       Adapt to read and write output from Orion BLS code
c       Parallelised the main loops
c       Reduced size of some arrays to prevent stacksize problems
c       Dump chsmin to output file

        implicit none
        
c       real*8 pi
        
        real*8 t0,period,dmag,wday,k1,vsi
        real*8 sigt0,sigper,sigdm,sigw,sigvk,sigvs
        real*8 sigb, siglam
c  ***  Limb-darkening coeffs from Claret (2000)
        
        integer i, j, k, idum, jtemp, ktemp
        integer maxjmp
        parameter(maxjmp=10000)
        integer njump
        real*8 t(maxjmp)
        real*8 p(maxjmp)
        real*8 rs(maxjmp)
        real*8 ms(maxjmp)
        real*8 rp(maxjmp)
        real*8 b(maxjmp)
        real*8 d(maxjmp)
        real*8 wf(maxjmp)
        real*8 vk(maxjmp)
        real*8 vs(maxjmp)
        real*8 gam(maxjmp)
        real*8 lam(maxjmp)
        real*8 rh(maxjmp)
        real*8 chisq(maxjmp)
        real*8 chsp(maxjmp)
        real*8 chph(maxjmp)
        real*8 mpjup(maxjmp)
        real*8 rpjup(maxjmp)
        real*8 rstar(maxjmp)
        real*8 aau(maxjmp)
        real*8 deginc(maxjmp)
        integer idx(maxjmp)
        real*8 dxl,xml,dxh
        real*8 sigwf
c       real*8 mstar
        real*8 cosi
c       real*8 psec,akm,q,vt,sini
        real*8 scfac, oldscfac
c       real*8 dchisq,pjump,prob
        character*1000 photfile,rvfile
c       character*80 parfile
        integer lu
        integer mxobs,mxvel
c       real*8 x,u,w,sx,su,sw,sxx,sxu,suu,xhat,uhat
c       integer nx
c       integer status
        real*8 wfrac,hw1,hw2
c       integer norb
        real*8 chsmin,t0best,pbest,wfbest,dbest,vkbest,vsbest,bbest
        real*8 rpjbest,gambest, lambest
        real*8 chsbst,chpbst
        real*8 b0,lam0
        real*8 rsbest,rpbest
c       real*8 wbest
c       logical constrainmr
c       real*8 cphi,sphi
        character*1 yesno
        integer pset
c       integer fobs(100)
c       integer lobs(100)
        integer np(100)
c       real*8 adj(100)
        real*8 varscl(100)
        integer nmod
        real*8 ph,pr,xp,yp,zp,up
        real*8 z(1000),mu(1000),nu(1000)
        logical radvel
c       logical burned_in
c       real*8 dvjit
        character*30 swaspid
        integer maxpk
        parameter(maxpk=5)
        integer ipk
        real*8 eppk(maxpk)
        real*8 ppk(maxpk)
        real*8 wdpk(maxpk)
        real*8 dmpk(maxpk)
        real*8 dcpk(maxpk)
        real*8 snrpk(maxpk)
        integer ntrpk(maxpk)
        real*8 aellpk(maxpk)
        real*8 snellpk(maxpk)
        real*8 transfpk(maxpk)
        real*8 gaprpk(maxpk)
        real*8 dchsantpk(maxpk)
        real*8 jh,rpmj,rpmj_diff
        real*8 rs0
c       real*8 prp,prs
        real*8 dcmax
        integer ipmax
c       real*8 prat
c       real*8 dch_rs,dch_rp
        real*8 t0best_mc
        real*8 pbest_mc 
        real*8 msbest_mc
        real*8 wfbest_mc
        real*8 dbest_mc 
        real*8 vkbest_mc
        real*8 vsbest_mc
        real*8 bbest_mc 
        real*8 lambest_mc
        real*8 gambest_mc
        real*8 dincbest,rstbest,mstbest
        integer iday(6), id, mm, iyyy, jdtrunc
        real*8 hjdtoday,tnow(maxjmp)
        integer norbnow
c       real*8 t2, t1
        integer mcorr
        integer mxcorr
        parameter(mxcorr = 100)
        real*8 c_t(mxcorr)
        real*8 c_p(mxcorr)
        real*8 c_d(mxcorr)
        real*8 c_w(mxcorr)
        real*8 c_b(mxcorr)
        real*8 c_ms(mxcorr)
        real*8 c_vs(mxcorr)
        real*8 c_vk(mxcorr)
        real*8 c_lam(mxcorr)
        real*8 cl_t,cl_p,cl_d,cl_w,cl_b,cl_ms,cl_vk,cl_vs,cl_lam
        logical reject
        real*8 ratio
        integer kst, ken
c       double precision st,sp,sm,sw,sd,sk,sv,sb
c       double precision stt,spp,smm,sww,sdd,skk,svv,sbb
        integer nburn
        real*8 prp,prs,prb
        integer strikes
        real*8 xfunk(9)
        real*8 funk
        real*8 yval(10)
        integer indy(10)
        real*8 parms(10,9)
        real*8 ftol
        integer iter
        integer kbest
        real*8 chisqbest,chphbest,chspbest
        real*8 dchisq_mr
        real*8 rhobest,aaubest,mpjbest
        real*8 chisq_con,chisq_unc
        real*8 cosl, sinl
        real*8 sigma_t0, sigma_p, sigma_w,sigma_d
        real*8 sigma_rst,sigma_mst,sigma_rp
        integer pkidx, numpk, cnd_idx, cat_idx, rstcnt

        real*8 rpsq(maxjmp)
        real*8 qrat,gacc_p,gacc_s
        real*8 tau_s(maxjmp)
        real*8 tau_p(maxjmp)
        real*8 logg_s(maxjmp)
        real*8 logg_p(maxjmp)
        real*8 rho_p(maxjmp)
        real*8 teql_p(maxjmp)
        real*8 tjh
c       real*8 chph,chsp,funk,varscl
        
	real*8  aellip,snellip		!get_ellip
	integer ntrn,nsel		!get_ellip
	real*8	snred			!rednoise
	real		vk_teff,jh_teff
	real		rstar_vk,rstar_jh,rpl_vk,rpl_jh
	integer*2	giant_flg,cat_flg

        include 'common.inc'
        
        external funk, iargc, time
        integer julday, iargc, time

        character*80 tmpstr

        pi = 4d0*atan2(1d0,1d0)
        idum = time()
        idum = mod(idum,50000)
                
        pset = 0
        ndata = 0

        if (iargc().lt.2) then
          print *,'Usage: mcmc_orion <inputfile> <swasp_id> [<peak>]'
          stop
        else
          call getarg(1,photfile)
          call getarg(2,swaspid)
          if(iargc().ge.3) then 
             call getarg(3,tmpstr)
             read(tmpstr, fmt=*) pkidx
          else
             pkidx=1
          endif
c          call getarg(4, tmpstr)
c          read(tmpstr, fmt=*) PMAX
        endif

c       print *,' Enter filename with LCFINDER photometry : '
c       read(*,'(a)') photfile
        
c  ***  Read photometric data from LCFINDER output file 
c       photfile = 'phot.dat'
c       lu = 42
        mxobs = maxobs
        pset = pset + 1
        fobs(pset) = ndata+1
        call read_orion(photfile,swaspid,pkidx,numpk,jh,rpmj,rpmj_diff,
     :             eppk,ppk,wdpk,dmpk,dcpk,snrpk,ntrpk,
     :             aellpk,snellpk,transfpk,gaprpk,dchsantpk,
     :             mxobs,np(pset),hjd(ndata+1),mag(ndata+1),err(ndata+1),
     $       cat_idx,cnd_idx,vk_teff,jh_teff,rstar_vk,rstar_jh,giant_flg,rpl_vk,
     $       rpl_jh,cat_flg)

        ndata = ndata + np(pset)
        lobs(pset) = ndata
             
c       print *,' Read another photometric dataset?(Y/N) [N] : '
c       read(*,'(a)') yesno
c       if ( yesno(1:1).eq.'y' .or. yesno(1:1).eq.'Y' ) then
c          go to 12
c       else
c          go to 13
c       end if
        
c12     print *,' Enter filename with ancillary photometry : '
c       read(*,'(a)') photfile
        
c  ***  Read photometric data from file 
c       photfile = 'phot.dat'
c       lu = 42
c       pset = pset + 1
c       fobs(pset) = ndata+1
c       call rddata(lu,photfile,mxobs,np(pset),
c     : hjd(ndata+1),mag(ndata+1),err(ndata+1))
c       ndata = ndata + np(pset)
c       lobs(pset) = ndata
        
c       print *,' Read another photometric dataset?(Y/N) [N] : '
c       read(*,'(a)') yesno
c       if ( yesno(1:1).eq.'y' .or. yesno(1:1).eq.'Y' ) then
c          go to 12
c       end if
        
13      npset = pset
        
c  ***  Read radial velocity data from file
c       print *,' Enter filename with radial velocity data [none]: '
c       read(*,'(a)') rvfile
c       if ( rvfile .eq. ' ' .or. rvfile.eq.'none' 
c     :  .or. rvfile.eq.'NONE' ) then
c          radvel = .false.
c          nvel = 0
c       else
c          lu = 44
c          mxvel = maxrv
c          call rddata(lu,rvfile,mxvel,nvel,hjdrv,rv,rverr)
c          radvel = .true.
c          print *,'Enter jitter in km/sec : '
c          read(*,*) dvjit
c       end if
        
c  ***  Read J-H colour (no longer needed as J-H is now obtained 
c       by readnewlc from the header of the .lc file.

c       print *,' Enter 2MASS J-H colour : '
c       read(*,*) jh

c  ***  Estimate stellar parameters from J-H
        call getparms(jh,rs0,ms0,c1,c2,c3,c4)
        
c        print *,' You may either let the data determine '
c        print *,' the stellar radius, or impose a main '
c        print *,' sequence mass-radius relation if the '
c        print *,' S:N of the photometry is poor.' 
c        print *, '  '
c11     print *,' Impose main-sequence mass-radius relation?(Y/N)[N] : '
c       read(*,'(a)') yesno
        yesno='y'
        if ( yesno(1:1).eq.'y' .or. yesno(1:1).eq.'Y'  ) then
           constrainmr = .true. 
           print *,'Main sequence radius constraint ON'
        else if ( yesno(1:1).eq.'n' .or. yesno(1:1).eq.'N' 
     :  .or. yesno(1:1).eq.' ') then
           constrainmr = .false.
           print *,'Main sequence radius constraint OFF'
        else
c          go to 11
        end if
        
c       print *,' Enter number of iterations : '
c       read(*,*) njump
        njump=2500
        
c  ***  Allow 500 steps for burn-in
        nburn = 500
        njump = njump + nburn
        
c       dcmax = 0d0
c       do ipk = 1, 5
c          print *,' Peak ',ipk,'  dchisq = ',dcpk(ipk)
c          dcmax = max(dcmax,dcpk(ipk))
c          if(dcmax .eq.dcpk(ipk)) ipmax = ipk
c       end do
        
c       print *,' Select HUNTER peak 1,2,3,4, 5 or 0 to define :'
c       read(*,*) ipk
        ipk=1
        
c       if(ipk .lt.0 ) then
c          ipk = ipmax
c          print *, ' Automatically choosing peak ',ipk
c       end if
        
c       if(ipk.eq.0)then
c          print *,' Enter t0,p,wday,dmag : '
c          read(*,*) t0, period, wday, dmag
c       else
           t0 = eppk(ipk)
           period = ppk(ipk)
           wday = wdpk(ipk)
           dmag = dmpk(ipk)
c       end if

c       print*,'wday=',wday,' period=',period,' epoch=',t0,' dmag=',dmag

        rstcnt=0          
3       k1 = 0.1d0
        vsi = 5d0
        sigt0 = 2d0 / 60d0 /24d0  
        sigdm = 0.002d0
        sigw = 0.003d0
        sigvk = 0.0d0
        sigvs = 0.0d0
        siglam = 0.0d0
        sigms = ms0/10d0
        wfrac = wday / period
        sigwf = sigw / period
        hw1 = wfrac / 2d0
        hw2 = 1d0 - hw1

c  ***  Orthogonalise T0 and p by adjusting epoch of transit
c       to centre of mass of data taken in transit.

        call orthtp(ndata,hjd,err,nvel,hjdrv,rverr,
     :                    t0,sigt0,period,sigper,hw1,hw2)

c  ***  Calculate number of orbits elapsed to current epoch

        call idate(iday)
        id = iday(1)
        mm = iday(2)
        iyyy = iday(3)
        jdtrunc = julday(mm,id,iyyy) - 2450000
        
        hjdtoday = dfloat(jdtrunc)-0.5d0
        
        
c        norbnow = nint((hjdtoday-t0)/period)
        norbnow = nint(t0/period)

c  ***  Compute initial estimate of K1
        call orthk1(nvel,hjdrv,rv,err,t0,period,k1,sigvk)

c  ***  Initialise counters

        k = 1
        ktemp = 0
        j = 1
        jtemp = 0
        scfac = .5d0
        b0 = 0.5d0
        sigb = 0.05d0
        lam0 = 0d0
        siglam = 0d0
        
c  ***  Define initial parameter set
        t(k) = t0
        p(k) = period
        wf(k) = wfrac
        ms(k) = ms0
        d(k) = dmag
        vk(k) = k1
        vs(k) = vsi
        lam(k) = lam0
        b(k) = b0
        
c  ***  Convert proposal parameters to physical parameters

        call physparms(p(k),wf(k),ms(k),d(k),b(k),vk(k),
     :                 rs(k),rh(k),aau(k),rstar(k),rp(k),
     :                 cosi,deginc(k),mpjup(k),rpjup(k))
        
c  ***  Generate model from parameters; fit to data and compute
c       penalty function chisq(k)

        call eval(t(k),p(k),ms(k),rs(k),rp(k),cosi,
     :            vk(k),vs(k),gam(k),lam(k),rstar(k),
     :            c1,c2,c3,c4,ms0,sigms,constrainmr,
     :            ndata,npset,fobs,lobs,adj,hjd,mag,err,
     :            nvel,hjdrv,rv,rverr,dvjit,
     :            chph(k),chsp(k),chisq(k),varscl)


c  ***  Adjust errors so that post-fit chisq ~ N -- commented out because
c       I want to defer this until we've got closer to the optimum solution; 
c       doing it here is premature and will lead to overestimation of the 
c       errors.

c       do pset = 1, npset
c          do i = fobs(pset),lobs(pset)
c             err(i) = err(i) * sqrt(varscl(pset))
c          end do
c          print *,pset,sqrt(varscl(pset))
c       end do

c  ***  Call eval again to readjust initial chisq
        call eval(t(k),p(k),ms(k),rs(k),rp(k),cosi,
     :            vk(k),vs(k),gam(k),lam(k),rstar(k),
     :            c1,c2,c3,c4,ms0,sigms,constrainmr,
     :            ndata,npset,fobs,lobs,adj,hjd,mag,err,
     :            nvel,hjdrv,rv,rverr,dvjit,
     :            chph(k),chsp(k),chisq(k),varscl)

c  ***  Start of main Markov-chain Monte Carlo loop
        chsmin = chisq(k)

        strikes = 0     
2       jtemp = 0

c  ***  Take another 100 jumps
        do ktemp = 1, 100
        
           k = k + 1
           
           if( k.eq.400 ) then     
c  ***        By jump 400 we should be near enough to the optimum solution
c             that it will be safe to scale the error bars on the data 
c             and force post-fit chisq ~ N.

              do pset = 1, npset
                 do i = fobs(pset),lobs(pset)
                    err(i) = err(i) * sqrt(varscl(pset))
                 end do
                 print *,pset,sqrt(varscl(pset))
              end do

c  ***        Call eval to rescale previous chisq
              call eval(t(k-1),p(k-1),ms(k-1),rs(k-1),rp(k-1),cosi,
     :                  vk(k-1),vs(k-1),gam(k-1),lam(k-1),rstar(k-1),
     :                  c1,c2,c3,c4,ms0,sigms,constrainmr,
     :                  ndata,npset,fobs,lobs,adj,hjd,mag,err,
     :                  nvel,hjdrv,rv,rverr,dvjit,
     :                  chph(k-1),chsp(k-1),chisq(k-1),varscl)     
           end if

c  ***     Generate new proposal parameters     
1          call propose(t(k-1),p(k-1),ms(k-1),wf(k-1),d(k-1),
     :          vk(k-1),vs(k-1),b(k-1),lam(k-1),
     :          t(k),p(k),ms(k),wf(k),d(k),vk(k),vs(k),b(k),lam(k),
     :          sigt0,sigper,sigms,sigwf,sigdm,
     :          sigvk,sigvs,sigb,siglam,scfac,idum)
           
           jtemp = jtemp + 1
           
c          Emergency restart in case algorithm gets stuck
           if ( jtemp .gt. 5000 ) then
              print *,' Markov chain stuck at trial ',k
              if(k.lt.5) then
                 print*,'Useless case'
                 stop
              endif
c              print *, t(k),p(k),ms(k),wf(k),d(k),vk(k),vs(k),b(k)
c              print *, chisq(k)
              do pset = 1, npset
c                 print *, pset, lobs(pset)-fobs(pset)+1,varscl(pset)
              end do
              print *,' Backtracking to trial ',k - ktemp 
              k = k - ktemp
              strikes = strikes + 1
c              print *, t(k),p(k),ms(k),wf(k),d(k),vk(k),vs(k),b(k)
              scfac = 0.4d0
              if(strikes .ge. 2 ) then
                 rstcnt=rstcnt+1
                 if(rstcnt.le.2) then
                    print *, 'Failed 3 times; restarting'
                    go to 3
                 else
                    print *, 'Chain got stuck too many times, abandoning'
                    stop
                 endif
              else
                 go to 2
              end if
           end if
        
c  ***     Convert proposal parameters to physical parameters

           call physparms(p(k),wf(k),ms(k),d(k),b(k),vk(k),
     :                 rs(k),rh(k),aau(k),rstar(k),rp(k),
     :                 cosi,deginc(k),mpjup(k),rpjup(k))
        
c          print *,'finished physparms'
c  ***     Generate model from parameters; fit to data and compute
c          penalty function chisq(k)
           call eval(t(k),p(k),ms(k),rs(k),rp(k),cosi,
     :            vk(k),vs(k),gam(k),lam(k),rstar(k),
     :            c1,c2,c3,c4,ms0,sigms,constrainmr,
     :            ndata,npset,fobs,lobs,adj,hjd,mag,err,
     :            nvel,hjdrv,rv,rverr,dvjit,
     :            chph(k),chsp(k),chisq(k),varscl)
     

c  ***     Save most probable proposal parameters
           if ( chisq(k) .lt. chsmin ) then
              t0best_mc = t(k)
              pbest_mc = p(k)
              msbest_mc = ms(k)
              wfbest_mc = wf(k)
              dbest_mc = d(k)
              vkbest_mc = vk(k)
              vsbest_mc = vs(k)
              bbest_mc = b(k)
              lambest_mc = lam(k)
              chpbst = chph(k)
              chsbst = chsp(k)
              chsmin = chisq(k)    
           end if
           
c  ***     Extrapolate transit epoch to late 2006
           tnow(k) = mod(t(k), p(k)) + p(k)*dfloat(norbnow)

           call methast(idum,chisq(k),chisq(k-1),reject)
c          print *,'finished methast'
c          print *,k,chisq(k),chisq(k-1)

           if(reject) go to 1
        end do     
           
c  ***  Every 100 steps, perform housekeeping tasks.

c  ***  Adaptive step-size control 
c        print *,' '
        print *,'Completed ',k,' of ',njump,' proposals.'
        
c        print *,'Chisq (min)      = ', chsmin

        ratio = dfloat(ktemp) / dfloat(jtemp)
c        print *,'Acceptance rate  = ',ratio

        oldscfac = scfac
c        print *,'Old scale factor = ',oldscfac

        scfac = scfac * ratio * 4d0
        scfac = max(scfac,0.4d0)
        scfac = min(scfac,1.0d0)
        scfac = min(scfac,oldscfac*1.3d0)
c        print *,'New scale factor = ', scfac

c  ***  Recompute statistics for proposal parameters
        kst = k - 99
        ken = k
        if ( kst .gt. nburn ) then
           
c           print *,'calling newstats'
           call newstats(njump+1,t,p,ms,wf,d,vk,vs,b,lam,
     :          kst,ken,
     :          sigt0,sigper,sigms,sigwf,sigdm,
     :          sigvk,sigvs,sigb,siglam,radvel)
        end if
           
c        print *,sigt0,sigper,sigms,sigwf,sigdm,sigvk,sigvs,sigb
        
c  ***  Are we done yet?        
        if(k .lt. njump) go to 2

c  ***  End of main Markov-chain Monte Carlo loop

c  ***  Report best-fit parameters. Use sorting to establish
c       confidence limits.
        print *,' Best-fit parameter set: '
        
        call conflim(nburn,njump,t,chisq,idx,dxl,xml,dxh)
        sigma_t0 = ( dxl + dxh )/2d0
        print *,' Transit epoch      = ',xml,' +',dxh,' -',dxl
        
        call conflim(nburn,njump,p,chisq,idx,dxl,xml,dxh)
        sigma_p = ( dxl + dxh )/2d0
        print *,' Orbital period     = ',xml,' +',dxh,' -',dxl,' days'

        call conflim(nburn,njump,d,chisq,idx,dxl,xml,dxh)
        sigma_d = ( dxl + dxh )/2d0
        print *,' Ratio (Rp/Rs)^2    = ',xml,' +',dxh,' -',dxl
        
        call conflim(nburn,njump,wf,chisq,idx,dxl,xml,dxh)
        dxl = dxl * pbest_mc
        xml = xml * pbest_mc
        dxh = dxh * pbest_mc
        sigma_w = ( dxl + dxh )/2d0
        print *,' Transit width      = ',xml,' +',dxh,' -',dxl,' days'
        
        call conflim(nburn,njump,b,chisq,idx,dxl,xml,dxh)
c       bbest_mc = xml
        print *,' Impact parameter   = ',xml,' +',dxh,' -',dxl,' rstar'
        
        vkbest_mc = k1
        vsbest_mc = vsi
        lambest_mc = lam0
        if ( nvel .ge. 2 ) then
        print *, ' '
        call conflim(nburn,njump,vk,chisq,idx,dxl,xml,dxh)      
c       vkbest_mc = xml
        print *,' Reflex velocity    = ',xml,' +',dxh,' -',dxl,' km/sec'
        
        call conflim(nburn,njump,gam,chisq,idx,dxl,xml,dxh)     
        gambest_mc = xml
        print *,' Gamma velocity     = ',xml,' +',dxh,' -',dxl,' km/sec'
        
        call conflim(nburn,njump,vs,chisq,idx,dxl,xml,dxh)      
c       vsbest_mc = xml
        print *,' Rotation v sin  i  = ',xml,' +',dxh,' -',dxl,' km/sec'
        
        call conflim(nburn,njump,lam,chisq,idx,dxl,xml,dxh)     
c       lambest_mc = xml
        dxl = dxl * 180d0 / pi
        xml = xml * 180d0 / pi
        dxh = dxh * 180d0 / pi
        print *,' Rossiter angle lam = ',xml,' +',dxh,' -',dxl,' deg'
        end if
        
        call conflim(nburn,njump,tnow,chisq,idx,dxl,xml,dxh)
        print *,' Current epoch range= ',xml,' +',dxh,' -',dxl
        
        print *, ' '
        call conflim(nburn,njump,rh,chisq,idx,dxl,xml,dxh)
        print *,' Stellar density    = ',xml,' +',dxh,' -',dxl,' rhsun'
c       rhobest = xml
        
        print *, ' '
        call conflim(nburn,njump,ms,chisq,idx,dxl,xml,dxh)
        print *,' Stellar mass       = ',xml,' +',dxh,' -',dxl,' M_sun'
        sigma_mst = ( dxl + dxh )/2d0
        mstbest = xml
        
        call conflim(nburn,njump,rstar,chisq,idx,dxl,xml,dxh)
        print *,' Stellar radius     = ',xml,' +',dxh,' -',dxl,' R_sun'
        sigma_rst = ( dxl + dxh )/2d0
        rstbest = xml
        
        print *, ' '
        call conflim(nburn,njump,aau,chisq,idx,dxl,xml,dxh)
        print *,' Orbital separation = ',xml,' +',dxh,' -',dxl,' AU'
        
        call conflim(nburn,njump,deginc,chisq,idx,dxl,xml,dxh)  
        print *,' Orbital inclination= ',xml,' +',dxh,' -',dxl,' deg'
        
        print *, ' '
        call conflim(nburn,njump,rpjup,chisq,idx,dxl,xml,dxh)   
        print *,' Planet radius      = ',xml,' +',dxh,' -',dxl,' R_jup'
        sigma_rp = ( dxl + dxh )/2d0
        rpjbest = xml

        call conflim(nburn,njump,chisq,chph,idx,dxl,xml,dxh)
        print *,' chisq at best chph = ',xml
        call conflim(nburn,njump,chph,chph,idx,dxl,xml,dxh)
        print *,' Best chph = ',xml

        if(nvel.ge.2)then
        call conflim(nburn,njump,mpjup,chisq,idx,dxl,xml,dxh)   
        print *,' Planet mass        = ',xml,' +',dxh,' -',dxl,' M_jup'
        call conflim(nburn,njump,chisq,chsp,idx,dxl,xml,dxh)
        print *,' chisq at best chsp = ',xml
        call conflim(nburn,njump,chsp,chsp,idx,dxl,xml,dxh)
        print *,' Best chsp = ',xml
        end if
        
        print *, ' '
        print *,' chisq_phot         = ',chpbst
        print *,' nphot              = ',ndata

        if(nvel.ge.2)then       
        print *, ' '
        print *,' chisq_spec         = ',chsbst
        print *,' nvel               = ',nvel
        end if
        
c  ***  Compute other secondary quantities of interest

        do k = 1, njump
c          Planet/star flux ratio for geometric albedo p=1:
           rpsq(k) = rp(k)*rp(k)
           
c  ***     Compute planet/star mass ratio 
           qrat = mpjup(k) / ms(k) / 1047.52d0

c          Synchronisation timescale for primary (Zahn 1977)
           tau_s(k) = 1.2 / qrat / qrat / rs(k)**6

c          Synchronisation timescale for planet (Zahn 1977)
           tau_p(k) = 1.2 * qrat * qrat / rp(k)**6

c          Planet surface gravity in jovian units
           gacc_p = mpjup(k) / rpjup(k) / rpjup(k)
           
c          Log planet surface gravity (cgs)
           logg_p(k) = log10 ( gacc_p * 2288d0 )

c          Stellar surface gravity in solar units
           gacc_s = ms(k) / rstar(k) / rstar(k)
           
c          Log stellar surface gravity (cgs)
           logg_s(k) = log10 ( gacc_s * 27400d0 )

c          Planet density in jovian units
           rho_p(k) = gacc_p / rpjup(k)
           
c  ***     Use DW's optimised FGK relations
c          Teff in kelvin       
           tjh = -4369.5d0*jh + 7188.2d0
           teql_p(k) = tjh * sqrt(rs(k)/2d0)

        end do
        
        print *, ' '
        call conflim(nburn,njump,rpsq,chisq,idx,dxl,xml,dxh)
        print *,' Opp flux ratio(p=1)= ',xml,' +',dxh,' -',dxl,' '
        call conflim(nburn,njump,tau_s,chisq,idx,dxl,xml,dxh)
        print *,' tsync (star)       = ',xml,' +',dxh,' -',dxl,' y'
        call conflim(nburn,njump,tau_p,chisq,idx,dxl,xml,dxh)
        print *,' tsync (planet)     = ',xml,' +',dxh,' -',dxl,' y'
        call conflim(nburn,njump,logg_s,chisq,idx,dxl,xml,dxh)
        print *,' Log g(star) [cgs]  = ',xml,' +',dxh,' -',dxl,' '
        call conflim(nburn,njump,logg_p,chisq,idx,dxl,xml,dxh)
        print *,' Log g(planet) [cgs]= ',xml,' +',dxh,' -',dxl,' '
        call conflim(nburn,njump,rho_p,chisq,idx,dxl,xml,dxh)
        print *,' Planet density     = ',xml,' +',dxh,' -',dxl,' rho_J'
        call conflim(nburn,njump,teql_p,chisq,idx,dxl,xml,dxh)
        print *,' Planet Teql (A=0)  = ',xml,' +',dxh,' -',dxl,' K'
        print *,' Stellar Teff(J-H)  = ',tjh,' K'
        
c  ***  The key results for planet-selection purposes are:

c       (1) the probability prp that the planet has a radius
c           less than 1.5 Rjup, with constrainmr = .true.
c           "Good" candidates should have prp > 0.5
c
c       (2) the probability prs that the star has a radius
c           less than 1.2 ms^0.8, with constrainmr = .true.
c           "Good" candidates should have prp > 0.1
c
c       (3) the difference between the best photometric chisquared
c           (chpbst) obtained for a run with constrainmr = .true
c           and that obtained with constrainmr = .false.
c           "Good" candidates should have a difference in chpbst
c           of no more than 5 or so.
c
c       (4) the one-sigma error in the fitted period and transit
c           width. False positives will tend to have less 
c           well-constrained epochs, periods and transit widths
c           than objects showing genuine boxy transits.
c
c  ***  Compute the probabilities for (1) and (2) above and write to
c       a one-line summary file bestparms.dat
        call probs(njump,rpjup,rstar,ms,b,prp,prs,prb)

c  ***  Switch on main-sequence mass-radius constraint
        constrainmr = .true.

c  ***  Load the first nine successful steps into the amoeba array              
        do k = 1, 9
c  ***     Use logarithmic scale for non-negative parameters,
c          and a tanh-like function to restrict impact 
c          parameter to 0 < b < 1 
           xfunk(1) = t(k) 
           xfunk(2) = p(k) 
           xfunk(3) = log(wf(k))
           xfunk(4) = log(ms(k))
           xfunk(5) = log(d(k))
           xfunk(6) = log(vk(k))
           xfunk(7) = log(vs(k))
           xfunk(8) = log( 1d0/(1-b(k))-1d0 )
           xfunk(9) = log( 2d0/(1-lam(k)/pi)-1d0 )
           
           do j = 1, 9
              parms(k,j) = xfunk(j)
           end do
           
           yval(k) = funk(xfunk)        
        end do

c  ***  In the tenth slot, put the best-fitting set of parameters
c       from the Markov chain

        k = 10
        xfunk(1) = t0best_mc 
        xfunk(2) = pbest_mc 
        xfunk(3) = log(wfbest_mc)
        xfunk(4) = log(msbest_mc)
        xfunk(5) = log(dbest_mc)
        xfunk(6) = log(vkbest_mc)
        xfunk(7) = log(vsbest_mc)
        xfunk(8) = log( 1d0/(1-bbest_mc)-1d0 )
        xfunk(9) = log( 2d0/(1-lambest_mc/pi)-1d0 )
        
        do j = 1, 9
           parms(k,j) = xfunk(j)
        end do
        
        yval(k) = funk(xfunk)        
        

c  ***  Find best solution with main-sequence constraint
        ftol = 1d-5
        call amoeba(parms,yval,10,9,9,ftol,funk,iter)
        call indexx(10,yval,indy)
        kbest = indy(1)
        print *, 'Chisq = ',yval(kbest),' after ',iter,' amoeba steps'
        chisq_con = yval(kbest)

c  ***  Convert proposal parameters to physical parameters

        do j = 1, 9
           xfunk(j) = parms(kbest,j)
        end do
        t0best = xfunk(1)
        pbest = xfunk(2)
        wfbest = exp(xfunk(3))
        mstbest = exp(xfunk(4))
        dbest = exp(xfunk(5))
        vkbest = exp(xfunk(6))
        vsbest = exp(xfunk(7))
        bbest = 1d0 - 1d0/(1d0 + exp(xfunk(8)))
        lambest = pi * ( 1d0 - 2d0/(1d0 + exp(xfunk(9))))
        call physparms(pbest,wfbest,mstbest,
     :                 dbest,bbest,vkbest,
     :                 rsbest,rhobest,aaubest,rstbest,rpbest,
     :                 cosi,dincbest,mpjbest,rpjbest)

        call eval(t0best,pbest,mstbest,rsbest,rpbest,cosi,
     :            vkbest,vsbest,gambest,lambest,rstbest,
     :            c1,c2,c3,c4,ms0,sigms,constrainmr,
     :            ndata,npset,fobs,lobs,adj,hjd,mag,err,
     :            nvel,hjdrv,rv,rverr,dvjit,
     :            chphbest,chspbest,chisqbest,varscl)
     
c       chisq_con = chphbest + chspbest
        chisq_con = chisqbest
        dchisq_mr = chisqbest - chphbest - chspbest

c  ***  Switch off main-sequence mass-radius constraint
        constrainmr = .false.

c  ***  Load the first ten successful steps into the amoeba array               
        do k = 1, 10
        
           xfunk(1) = t(k) 
           xfunk(2) = p(k) 
           xfunk(3) = log(wf(k))
           xfunk(4) = log(ms(k))
           xfunk(5) = log(d(k))
           xfunk(6) = log(vk(k))
           xfunk(7) = log(vs(k))
           xfunk(8) = log( 1d0/(1-b(k))-1d0 )
           xfunk(9) = log( 2d0/(1-lam(k)/pi)-1d0 )
           
           do j = 1, 9
              parms(k,j) = xfunk(j)
           end do
           
           yval(k) = funk(xfunk)        
        end do  

c  ***  Find best solution without main-sequence constraint
        constrainmr = .false.
        ftol = 1d-5
        call amoeba(parms,yval,10,9,9,ftol,funk,iter)
        call indexx(10,yval,indy)
        kbest = indy(1)
        print *, 'Chisq = ',yval(kbest),' after ',iter,' amoeba steps'
        chisq_unc = yval(kbest)
        
        print *,prp*prs
        print *,chisq_con-chisq_unc
        print *,sqrt ( sigma_p*sigma_w )
        if ( prp*prs .gt. 0.3d0 
     :       .and. abs( chisq_con-chisq_unc ) .lt. 10d0 ) then
           print *,swaspid,' is a good candidate. '
        end if

c  ***	Compute ellipsoidal variability

	call conflim(nburn,njump,rp,chisq,idx,dxl,rpbest,dxh)	     
	call conflim(nburn,njump,rs,chisq,idx,dxl,rsbest,dxh)	     

	call get_ellip(t0best,pbest,wfbest,fobs(1),lobs(1),lobs(1),
     :                 hjd,mag,err,aellip,snellip,ntrn,nsel)
	print *,'Aellip,SN_ellip ',aellip,snellip,ntrn,nsel,dbest

        call rednoise(dbest,lobs(1),hjd,mag,err,fobs(1),lobs(1),
     :        nsel,ntrn,snred)
      print *,'SN rednoise',snred

        call unlink('bestparms.dat')
        open(unit=49,file='bestparms.dat',form='formatted',status='new')

        write(49,20)swaspid,rpjbest,bbest,rstbest,mstbest,prp,prs,
     :  chisq_con,chisq_unc,sigma_t0,sigma_p,sigma_w,
     :  snrpk(ipk),ntrpk(ipk),aellpk(ipk),
     :  snellpk(ipk),transfpk(ipk),gaprpk(ipk),dchsantpk(ipk),
     :  prb,t0best,pbest,wfbest,-dbest,sigma_d,sigma_rp,sigma_rst,
     :  sigma_mst,jh,ipk,dchisq_mr
20      format(a26,1x,6(f8.4,1x),2(f11.4,1x),3(f11.6,1x),
     :         f11.4,1x,i4,1x,5(f11.6,1x),f8.4,1x,1x,f11.6,1x,
     :         f9.7,2(1x,f7.5),4(1x,f11.6),1x,f8.1,i4,1x,f11.4)

        print *,swaspid,rpjbest,bbest,rstbest,mstbest,prp,prs,
     :  vsbest,lambest,chisq_con,chisq_unc,sigma_t0,sigma_p,sigma_w

        print*,'rpmj=',rpmj,' rpmj_diff=',rpmj_diff
        call write_orion(photfile,cat_idx,cnd_idx,t0best,pbest,wfbest,dbest,
     $       rpjbest,bbest,rstbest,mstbest,prp,prs,vsbest,lambest,
     $       chisq_con,chisq_unc,sigma_t0,sigma_p,sigma_w,prb,dchisq_mr,
     $       sigma_d,sigma_rp,sigma_rst,sigma_mst,aellip,snellip,snred,
     $       chsmin,vk_teff,jh_teff,rstar_vk,rstar_jh,giant_flg,rpmj,
     $       rpmj_diff,cat_flg)

        close(49)
        
c  ***  --------------------------------------------------------------  
c  ***  Everything from here on in is expendable for a cut-down planet
c       selection code, though the output files could be used to construct
c       the graphics for a "wanted" poster.     

        mcorr = mxcorr
        call correl(nburn,njump,t,mcorr,c_t,cl_t)
        print *,' Correlation length for t0     = ',cl_t
        call correl(nburn,njump,p,mcorr,c_p,cl_p)
        print *,' Correlation length for period = ',cl_p
        call correl(nburn,njump,d,mcorr,c_d,cl_d)
        print *,' Correlation length for depth  = ',cl_d
        call correl(nburn,njump,wf,mcorr,c_w,cl_w)
        print *,' Correlation length for wf     = ',cl_w
        call correl(nburn,njump,ms,mcorr,c_ms,cl_ms)
        print *,' Correlation length for ms     = ',cl_ms
        call correl(nburn,njump,b,mcorr,c_b,cl_b)
        print *,' Correlation length for b      = ',cl_b
        if(nvel.gt.0)then
           call correl(nburn,njump,vk,mcorr,c_vk,cl_vk)
           print *,' Correlation length for vk     = ',cl_vk
           call correl(nburn,njump,vs,mcorr,c_vs,cl_vs)
           print *,' Correlation length for vs     = ',cl_vs
           call correl(nburn,njump,lam,mcorr,c_lam,cl_lam)
           print *,' Correlation length for lam    = ',cl_lam
        end if
                
c  ***  Report stellar and planetary parameters

        call unlink('output.csv')
        open(unit=43,file='output.csv',form='formatted',status='new')
        do k = 501, njump

           write(43,10)t(k),p(k),ms(k),wf(k),d(k),vk(k),vs(k),gam(k),
     :                 rstar(k),rpjup(k),mpjup(k),b(k),
     :                 chph(k),chsp(k),chisq(k),tnow(k),lam(k)*180d0/pi
10         format(f11.6,1x,f10.7,3(1x,f7.5),3(1x,f12.5),
     :             4(1x,f12.5),3(1x,f12.4),2(1x,f11.6))
           
           
c          write(43,10)t(k),p(k),gam(k),vk(k),vs(k),
c     :        cosi(k)/rs(k),rss(k),rpj(k),mpj(k),chisq(k),chsp(k)
c10        format(f9.4,1x,f9.7,1x,f9.4,1x,f6.4,1x,f6.3,1x,f10.6,1x,f6.3,
c     :           f6.4,1x,f5.3,1x,f5.3,1x,f5.3,1x,f10.3,1x,f10.3)
        end do
        close(43)
        
	call conflim(nburn,njump,rp,chisq,idx,dxl,rpbest,dxh)	     
	call conflim(nburn,njump,rs,chisq,idx,dxl,rsbest,dxh)	     

        if(nvel.ge.2)then
           call conflim(nburn,njump,gam,chisq,idx,dxl,gambest,dxh)      
           call conflim(nburn,njump,vs,chisq,idx,dxl,vsbest,dxh)        
           call conflim(nburn,njump,lam,chisq,idx,dxl,lambest,dxh)      
        end if
        pr = rpbest / rsbest
        cosi = bbest*rsbest
        
        sinl = sin(lambest)
        cosl = cos(lambest)
        print *,'lambest, sinl, cosl = ', sinl, cosl
        
c  ***	Compute STATS like ellipsoidal var and SNRED

        nmod = 1000
        
        do i = 1, nmod
           ph = dfloat(i-1)/dfloat(nmod-1) + 0.5d0
           
c  ***     Coordinates of planet in orbital plane ( a = 1 )
           xp = sin ( 2d0 * pi * ph )
           yp = cos ( 2d0 * pi * ph )

c  ***     Rotate through 90-i about X axis to get zp
           zp = - yp * cosi
                
c  ***     Impact parameter in units of primary radius
           if ( yp.gt.0d0 ) then
              z(i) = sqrt(xp*xp+zp*zp) / rsbest
           else
              z(i) = 1d0 / rsbest
           end if
           
           nu(i) = 0d0
c          
        end do
        
c  ***  Compute model flux
        call occultsmall(pr,c1,c2,c3,c4,nmod,z,mu)
        
        if(nvel.ge.2)then          
c  ***     Compute model RV curve. 
           do i = 1, nmod
c  ***        Determine orbital phase
              ph = dfloat(i-1)/dfloat(nmod-1) + 0.5d0
           
c  ***        Coordinates of planet in orbital plane ( a = 1 )
              xp = sin ( 2d0 * pi * ph ) 
              nu(i) = - vkbest * xp + gambest

              yp = cos ( 2d0 * pi * ph )

c  ***        Rotate through 90-i about X axis to get zp
              zp = - yp * cosi
                   
c  ***        Rotate through lambda about y axis to get up

              up = xp * cosl - zp * sinl

c  ***        Code Rossiter effect here. Impact param is z(i),
c             x coord in stellar rotational coordinate system
c             is up, flux diminution is 1 - mu(i)

              nu(i) = nu(i) - vsbest * ( 1d0 - mu(i) ) * up / rsbest          

           end do
        end if
        
        call unlink('pltmod.dat')
        open(unit=45,file='pltmod.dat',form='formatted',status='new')   
        do i = 1, nmod
           ph = dfloat(i-1)/dfloat(nmod-1) + 0.5d0
           write(45,*) ph, -2.5d0*log10(mu(i)), nu(i)
        end do  
        close(45)
        
        call unlink('pltphot.dat')
        open(unit=46,file='pltphot.dat',form='formatted',status='new')
        do pset = 1, npset
           do i = fobs(pset),lobs(pset)
c  ***        Determine orbital phase
              ph = mod(hjd(i)-t0best,pbest)/pbest
              if ( ph.lt.-0.5d0 ) ph = ph + 2d0
              if ( ph.lt.0.5d0 ) ph = ph + 1d0
              write(46,*) ph, mag(i)-adj(pset), err(i)
           end do
        end do
        close(46)
        
        if(nvel.ge.2)then
        call unlink('pltrvel.dat')
        open(unit=47,file='pltrvel.dat',form='formatted',status='new')
        do i = 1, nvel
c  ***     Determine orbital phase
           ph = mod(hjdrv(i)-t0best,pbest)/pbest
           if ( ph.lt.-0.5d0 ) ph = ph + 2d0
           if ( ph.lt.0.5d0 ) ph = ph + 1d0
c          if ( ph.gt.1.5d0 ) ph = ph - 1d0
           write(47,*) hjdrv(i), ph, rv(i), rverr(i)
        end do
        close(47)
        end if
        
        print *, ' '
        print *,' j = ',j , '    k = ', k
        print *,' All done'     
        
        end

C+      ----------------------------------------------------------------
C+
        subroutine newstats(njump,t,p,ms,wf,d,vk,vs,b,lam,
     :        kst,ken,
     :        sigt0,sigper,sigms,sigwf,sigdm,
     :        sigvk,sigvs,sigb,siglam,radvel)

        implicit none
             
c  ***  Subroutine parameters
        integer njump
        double precision t(njump)
        double precision p(njump)
        double precision ms(njump)
        double precision wf(njump)
        double precision d(njump)
        double precision vk(njump)
        double precision vs(njump)
        double precision b(njump)
        double precision lam(njump)
        integer kst, ken
        double precision sigt0,sigper,sigms,sigwf,sigdm
        double precision sigvk,sigvs,sigb,siglam
        logical radvel
        
c  ***  Local variables
        integer i
        integer nx
        double precision st,sp,sm,sw,sd,sk,sv,sb,sl
        double precision stt,spp,smm,sww,sdd,skk,svv,sbb,sll

c  ***  Need to initialise the accumulators in the calling routine; 
c       this allows us to increment them using just the most recent
c       set of values.
        
        nx = ken - kst + 1
        
        st = 0
        stt = 0
        do i = kst,ken
           st = st + t(i)
           stt = stt + t(i)*t(i)
        end do
        if ( stt - st*st/dfloat(nx) .gt. 1d-30 ) then
           sigt0 = sqrt((stt - st*st/dfloat(nx))/dfloat(nx-1))
        end if
        
        sp = 0
        spp = 0
        do i = kst,ken
           sp = sp + p(i)
           spp = spp + p(i)*p(i)
        end do
        if ( spp - sp*sp/dfloat(nx) .gt. 1d-30 ) then
           sigper = sqrt((spp - sp*sp/dfloat(nx))/dfloat(nx-1))
        end if
        
c       sm = 0
c       smm = 0
c       do i = kst,ken
c          sm = sm + ms(i)
c          smm = smm + ms(i)*ms(i)
c       end do
c       if ( smm - sm*sm/dfloat(nx) .gt. 1d-30 ) then
c          sigms = sqrt((smm - sm*sm/dfloat(nx))/dfloat(nx-1))
c       end if
        
        sw = 0
        sww = 0
        do i = kst,ken
           sw = sw + wf(i)
           sww = sww + wf(i)*wf(i)
        end do
        if ( sww - sw*sw/dfloat(nx) .gt. 1d-30 ) then
           sigwf = sqrt((sww - sw*sw/dfloat(nx))/dfloat(nx-1))
        end if
        
        sd = 0
        sdd = 0
        do i = kst,ken
           sd = sd + d(i)
           sdd = sdd + d(i)*d(i)
        end do
        if ( sdd - sd*sd/dfloat(nx) .gt. 1d-30 ) then
           sigdm = sqrt((sdd - sd*sd/dfloat(nx))/dfloat(nx-1))
        end if
        
        if ( radvel ) then
           sk = 0
           skk = 0
           do i = kst,ken
              sk = sk + vk(i)
              skk = skk + vk(i)*vk(i)
           end do
           if ( skk - sk*sk/dfloat(nx) .gt. 1d-30 ) then
              sigvk = sqrt((skk - sk*sk/dfloat(nx))/dfloat(nx-1))
           end if

           sv = 0
           svv = 0
           do i = kst,ken
              sv = sv + vs(i)
              svv = svv + vs(i)*vs(i)
           end do
           if ( svv - sv*sv/dfloat(nx) .gt. 1d-30 ) then
              sigvs = sqrt((svv - sv*sv/dfloat(nx))/dfloat(nx-1))
           end if
           
           sl = 0
           sll = 0
           do i = kst,ken
              sl = sl + lam(i)
              sll = sll + lam(i)*lam(i)
           end do
           if ( sll - sl*sl/dfloat(nx) .gt. 1d-30 ) then
              siglam = sqrt((sll - sl*sl/dfloat(nx))/dfloat(nx-1))
           end if
           
        end if

c       sb = 0
c       sbb = 0
c       do i = kst,ken
c          sb = sb + b(i)
c          sbb = sbb + b(i)*b(i)
c       end do
c       if ( sbb - sb*sb/dfloat(nx) .gt. 1d-30 ) then
c          sigb = sqrt((sbb - sb*sb/dfloat(nx))/dfloat(nx-1))
c          Don't let sigb grow too big
c          sigb = min(sigb,0.05)
c       end if
        
        end
C+      ----------------------------------------------------------------
        subroutine propose(told,pold,msold,wfold,dold,vkold,
     :                     vsold,bold,lamold,   
     :                     t, p, ms, wf, d, vk, vs, b, lam,
     :                     sigt0,sigper,sigms,sigwf,sigdm,
     :                     sigvk,sigvs,sigb,siglam,scfac,idum)
  
        implicit none
            
c  ***  Subroutine parameters
        real*8 told,pold,msold,wfold,dold,vkold,vsold,bold,lamold
        real*8 t, p, ms, wf, d, vk, vs, b, lam
        real*8 sigt0,sigper,sigms,sigwf,sigdm,sigvk,sigvs,sigb,siglam
        real*8 scfac
        integer idum
        double precision pi
                
        real gasdev
            
        pi = 4d0*atan2(1d0,1d0)
        
        t = told + sigt0 * dble(gasdev(idum)) * scfac
        p = pold + sigper * dble(gasdev(idum)) * scfac
1       ms = msold + sigms * dble(gasdev(idum)) * scfac
        if ( ms .lt. 0d0 ) go to 1
2       wf = wfold + sigwf * dble(gasdev(idum)) * scfac
        if ( wf .lt. 0d0 ) go to 2
3       d = dold + sigdm * dble(gasdev(idum)) * scfac
        if ( d .lt. 0d0 ) go to 3
4       vk = vkold + sigvk * dble(gasdev(idum)) * scfac
        if ( vk .lt. 0d0 ) go to 4
5       vs = vsold + sigvs * dble(gasdev(idum)) * scfac
        if ( vs .lt. 0d0 ) go to 5
6       b = bold + sigb * dble(gasdev(idum)) * scfac
        if ( b .lt. 0d0 .or. b.ge.1d0 ) go to 6
7       lam = lamold + siglam * dble(gasdev(idum)) * scfac
        if ( lam .lt. -pi .or. lam.ge.pi ) go to 7
         
        end
C+      ----------------------------------------------------------------
C+
        subroutine physparms(p,wf,ms,d,b,vk,
     :                       rs,rh,aau,rstar,rp,cosi,deginc,mpjup,rpjup)

        implicit none
                
c  ***  Subroutine parameters
        double precision p,wf,ms,d,b,vk
        double precision rs,rh,aau,rstar,rp,cosi,deginc,mpjup,rpjup

c  ***  Local variables
        double precision prat,pyr
        double precision akm,sini,psec,vt,q
        double precision pi
        
        pi = 4d0*atan2(1d0,1d0)
        
c       rp/rs for grazing transit ...
        prat = ((b-1d0)+sqrt((1d0-b)**2+8d0*d))/2d0
c       ...or for complete transit
        prat = max(prat,sqrt(d))
        
c       Compute rs using eclipse width from 1st to 4th contact
        rs = pi * wf / sqrt((1d0+prat)**2 - b*b)

c       Compute density in solar units from rs and period
        rh = 0.0134063d0 / rs**3 / p**2
        
c  ***  Compute mass-dependent physical parameters
        pyr = p/365.256363d0
        aau = (pyr*pyr*ms)**(1d0/3d0)
        
c  ***  Stellar radius in solar units
        rstar = rs * aau * 215.094177d0
        
c  ***  Compute planet radius from transit depth
        rp = rs * prat
        cosi = b * rs
        
        deginc = acos(cosi) * 180d0 / pi
        akm = aau * 149598000d0
        sini = sqrt(1d0 - cosi*cosi)
        psec = p * 86400d0
        vt = 2d0 * pi * akm / psec * sini
        q = vk / ( vt - vk )

c  ***  Compute planet mass in jovian units
        mpjup = q * ms * 1047.52d0
        rpjup = rp*aau*2092.51385d0

        end
C+      ----------------------------------------------------------------

        subroutine orthk1(nvel,hjdrv,rv,err,t0,period,k1,sigvk)
        
        implicit none
        
c  ***  Subroutine parameters
        integer nvel
        real*8 hjdrv(nvel)
        real*8 rv(nvel)
        real*8 err(nvel)
        real*8 t0, period
        real*8 k1, sigvk
        
c  ***  Local variables
        real*8 sx, su, sw
        integer i
        real*8 w, x, u
        real*8 phi, sphi
        real*8 xhat, uhat
        real*8 sxu, suu
        real*8 pi
        
        pi = 4d0*atan2(1d0,1d0)
        
        if(nvel .gt. 2)then
        
c           Orthogonalise data and sine function, 
c           adding 50 m/sec additional error to region around transit
            sx = 0d0
            su = 0d0
            sw = 0d0
            do i = 1, nvel
               phi = mod( hjdrv(i)-t0, period ) / period
               if ( phi .gt. 0.05 .and. phi .lt. 0.95 ) then
                  w = 1d0 / err(i) / err(i)
               else
                  w = 1d0 / ( err(i) * err(i) + .05*.05 )
               end if
               sphi = sin( 2d0 * pi * phi )
               sx = sx + rv(i) * w
               su = su + sphi * w
               sw = sw + w
            end do
            xhat = sx / sw
            uhat = su / sw

c           Optimal scaling of sine function to fit data,           
c           adding 50 m/sec additional error to region around transit
            sxu = 0d0
            suu = 0d0
            do i = 1, nvel
               phi = mod( hjdrv(i)-t0, period ) / period
               if ( phi .gt. 0.05 .and. phi .lt. 0.95 ) then
                  w = 1d0 / err(i) / err(i)
               else
                  w = 1d0 / ( err(i) * err(i) + .05*.05 )
               end if
               sphi = sin( 2d0 * pi * phi )
               x = rv(i) - xhat
               u = sphi - uhat
               sxu = sxu + x * u * w
               suu = suu + u * u * w
            end do
            k1 = - sxu / suu
            sigvk = sqrt(1d0 / suu)
        else
           k1 = 0.1d0
           sigvk = 0d0
        end if
        
c        print *,'k1    = ',k1
c        print *,'sigvk = ',sigvk
        
        end     


C+      ----------------------------------------------------------------
        subroutine orthtp(ndata,hjd,err,nvel,hjdrv,rverr,
     :                    t0,sigt0,period,sigper,hw1,hw2)
     
        implicit none
        
c  ***  Orthogonalise T0 and p by adjusting epoch of transit
c       to centre of mass of data taken in transit.
        
c  ***  Subroutine parameters
        integer ndata
        real*8 hjd(ndata)
        real*8 err(ndata)
        integer nvel
        real*8 hjdrv(nvel)
        real*8 rverr(nvel)
        real*8 t0, sigt0
        real*8 period, sigper
        real*8 hw1,hw2
        
c  ***  Local variables
        real*8 sxw,sw,w
        real*8 t1, t2
        integer i
        real*8 phi, cphi
        integer norb
        real*8 tcog
        real*8 pi
        
        pi = 4d0*atan2(1d0,1d0)
        
        sxw = 0d0
        sw = 0d0
        t1 = 1d38
        t2 = -1d38
        do i = 1, ndata
           phi = mod( hjd(i)-t0, period ) / period
           if( phi .lt.0d0 ) phi = phi + 1d0
           if( phi.lt.hw1 .or. phi.gt.hw2 ) then
              w = 1d0 / err(i) / err(i)
              sxw = sxw + hjd(i) * w
              sw = sw + w
              t1 = min(t1,hjd(i))
              t2 = max(t2,hjd(i))
           end if
        end do
        
        norb = nint ( (t2 - t1 ) / period )
        sigper = sigt0 / dfloat(norb)

        do i = 1, nvel
           phi = mod( hjdrv(i)-t0, period ) / period
           cphi = cos( 2d0 * pi * phi )
c          Strictly we should weight this as cos(phi)
           w = cphi * cphi / rverr(i) / rverr(i)
           sxw = sxw + hjdrv(i) * w
           sw = sw + w
        end do
        
        tcog = sxw / sw
        norb = nint((tcog - t0) / period)
        t0 = t0 + dfloat(norb) * period
        
        end
C+      ----------------------------------------------------------------
C+
        subroutine readtamuz(lu,photfile,
     :             swaspid,epoch,period,wday,dmag,dchs,
     :             mxobs,ndata,hjd,dat,err)
     
c  ***  This is a more advanced version of rddata. It reads HUNTER
c       parameters as well as photometric data from a SuperWASP
c       post-tamuz (LCFINDER) light curve listing.
        
        implicit none
        integer lu
        character*80 photfile
        integer mxobs
        real*8 hjd(mxobs)
        real*8 dat(mxobs)
        real*8 err(mxobs)
        integer ndata
        integer i
        character*80 record
        character*26 swaspid
        real*8 dummy
        integer ipk
        integer mxpk
        parameter(mxpk = 5)
        real*8 epoch(mxpk)
        real*8 period(mxpk)
        real*8 wday(mxpk)
        real*8 dmag(mxpk)
        real*8 dchs(mxpk)
                
c  ***  Read data
        i = 0
        ipk = 0
        open(unit=lu,file=photfile,status='old')
3       read(lu,'(a)',end=998)record
        if(record(1:23) .eq. ' # Lightcurve for star ') then
           read(record(24:49),'(a)')swaspid
           print *,' swaspid = ',swaspid
           go to 3
        else if (record(1:8) .eq. ' # Peak:')then
           print *,record(9:10)
           read(record(9:10),*)ipk
           print *,'ipk=',ipk
           read(lu,'(a)',end=998)record
           read(record(13:22),*)epoch(ipk)
           print *,'epoch=',epoch(ipk)
           read(lu,'(a)',end=998)record
           read(record(13:23),*)period(ipk)
           print *,'period=',period(ipk)
           read(lu,'(a)',end=998)record
           read(record(16:25),*)dummy
           wday(ipk) = dummy / 24d0
           print *,'wday=',wday(ipk)
           read(lu,'(a)',end=998)record
           read(record(11:25),*)dummy
           dmag(ipk) = -dummy
           print *,'dmag=',dmag(ipk)
           read(lu,'(a)',end=998)record
           read(record(17:28),*)dchs(ipk)
           print *,'dchs=',dchs(ipk)
           go to 3
        else if ( record(1:2) .eq. ' #' ) then
           go to 3
        end if

c  ***  When we get to here, we've read all the header records
c       so read the light curve data.
        i = i + 1
        read(record,*)hjd(i),dat(i),err(i)
c       print *,hjd(i),dat(i),err(i)
        go to 3
        
998     close(lu)
        ndata = i
        
        end

C+      ----------------------------------------------------------------
C+
      subroutine read_orion(photfile,swaspid,pkidx,numpk,jh,rpmj,rpmj_diff,
     $     epoch,period,wday,dmag,dchs,snred,ntransit,aellip,snellip,
     $     transfrac,gaprat,dchsanti,mxobs,ndata,hjd,dat,err,cat_idx,cnd_idx,
     $     vk_teff,jh_teff,rstar_vk,rstar_jh,giant_flg,rpl_vk,rpl_jh,catflg)
     
c  *** RGW  Reads output from orion BLS code
        
        implicit none
        integer cnd_idx, cat_idx
        character*1000 photfile
        integer mxobs
        real*8 hjd(mxobs)
        real*8 dat(mxobs)
        real*8 err(mxobs)
        integer ndata
        integer i
        character*80 record
        character*30 swaspid
        integer ngood
        real*8 jh,rpmj,rpmj_diff
        real*8 dummy
        integer ipk, pkidx
        integer mxpk
        parameter(mxpk = 5)
        real*8 epoch(mxpk)
        real*8 period(mxpk)
        real*8 wday(mxpk)
        real*8 dmag(mxpk)
        real*8 dchs(mxpk)
        real*8 snred(mxpk)
        integer ntransit(mxpk)
        real*8 aellip(mxpk)
        real*8 snellip(mxpk)
        real*8 transfrac(mxpk)
        real*8 gaprat(mxpk)
        real*8 dchsanti(mxpk)
        integer numpk, ival

        integer funit, status, idum, col, nrows, idx, cnt, off
        character*30 obj_id
        logical found, anyf, done
        integer pcol, dcol, wcol, ecol, lcol, lcidx(mxpk), ccol, rcol,
     $       rank
	real		jmag,hmag,kmag,vtmag
	real		mura,mudec,emura,emudec
	real		vk_teff,jh_teff,mu
	real		rstar_vk,rstar_jh,vk
	real		rpmjfunc,rpl_jh,rpl_vk
	integer*2	catflg,giant_flg
	real 		null

        status=0
        funit=0
        call ftopen(funit, photfile, 0, idum, status)
        if(status.ne.0) call fits_err(status, 'opening input file')
        call ftmnhd(funit, 2, 'CATALOGUE', 0, status)
        call ftgnrw(funit, nrows, status)
        call ftgcno(funit, .false., 'OBJ_ID', col, status)
        idx=1
        found=.false.
        do while(idx.le.nrows.and..not.found)
           call ftgcvs(funit, col, idx, 1, 1, '', obj_id, anyf, status)
           if(obj_id.eq.swaspid) then
              found=.true.
           else
              idx=idx+1
           endif
        enddo
        if(status.ne.0) call fits_err(status, 'scanning catalogue')
        if(.not.found) then
           print*,'Cannot find ', swaspid, ' in the catalogue'
           stop
        endif
        print*,'Found object at row', idx
        cat_idx=idx

        call ftgcno(funit, .false., 'JMAG', col, status)
        call ftgcve(funit, col, idx, 1, 1, -99.0, jmag, anyf, status)
        call ftgcno(funit, .false., 'HMAG', col, status)
        call ftgcve(funit, col, idx, 1, 1, -99.0, hmag, anyf, status)
        if(jmag.eq.-99.0.or.hmag.eq.-99.0) then
           jh=0.35      !set to ~G5V color
        else
           jh=jmag-hmag
           if (jh.lt.0.0) jh=0.0 !T-R relation valid Teff<7200,J-H>0
           if (jh.gt.0.84)jh=0.84 !LD coeffs good to T>3500, J-H<0.84
        endif
        print *,'# J-H               :',jh


C **** READ IN COLUMN INFORMATION FROM 2nd extension of catdata.fits
C	THIS MUST MATCH column headers wanted in nlimits.inc
	null=-99.
        call ftgcno(funit, .false., 'VMAG', col, status)
        call ftgcve(funit, col, idx, 1, 1, null, vtmag, anyf,status)
        call ftgcno(funit, .false., 'KMAG', col, status)
        call ftgcve(funit, col, idx, 1, 1, null, kmag, anyf, status)
        call ftgcno(funit, .false., 'MU_RA', col, status)
        call ftgcve(funit, col, idx, 1, 1, null, mura, anyf, status)
        call ftgcno(funit, .false., 'MU_DEC', col, status)
        call ftgcve(funit, col, idx, 1, 1, null, mudec, anyf, status)
        call ftgcno(funit, .false., 'MU_RA_ERR', col, status)
        call ftgcve(funit, col, idx, 1, 1, null, emura, anyf, status)
        call ftgcno(funit, .false., 'MU_DEC_ERR', col, status)
        call ftgcve(funit, col, idx, 1, 1, null, emudec, anyf, status)
        catflg=0
        if(jmag.ne.null .or. kmag.ne.null .or. hmag.ne.null .or.
     $       mura.ne.null .or. mudec.ne.null) catflg=1

C*** Get temperature estimates

	 if (catflg.ne.0) then

	  if (vtmag.ne.null.and.kmag.ne.null) then

C *** USE VK color to get temperature estimate

      	   vk=vtmag-kmag
	   if (vk.lt.4.0.and.vk.gt.0.7) then

  	     vk_teff=int((220.05*vk*vk)-(1948.7*vk)+8361.6)
             rstar_vk =(((-3.925d-14*vk_teff
     :         + 8.3909d-10)*vk_teff
     :         - 6.555d-6)*vk_teff
     :         + 0.02245d0)*vk_teff
     :         - 27.9788d0
	   else
	    rstar_vk=null
	    vk_teff=null
	   endif

	  else 		! if no V or K mag
	    rstar_vk=null
	    vk_teff=null
	    vk=null
	  endif		

	  if (jmag.ne.null.and.hmag.ne.null) then

C *** JH color to get temperature estimate

          jh=jmag-hmag
	  if (jh.lt.0.65.and.jh.gt.0.1) then
	    jh_teff=int(-4369.5*jh+7188.2)

            rstar_jh =(((-3.925d-14*jh_teff
     :         + 8.3909d-10)*jh_teff
     :         - 6.555d-6)*jh_teff
     :         + 0.02245d0)*jh_teff
     :         - 27.9788d0

	  else		! if outside allowed limits
	   rstar_jh=null
	   jh_teff=null
	  endif

C *** Use proper motions to guess at dwarf/giant
	  mu=sqrt(mura*mura/1.0e6+mudec*mudec/1.0e6)	! arcs/yr
	  if (mu.gt.0.0) then
            rpmj=jmag+5.0*log10(mu)
	  rpmjfunc=-141.25*jh**4+473.18*jh**3-583.6*jh*jh+313.42*jh-58.
	    rpmj_diff=rpmjfunc-rpmj
	    if (rpmj_diff.le.0.0) giant_flg=0
	    if (rpmj_diff.gt.0.0) giant_flg=1
	  else 
            rpmj=null
	    rpmj_diff=null
	    giant_flg=0
	  endif

	 else 		! if no J or H mag
	  rstar_jh=null
	  jh_teff=null
	  jh=null
	  rpmj_diff=null
	  rpmj=null
	  giant_flg=0
	 endif		

	else		! if no catalogue match
	  rstar_vk=null
	  rstar_jh=null
	  vk_teff=null
	  jh_teff=null
	  vtmag=null
	  jmag=null
	  hmag=null
	  kmag=null
	  jh=null
	  vk=null
	  rpmj_diff=null
	  rpmj=null
	  giant_flg=0
	endif		

C       Now look for the transit parameters in the candidates extension
        call ftmnhd(funit, 2, 'CANDIDATES', 0, status)
        call ftgnrw(funit, nrows, status)
        call ftgcno(funit, .false., 'OBJ_ID', col, status)
        call ftgcno(funit, .false., 'RANK', rcol, status)
        idx=1
        found=.false.
        do while(idx.le.nrows.and..not.found)
           call ftgcvs(funit, col, idx, 1, 1, '', obj_id, anyf, status)
           call ftgcvj(funit, rcol, idx, 1, 1, 0, rank, anyf, status)
           if(obj_id.eq.swaspid.and.rank.eq.pkidx) then
              found=.true.
           else
              idx=idx+1
           endif
        enddo
        if(.not.found) then
           print*,'Cannot find peak',pkidx,' for object ',swaspid
           stop
        endif

        cnd_idx=idx
        call ftgcno(funit, .false., 'PERIOD', pcol, status)
        call ftgcno(funit, .false., 'DEPTH', dcol, status)
        call ftgcno(funit, .false., 'WIDTH', wcol, status)
        call ftgcno(funit, .false., 'EPOCH', ecol, status)
        call ftgcno(funit, .false., 'DELTA_CHISQ', ccol, status)
        call ftgcno(funit, .false., 'LC_IDX', lcol, status)
        numpk=1
        call ftgcvd(funit, pcol, idx, 1, 1, 0.0, period(numpk), anyf, status)
        call ftgcvd(funit, wcol, idx, 1, 1, 0.0, wday(numpk), anyf, status)
        call ftgcvd(funit, ecol, idx, 1, 1, 0.0, epoch(numpk), anyf, status)
        call ftgcvd(funit, dcol, idx, 1, 1, 0.0, dmag(numpk), anyf, status)
        dmag(numpk)=-dmag(numpk)
        call ftgcvd(funit, ccol, idx, 1, 1, 0.0, dchs(numpk), anyf, status)
        call ftgcvj(funit, lcol, idx, 1, 1, 0, lcidx(numpk), anyf, status)
        period(numpk)=period(numpk)/86400.0
        wday(numpk)=wday(numpk)/86400.0
        epoch(numpk)=epoch(numpk)/86400.0+3005.5
        if(status.ne.0) call fits_err(status, 'reading candidate attributes')

c
        rpl_vk=-99.0
        rpl_jh=-99.0
        if(rstar_vk.ne.-99.0) rpl_vk=sqrt(abs(dmag(numpk)/1.3))*rstar_vk*9.728
        if(rstar_jh.ne.-99.0) rpl_jh=sqrt(abs(dmag(numpk)/1.3))*rstar_jh*9.728

C       Read the light-curve
        print*,'lightcurve is ', lcidx(numpk)
        call ftmnhd(funit, 2, 'LIGHTCURVES', 0, status)
        call ftgcno(funit, .false., 'HJD', col, status)
        call ftgdes(funit, col, lcidx(numpk), cnt, off, status)
        call ftgcvd(funit, col, lcidx(numpk), 1, cnt, 0.0, hjd, anyf, status)
        do i=1, cnt
           hjd(i)=hjd(i)/86400.0+3005.5
        enddo
        call ftgcno(funit, .false., 'MAG', col, status)
        call ftgcvd(funit, col, lcidx(numpk), 1, cnt, 0.0, dat, anyf, status)
        call ftgcno(funit, .false., 'MAG_ERR', col, status)
        call ftgcvd(funit, col, lcidx(numpk), 1, cnt, 0.0, err, anyf, status)
        if(status.ne.0) call fits_err(status, 'reading light-curve')
        print*,'lightcurve contains', cnt, ' points'
        ndata=cnt
        call ftclos(funit, status)
        
        end

C+      ----------------------------------------------------------------
C+
        subroutine getparms(jh,rstar,mstar,a1,a2,a3,a4)
        
c  ***  Given the J-H colour index of a transit candidate,
c       uses DW's polynomial fits to estimate Teff and Mstar,
c       then interpolates nonlinear limb-darkening coefficients
c       from grid of values taken from Claret (2000) for [Fe/H]=0.1
        
        implicit none
        character*120 record
        double precision jh,rstar,mstar,a1,a2,a3,a4
        
        double precision teff(13)
        double precision arr1(13)
        double precision arr2(13)
        double precision arr3(13)
        double precision arr4(13)
        double precision tjh, frac
        integer i, jlo, jhi
        character*120 infile

        data teff/3500, 3750, 4000, 4250, 4500, 4750, 5000, 5250, 5500,
     $       5750, 6000, 6250, 6500/
        data arr1/0.6374, 0.6405, 0.6523, 0.7374, 0.7775, 0.7291, 0.7163,
     $       0.7136, 0.6943, 0.6523, 0.6035, 0.555, 0.5115/
        data arr2/-0.2428, -0.2078, -0.3656, -0.7893, -0.9521, -0.813,
     $       -0.7272, -0.6502, -0.5215, -0.3225, -0.0994, 0.1353, 0.3367/
        data arr3/0.87, 0.7767, 0.9251, 1.4499, 1.6642, 1.5386, 1.4444,
     $       1.3331, 1.1484, 0.8653, 0.5389, 0.1773, -0.138/
        data arr4/-0.4321, -0.3741, -0.3645, -0.539, -0.6421, -0.6296,
     $       -0.6236, -0.6018, -0.5451, -0.4402, -0.3091, -0.1538, -0.015/

        
c1      print *,' Enter filename with Claret limb-darkening tables : '
c       read(*,'(a)',err=1) infile
        infile='claret.dat'
c        open(unit=63,file=infile,status='old',form='formatted')
        
c        read(63,*) record
c        read(63,*) (teff(i),i=1,13)
        
c        read(63,*) record
c        read(63,*) (arr1(i),i=1,13)
        
c        read(63,*) record
c        read(63,*) (arr2(i),i=1,13)
        
c        read(63,*) record
c        read(63,*) (arr3(i),i=1,13)
        
c        read(63,*) record
c        read(63,*) (arr4(i),i=1,13)
        
c        close(63)

c  ***  Use DW's optimised FGK relations
c       Teff in kelvin  
        tjh = -4369.5d0*jh + 7188.2d0

c       Radius in Rsun  
        rstar =(((-3.925d-14*tjh 
     :         + 8.3909d-10)*tjh 
     :         - 6.555d-6)*tjh
     :         + 0.02245d0)*tjh
     :         - 27.9788d0

c       Mass in Msun     
        mstar = rstar**(1d0/0.8d0)

c       Locate temperature in interpolation grid
        jlo = 0
        call huntdp(teff,13,tjh,jlo)
        
        if(jlo .eq.0) then
           a1 = arr1(1) 
           a2 = arr2(1) 
           a3 = arr3(1) 
           a4 = arr4(1) 
        else if(jlo .eq.0.or.jlo.ge.13) then
           a1 = arr1(13)        
           a2 = arr2(13)        
           a3 = arr3(13)        
           a4 = arr4(13)        
        else
           jhi = jlo + 1
           frac = (arr1(jhi)-arr1(jlo))/(teff(jhi)-teff(jlo))
           a1 = arr1(jlo) + (tjh-teff(jlo)) * frac      
           frac = (arr2(jhi)-arr2(jlo))/(teff(jhi)-teff(jlo))
           a2 = arr2(jlo) + (tjh-teff(jlo)) * frac      
           frac = (arr3(jhi)-arr3(jlo))/(teff(jhi)-teff(jlo))
           a3 = arr3(jlo) + (tjh-teff(jlo)) * frac      
           frac = (arr4(jhi)-arr4(jlo))/(teff(jhi)-teff(jlo))
           a4 = arr4(jlo) + (tjh-teff(jlo)) * frac      
        end if
        
        end
C+      ----------------------------------------------------------------
C+      
        SUBROUTINE HUNTDP(XX, N, X, JLO)

C  ***  Given an array XX of length N, and given a value X, returns a value JLO
c       such that X is between XX(JLO) and XX(JLO+1).  XX must be monotonic, 
c       eitherincreasing or decreasing. JLO = 0 or JLO = N is returned to
c       indicate that X is out of range. JLO on input is taken as the initial
c       guess for JLO on output.

        REAL*8 XX(N)
        REAL*8 X
        LOGICAL ASCND

        ASCND = XX(N).GT.XX(1)
        IF(JLO.LE.0 .OR. JLO.GT.N)THEN
          JLO = 0
          JHI = N+1
          GO TO 3
        END IF
        INC = 1
        IF(X.GE.XX(JLO) .EQV. ASCND)THEN
1         JHI = JLO + INC
          IF(JHI.GT.N)THEN
            JHI = N + 1
          ELSE IF(X.GE.XX(JHI) .EQV. ASCND)THEN
            JLO = JHI
            INC = INC + INC
            GO TO 1
          END IF
        ELSE
          JHI = JLO
2         JLO = JHI - INC
          IF(JLO.LT.1)THEN
            JLO = 0
          ELSE IF(X.LT.XX(JLO) .EQV. ASCND)THEN
            JHI = JLO
            INC = INC + INC
            GO TO 2
          END IF
        END IF

3       IF(JHI-JLO.EQ.1)RETURN
        JM = (JHI+JLO)/2
        IF(X.GT.XX(JM) .EQV. ASCND)THEN
          JLO = JM
        ELSE
          JHI = JM
        END IF
        GO TO 3

        END

C+      ----------------------------------------------------------------
C+      
        subroutine eval(t0,period,ms,rs,rp,cosi,vk,vsi,gam,lam,rstar,
     :                  c1,c2,c3,c4,ms0,sigms,constrainmr,
     :                  ndata,npset,fobs,lobs,adj,hjd,mag,err,
     :                  nvel,hjdrv,rv,rverr,dvjit,chphot,chspec,chs,scl)

        implicit none
        
        integer ndata, npset
        integer fobs(npset)
        integer lobs(npset)
        real*8 scl(npset)
        real*8 adj(npset)
        real*8 hjd(ndata)
        real*8 mag(ndata)
        real*8 err(ndata)
        real*8 z(ndata)
        real*8 mu(ndata)
        
        integer nvel    
        real*8 hjdrv(nvel)
        real*8 rv(nvel)
        real*8 rverr(nvel)
        real*8 dvjit
        real*8 vz(nvel)
        real*8 nu(nvel)
        
c  ***  System parameters
        real*8 t0,period,ms,rs,rp,cosi,vk,vsi,gam,lam,rstar
        real*8 ms0, sigms, rs0, sigrs
        
c  ***  Limb-darkening coeffs from Claret (2000)
        real*8 c1, c2, c3, c4
        
c  ***  Accumulators and statistics
        real*8 w, sw, sxw, spw, xhat, phat, r
        real*8 chpset, chphot, chspec, chs
        integer ipset
        integer ndof
        logical constrainmr
        
        integer i
        
c  ***  Compute transit model
        call transmod(t0,period,rp,rs,cosi,vk,vsi,lam,
     :                      c1,c2,c3,c4,ndata,hjd,z,mu,nvel,hjdrv,vz,nu)

c  ***  Orthogonalise each dataset and corresponding 
c       model points independently.
        chphot = 0d0
        do ipset = 1, npset
           sw = 0d0
           sxw = 0d0
           spw = 0d0
c$omp parallel do private(i,w) reduction(+:sxw,spw,sw)
           do i = fobs(ipset), lobs(ipset)
              w = 1d0 /err(i)/err(i)
              sxw = sxw + mag(i) * w
              spw = spw + mu(i) * w
              sw = sw + w
           end do
           xhat = sxw / sw
           phat = spw / sw
           adj(ipset) = xhat - phat
           
c  ***     Compute chi-squared from orthogonalised residuals
           chpset = 0d0
c$omp parallel do private(i,w,r) reduction(+:chpset)
           do i = fobs(ipset), lobs(ipset)
              w = 1d0 /err(i)/err(i)
              r = mag(i) - mu(i) - adj(ipset)
              chpset = chpset + r * r * w
           end do
           ndof = lobs(ipset) - fobs(ipset) + 1
           ndof = ndof - 1
           scl(ipset) = chpset / dfloat(ndof)
           chphot = chphot + chpset
        end do
        
c  ***  Now orthogonalise radial velocities
        sw = 0d0
        sxw = 0d0
        spw = 0d0
        do i = 1, nvel
           w = 1d0 /(rverr(i)*rverr(i) + dvjit*dvjit)
           sxw = sxw + rv(i) * w
           spw = spw + nu(i) * w
           sw = sw + w
        end do
        if ( sw .gt. 1d-30 ) then
           xhat = sxw / sw
           phat = spw / sw
        else
           xhat = 0d0
           phat = 0d0
        end if
        gam = xhat - phat
        
        chspec = 0d0
        do i = 1, nvel
           w = 1d0 /(rverr(i)*rverr(i) + dvjit*dvjit)
           r = rv(i) - nu(i) - gam
           chspec = chspec + r * r * w
        end do
        
        chs = chphot + chspec
        
c  ***  Add Bayesian penalty for silly stellar masses   
        r = ms - ms0
        w = 1d0 / sigms / sigms
        chs = chs + r * r * w
        
c  ***  Add Bayesian penalty for silly v sin i  -- pending
c       r = vsi - vs0
c       w = 1d0 / sigvs / sigvs
c       chs = chs + r * r * w
        
c  ***  Add Bayesian penalty for silly stellar radii
        if ( constrainmr ) then
           rs0 = ms0 ** 0.8d0
           sigrs = 0.8d0 * sigms * rs0 / ms0 
           r = rstar - rs0
           w = 1d0 / sigrs / sigrs
           chs = chs + r * r * w
        end if
        
        end

C+      ----------------------------------------------------------------
C+
        subroutine transmod(t0,period,rp,rs,cosi,vk,vsi,lam,
     :                      c1,c2,c3,c4,ndata,hjd,z,mu,nvel,hjdrv,vz,nu)
        
c       t0 = epoch of transit
c       period = period
c       hjd = array of obs dates
c       rp = planet radius / orbital sep
c       rs = stellar radius / orbital sep
c       vk = stellar reflex velocity amp in km/sec
c       vsi = stellar v sin i in km/sec
c       gam = stellar centre-of-mass velocity in km/sec

        implicit none

        integer i, ndata,nvel
        real*8 t0, period, rp, rs, cosi, vk, vsi, lam
        real*8 c1, c2, c3, c4
        real*8 hjd(ndata)
        real*8 z(ndata)
        real*8 mu(ndata)
        real*8 hjdrv(nvel)
        real*8 vz(nvel)
        real*8 vmu(nvel)
        real*8 nu(nvel)
        real*8 pi, p, ph, xp, yp, zp, up
        real*8 cosl, sinl
        
        pi = 4d0*atan2(1d0,1d0)
        
c  ***  Ratio of radii
        p = rp / rs
        
c$omp parallel do private(i, ph, xp, yp, zp)
        do i = 1, ndata

c  ***     Determine orbital phase
           ph = mod(hjd(i)-t0,period)/period
        
c  ***     Coordinates of planet in orbital plane ( a = 1 )
           xp = sin ( 2d0 * pi * ph )
           yp = cos ( 2d0 * pi * ph )

c  ***     Rotate through 90-i about X axis to get zp
           zp = - yp * cosi
                
c  ***     Impact parameter in units of primary radius
           if ( yp.gt.0d0 ) then
              z(i) = sqrt(xp*xp+zp*zp) / rs
           else
              z(i) = 1d0 / rs
           end if
c          print *,xp,yp,zp,cosi,z(i)
        end do
        
c  ***  Compute model light curve
        call occultsmall(p,c1,c2,c3,c4,ndata,z,mu)
        
c  ***  Convert mu to magnitudes
c$omp parallel do private(i)
        do i = 1, ndata
           mu(i) = -2.5d0*log10(mu(i))
c          if(mu(i).lt.-1d-10)print *,z(i),mu(i)
        end do  
        
c  ***  Compute model RV curve. First get photometry for Rossiter.
        do i = 1, nvel
c  ***     Determine orbital phase
           ph = mod(hjdrv(i)-t0,period)/period
        
c  ***     Coordinates of planet in orbital plane ( a = 1 )
           xp = sin ( 2d0 * pi * ph )
           yp = cos ( 2d0 * pi * ph )

c  ***     Rotate through 90-i about X axis to get zp
           zp = - yp * cosi
                
c  ***     Impact parameter in units of primary radius
           if ( yp.gt.0d0 ) then
              vz(i) = sqrt(xp*xp+zp*zp) / rs
           else
              vz(i) = 1d0 / rs
           end if
c          print *,xp,yp,zp,cosi,z(i)
        end do
        
c  ***  Compute model flux
        call occultsmall(p,c1,c2,c3,c4,nvel,vz,vmu)
        
c  ***  Rotation matrix elements for misalignment of rotation axes
        cosl = cos(lam)
        sinl = sin(lam)
                   
c  ***  Compute model RV curve. 
        do i = 1, nvel
c  ***     Determine orbital phase
           ph = mod(hjdrv(i)-t0,period)/period
        
c  ***     Coordinates of planet in orbital plane ( a = 1 )
           xp = sin ( 2d0 * pi * ph ) 
           nu(i) = - vk * xp 
           
           yp = cos ( 2d0 * pi * ph )

c  ***     Rotate through 90-i about X axis to get zp
           zp = - yp * cosi
                
c  ***     Rotate through lambda about y axis to get up

           up = xp * cosl - zp * sinl

c  ***     Code Rossiter effect here. Impact param is vz(i),
c          x coord is xp, flux diminution is 1 - vmu(i)

           nu(i) = nu(i) - vsi * up * ( 1d0 - vmu(i) )     

        end do
        
        end

C+      ----------------------------------------------------------------
C+
      subroutine occultsmall(p,c1,c2,c3,c4,nz,z,mu)
      implicit none
      integer i,nz
c      parameter (nz=201)
      real*8 p,c1,c2,c3,c4,z(nz),mu(nz),i1,norm,
     &       x,tmp,iofr,pi
C This routine approximates the lightcurve for a small 
C planet. (See section 5 of Mandel & Agol (2002) for
C details):
C Input:
C  p      ratio of planet radius to stellar radius
C  c1-c4  non-linear limb-darkening coefficients
C  z      impact parameters (positive number normalized to stellar 
C        radius)- this is an array which MUST be input to the routine
C  NOTE:  nz must match the size of z & mu in calling routine
C Output:
C  mu     flux relative to unobscured source for each z
C
      pi=acos(-1.d0)
      norm=pi*(1.d0-c1/5.d0-c2/3.d0-3.d0*c3/7.d0-c4/2.d0)
      i1=1.d0-c1-c2-c3-c4
c$omp parallel do private(i, tmp, x)
      do i=1,nz
        mu(i)=1.d0
        if(z(i).gt.1.d0-p.and.z(i).lt.1.d0+p) then
          x=1.d0-(z(i)-p)**2
          tmp=(1.d0-c1*(1.d0-0.8d0*x**0.25d0)
     &             -c2*(1.d0-2.d0/3.d0*x**0.5d0)
     &             -c3*(1.d0-4.d0/7.d0*x**0.75d0)
     &             -c4*(1.d0-0.5d0*x))
          mu(i)=1.d0-tmp*(p**2*acos((z(i)-1.d0)/p)
     &        -(z(i)-1.d0)*sqrt(p**2-(z(i)-1.d0)**2))/norm
        endif
        if(z(i).le.1.d0-p.and.z(i).ne.0.d0) then
          mu(i)=1.d0-pi*p**2*iofr(c1,c2,c3,c4,z(i),p)/norm
        endif
        if(z(i).eq.0.d0) then
          mu(i)=1.d0-pi*p**2/norm
        endif
      enddo
      return
      end
      
      function iofr(c1,c2,c3,c4,r,p)
      implicit none
      real*8 r,p,c1,c2,c3,c4,sig1,sig2,iofr
      sig1=sqrt(sqrt(1.d0-(r-p)**2))
      sig2=sqrt(sqrt(1.d0-(r+p)**2))
      iofr=1.d0-c1*(1.d0+(sig2**5-sig1**5)/5.d0/p/r)
     &         -c2*(1.d0+(sig2**6-sig1**6)/6.d0/p/r)
     &         -c3*(1.d0+(sig2**7-sig1**7)/7.d0/p/r)
     &         -c4*(p**2+r**2)
      return
      end
C+
        subroutine rddata(lu,photfile,mxobs,ndata,hjd,dat,err)
        
        implicit none
        integer lu
        character*80 photfile
        integer mxobs
        real*8 hjd(mxobs)
        real*8 dat(mxobs)
        real*8 err(mxobs)
        integer ndata
        integer i
        character*80 record
                
c  ***  Read data
        i = 0
        open(unit=lu,file=photfile,status='old')
3       read(lu,'(a)',end=998)record
        if(record(1:2) .eq. ' #')go to 3
        i = i + 1
        read(record,*)hjd(i),dat(i),err(i)
c       print *,hjd(i),dat(i),err(i)
        go to 3
        
998     close(lu)
        ndata = i
        
        end
C+
        subroutine methast(idum,chsnew,chsold,reject)
c  ***  Metropolis-Hastings decision maker
        
        implicit none
        real*8 chsnew, chsold
        logical reject
        real*8 dchisq,pjump,prob
        integer idum
        real ran1
        
        dchisq = chsnew - chsold
        
c  ***  Here comes the Markov-chain Monte Carlo decision
c       point. Do we accept the jump or not?

        if(dchisq .lt. 0d0) then
c  ***     Accept the proposal and move on
           reject = .false.
        else
c  ***     Set probability of next jump
           pjump = exp(-dchisq/2)
c  ***     Pick a uniform random deviate in range 0 to 1
           prob = dble(ran1(idum))
           if ( prob .lt. pjump ) then
c  ***        OK, accept the proposal
              reject = .false.
           else
c  ***        No, reject the proposal
              reject = .true.
           end if
        end if
        
        end     
C+
        FUNCTION RAN1(IDUM)
        
        SAVE

C  ***  Returns a uniform random deviate between 0.0 and 1.0. Set IDUM
c       to any negative value to initialise or reinitialise the sequence

        DIMENSION R(97)
        integer m1,m2,m3
        real rm1,rm2
        PARAMETER(M1=259200, IA1 = 7141, IC1 = 54773, RM1 = 1./M1) 
        PARAMETER(M2=134456, IA2 = 8121, IC2 = 28411, RM2 = 1./M2) 
        PARAMETER(M3=243000, IA3 = 4561, IC3 = 51349)
        DATA IFF/0/
        
        
        

        IF(IDUM.LT.0 .OR. IFF.EQ.0)THEN
          IFF = 1
          IX1 = MOD(IC1-IDUM,M1)
          IX1 = MOD(IA1*IX1+IC1,M1)
          IX2 = MOD(IX1,M2)
          IX1 = MOD(IA1*IX1+IC1,M1)
          IX3 = MOD(IX1,M3)
          DO 11 J=1, 97
            IX1 = MOD(IA1*IX1+IC1,M1)
            IX2 = MOD(IA2*IX2+IC2,M2)
            R(J) = (FLOAT(IX1)+FLOAT(IX2)*RM2)*RM1
11        CONTINUE
          IDUM = 1
        END IF

        IX1 = MOD(IA1*IX1+IC1,M1)
        IX2 = MOD(IA2*IX2+IC2,M2)
        IX3 = MOD(IA3*IX3+IC3,M3)
        J = 1 + (97*IX3)/M3
        IF(J.GT.97 .OR. J.LT.1)PAUSE
        RAN1 = R(J)
        R(J) = (FLOAT(IX1)+FLOAT(IX2)*RM2)*RM1

        RETURN
        END

        FUNCTION GASDEV(IDUM)
        
C  ***  Returns a normally distributed deviate with zero mean and unit
c       variance, using RAN1(IDUM) as the source of uniform deviates.

        SAVE

        DATA ISET/0/
        
        IF(ISET.EQ.0)THEN
1         V1 = 2.*RAN1(IDUM)-1.
          V2 = 2.*RAN1(IDUM)-1.
          R = V1*V1 + V2*V2
          IF(R.GE.1.)GO TO 1
          FAC = SQRT(-2.*LOG(R)/R)
          GSET = V1*FAC
          GASDEV = V2*FAC
          ISET = 1
        ELSE
          GASDEV = GSET
          ISET = 0
        END IF

        RETURN
        END
        
C+
C+
        SUBROUTINE INDEXX(N,ARRIN,INDX)

        IMPLICIT NONE
        
        INTEGER N
        REAL*8 ARRIN(N)
        INTEGER INDX(N)
        REAL*8  Q
        INTEGER J, L, IR, I, INDXT

        DO 11 J=1, N
           INDX(J) = J
11      CONTINUE

        L = N/2 + 1
        IR = N
        
10      CONTINUE
          IF(L.GT.1)THEN
            L = L-1
            INDXT = INDX(L)
            Q = ARRIN(INDXT)
          ELSE
            INDXT = INDX(IR)
            Q = ARRIN(INDXT)
            INDX(IR) = INDX(1)
            IR = IR-1
            IF(IR.EQ.1)THEN
              INDX(1)=INDXT
              RETURN
            ENDIF
          ENDIF

          I=L
          J = L+L
20        IF(J.LE.IR)THEN
            IF(J.LT.IR)THEN
              IF(ARRIN(INDX(J)).LT.ARRIN(INDX(J+1))) J=J+1
            ENDIF
            IF(Q.LT.ARRIN(INDX(J)))THEN
              INDX(I) = INDX(J)
              I=J
              J=J+J
            ELSE
              J=IR+1
            ENDIF
          GO TO 20
          ENDIF
          INDX(I) = INDXT
        GO TO 10
        END
C+
        subroutine conflim(m,n,x,chs,idx,dxl,xml,dxh)
        
        implicit none
        
        integer m,n
        real*8 x(n)
        real*8 chs(n)
        integer idx(n)
        integer idx2(n)
        integer ilo,ihi
        real*8 dxl,xml,dxh,xmax,xmin
        
        integer i
        integer nw
        real*8 wrk(n)
        real*8 wrk2(n)
        real*8 xsrt(n)
c       real*8 sx, sxx, xav, xsd
        
c  ***  Given an array x and associated chisq values from
c       a Monte Carlo simulation, determine max likelihood
c       value and 68.3 percent error limits.
        
c  ***  Sort the data in order of increasing chisq. Exclude
c       the first m elements.
        nw = 0
        do i = m+1, n
           nw  = nw + 1
           wrk(nw) = chs(i)
        end do
        call indexx(nw,wrk,idx)
        
c  ***  Also sort the data in order of increasing data value.
        nw = 0
        do i= m+1,n
          nw = nw + 1
          wrk2(nw) = x(i)
        end do
        call indexx(nw,wrk2,idx2)
                
c  ***  Array xsrt contains the x values in ascending order.
        do i= 1,nw
          xsrt(i) = wrk2(idx2(i))
        end do

c  ***  Pick out the maximum-likelihood value as the one corresponding  
c       to the lowest chi-squared.
        xml = wrk2(idx(1))
        
c  ***  Initialise the upper and lower confidence limits, setting them 
c       equal to xml
        xmax = xml
        xmin = xml
        do i = 2, nw
c  ***     Compare the x value corresponding to next highest value of 
c          chisq with the upper and lower confidence limits.
           xmax = max(xmax,wrk2(idx(i)))
           xmin = min(xmin,wrk2(idx(i)))
           
c  ***     Determine where xmax and xmin lie in the sorted x array
           call huntdp(xsrt,nw,xmin,ilo)
           call huntdp(xsrt,nw,xmax,ihi)
           
c  ***     Update the confidence intervals relative to XML
           dxl = xml - xmin
           dxh = xmax - xml
           
c  ***     As soon as xmax and xmin enclose 68.3 percent of the x
c          values, terminate the search.
           if ( dfloat(ihi-ilo) .ge. 0.683d0*dfloat(nw) ) then
              go to 999
           end if
        end do
           
999     continue

c  ***  Compute mean, stdev for sanity checking 
c       sx = 0d0
c       sxx = 0d0
c       do i = 1, n
c          sx = sx + x(i)
c          sxx = sxx + x(i)*x(i)
c       end do
c       xav = sx / n
c       xsd = sqrt((sxx - sx*sx/dfloat(n))/dfloat(n-1))
c       print *,'mean = ',xav,' sd = ',xsd
        
        end
C+      
        
C+
        subroutine correl(nb,n,x,m,c,cl)
        
c  ***  Computes the autocorrelation function c and correlation 
c       length cl of a Markov chain x. 
c       Recipe is from Tegmark et al 2004.
        
        implicit none
        
        integer nb, n
        real*8 x(n)
        integer m
        real*8 c(m)
        integer i,j, ioff
        real*8 si, sii, sij
        real*8 aii, aij
        real*8 xbar,numer,denom
        real*8 cl
        real*8 half, clo, chi, f, llo, lhi
        integer jlo, jhi
        
c       Exclude first nb points generated during burn-in
        si = 0d0
        do i = nb+1, n
           si = si + x(i)
        end do
        xbar = si / dfloat(n-nb)
        
        sii = 0d0
        do i = nb+1, n
           sii = sii + (x(i)-xbar)*(x(i)-xbar)
        end do
        aii = sii / dfloat(n-nb)
        denom = aii
        
        if ( denom .ge. 1d-20 ) then

           do j = 1, m
              sij = 0d0
              ioff = j - 1
              do i = nb+1, n - ioff
                 sij = sij + (x(i)-xbar)*(x(i+ioff)-xbar)
              end do
              aij = sij / dfloat(n-nb-ioff)
              numer = aij 
              c(j) = numer / denom
           end do
           
c  ***     Determine where correlation function drops below 0.5
           half = 0.5d0
           jlo = 1
           call huntdp(c,m,half,jlo)

c  ***     Interpolate to get correlation length
           if(jlo.ge.m)then
              cl = -dfloat(m)
              print *,'DBG: CORREL ERROR: m insufficient to determine cl.'
c             print *,'Dumping correlation function:'
c             do j = 1, m
c                print *,j-1,c(j)
c             end do
           else if ( jlo.lt.1 ) then
              cl = 0d0
              print *,'DBG: CORREL ERROR: zero correlation length!.'
           else
              jhi = jlo + 1
              clo = c(jlo)
              chi = c(jhi) 
              f = ( half - clo ) / ( chi - clo )
              llo = dfloat(jlo)
              lhi = dfloat(jlo+1)
              cl = llo + ( lhi - llo ) * f
           end if
        
        else
c  ***     If we got to here, the variance is zero so return
c          an obviously silly correlation length.
           cl = -99d0
        end if
        
        end
C+
        subroutine wrparms2(lu,parfile,t0,period,dmag,wday,k1,vsi,
     :               sigt0,sigper,sigdm,sigw,sigvk,sigvs,
     :               mstar,sigms,c1,c2,c3,c4)
     
        implicit none
        
        integer lu
        character*80 parfile
        real*8 t0,period,dmag,wday,k1,vsi
        real*8 sigt0,sigper,sigdm,sigw,sigvk,sigvs
c  ***  Limb-darkening coeffs from Claret (2000)
        real*8 mstar,sigms
        real*8 c1, c2, c3, c4
        
c  ***  Read basic parameters derived by HUNTER
        call unlink(parfile)
        open(unit=lu,file=parfile,form='formatted',status='new')
        write(lu,*)t0,period,dmag,wday,k1,vsi
        write(lu,*)sigt0,sigper,sigdm,sigw,sigvk,sigvs
        write(lu,*)mstar,sigms
        write(lu,*)c1,c2,c3,c4
        close(lu)
        
        end
C+      
        subroutine probs(n,rpjup,rstar,mstar,b,prp,prs,prb)
        
        implicit none
        
        integer n, i, jlo
        real*8 rpjup(n) 
        real*8 rstar(n) 
        real*8 mstar(n) 
        real*8 b(n)     
        real*8 frac
        real*8 prp,prs,prb      
        
c  ***  Determine fraction of trials that yield Rp < 1.5 Rj
        jlo = 0
        do i = 1, n
           if ( rpjup(i) .lt. 1.5d0 ) jlo = jlo + 1
        end do
        
        prp = dfloat(jlo)/dfloat(n)
        
c  ***  Determine fraction of trials that yield rs < 1.2 ms^0.8
        jlo = 0
        do i = 1, n
           frac = rstar(i) / mstar(i)**0.8d0
           if ( frac .lt. 1.2d0 ) jlo = jlo + 1
        end do
        prs = dfloat(jlo)/dfloat(n)
        
c  ***  Determine fraction of trials that yield b < 0.8
        jlo = 0
        do i = 1, n
           if ( b(i) .lt. 0.8d0 ) jlo = jlo + 1
        end do
        prb = dfloat(jlo)/dfloat(n)
        
        end
C+
        function funk(x)
        
c  ***  Wrapper for physparms and eval, for use in amoeba.

        implicit none
        
        real*8 x(9)
        real*8 t,p,wf,ms,d,b,vk,vs,lam
        real*8 rs,rh,aau,rstar,rp,cosi,deginc,mpjup,rpjup,gam
        real*8 chph,chsp,funk,varscl

        include 'common.inc'
        
        pi = 4d0*atan2(1d0,1d0)
        
        t = x(1)
        p = x(2)
        wf = exp(x(3))
        ms = exp(x(4))
        d = exp(x(5))
        vk = exp(x(6))
        vs = exp(x(7))
        b = 1d0 - 1d0/(1d0 + exp(x(8)))
        lam = pi * ( 1d0 - 2d0/(1d0 + exp(x(9))))
                
        
c  ***  Convert proposal parameters to physical parameters

        call physparms(p,wf,ms,d,b,vk,
     :                 rs,rh,aau,rstar,rp,
     :                 cosi,deginc,mpjup,rpjup)
        
c  ***  Generate model from parameters; fit to data and compute
c       penalty function chisq(k)

        call eval(t,p,ms,rs,rp,cosi,
     :            vk,vs,gam,lam,rstar,
     :            c1,c2,c3,c4,ms0,sigms,constrainmr,
     :            ndata,npset,fobs,lobs,adj,hjd,mag,err,
     :            nvel,hjdrv,rv,rverr,dvjit,
     :            chph,chsp,funk,varscl)

        end     
C+
      SUBROUTINE amoeba(p,y,mp,np,ndim,ftol,funk,iter)
      INTEGER iter,mp,ndim,np,NMAX,ITMAX
      DOUBLE PRECISION ftol,p(mp,np),y(mp),funk
      PARAMETER (NMAX=20,ITMAX=5000)
      EXTERNAL funk
CU    USES amotry,funk
      INTEGER i,ihi,ilo,inhi,j,m,n
      DOUBLE PRECISION rtol,sum,swap,ysave,ytry,psum(NMAX),amotry
      iter=0
1     do 12 n=1,ndim
        sum=0.d0
        do 11 m=1,ndim+1
          sum=sum+p(m,n)
11      continue
        psum(n)=sum
12    continue
2     ilo=1
      if (y(1).gt.y(2)) then
        ihi=1
        inhi=2
      else
        ihi=2
        inhi=1
      endif
      do 13 i=1,ndim+1
        if(y(i).le.y(ilo)) ilo=i
        if(y(i).gt.y(ihi)) then
          inhi=ihi
          ihi=i
        else if(y(i).gt.y(inhi)) then
          if(i.ne.ihi) inhi=i
        endif
13    continue
      rtol=2.d0*abs(y(ihi)-y(ilo))/(abs(y(ihi))+abs(y(ilo)))
      if (rtol.lt.ftol) then
        swap=y(1)
        y(1)=y(ilo)
        y(ilo)=swap
        do 14 n=1,ndim
          swap=p(1,n)
          p(1,n)=p(ilo,n)
          p(ilo,n)=swap
14      continue
        return
      endif
c      if (iter.ge.ITMAX) pause 'ITMAX exceeded in amoeba'
      if (iter.ge.ITMAX) return
      iter=iter+2
      ytry=amotry(p,y,psum,mp,np,ndim,funk,ihi,-1.0d0)
      if (ytry.le.y(ilo)) then
        ytry=amotry(p,y,psum,mp,np,ndim,funk,ihi,2.0d0)
      else if (ytry.ge.y(inhi)) then
        ysave=y(ihi)
        ytry=amotry(p,y,psum,mp,np,ndim,funk,ihi,0.5d0)
        if (ytry.ge.ysave) then
          do 16 i=1,ndim+1
            if(i.ne.ilo)then
              do 15 j=1,ndim
                psum(j)=0.5d0*(p(i,j)+p(ilo,j))
                p(i,j)=psum(j)
15            continue
              y(i)=funk(psum)
            endif
16        continue
          iter=iter+ndim
          goto 1
        endif
      else
        iter=iter-1
      endif
      goto 2
      END
C  (C) Copr. 1986-92 Numerical Recipes Software &H1216.
C+
      FUNCTION amotry(p,y,psum,mp,np,ndim,funk,ihi,fac)
      INTEGER ihi,mp,ndim,np,NMAX
      DOUBLE PRECISION amotry,fac,p(mp,np),psum(np),y(mp),funk
      PARAMETER (NMAX=20)
      EXTERNAL funk
CU    USES funk
      INTEGER j
      DOUBLE PRECISION fac1,fac2,ytry,ptry(NMAX)
      fac1=(1.d0-fac)/ndim
      fac2=fac1-fac
      do 11 j=1,ndim
        ptry(j)=psum(j)*fac1-p(ihi,j)*fac2
11    continue
      ytry=funk(ptry)
      if (ytry.lt.y(ihi)) then
        y(ihi)=ytry
        do 12 j=1,ndim
          psum(j)=psum(j)-p(ihi,j)+ptry(j)
          p(ihi,j)=ptry(j)
12      continue
      endif
      amotry=ytry
      return
      END
C  (C) Copr. 1986-92 Numerical Recipes Software &H1216.
        
       FUNCTION JULDAY(MM,ID,IYYY)
       PARAMETER(IGREG=15+31*(10+12*1582))

       IF(IYYY.EQ.0) PAUSE 'There is no Year Zero.'
       IF(IYYY.LT.0)IYYY=IYYY+1
       IF(MM.GT.2) THEN
          JY = IYYY
          JM = MM+1
       ELSE
          JY = IYYY-1
          JM = MM+13
       ENDIF
       JULDAY=INT(365.25*JY) + INT(30.6001*JM) + ID + 1720995
       IF(ID+31*(MM+12*IYYY).GE.IGREG) THEN
          JA = INT(0.01*JY)
          JULDAY = JULDAY+2-JA+INT(0.25*JA)
       ENDIF
       RETURN
       END

        subroutine fits_err(status, comment)
        integer status
        character*(*) comment
        print*,'FITS error ', status, ' ', comment
        stop
        end

        subroutine write_orion(photfile,cat_idx,cnd_idx,t0best,pbest,wfbest,dbest,
     $     rpjbest,bbest,rstbest,mstbest,prp,prs,vsbest,lambest,chisq_con,
     $     chisq_unc,sigma_t0,sigma_p,sigma_w,prb,dchisq_mr,sigma_d,sigma_rp,
     $     sigma_rst,sigma_mst,aellip,snellip,snred,chsmin,vk_teff,jh_teff,
     $     rstar_vk,rstar_jh,giant_flg,rpmj,rpmj_diff,cat_flg)

     
c  *** RGW  Write MCMC parameters to orion output file
        
        implicit none
        character*1000 photfile
        integer funit, status, idum, col, cnd_idx, cat_idx
        double precision t0best,pbest,wfbest,dbest,prb,chsmin
        double precision rpjbest,bbest,rstbest,mstbest,prp,prs,vsbest,lambest
        double precision chisq_con,chisq_unc,sigma_t0,sigma_p,sigma_w,dchisq_mr
        double precision sigma_d,sigma_rp,sigma_rst,sigma_mst,aellip,snellip,snred
        character*30 obj_id,swaspid
        logical found, anyf, done
        integer pcol, dcol, wcol, ecol, lcol, ccol, rcol, rank
        integer*2 giant_flg,cat_flg
        real vk_teff,jh_teff,rstar_vk,rstar_jh,rpl_jh,rpl_vk
        real*8 rpmj,rpmj_diff

        status=0
        funit=0
        call ftopen(funit, photfile, 1, idum, status)
        if(status.ne.0) call fits_err(status, 'opening input file')
        print*,'Writing results to input file'

        call ftmnhd(funit, 2, 'CATALOGUE', 0, status)
        call ftgcno(funit, .false., 'TEFF_VK', col, status)
        call ftpcle(funit, col, cat_idx, 1, 1, vk_teff, status)
        call ftgcno(funit, .false., 'TEFF_JH', col, status)
        call ftpcle(funit, col, cat_idx, 1, 1, jh_teff, status)
        call ftgcno(funit, .false., 'RSTAR_VK', col, status)
        call ftpcle(funit, col, cat_idx, 1, 1, rstar_vk, status)
        call ftgcno(funit, .false., 'RSTAR_JH', col, status)
        call ftpcle(funit, col, cat_idx, 1, 1, rstar_jh, status)
        call ftgcno(funit, .false., 'RPMJ', col, status)
        call ftpcld(funit, col, cat_idx, 1, 1, rpmj, status)
        call ftgcno(funit, .false., 'RPMJ_DIFF', col, status)
        call ftpcld(funit, col, cat_idx, 1, 1, rpmj_diff, status)
        call ftgcno(funit, .false., 'GIANT_FLG', col, status)
        call ftpcli(funit, col, cat_idx, 1, 1, giant_flg, status)
        call ftgcno(funit, .false., 'CAT_FLG', col, status)
        call ftpcli(funit, col, cat_idx, 1, 1, cat_flg, status)

C       Now look for the transit parameters in the candidates extension
        call ftmnhd(funit, 2, 'CANDIDATES', 0, status)
        call ftgcno(funit, .false., 'MCMC_PERIOD', col, status)
        call ftpcld(funit, col, cnd_idx, 1, 1, pbest, status)
        call ftgcno(funit, .false., 'MCMC_EPOCH', col, status)
        call ftpcld(funit, col, cnd_idx, 1, 1, t0best, status)
        call ftgcno(funit, .false., 'MCMC_DEPTH', col, status)
        call ftpcld(funit, col, cnd_idx, 1, 1, dbest, status)
        call ftgcno(funit, .false., 'MCMC_WIDTH', col, status)
        call ftpcld(funit, col, cnd_idx, 1, 1, wfbest*pbest, status)
        call ftgcno(funit, .false., 'MCMC_RSTAR', col, status)
        call ftpcld(funit, col, cnd_idx, 1, 1, rstbest, status)
        call ftgcno(funit, .false., 'MCMC_MSTAR', col, status)
        call ftpcld(funit, col, cnd_idx, 1, 1, mstbest, status)
        call ftgcno(funit, .false., 'MCMC_RPLANET', col, status)
        call ftpcld(funit, col, cnd_idx, 1, 1, rpjbest, status)
        call ftgcno(funit, .false., 'MCMC_PRS', col, status)
        call ftpcld(funit, col, cnd_idx, 1, 1, prs, status)
        call ftgcno(funit, .false., 'MCMC_PRP', col, status)
        call ftpcld(funit, col, cnd_idx, 1, 1, prp, status)
        call ftgcno(funit, .false., 'MCMC_PRB', col, status)
        call ftpcld(funit, col, cnd_idx, 1, 1, prb, status)
        call ftgcno(funit, .false., 'MCMC_IMPACT', col, status)
        call ftpcld(funit, col, cnd_idx, 1, 1, bbest, status)
        call ftgcno(funit, .false., 'MCMC_CHISQ_CONS', col, status)
        call ftpcld(funit, col, cnd_idx, 1, 1, chisq_con, status)
        call ftgcno(funit, .false., 'MCMC_CHISQ_UNC', col, status)
        call ftpcld(funit, col, cnd_idx, 1, 1, chisq_unc, status)
        call ftgcno(funit, .false., 'MCMC_DCHISQ_MR', col, status)
        call ftpcld(funit, col, cnd_idx, 1, 1, dchisq_mr, status)
        call ftgcno(funit, .false., 'MCMC_PERIOD_ERR', col, status)
        call ftpcld(funit, col, cnd_idx, 1, 1, sigma_p, status)
        call ftgcno(funit, .false., 'MCMC_EPOCH_ERR', col, status)
        call ftpcld(funit, col, cnd_idx, 1, 1, sigma_t0, status)
        call ftgcno(funit, .false., 'MCMC_WIDTH_ERR', col, status)
        call ftpcld(funit, col, cnd_idx, 1, 1, sigma_w, status)
        call ftgcno(funit, .false., 'MCMC_DEPTH_ERR', col, status)
        call ftpcld(funit, col, cnd_idx, 1, 1, sigma_d, status)
        call ftgcno(funit, .false., 'MCMC_RPLANET_ERR', col, status)
        call ftpcld(funit, col, cnd_idx, 1, 1, sigma_rp, status)
        call ftgcno(funit, .false., 'MCMC_RSTAR_ERR', col, status)
        call ftpcld(funit, col, cnd_idx, 1, 1, sigma_rst, status)
        call ftgcno(funit, .false., 'MCMC_MSTAR_ERR', col, status)
        call ftpcld(funit, col, cnd_idx, 1, 1, sigma_mst, status)
        call ftgcno(funit, .false., 'SN_ELLIPSE', col, status)
        call ftpcld(funit, col, cnd_idx, 1, 1, snellip, status)
        call ftgcno(funit, .false., 'AMP_ELLIPSE', col, status)
        call ftpcld(funit, col, cnd_idx, 1, 1, aellip, status)
        call ftgcno(funit, .false., 'SN_RED', col, status)
        call ftpcld(funit, col, cnd_idx, 1, 1, snred, status)
        call ftgcno(funit, .false., 'MCMC_CHSMIN', col, status)
        call ftpcld(funit, col, cnd_idx, 1, 1, chsmin, status)
        call ftclos(funit, status)
        if(status.ne.0) call fits_err(status, 'updating candidates table')

        end

C     ---------------------------------------------------------------------

      subroutine get_ellip(t0best,pbest,wfbest,fobs,lobs,nobs,
     :     hjd,mag,err,aellip,snellip,ntrn,nsel)

      integer nobs,fobs,lobs,ntrn,nsel
      real*8	t0best,pbest,wfbest
      real*8 	hjd(nobs),mag(nobs),err(nobs)
      include 'limit.inc'
      real*8, allocatable ::  tmag(:),merr(:),phase(:),phsort(:)

      integer it, jst, jen
      integer iotrans, notrans
      integer norb,oldorb
      integer, allocatable :: indx(:),iout(:)

      real*8  phwid,phstart,phend

      real*8	x, w, xw, xxw
      real*8	varl,varh,vl,vh,top,bot

      real*8	pi
      real*8	u, v, theta, p, xpw, ppw, aellip, snellip,aerr

c     ***  Functions
      real betai
      real gammq

      pi = 4d0*atan2(1d0,1d0)

c     ***  phwid is the half-width in phase of the region around transit
c     to be searched. We set the half width equal to the HWHM of the
c     model transit for BLS searches.

c     print *,t0best,pbest,wfbest

      allocate(tmag(maxobs))
      allocate(merr(maxobs))
      allocate(phase(maxobs))
      allocate(phsort(maxobs))
      allocate(indx(maxobs))
      allocate(iout(maxobs))

      phwid = wfbest / 2.0
      phstart = 1.0 - phwid
      phend = phwid

      k=0
      norb=0
      ntrn=0
      nsel=0
      do it = fobs, lobs
         k=k+1
         phase(k) = mod(hjd(it)-t0best,pbest) / pbest
         if (phase(k).lt.0.d0) phase(k)=phase(k)+1.d0
         tmag(k)=mag(it)
         merr(k)=err(it)*err(it)
         if((phase(k).lt.phend).or.(phase(k).gt.phstart)) then
            oldorb=norb
            norb=nint((hjd(it)-t0best)/pbest)
            if(oldorb.ne.norb) ntrn=ntrn+1
            nsel=nsel+1
         endif
      end do

      nmeas=k
      jst = nmeas / 2
      jen = nmeas / 2

c     ***  Sort the phases in ascending order
      if (nmeas .gt. 0) then
         call indexx(nmeas,phase,indx)
      end if
      do it = 1,nmeas
         phsort(it) = phase(indx(it))
      end do

c     ***  Find the start and end of the transit region
      call huntdp(phsort,nmeas,phstart,jst)
      call huntdp(phsort,nmeas,phend,jen)

c     print *,jst,jen,phstart,phend,wfbest

c     ***  Loop over out-of-transit observations
      iotrans = 0
      if(jen.gt.0 .and. jst.lt.nmeas)then
         do it = jen + 1, jst
            iotrans = iotrans + 1
            iout(iotrans) = indx(it)
         end do
      end if
      notrans = iotrans

c     ***     Fit a cosine-squared function to points outwith transit
      u = 0d0
      v = 0d0
      do iotrans = 1, notrans
         x = tmag(iout(iotrans))
         theta = 2d0*pi*phase(iout(iotrans))
         p = cos(2d0*theta)
         w = 1d0 / merr(iout(iotrans))
         xpw = x * p * w
         ppw = p * p * w
         u = u + xpw
         v = v + ppw
      end do
      if(v .gt. 1d-10)then
         aellip = u / v
         aerr = sqrt( 1d0 / v )
      else
         aellip = 9.999d0
         aerr = 9.999d0
      end if
      snellip=abs(aellip/aerr)
      
      deallocate(tmag)
      deallocate(merr)
      deallocate(phase)
      deallocate(phsort)
      deallocate(indx)
      deallocate(iout)

      end

C     ----------------------------------------------------------


*     *
*     * SUBROUTINE rednoise
*     *
*     * 
*     * Version    Date        Author    Comments
*     * 0.0                    ACC       Much of original code came from Hunter 1.0
*     * 1.0        2007-02-19  LHH       Calculate rednoise in lightcurve
*     *            2007-04-22  LHH       Fixed binwrms bug
*     *            2008-10-20  LHH       Adjust to go into mcmc_spitzer

      subroutine rednoise(dbest,maxobs,hjd,mag,merr,fobs,lobs,
     :     nsel,ntrn,snred)

      implicit none

      integer   nsumsize,maxobs,nbsize
      real*8	  tbin          ! duration of rednoise bin (hrs)
      parameter (nsumsize=2000,tbin=2.5,nbsize=10000)

      integer	   	ngt,nsel,ntrn
      integer	   	fobs,lobs
      real*8		hjd(maxobs),mag(maxobs),merr(maxobs)
      real*8		snred,dbest

      integer*4	   ii,jj,nbin(nbsize)
      integer*4	   nindex(nbsize,2),id1,id2

      integer*4          ntot,ndat,ndatmax
      integer*4          idat,isum,ibin,iend,istart,iobs
      integer*4          s_n,nsum(nsumsize)

      double precision   tstart,tend,ttbin,tlast
      double precision   mm,s_mm,s_mmw,s_mw
      double precision   s_xw,s_pw,x,p,w,s_w
      double precision   wt,twt
      double precision   phat,xhat,logx0
      double precision   ntrn_f,nsel_f
      double precision   sigma,beta,binwrms,n_ave

c      double precision   xlog(nsumsize),plog(nsumsize)
c      double precision   wlog(nsumsize),binrms
      double precision, allocatable :: xlog(:), plog(:), wlog(:)
      double precision binrms

c     double precision   pmag_avg(nbsize,nsumsize)
c     double precision   pmag_var(nbsize,nsumsize)
      double precision, allocatable :: pmag_avg(:,:), pmag_var(:,:)

c     -----------------------------------------------

      ttbin=tbin/24d0

      allocate(xlog(maxobs))
      allocate(plog(maxobs))
      allocate(wlog(maxobs))
      allocate(pmag_avg(nbsize,nsumsize))
      allocate(pmag_var(nbsize,nsumsize))

C     get start and end indices for each night

      ngt=1
      nindex(ngt,1)=1
      do ii=fobs+1,lobs
         if(hjd(ii)-hjd(ii-1).gt.0.5d0) then
            nindex(ngt,2)=ii-1
            ngt=ngt+1
            nindex(ngt,1)=ii
         endif
      enddo
      nindex(ngt,2)=lobs

C     ------------ From Andrews hunter 1.0 -----------

C     *** For each night, determine num of full 2.5-hour bins
      do ii = 1, ngt
         nbin(ii) = 0
         id1=nindex(ii,1)
         id2=nindex(ii,2)
         tlast = hjd(id2)
         do jj = id1,id2
	    tstart = hjd(jj)
	    tend = tstart + ttbin
            if(tend .le. tlast) nbin(ii)=nbin(ii)+1	
         end do
c     print *,ii,id1,id2,nbin(ii),hjd(id1),hjd(id2)
      end do

C     *** Initialize
      do idat = 1, nsumsize		
         nsum(idat) = 0
      end do
      ndatmax = -1
      
      s_mm = 0d0
      s_mmw = 0d0
      twt = 0d0
      s_n = 0			! num points in complete bin
      ntot = 0                  ! num complete ttbin size bins
      do ii = 1, ngt
         id1=nindex(ii,1)
         id2=nindex(ii,2)

C     *** First do the incomplete intervals at start of night	   
         ndat = 0
         s_mw = 0d0
         twt = 0d0
         tstart = hjd(id1)
         do iend = id1,id2
	    tend = hjd(iend)
	    if(tend-tstart .lt. ttbin)then
               wt = 1d0 / merr(iend)
               s_mw = s_mw + mag(iend)*wt
               twt = twt + wt
               ndat = ndat + 1
C     *** Increment number of sequences having ndat data points
               nsum(ndat) = nsum(ndat) + 1
c     print *,ndat,nsum(ndat)
C     *** Record average mag and associated variance
               pmag_avg(nsum(ndat),ndat) = s_mw / twt
               pmag_var(nsum(ndat),ndat) = 1d0 / twt
               ndatmax = max(ndatmax,ndat)
            end if
         end do

c     *** Now do the complete intervals	   
         do jj = 1, nbin(ii)  
	    ibin=id1+jj
	    tstart = hjd(ibin)
	    tend = tstart + ttbin
	    ndat = 0
	    s_mw = 0d0
	    twt = 0d0
	    do iobs = ibin,id2
               if(hjd(iobs).le.tend)then
                  ndat = ndat + 1
                  wt = 1d0 / merr(iobs)
                  s_mw = s_mw + mag(iobs)*wt
                  twt = twt + wt 
               end if
	    end do

C     *** Compute weighted rms in binned mags -- as you loop through bins
	    mm=s_mw / twt	!mag_avg
	    s_mm = s_mm + mm*mm
	    s_mmw = s_mmw + mm*mm*twt
	    s_w = s_w + twt
	    s_n = s_n + ndat
	    ntot = ntot + 1

	    ndatmax=max(ndatmax,ndat)
	    nsum(ndat)=nsum(ndat)+1
	    pmag_avg(nsum(ndat),ndat) = s_mw / twt
	    pmag_var(nsum(ndat),ndat) = 1d0 / twt

         enddo

c     *** Finally do the incomplete intervals at end of night	   
         ndat = 0
         s_mw = 0d0
         twt = 0d0
         tend = hjd(id2)
         do istart = id2,id1,-1
            tstart = hjd(istart)
            if(tend-tstart .lt. ttbin)then
               wt = 1d0 / merr(istart)
               s_mw = s_mw + mag(istart)*wt
               twt = twt + wt
               ndat = ndat + 1
               nsum(ndat) = nsum(ndat) + 1
               pmag_avg(nsum(ndat),ndat) = s_mw / twt
               pmag_var(nsum(ndat),ndat) = 1d0 / twt
               ndatmax = max(ndatmax,ndat)
            endif
         end do

      end do                    ! DONE WIITH night loop

c     *** Weighted & unweighted RMS scatter in binned magnitudes
c     and average number of data points in 2.5 hour bin
      binwrms = sqrt(s_mmw / s_w)
      if (ntot.gt.0) then
         binrms = sqrt(s_mm / dfloat(ntot))
         n_ave = dfloat(s_n) / dfloat(ntot)
      else
         binrms = 0.d0
         n_ave = 0.d0
      endif
      
C     *** Compute the weighted RMS scatter in the binned magnitudes
C     for bins with different numbers of observations

      do idat = 1, ndatmax
         s_mm = 0d0
         s_mmw = 0d0
         twt = 0d0
         s_n = 0
         ntot = 0
         do isum = 1, nsum(idat)
            mm = pmag_avg(isum,idat)
            wt = 1d0 / pmag_var(isum,idat)
            s_mm = s_mm + mm*mm
            s_mmw = s_mmw + mm*mm*wt
            twt = twt + wt
            s_n = s_n + idat
            ntot = ntot + 1
         end do
c     *** Weighted & unweighted RMS scatter in binned magnitudes
c     arrurms(idat) = sqrt(s_mm / dble(ntot))
         xlog(idat) = log10(sqrt(s_mmw / twt))
         plog(idat) = log10(dble(idat))
         wlog(idat) = nsum(idat)
      end do

c     ***	   Orthogonalise
      
      s_xw = 0d0
      s_pw = 0d0
      twt = 0d0
      do idat = 1, ndatmax-1
         wt = wlog(idat)
         s_xw = s_xw + xlog(idat)*wt
         s_pw = s_pw + plog(idat)*wt
         twt = twt + wt
      end do
      xhat = s_xw / twt
      phat = s_pw / twt

c     ***	   Finally do a linear regression on 
c     log(arrwrms) versus log(arrnsum) to determine the colour
c     of the noise.

      s_xw = 0d0
      s_pw = 0d0
      do idat = 1, ndatmax-1
         x = xlog(idat) - xhat
         p = plog(idat) - phat
         w = wlog(idat)
         s_xw = s_xw + x*p*w
         s_pw = s_pw + p*p*w
      end do
      beta = s_xw / s_pw
      logx0 = xhat - phat*beta
      sigma = 10d0 ** logx0

c     ***	Calculate signal-to-rednoise 

      ntrn_f=dfloat(ntrn)
      nsel_f=dfloat(nsel)
      if (nsel.eq.0.or.ntrn.eq.0.)then
         snred=0.0
      else
         snred=-1d0*dbest*sqrt(ntrn_f)/(sigma*(nsel_f/ntrn_f)**beta)
      endif

      deallocate(pmag_avg)
      deallocate(pmag_var)
      deallocate(xlog)
      deallocate(plog)
      deallocate(wlog)

      return
      end

