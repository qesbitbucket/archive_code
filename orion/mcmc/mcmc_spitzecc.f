C+	
	program mcmctransit
	
c  ***	This is Andrew Cameron's parameter-fitting code for
c	transiting exoplanets. 

c  ***	This version reads 
c  ***	Important: please read the blurb at line 540

c  ***	Recent History:

c	v1.3 2007-09-04	ACC at St A 
c	Calculate and report opposition flux ratio for unit albedo,
c	synchronisation timescales for star and planet, Log g for
c	star and planet, planet density in jovian units.
	
c	v1.4 2007-09-05	ACC at St A 
c	Reduce initial guesstimate of transit epoch uncertainty from
c	5 min to 2 min when setting starting parameters.
c	This reduces the likelihood of jumping too far during burn-in.
		
c	v1.5 2007-09-18	ACC at St A 
c	Report planet/star blackbody equilibrium temperature ratio.

c	v1.6 2008-02-18	ACC at St A 
c	Rearrange to report data and best-fit model params from
c	MCMC before doing the amoeba thing. If amoeba crashes, you
c	still get useful output for plotting.
c	Take t0, p, rp, rs and b out of conditional block at lines
c	800-814. Avoid segfaults when ipk = 0 is selected (manual
c	ephemeris) by setting ipk=max(ipk,1) at line 1055. 
c	Set vsi = 2d0 -- when set to 0d0 (which is physically 
c	very unlikely) a NaN was generated at line 1069-1070)

c	v1.7 2008-02-22	ACC at St A 
c	Allow multiple RV datasets with different zero points.
c	Report gamma for the first one only; adjust the gamma
c	for subsequent datasets to match, when generating output
c	for plotting.

c	v1.8 2008-02-22	ACC at St A 
c	Introduce different limb-darkening coefficients for different
c	photometric bandpasses.

c	v1.9 2008-02-28	ACC at St A 
c	Introduce fitting of planet/star flux ratio from
c	secondary-eclipse measurements in up to 2 bandpasses.

c	v1.10 2008-03-01	ACC at St A 
c	Introduce counter jtry to spot when Markov chain gets badly 
c	stuck at an individual step, replacing the old cumulative 
c	method using jtemp. This is more robust.

c	v1.11 2008-03-04	ACC at St A 
c	Prompt user for value of v sin i (currently frozen with sigvs=0)
c	Allow user to redefine ms0 manually if required
c	Don't call newstats after burn-in period.

c	v1.12 2008-05-11	ACC at St A 
c	Tweak to orthtp at line 2083 to give a smaller estimate of sigper
c	which might help to prevent Markov chain sticking.

c	v1.13 2008-05-14	ACC at St A 
c	Reduce sigt0 by factor of 10 (to 12s) when ipk=0 is selected, to
c	prevent Markov chain sticking.

c	v1.14 2008-10-03	LHH at StA
c	Added option to adjust stellar Teff manually. Added outputs 
c	to compute secondary eclipse duration and other quantities 
c	needed to help prepare Spitzer AORs.

c	v1.15 2008-10-13	ACC at StA
c	Allow for orbital eccentricity when computing rs from wf in
c	subroutine physparms. This means that the final wf bears a 
c	much closer resemblance to the actual transit duration 
c	for planets with significant orbital eccentricities.

c	v1.16 2008-10-17	ACC at StA
c	Use ecc*cos(om) and ecc*sin(om) as jump variables in place of
c	ecc and om as recommended by Eric Ford

c	v1.17 2008-10-17	ACC at StA
c	Use vsi*cos(lam) and vsi*sin(lam) as jump variables in place of
c	vsi and lam when fitting Rossiter effect, by analogy with ecc 
c	and om above. Change last few cols of output.csv to list
c	Markov chains for vcosl, vsinl, ecosw, esinw for easy plotting.
c	Un-comment lines 671 and 672 to model Rossiter effect.

c	v1.18 2008-10-19	ACC at StA
c	When computing secondary eclipse duration, include change
c	in impact parameter due to change in star-planet separation
c	across eccentric orbit. 

c	v1.19 2008-10-22	ACC at StA
c	Give user control over whether or not to fit (a) an eccentric
c	orbit, or (b) the Rossiter effect. 

c	v1.20 2008-10-25	ACC at StA
c	Modify Rossiter effect treatment to improve approximation to
c	anomalous velocity during partial phases, using a linear 
c	approximation to the distance of the centroid of the occulted
c	part of the stellar disc from the centre of the star. 

c	v1.21 2008-10-28	ACC at StA
c	Simplify subroutine conflim.
c	Given an array x from a Monte Carlo simulation, 
c	determine median value and give lower and upper error
c	bars at 15.85 and 84.15 percentile limits. These enclose
c	the central 68.3 percent of the probability distribution
c	projected on to the parameter of interest. This should 
c	reduce the stochastic variations from one run to the next
c	that occurred with the old version, which reported the 
c	parameter value at the global maximum likelihood solution.

c	v1.22 2008-11-20	ACC at StA
c	Increased maxrv from 100 to 200. Report chsmin to standard 
c	output and to mcmc.out.

c	v1.23 2008-11-23	ACC at StA
c	Fix error in expression for cosi at lines 1466, 2131

c	v1.24 2009-02-09	LHH at StA
c	Added spiral-in time for planets from Levard et al.  
c	Corrected planet roche radius calcluation.

c	v1.25 2009-02-18	ACC/CS at StA
c	Fix typo-induced bug in call to orthk1 at line 700 
c	(array err was being used where rverr sould have been).
c	No serious side effects will have resulted since it only
c	yields an initial estimate.

c	v1.26 2009-02-21	ACC at StA
c	Read xy positional information in Spitzer channels 1 and 2,
c	even if we don't do anything with it yet.

c	v1.27 2009-02-23	ACC at StA
c	Change subroutine parameter lists to include parameters needed
c	for intra-pixel sensitivity corrections in Spitzer channels 
c	1 and 2, and quadratic fit in log elapsed time for channels 
c	3 and 4.

c	v1.28 2009-02-24	ACC at StA
c	Implement quadratic fit in log elapsed time for Spitzer/IRAC 
c	channels 3 and 4. 

c	v1.29 2009-02-24	ACC at StA
c	Implement quadratic fit in x, y subpixel position for 
c	Spitzer/IRAC channels 1 and 2. 

	implicit none
	
	real*8 pi
	
	integer maxobs
	parameter(maxobs=30000)
	real*8 hjd(maxobs)
	real*8 mag(maxobs)
	real*8 err(maxobs)
	integer ndata
	real*8 xp(maxobs),yp(maxobs)
	
	integer maxrv
	parameter(maxrv = 200)
	real*8 hjdrv(maxrv)
	real*8 rv(maxrv)
	real*8 rverr(maxrv)
	integer nvel
	
	integer pset, npset
	integer mxpset
	parameter ( mxpset = 20)
	integer fobs(mxpset)
	integer lobs(mxpset)
	integer np(mxpset)
	real*8 adj(mxpset)
	real*8 varscl(mxpset)
c  ***	Limb-darkening coeffs from Claret (2000)
	real*8 c1(mxpset), c2(mxpset), c3(mxpset), c4(mxpset)

	integer mxvset
	parameter ( mxvset = 5)
	integer vset, nvset
	integer fvel(mxvset),nv(mxvset),lvel(mxvset)
	real*8 gadj(mxvset)
	real*8 varscv(mxvset)
	
	integer mxsset
	parameter ( mxsset = 4)
	integer isset, nsset
	integer firpt(mxsset),nirpt(mxsset),lirpt(mxsset),ichan(mxsset)
	real*8 fs(mxsset)
	real*8 sdp(mxsset),sigsdp(mxsset)
	real*8 rmp1(mxsset),sigrmp1(mxsset)
	real*8 rmp2(mxsset),sigrmp2(mxsset)
	real*8 cx(mxsset),cxx(mxsset)
	real*8 cy(mxsset),cyy(mxsset)
	real*8 sigcx(mxsset),sigcxx(mxsset)
	real*8 sigcy(mxsset),sigcyy(mxsset)
		
	real*8 sigsdp1,sigcx1,sigcxx1,sigcy1,sigcyy1
	real*8 sigsdp2,sigcx2,sigcxx2,sigcy2,sigcyy2
	real*8 sigsdp3,sigrmp31,sigrmp32
	real*8 sigsdp4,sigrmp41,sigrmp42
	integer maxsecl
	parameter(maxsecl = 10000)
	integer nsecl, mxsecl
	real*8 hjdsecl(maxsecl)
	real*8 flxsecl(maxsecl)
	real*8 errsecl(maxsecl)
	real*8 xpos(maxsecl)
	real*8 ypos(maxsecl)
	real*8 zsecl(maxsecl)
	real*8 eta(maxsecl)
	logical sececl(4)
	integer irpt
	
	real*8 t0,period,dmag,wday,k1,vsi
	real*8 sigt0,sigper,sigdm,sigw,sigvk,sigb
	real*8 sigvcosl, sigvsinl
	real*8 sigecosw, sigesinw
	
	integer i, j, k, idum, jtemp, ktemp
	integer maxjmp
	parameter(maxjmp=100000)
	integer njump
	real*8 t(maxjmp)
	real*8 p(maxjmp)
	real*8 rs(maxjmp)
	real*8 ms(maxjmp)
	real*8 rp(maxjmp)
	real*8 b(maxjmp)
	real*8 d(maxjmp)
	real*8 wf(maxjmp)
	real*8 vk(maxjmp)
	real*8 vs(maxjmp)
	real*8 vcosl(maxjmp)
	real*8 vsinl(maxjmp)
	real*8 gam(maxjmp)
	real*8 ecosw(maxjmp)
	real*8 esinw(maxjmp)
	real*8 ecc(maxjmp)
	real*8 om(maxjmp)
	real*8 lam(maxjmp)
	
	real*8 fs1(maxjmp)
	real*8 fs2(maxjmp)
	real*8 fs3(maxjmp)
	real*8 fs4(maxjmp)
	real*8 rmp31(maxjmp)
	real*8 rmp32(maxjmp)
	real*8 rmp41(maxjmp)
	real*8 rmp42(maxjmp)
	real*8 sdp1(maxjmp)
	real*8 sdp2(maxjmp)
	real*8 sdp3(maxjmp)
	real*8 sdp4(maxjmp)
	real*8 cx1(maxjmp)
	real*8 cxx1(maxjmp)
	real*8 cy1(maxjmp)
	real*8 cyy1(maxjmp)
	real*8 cx2(maxjmp)
	real*8 cxx2(maxjmp)
	real*8 cy2(maxjmp)
	real*8 cyy2(maxjmp)
	
	real*8 rh(maxjmp)
	real*8 chisq(maxjmp)
	real*8 chsp(maxjmp)
	real*8 chph(maxjmp)
	real*8 chse(maxjmp)
	real*8 mpjup(maxjmp)
	real*8 rpjup(maxjmp)
	real*8 rstar(maxjmp)
	real*8 aau(maxjmp)
	real*8 deginc(maxjmp)
	real*8 dgam(maxjmp)
	integer idx(maxjmp)
	real*8 dxl,xml,dxh
	real*8 sigms,ms0,sigwf
c	real*8 mstar
	real*8 cosi
	real*8 sini
c	real*8 psec,akm,q,vt,sini
	real*8 scfac, oldscfac
c	real*8 dchisq,pjump,prob
	character*80 photfile,rvfile,seclfile
c	character*80 parfile
	character*120 infile(mxpset)
	integer lu
	integer mxobs,mxvel
c	real*8 x,u,w,sx,su,sw,sxx,sxu,suu,xhat,uhat
c	integer nx
c	integer status
	real*8 wfrac,hw1,hw2
c	integer norb
	real*8 chsmin,t0best,pbest,wfbest,dbest,vkbest,vsbest,bbest
	real*8 rpjbest,gambest, lambest
	real*8 vcosl0,vsinl0
	real*8 vcoslbest,vsinlbest
	real*8 chsbst,chpbst
	real*8 b0
	real*8 rsbest,rpbest
	real*8 ecosw0,esinw0
	real*8 eccbest,ombest
	real*8 ecoswbest,esinwbest
c	real*8 wbest
	logical constrainmr
c	real*8 cphi,sphi
	character*1 yesno
	integer nmod
	real*8 ph,pr,zp,up
	real*8 z(maxobs),mu(maxobs),vz(maxrv),nu(maxrv)
	real*8 zmod(1000),mumod(1000),numod(1000),alpha(1000),rau(1000)
	real*8 etamod(1000)	
	logical radvel
c	logical burned_in
	real*8 dvjit
	character*26 swaspid
	integer maxpk
	parameter(maxpk=5)
	integer ipk
	real*8 eppk(maxpk)
	real*8 ppk(maxpk)
	real*8 wdpk(maxpk)
	real*8 dmpk(maxpk)
	real*8 dcpk(maxpk)
	real*8 snrpk(maxpk)
	integer ntrpk(maxpk)
     	real*8 aellpk(maxpk)
	real*8 snellpk(maxpk)
	real*8 transfpk(maxpk)
	real*8 gaprpk(maxpk)
	real*8 dchsantpk(maxpk)
	real*8 jh,rpmj,drpmj
	real*8 rs0
c	real*8 prp,prs
	real*8 dcmax
	integer ipmax
c	real*8 prat
c	real*8 dch_rs,dch_rp
 	real*8 t0best_mc
  	real*8 pbest_mc 
  	real*8 msbest_mc
  	real*8 wfbest_mc
  	real*8 dbest_mc 
  	real*8 vkbest_mc
  	real*8 vcoslbest_mc
  	real*8 vsinlbest_mc
  	real*8 bbest_mc 
  	real*8 vsbest_mc
  	real*8 lambest_mc
	real*8 gambest_mc
   	real*8 ecoswbest_mc
   	real*8 esinwbest_mc
   	real*8 eccbest_mc
   	real*8 ombest_mc
	     	      
	real*8 cx1best_mc 
	real*8 cx2best_mc 
	real*8 cxx1best_mc
	real*8 cxx2best_mc
	real*8 cy1best_mc 
	real*8 cy2best_mc 
	real*8 cyy1best_mc
	real*8 cyy2best_mc
	real*8 fs1best_mc
	real*8 fs2best_mc
	real*8 fs3best_mc
	real*8 fs4best_mc
	real*8 rmp31best_mc
	real*8 rmp32best_mc
	real*8 rmp41best_mc
	real*8 rmp42best_mc
	real*8 sdp1best_mc 
	real*8 sdp2best_mc 
	real*8 sdp3best_mc 
	real*8 sdp4best_mc 
	
	real*8 dincbest,rstbest,mstbest
	integer iday(3), id, mm, iyyy, jdtrunc
	real*8 hjdtoday,tnow(maxjmp)
	integer norbnow
c	real*8 t2, t1
	integer mcorr
	integer mxcorr
	parameter(mxcorr = 100)
	real*8 c_t(mxcorr)
	real*8 c_p(mxcorr)
	real*8 c_d(mxcorr)
	real*8 c_w(mxcorr)
	real*8 c_b(mxcorr)
	real*8 c_ms(mxcorr)
	real*8 c_vs(mxcorr)
	real*8 c_vk(mxcorr)
	real*8 c_lam(mxcorr)
 	real*8 c_ecc(mxcorr)
 	real*8 c_om(mxcorr)
 	real*8 cl_t,cl_p,cl_d,cl_w,cl_b,cl_ms,cl_vk,cl_vs
 	real*8 cl_lam,cl_ecc,cl_om
	logical reject
	real*8 ratio
	integer kst, ken
c	double precision st,sp,sm,sw,sd,sk,sv,sb
c	double precision stt,spp,smm,sww,sdd,skk,svv,sbb
	integer nburn
	real*8 prp,prs,prb
	integer strikes
	real*8 xfunk(11)
	real*8 funk
 	real*8 yval(12)
 	integer indy(12)
 	real*8 parms(12,11)
	real*8 ftol
	integer iter
	integer kbest
	real*8 chisqbest,chphbest,chspbest,chsebest
	real*8 dchisq_mr
	real*8 rhobest,aaubest,mpjbest
	real*8 chisq_con,chisq_unc
	real*8 cosl, sinl
	real*8 sigma_t0, sigma_p, sigma_w,sigma_d
        real*8 sigma_rst,sigma_mst,sigma_rp
	
 	real*8 jd
 	real*8 nutrans,mtrans,dttrans,meananom,tperi
 	real*8 tanhalfecc,eccanom,rtrans
	real*8 eccfactor, bocc	

	real*8 rpsq(maxjmp)
	real*8 qrat,gacc_p,gacc_s
	real*8 tau_s(maxjmp)
	real*8 tau_p(maxjmp)
	real*8 logg_s(maxjmp)
	real*8 logg_p(maxjmp)
	real*8 rho_p(maxjmp)
	real*8 teql_p(maxjmp)
	real*8 tide(maxjmp)
	real*8 roche(maxjmp),qth,rocherat(maxjmp),mpl,tspiral(maxjmp)
	real*8 ltot(maxjmp),lrat(maxjmp),lcrit,Istar,Ipl,GMsun
	real*8 irrad(maxjmp)
	real*8 phsec(maxjmp)
	real*8 enow(maxjmp)
	real*8 ewf(maxjmp)
	real*8 t1_t2(maxjmp)
	real*8 et1_t2(maxjmp)
	real*8 nuocclt,mocclt
	real*8 tjh,prat,tf
	integer ipset
	character*32 modfile
	integer jtry
	real*8 spw,sw,wsc,tsc,psc,xsc,rsc,that
	real*8 su, sv, uhat, vhat
	real*8 fsbest(mxsset)
	real*8 sdpbest(mxsset)
	real*8 rmp1best(mxsset)
	real*8 rmp2best(mxsset)
	real*8 cxbest(mxsset)
	real*8 cxxbest(mxsset)
	real*8 cybest(mxsset)
	real*8 cyybest(mxsset)
	logical fitecc, fitross
	real*8 zscfac
	
	common	/coeffs/	c1,c2,c3,c4
	common	/masses/	ms0,sigms
	common	/constr/	constrainmr
	common	/phodim/	ndata,npset
	common	/hjdpho/	hjd	
	common	/datpho/	mag	
	common	/errpho/	err
	common	/part01/	fobs	
	common	/part02/	lobs
	common	/np/		np
	common	/rvedim/	nvel,nvset
	common	/hjdrve/	hjdrv
	common	/datrve/	rv
	common	/errrve/	rverr
	common	/jitter/	dvjit
	common	/adjust/	adj
	common	/part03/	fvel
	common	/part04/	lvel
	common	/gadjst/	gadj
	common	/mu/		mu
	common	/nu/		nu
	common	/secdim/	nsecl,nsset
	common	/part05/	firpt
	common	/part06/	lirpt
	common	/nirpt/		nirpt
	common	/hjdsecl/	hjdsecl	
	common	/flxsecl/	flxsecl	
	common	/errsecl/	errsecl	
	common	/xsecl/		xpos	
	common	/ysecl/		ypos	
	common	/sdp/		sdp
c	common	/rmp/		rmp
	common	/fs/		fs
	
	external funk
	integer julday

	pi = 4d0*atan2(1d0,1d0)
	idum = time()
	idum = mod(idum,50000)
		
	pset = 0
	ndata = 0

	print *,' Enter filename with LCFINDER photometry : '
	read(*,'(a)') photfile
	
c  ***	Read photometric data from LCFINDER output file	
c	photfile = 'phot.dat'
	lu = 42
	mxobs = maxobs
	pset = pset + 1
	fobs(pset) = ndata+1
	call readnewlc(lu,photfile,
     :	           swaspid,jh,rpmj,drpmj,
     :	           eppk,ppk,wdpk,dmpk,dcpk,snrpk,ntrpk,
     :	           aellpk,snellpk,transfpk,gaprpk,dchsantpk,
     :	           mxobs,np(pset),
     :	           hjd(ndata+1),mag(ndata+1),err(ndata+1))
	ndata = ndata + np(pset)
	lobs(pset) = ndata
	tjh = -4369.5d0*jh + 7188.2d0	!Teff in Kelvin
	     
c  ***	Estimate stellar parameters from J-H
8	print *,' Enter filename with Claret limb-darkening tables : '
	read(*,'(a)',err=8) infile(pset)
	call getparms(jh,rs0,tjh,infile(pset),
     :       c1(pset),c2(pset),c3(pset),c4(pset))
	
	print *,' Read another photometric dataset?(Y/N) [N] : '
	read(*,'(a)') yesno
	if ( yesno(1:1).eq.'y' .or. yesno(1:1).eq.'Y' ) then
	   go to 12
	else
	   go to 13
	end if
	
12	print *,' Enter filename with ancillary photometry : '
	read(*,'(a)') photfile
	
c  ***	Read photometric data from file	
c	photfile = 'phot.dat'
	lu = 42
	pset = pset + 1
	fobs(pset) = ndata+1
	call rddata(lu,photfile,mxobs,np(pset),
     :	hjd(ndata+1),mag(ndata+1),err(ndata+1))
	ndata = ndata + np(pset)
	lobs(pset) = ndata
	
c  ***	Estimate stellar parameters from J-H
14	print *,' Enter filename with Claret limb-darkening tables : '
	read(*,'(a)',err=14) infile(pset)
	call getparms(jh,rs0,tjh,infile(pset),
     :       c1(pset),c2(pset),c3(pset),c4(pset))

	print *,' Read another photometric dataset?(Y/N) [N] : '
	read(*,'(a)') yesno
	if ( yesno(1:1).eq.'y' .or. yesno(1:1).eq.'Y' ) then
	   go to 12
	end if
	
13	npset = pset
	
        print *,' Stellar temp from J-H is tjh = ',tjh
        print *,' Do you want to change tjh? '
        read(*,'(a)') yesno
        if ( yesno(1:1).eq.'y' .or. yesno(1:1).eq.'Y' ) then
           print *,' Enter revised value for tjh : '
           read(*,*) tjh
 	 do pset=1,npset
	 call getparms(jh,rs0,tjh,infile(pset),
     :        c1(pset),c2(pset),c3(pset),c4(pset))
	 print *,pset,c1(pset),c2(pset),c3(pset),c4(pset)
	 enddo
        end if

c Mass in Msun
     	ms0 = rs0**(1d0/0.8d0)

	print *,' Stellar mass from Teff is ms0 = ',ms0
	print *,' Do you want to change ms0? '
	read(*,'(a)') yesno
	if ( yesno(1:1).eq.'y' .or. yesno(1:1).eq.'Y' ) then
	   print *,' Enter revised value for ms0 : '
	   read(*,*) ms0
	end if
	
	vset = 0
	nvel = 0
c  ***	Read radial velocity data from file
	print *,' Enter filename with radial velocity data [none]: '
	read(*,'(a)') rvfile
	if ( rvfile .eq. ' ' .or. rvfile.eq.'none' 
     :	 .or. rvfile.eq.'NONE' ) then
     	   radvel = .false.
	else
21	   lu = 44
	   vset = vset + 1
	   fvel(vset) = nvel + 1
	   mxvel = maxrv
	   call rddata(lu,rvfile,mxvel,nv(vset),
     :	                hjdrv(nvel+1),rv(nvel+1),rverr(nvel+1))
	   radvel = .true.
	   nvel = nvel + nv(vset)
	   lvel(vset) = nvel
	   
	   print *,vset,fvel(vset),lvel(vset),nv(vset),nvel
	   print *,' Read another RV dataset?(Y/N) [N] : '
	   read(*,'(a)') yesno
	   if ( yesno(1:1).eq.'y' .or. yesno(1:1).eq.'Y' ) then
	      print *,' Enter filename with ancillary RV data : '
	      read(*,'(a)') rvfile
	      go to 21
	   end if
	   
	end if
	
	nvset = vset

	if ( nvel.gt.0 ) then
	   print *,'Enter jitter in km/sec : '
	   read(*,*) dvjit
	   print *,'Enter v sin i in km/sec : '
	   read(*,*) vsi
	   
	   print *,' Force orbit to be circular ? (Y/N) [N] '
	   read(*,'(a)') yesno
	   fitecc = .true.
	   if ( yesno(1:1).eq.'y' .or. yesno(1:1).eq.'Y' ) then
	      fitecc = .false.
	   end if

	   print *,' Fit Rossiter-McLaughlin effect ? (Y/N) [N] '
	   read(*,'(a)') yesno
	   fitross = .false.
	   if ( yesno(1:1).eq.'y' .or. yesno(1:1).eq.'Y' ) then
	      fitross = .true.
	   end if

	end if
	
	
	isset = 0
	nsecl = 0
c  ***	Read (IR) secondary eclipse photometry from file
	print *,' Enter filename with Spitzer ch1 data [none]: '
	read(*,'(a)') seclfile
	if ( seclfile .eq. ' ' .or. seclfile.eq.'none' 
     :	 .or. seclfile.eq.'NONE' ) then
     	   sececl(1) = .false.
	else
	   lu = 45
	   isset = isset + 1
	   firpt(isset) = nsecl + 1
	   mxsecl = maxsecl
	   call rdspxy(lu,seclfile,mxsecl,nirpt(isset),
     :	             hjdsecl(nsecl+1),flxsecl(nsecl+1),errsecl(nsecl+1),
     :	             xpos(nsecl+1),ypos(nsecl+1))
	   sececl(1) = .true.
	   nsecl = nsecl + nirpt(isset)
	   lirpt(isset) = nsecl
	   ichan(isset) = 1	   
	end if
	
	print *,' Enter filename with Spitzer ch2 data [none]: '
	read(*,'(a)') seclfile
	if ( seclfile .eq. ' ' .or. seclfile.eq.'none' 
     :	 .or. seclfile.eq.'NONE' ) then
     	   sececl(2) = .false.
	else
	   lu = 45
	   isset = isset + 1
	   firpt(isset) = nsecl + 1
	   mxsecl = maxsecl
	   call rdspxy(lu,seclfile,mxsecl,nirpt(isset),
     :	             hjdsecl(nsecl+1),flxsecl(nsecl+1),errsecl(nsecl+1),
     :	             xpos(nsecl+1),ypos(nsecl+1))
	   sececl(2) = .true.
	   nsecl = nsecl + nirpt(isset)
	   lirpt(isset) = nsecl	 
	   ichan(isset) = 2  
	end if
	
	print *,' Enter filename with Spitzer ch3 data  [none]: '
	read(*,'(a)') seclfile
	if ( seclfile .eq. ' ' .or. seclfile.eq.'none' 
     :	 .or. seclfile.eq.'NONE' ) then
     	   sececl(3) = .false.
	else
	   lu = 45
	   isset = isset + 1
	   firpt(isset) = nsecl + 1
	   mxsecl = maxsecl
	   call rddata(lu,seclfile,mxsecl,nirpt(isset),
     :	             hjdsecl(nsecl+1),flxsecl(nsecl+1),errsecl(nsecl+1))
	   sececl(3) = .true.
	   nsecl = nsecl + nirpt(isset)
	   lirpt(isset) = nsecl
	   ichan(isset) = 3
	end if
	
	print *,' Enter filename with Spitzer ch4 data  [none]: '
	read(*,'(a)') seclfile
	if ( seclfile .eq. ' ' .or. seclfile.eq.'none' 
     :	 .or. seclfile.eq.'NONE' ) then
     	   sececl(4) = .false.
	else
	   lu = 45
	   isset = isset + 1
	   firpt(isset) = nsecl + 1
	   mxsecl = maxsecl
	   call rddata(lu,seclfile,mxsecl,nirpt(isset),
     :	             hjdsecl(nsecl+1),flxsecl(nsecl+1),errsecl(nsecl+1))
	   sececl(4) = .true.
	   nsecl = nsecl + nirpt(isset)
	   lirpt(isset) = nsecl
	   ichan(isset) = 4
	end if
	nsset = isset

c  ***	Truncate the JD
	do isset = 1, nsset
	   do irpt = firpt(isset),lirpt(isset)
	      hjdsecl(irpt) = hjdsecl(irpt) - 2450000d0
	   end do
	end do
	
	print *,' You may either let the data determine '
	print *,' the stellar radius, or impose a main '
	print *,' sequence mass-radius relation if the '
	print *,' S:N of the photometry is poor.' 
	print *, '  '
11	print *,' Impose main-sequence mass-radius relation?(Y/N)[N] : '
	read(*,'(a)') yesno
	if ( yesno(1:1).eq.'y' .or. yesno(1:1).eq.'Y'  ) then
	   constrainmr = .true. 
	   print *,'Main sequence radius constraint ON'
	else if ( yesno(1:1).eq.'n' .or. yesno(1:1).eq.'N' 
     :	.or. yesno(1:1).eq.' ') then
	   constrainmr = .false.
	   print *,'Main sequence radius constraint OFF'
	else
	   go to 11
	end if
	
 	print *,' Enter number of iterations during burn-in: '
 	read(*,*) nburn
 	
 	print *,' Enter number of iterations after burn-in: '
	read(*,*) njump
	
	njump = njump + nburn
	
	dcmax = 0d0
	do ipk = 1, 5
	   print *,' Peak ',ipk,'  dchisq = ',dcpk(ipk)
	   dcmax = max(dcmax,dcpk(ipk))
	   if(dcmax .eq.dcpk(ipk)) ipmax = ipk
	end do
	
	print *,' Select HUNTER peak 1,2,3,4, 5 or 0 to define :'
	read(*,*) ipk
	
	if(ipk .lt.0 ) then
	   ipk = ipmax
	   print *, ' Automatically choosing peak ',ipk
	end if
	
	if(ipk.eq.0)then
	   print *,' Enter t0,p,wday,dmag : '
	   read(*,*) t0, period, wday, dmag
	else
	   t0 = eppk(ipk)
	   period = ppk(ipk)
	   wday = wdpk(ipk)
	   dmag = dmpk(ipk)
	end if
	
3	k1 = 0.1d0
	sigt0 = 2d0 / 60d0 /24d0  
	sigdm = 0.002d0
	sigw = 0.003d0
	sigvk = 0.0d0
	sigvcosl = 0.0d0
	sigvsinl = 0.0d0
	sigms = ms0/10d0
	wfrac = wday / period
	sigwf = sigw / period
	hw1 = wfrac / 2d0
	hw2 = 1d0 - hw1
	
	if( ipk .gt. 0 ) then 
c  ***	Orthogonalise T0 and p by adjusting epoch of transit
c	to centre of mass of data taken in transit.
	   call orthtp(ndata,hjd,err,nvel,hjdrv,rverr,
     :	                  t0,sigt0,period,sigper,hw1,hw2)	
	else
	   sigt0 = 1d-3
	   sigper = 1d-6
	end if
	
c  ***	Calculate number of orbits elapsed to current epoch

	call idate(iday)
	id = iday(1)
	mm = iday(2)
	iyyy = iday(3)
	jdtrunc = julday(mm,id,iyyy) - 2450000
	
	hjdtoday = dfloat(jdtrunc)-0.5d0
	norbnow = nint((hjdtoday-t0)/period)

c  ***	Compute initial estimate of K1
	call orthk1(nvel,hjdrv,rv,rverr,t0,period,k1,sigvk)
c	k1 = 0.1
c	sigvk = 0.005

c  ***	Initialise counters

	k = 1
	ktemp = 0
	j = 1
	jtemp = 0
	scfac = .5d0
	
	b0 = 0.2d0
	sigb = 0.05d0

c  ***	Initialise eccentricity and arg of periastron
	ecosw0 = 0.001d0
	esinw0 = 0.001d0
	sigecosw = 0.002d0
	sigesinw = 0.002d0
	if(.not.fitecc)then
	   ecosw0 = 0d0
	   esinw0 = 0d0
	   sigecosw = 0d0
	   sigesinw = 0d0
	end if
	
c  ***	Initialise v sin i and projected obliquity lam
	vcosl0 = vsi	
	vsinl0 = 0d0
	sigvcosl = 0d0
	sigvsinl = 0d0
	if(fitross)then
c  ***	   Uncomment next two lines to fit R-M effect
	   sigvcosl = 0.1d0
	   sigvsinl = 0.1d0
	end if
	
c  ***	Uncomment next two lines to fit R-M effect
c	sigvcosl = 0.1d0
c	sigvsinl = 0.1d0
c  ***	If you get into trouble, uncomment some or all
c	of the following lines
c	sigt0 = 0d0 
c	sigper = 0d0
c	sigwf = 0d0
	
c  ***	Define initial parameter set
	t(k) = t0
	p(k) = period
	wf(k) = wfrac
	ms(k) = ms0
	d(k) = dmag
	vk(k) = k1
	vcosl(k) = vcosl0
	vsinl(k) = vsinl0
	b(k) = b0
	ecosw(k) = ecosw0
	esinw(k) = esinw0
	
c  ***	Convert proposal parameters to physical parameters

	call physparms(p(k),wf(k),ms(k),d(k),b(k),vk(k),ecosw(k),
     :	               esinw(k),rs(k),rh(k),aau(k),rstar(k),rp(k),
     :	               cosi,deginc(k),mpjup(k),rpjup(k),ecc(k),om(k),
     :	               vcosl(k),vsinl(k),vs(k),lam(k))
     
	
c  ***	Compute initial estimate of sec ecl depth assuming a
c	circular orbit and a linear ramp.
	do isset = 1, nsset
	   if(ichan(isset).eq.1)then
	      call orthse12(nirpt(isset),hjdsecl(firpt(isset)),
     :	               flxsecl(firpt(isset)),errsecl(firpt(isset)),
     :	               xpos(firpt(isset)),ypos(firpt(isset)),
     :	               t0,period,rp(k),rs(k),cosi,
     :	               vk(k),vs(k),lam(k),0d0,0d0,
     :	               sdp(isset),sigsdp(isset),
     :	               cx(isset),sigcx(isset),
     :	               cxx(isset),sigcxx(isset),
     :	               cy(isset),sigcy(isset),
     :	               cyy(isset),sigcyy(isset),
     :	               fs(isset))
	      sdp1(k) = sdp(isset)      
	      sigsdp1 = sigsdp(isset)      
	      cx1(k) = cx(isset)
	      sigcx1 = sigcx(isset)
	      cxx1(k) = cxx(isset)
	      sigcxx1 = sigcxx(isset)
	      cy1(k) = cy(isset)
	      sigcy1 = sigcy(isset)
	      cyy1(k) = cyy(isset)
	      sigcyy1 = sigcyy(isset)
	      fs1(k) = fs(isset)
	      
	   else if(ichan(isset).eq.2)then
	       call orthse12(nirpt(isset),hjdsecl(firpt(isset)),
     :	               flxsecl(firpt(isset)),errsecl(firpt(isset)),
     :	               xpos(firpt(isset)),ypos(firpt(isset)),
     :	               t0,period,rp(k),rs(k),cosi,
     : 		       vk(k),vs(k),lam(k),0d0,0d0,
     : 		       sdp(isset),sigsdp(isset),
     :	               cx(isset),sigcx(isset),
     :	               cxx(isset),sigcxx(isset),
     :	               cy(isset),sigcy(isset),
     :	               cyy(isset),sigcyy(isset),
     : 		       fs(isset))
	       sdp2(k) = sdp(isset)	 
	       sigsdp2 = sigsdp(isset)      
	       cx2(k) = sigcx(isset)
	       sigcx2 = sigcx(isset)
	       cxx2(k) = cxx(isset)
	       sigcxx2 = sigcxx(isset)
	       cy2(k) = cy(isset)
	       sigcy2 = sigcy(isset)
	       cyy2(k) = cyy(isset)
	       sigcyy2 = sigcyy(isset)
	       fs2(k) = fs(isset)
	       
	    else if(ichan(isset).eq.3)then
	       call orthse34(nirpt(isset),hjdsecl(firpt(isset)),
     :	               flxsecl(firpt(isset)),errsecl(firpt(isset)),t0,
     :	               period,rp(k),rs(k),cosi,
     :	               vk(k),vs(k),lam(k),0d0,0d0,
     :	               sdp(isset),sigsdp(isset),
     :	               rmp1(isset),sigrmp1(isset),
     :	               rmp2(isset),sigrmp2(isset),
     :	               fs(isset))
	      sdp3(k) = sdp(isset)      
	      sigsdp3 = sigsdp(isset)      
	      rmp31(k) = rmp1(isset)
	      sigrmp31 = sigrmp1(isset)
	      rmp32(k) = rmp2(isset)
	      sigrmp32 = sigrmp2(isset)
	      fs3(k) = fs(isset)
	      
	   else if(ichan(isset).eq.4)then
	      call orthse34(nirpt(isset),hjdsecl(firpt(isset)),
     :	               flxsecl(firpt(isset)),errsecl(firpt(isset)),t0,
     :	               period,rp(k),rs(k),cosi,
     :	               vk(k),vs(k),lam(k),0d0,0d0,
     :	               sdp(isset),sigsdp(isset),
     :	               rmp1(isset),sigrmp1(isset),
     :	               rmp2(isset),sigrmp2(isset),
     :	               fs(isset))
	      sdp4(k) = sdp(isset)      
	      sigsdp4 = sigsdp(isset)      
	      rmp41(k) = rmp1(isset)
	      sigrmp41 = sigrmp1(isset)
	      rmp42(k) = rmp2(isset)
	      sigrmp42 = sigrmp2(isset)
	      fs4(k) = fs(isset)
	   end if
	end do
	
c  ***	Generate model from parameters; fit to data and compute
c	penalty function chisq(k)

	call eval(t(k),p(k),ms(k),rs(k),rp(k),cosi,
     :	          vk(k),vs(k),gam(k),lam(k),rstar(k),ecc(k),om(k),
     :	          sdp1(k),cx1(k),cxx1(k),cy1(k),cyy1(k),fs1(k),
     :	          sdp2(k),cx2(k),cxx2(k),cy2(k),cyy2(k),fs2(k),
     :	          sdp3(k),rmp31(k),rmp32(k),fs3(k),
     :	          sdp4(k),rmp41(k),rmp42(k),fs4(k),
     :	          c1,c2,c3,c4,ms0,sigms,constrainmr,mu,nu,
     :	          ndata,npset,np,fobs,lobs,adj,hjd,mag,err,
     :	          nvel,nvset,fvel,lvel,gadj,hjdrv,rv,rverr,dvjit,
     :	          nsecl,nsset,nirpt,firpt,lirpt,ichan,
     :	          hjdsecl,flxsecl,errsecl,xpos,ypos,
     :	          chph(k),chsp(k),chse(k),chisq(k),varscl,varscv)

	if(nvset.gt.1)then
	   dgam(k) = gadj(2) - gadj(1)
	end if
	
c  ***	Adjust errors so that post-fit chisq ~ N -- commented out because
c	I want to defer this until we've got closer to the optimum solution; 
c	doing it here is premature and will lead to overestimation of the 
c	errors.

c	do pset = 1, npset
c	   do i = fobs(pset),lobs(pset)
c	      err(i) = err(i) * sqrt(varscl(pset))
c	   end do
c	   print *,pset,sqrt(varscl(pset))
c	end do

c  ***	Call eval again to readjust initial chisq
	call eval(t(k),p(k),ms(k),rs(k),rp(k),cosi,
     :	          vk(k),vs(k),gam(k),lam(k),rstar(k),ecc(k),om(k),
     :	          sdp1(k),cx1(k),cxx1(k),cy1(k),cyy1(k),fs1(k),
     :	          sdp2(k),cx2(k),cxx2(k),cy2(k),cyy2(k),fs2(k),
     :	          sdp3(k),rmp31(k),rmp32(k),fs3(k),
     :	          sdp4(k),rmp41(k),rmp42(k),fs4(k),
     :	          c1,c2,c3,c4,ms0,sigms,constrainmr,mu,nu,
     :	          ndata,npset,np,fobs,lobs,adj,hjd,mag,err,
     :	          nvel,nvset,fvel,lvel,gadj,hjdrv,rv,rverr,dvjit,
     :	          nsecl,nsset,nirpt,firpt,lirpt,ichan,
     :	          hjdsecl,flxsecl,errsecl,xpos,ypos,
     :	          chph(k),chsp(k),chse(k),chisq(k),varscl,varscv)

	if(nvset.gt.1)then
	   dgam(k) = gadj(2) - gadj(1)
	end if       

c  ***	Start of main Markov-chain Monte Carlo loop
	chsmin = chisq(k)

	strikes = 0	
2	jtemp = 0

c  ***	Take another 100 jumps
	do ktemp = 1, 100
	   jtry = 0
	   k = k + 1
	   
	   if( k.eq.400 ) then	   
c  ***	      By jump 400 we should be near enough to the optimum solution
c	      that it will be safe to scale the error bars on the data 
c	      and force post-fit chisq ~ N.

	      do pset = 1, npset
	 	 do i = fobs(pset),lobs(pset)
	 	    err(i) = err(i) * sqrt(varscl(pset))
	 	 end do
	 	 print *,pset,sqrt(varscl(pset))
	      end do

c  ***	      Call eval to rescale previous chisq
	      call eval(t(k-1),p(k-1),ms(k-1),rs(k-1),rp(k-1),cosi,
     :	       vk(k-1),vs(k-1),gam(k-1),lam(k-1),rstar(k-1),
     :	       ecc(k-1),om(k-1),
     :	       sdp1(k-1),cx1(k-1),cxx1(k-1),cy1(k-1),cyy1(k-1),fs1(k-1),
     :	       sdp2(k-1),cx2(k-1),cxx2(k-1),cy2(k-1),cyy2(k-1),fs2(k-1),
     :	       sdp3(k-1),rmp31(k-1),rmp32(k-1),fs3(k-1),
     :	       sdp4(k-1),rmp41(k-1),rmp42(k-1),fs4(k-1),
     :	       c1,c2,c3,c4,ms0,sigms,constrainmr,mu,nu,
     :	       ndata,npset,np,fobs,lobs,adj,hjd,mag,err,
     :	       nvel,nvset,fvel,lvel,gadj,hjdrv,rv,rverr,dvjit,
     :	       nsecl,nsset,nirpt,firpt,lirpt,ichan,
     :	       hjdsecl,flxsecl,errsecl,xpos,ypos,
     :	       chph(k-1),chsp(k-1),chse(k-1),chisq(k-1),varscl,varscv)
	   end if

c  ***	   Generate new proposal parameters	
1	   call propose(t(k-1),p(k-1),ms(k-1),wf(k-1),d(k-1),
     :	        vk(k-1),b(k-1),vcosl(k-1),vsinl(k-1),ecosw(k-1),esinw(k-1),
     :	        sdp1(k-1),cx1(k-1),cxx1(k-1),cy1(k-1),cyy1(k-1),fs1(k-1),
     :	        sdp2(k-1),cx2(k-1),cxx2(k-1),cy2(k-1),cyy2(k-1),fs2(k-1),
     :	        sdp3(k-1),rmp31(k-1),rmp32(k-1),fs3(k-1),
     :	        sdp4(k-1),rmp41(k-1),rmp42(k-1),fs4(k-1),
     :	        t(k),p(k),ms(k),wf(k),d(k),vk(k),b(k),vcosl(k),vsinl(k),
     :	        ecosw(k),esinw(k),
     :	        sdp1(k),cx1(k),cxx1(k),cy1(k),cyy1(k),fs1(k),
     :	        sdp2(k),cx2(k),cxx2(k),cy2(k),cyy2(k),fs2(k),
     :	        sdp3(k),rmp31(k),rmp32(k),fs3(k),
     :	        sdp4(k),rmp41(k),rmp42(k),fs4(k),
     :		sigt0,sigper,sigms,sigwf,sigdm,
     :	        sigvk,sigb,sigvcosl,sigvsinl,sigecosw,sigesinw,
     :	        sigsdp1,sigcx1,sigcxx1,sigcy1,sigcyy1,
     :	        sigsdp2,sigcx2,sigcxx2,sigcy2,sigcyy2,
     :	        sigsdp3,sigrmp31,sigrmp32,
     :	        sigsdp4,sigrmp41,sigrmp42,
     :	        scfac,idum)
     	   
	   jtemp = jtemp + 1
	   jtry = jtry + 1
	   
c	   Emergency restart in case algorithm gets stuck
	   if ( jtry .gt. 1000 ) then
	      print *,' Markov chain stuck at trial ',k 
	      print *, t(k),p(k),ms(k),wf(k),d(k),vk(k),b(k),
     :	               vcosl(k),vsinl(k),
     :	               ecosw(k),esinw(k)
	      print *, chph(k)/ndata,chsp(k)/nvel,chse(k)/nsecl,chisq(k)
	      do pset = 1, npset
	         print *, pset, lobs(pset)-fobs(pset)+1,varscl(pset)
	      end do
	      print *,' Backtracking to trial ',k - ktemp 
	      k = k - ktemp
	      strikes = strikes + 1
	      print *, t(k),p(k),ms(k),wf(k),d(k),vk(k),vs(k),b(k)
	      scfac = 0.4d0
	      if(strikes .ge. 3 ) then
	         print *, 'Failed 3 times; restarting'
		 go to 3
	      else
	         go to 2
	      end if
	   end if
	
c  ***	   Convert proposal parameters to physical parameters

	   call physparms(p(k),wf(k),ms(k),d(k),b(k),vk(k),ecosw(k),
     :	               esinw(k),rs(k),rh(k),aau(k),rstar(k),rp(k),
     :	               cosi,deginc(k),mpjup(k),rpjup(k),ecc(k),om(k),
     :	               vcosl(k),vsinl(k),vs(k),lam(k))
	
c	   print *,'finished physparms'
c  ***	   Generate model from parameters; fit to data and compute
c	   penalty function chisq(k)
	   call eval(t(k),p(k),ms(k),rs(k),rp(k),cosi,
     :	          vk(k),vs(k),gam(k),lam(k),rstar(k),ecc(k),om(k),
     :	          sdp1(k),cx1(k),cxx1(k),cy1(k),cyy1(k),fs1(k),
     :	          sdp2(k),cx2(k),cxx2(k),cy2(k),cyy2(k),fs2(k),
     :	          sdp3(k),rmp31(k),rmp32(k),fs3(k),
     :	          sdp4(k),rmp41(k),rmp42(k),fs4(k),
     :	          c1,c2,c3,c4,ms0,sigms,constrainmr,mu,nu,
     :	          ndata,npset,np,fobs,lobs,adj,hjd,mag,err,
     :	          nvel,nvset,fvel,lvel,gadj,hjdrv,rv,rverr,dvjit,
     :	          nsecl,nsset,nirpt,firpt,lirpt,ichan,
     :	          hjdsecl,flxsecl,errsecl,xpos,ypos,
     :	          chph(k),chsp(k),chse(k),chisq(k),varscl,varscv)

	   if(nvset.gt.1)then
	      dgam(k) = gadj(2) - gadj(1)
	   end if	

c  ***	   Save most probable proposal parameters
	   if ( chisq(k) .lt. chsmin ) then
	      t0best_mc = t(k)
	      pbest_mc = p(k)
	      msbest_mc = ms(k)
	      wfbest_mc = wf(k)
	      dbest_mc = d(k)
	      
	      vkbest_mc = vk(k)
	      gambest_mc = gam(k)
	      
	      vsbest_mc = vs(k)	      
	      lambest_mc = lam(k)
	      vcoslbest_mc = vcosl(k)
	      vsinlbest_mc = vsinl(k)
	      
	      bbest_mc = b(k)
	      
	      eccbest_mc = ecc(k)
	      ombest_mc = om(k)
	      ecoswbest_mc = ecosw(k)
	      esinwbest_mc = esinw(k)
	      
	      sdp1best_mc = sdp1(k)
	      cx1best_mc = cx1(k)
	      cxx1best_mc = cxx1(k)
	      cy1best_mc = cy1(k)
	      cyy1best_mc = cyy1(k)
	      fs1best_mc = fs1(k)
	      
	      sdp2best_mc = sdp2(k)
	      cx2best_mc = cx2(k)
	      cxx2best_mc = cxx2(k)
	      cy2best_mc = cy2(k)
	      cyy2best_mc = cyy2(k)
	      fs2best_mc = fs2(k)
	      
	      sdp3best_mc = sdp1(k)
	      rmp31best_mc = rmp31(k)
	      rmp32best_mc = rmp32(k)
	      fs3best_mc = fs3(k)
	      
	      sdp4best_mc = sdp2(k)
	      rmp41best_mc = rmp41(k)
	      rmp42best_mc = rmp42(k)
	      fs4best_mc = fs4(k)
	      
	      chpbst = chph(k)
	      chsbst = chsp(k)
	      chsmin = chisq(k)	   
c	   print *,k,chsp(k),chph(k),chisq(k),ecc(k)
	   end if
	   
c  ***	   Extrapolate transit epoch to late 2006
	   tnow(k) = t(k) + p(k)*dfloat(norbnow)

	   call methast(idum,chisq(k),chisq(k-1),reject)
c	   print *,'finished methast'
c	   print *,k,chisq(k),chisq(k-1)

	   if(reject) go to 1
	   
c	   print *,chph(k)/ndata,chsp(k)/nvel,chse(k)/nsecl,chisq(k),jtry
	   
	end do	   
	   
c  ***	Every 100 steps, perform housekeeping tasks.

c  ***	Adaptive step-size control 
	print *,' '
	print *,'Completed ',k,' of ',njump,' proposals.'
	
	print *,'Chisq (min)      = ', chsmin

	ratio = dfloat(ktemp) / dfloat(jtemp)
	print *,'Acceptance rate  = ',ratio

	oldscfac = scfac
	print *,'Old scale factor = ',oldscfac

	scfac = scfac * ratio * 4d0
	scfac = max(scfac,0.4d0)
	scfac = min(scfac,1.0d0)
	scfac = min(scfac,oldscfac*1.3d0)
	print *,'New scale factor = ', scfac

c  ***	Recompute statistics for proposal parameters after 
c	500th iteration
	kst = k - 99
	ken = k
	if ( kst .gt. 500 .and. kst .le. nburn) then
	   
	   print *,'calling newstats'
	   call newstats(njump,t,p,ms,wf,d,vk,b,vcosl,vsinl,ecosw,esinw,
     :	        kst,ken,
     :	        sigt0,sigper,sigms,sigwf,sigdm,
     :	        sigvk,sigb,sigvcosl,sigvsinl,sigecosw,sigesinw,radvel)
	end if
	   
	print *,sigt0,sigper,sigms,sigwf,sigdm,sigvk,sigb,sigvcosl,
     :    sigvsinl,sigecosw,sigesinw,chsp(k),chph(k),eccbest_mc,chisq(k)
	
c  ***	Are we done yet?	
	if(k .lt. njump) go to 2

c  ***	End of main Markov-chain Monte Carlo loop

c  ***	Report best-fit parameters. Use sorting to establish
c	confidence limits.
	call unlink('mcmc.out')
	open(unit=43,file='mcmc.out',form='formatted',status='new')
     	print *,' Best-fit parameter set: '
	
	call conflim(nburn,njump,t,chisq,idx,dxl,xml,dxh)
	sigma_t0 = ( dxl + dxh )/2d0
	print *,' Transit epoch      = ',xml,' +',dxh,' -',dxl
        write(43,*)' Transit epoch      = ',xml,' +',dxh,' -',dxl
	
	call conflim(nburn,njump,p,chisq,idx,dxl,xml,dxh)
	sigma_p = ( dxl + dxh )/2d0
	print *,' Orbital period     = ',xml,' +',dxh,' -',dxl,
     :      ' days'
        write(43,*)' Orbital period     = ',xml,' +',dxh,' -',dxl,
     :      ' days'

        call conflim(nburn,njump,d,chisq,idx,dxl,xml,dxh)
        sigma_d = ( dxl + dxh )/2d0
        print *,' Ratio (Rp/Rs)^2    = ',xml,' +',dxh,' -',dxl
        write(43,*)' Ratio (Rp/Rs)^2    = ',xml,' +',dxh,' -',dxl
	
	call conflim(nburn,njump,wf,chisq,idx,dxl,xml,dxh)
	dxl = dxl * pbest_mc
	xml = xml * pbest_mc
	dxh = dxh * pbest_mc
	sigma_w = ( dxl + dxh )/2d0
	print *,' Transit width      = ',xml,' +',dxh,' -',dxl,' days'
        write(43,*)' Transit width      = ',xml,' +',dxh,' -',dxl,
     :     ' days'
	
	call conflim(nburn,njump,b,chisq,idx,dxl,xml,dxh)
c	bbest_mc = xml
	print *,' Impact parameter   = ',xml,' +',dxh,' -',dxl,' rstar'
        write(43,*)' Impact parameter   = ',xml,' +',dxh,' -',dxl,
     :     ' rstar'
	
	vkbest_mc = k1
	vsbest_mc = max(vsi,1d0)
	lambest_mc = 0d0
	if ( nvel .ge. 2 ) then
	   print *, ' '
	   write(43,*) ' '
	   call conflim(nburn,njump,vk,chisq,idx,dxl,xml,dxh)	   
	   vkbest_mc = xml
	   print *,' Reflex velocity	= ',xml,' +',dxh,' -',dxl,
     :	           ' km/sec'
           write(43,*)' Reflex velocity = ',xml,' +',dxh,' -',dxl,
     :	           ' km/sec'
	   
	   call conflim(nburn,njump,gam,chisq,idx,dxl,xml,dxh)     
	   gambest_mc = xml
	   print *,' Gamma velocity	= ',xml,' +',dxh,' -',dxl,
     :	           ' km/sec'
           write(43,*)' Gamma velocity  = ',xml,' +',dxh,' -',dxl,
     :	           ' km/sec'

	   
	   call conflim(nburn,njump,ecc,chisq,idx,dxl,xml,dxh)     
	   print *,' Orb. eccentricity  = ',xml,' +',dxh,' -',dxl,'    '
           write(43,*)' Orb. eccentricity  = ',xml,' +',dxh,' -',dxl,
     :        '    '
	   
	   call conflim(nburn,njump,om,chisq,idx,dxl,xml,dxh)	   
	   dxl = dxl * 180d0 / pi
	   xml = xml * 180d0 / pi
	   dxh = dxh * 180d0 / pi
	   print *,' Arg. periastron om = ',xml,' +',dxh,' -',dxl,' deg'
           write(43,*)' Arg. periastron om = ',xml,' +',dxh,' -',dxl,
     :       ' deg'

	   
	   call conflim(nburn,njump,deginc,chisq,idx,dxl,xml,dxh)  
	   print *,' Orbital inclination= ',xml,' +',dxh,' -',dxl,' deg'
           write(43,*)' Orbital inclination= ',xml,' +',dxh,' -',dxl,
     :       ' deg'
	
	   call conflim(nburn,njump,vs,chisq,idx,dxl,xml,dxh)	   
	   vsbest_mc = xml
	   print *,' Rotation v sin  i  = ',xml,' +',dxh,' -',dxl,
     :	           ' km/sec'
           write(43,*)' Rotation v sin  i  = ',xml,' +',dxh,' -',dxl,
     :	           ' km/sec'

	   
	   call conflim(nburn,njump,lam,chisq,idx,dxl,xml,dxh)     
	   lambest_mc = xml
	   dxl = dxl * 180d0 / pi
	   xml = xml * 180d0 / pi
	   dxh = dxh * 180d0 / pi
	   print *,' Rossiter angle lam = ',xml,' +',dxh,' -',dxl,' deg'
           write(43,*)' Rossiter angle lam = ',xml,' +',dxh,' -',dxl,
     :        ' deg'
	end if
	
	call conflim(nburn,njump,tnow,chisq,idx,dxl,xml,dxh)
	print *,' Current epoch range= ',xml,' +',dxh,' -',dxl
        write(43,*)' Current epoch range= ',xml,' +',dxh,' -',dxl
	
	print *, ' '
	write(43,*) ' '
	call conflim(nburn,njump,rh,chisq,idx,dxl,xml,dxh)
	print *,' Stellar density    = ',xml,' +',dxh,' -',dxl,' rhsun'
        write(43,*)' Stellar density    = ',xml,' +',dxh,' -',dxl,
     :     ' rhsun'

	rhobest = xml
	
	print *, ' '
        write(43,*) ' '
	call conflim(nburn,njump,ms,chisq,idx,dxl,xml,dxh)
	print *,' Stellar mass       = ',xml,' +',dxh,' -',dxl,' M_sun'
        write(43,*)' Stellar mass       = ',xml,' +',dxh,' -',dxl,
     :     ' M_sun'
        sigma_mst = ( dxl + dxh )/2d0
	mstbest = xml
	
	call conflim(nburn,njump,rstar,chisq,idx,dxl,xml,dxh)
	print *,' Stellar radius     = ',xml,' +',dxh,' -',dxl,' R_sun'
        write(43,*)' Stellar radius     = ',xml,' +',dxh,' -',dxl,
     :     ' R_sun'
        sigma_rst = ( dxl + dxh )/2d0
	rstbest = xml
	
	print *, ' '
        write(43,*) ' '
	call conflim(nburn,njump,aau,chisq,idx,dxl,xml,dxh)
	print *,' Orbital separation = ',xml,' +',dxh,' -',dxl,' AU'
        write(43,*)' Orbital separation = ',xml,' +',dxh,' -',dxl,
     :    ' AU'
	
	call conflim(nburn,njump,deginc,chisq,idx,dxl,xml,dxh)	
	print *,' Orbital inclination= ',xml,' +',dxh,' -',dxl,' deg'
        write(43,*)' Orbital inclination= ',xml,' +',dxh,' -',dxl,
     :     ' deg'
	
	print *, ' '
        write(43,*) ' '
	call conflim(nburn,njump,rpjup,chisq,idx,dxl,xml,dxh)	
	print *,' Planet radius      = ',xml,' +',dxh,' -',dxl,' R_jup'
        write(43,*)' Planet radius      = ',xml,' +',dxh,' -',dxl,
     :     ' R_jup'
        sigma_rp = ( dxl + dxh )/2d0
	rpjbest = xml
	
	do isset = 1, nsset
	   if(ichan(isset).eq.1)then
	   call conflim(nburn,njump,fs1,chisq,idx,dxl,xml,dxh)	
	   print *,' IR band 1 flux fs1 = ',xml,' +',dxh,' -',dxl,' cts'
	   write(43,*)' IR band 1 flux fs1 = ',xml,' +',dxh,' -',dxl,
     :       ' cts'
	   fsbest(1) = xml
	   call conflim(nburn,njump,sdp1,chisq,idx,dxl,xml,dxh)	
	   print *,' IR band 1 fp1/fs1  = ',xml,' +',dxh,' -',dxl,' '
	   write(43,*)' IR band 1 fp1/fs1  = ',xml,' +',dxh,' -',dxl,' '
	   sdpbest(1) = xml
	   call conflim(nburn,njump,cx1,chisq,idx,dxl,xml,dxh)	
	   print *,' IR band 1 cx   = ',xml,' +',dxh,' -',dxl,' '
	   write(43,*)' IR band 1 cx   = ',xml,' +',dxh,' -',dxl,' '
	   cxbest(1) = xml
	   call conflim(nburn,njump,cxx1,chisq,idx,dxl,xml,dxh)	
	   print *,' IR band 1 cxx   = ',xml,' +',dxh,' -',dxl,' '
	   write(43,*)' IR band 1 cxx   = ',xml,' +',dxh,' -',dxl,' '
	   cxxbest(1) = xml
	   call conflim(nburn,njump,cy1,chisq,idx,dxl,xml,dxh)	
	   print *,' IR band 1 cy   = ',xml,' +',dxh,' -',dxl,' '
	   write(43,*)' IR band 1 cy   = ',xml,' +',dxh,' -',dxl,' '
	   cybest(1) = xml
	   call conflim(nburn,njump,cyy1,chisq,idx,dxl,xml,dxh)	
	   print *,' IR band 1 cyy   = ',xml,' +',dxh,' -',dxl,' '
	   write(43,*)' IR band 1 cyy   = ',xml,' +',dxh,' -',dxl,' '
	   cyybest(1) = xml

	   else if(ichan(isset).eq.2)then
	   call conflim(nburn,njump,fs2,chisq,idx,dxl,xml,dxh)	
	   print *,' IR band 2 flux fs2 = ',xml,' +',dxh,' -',dxl,' cts'
	   write(43,*)' IR band 2 flux fs2 = ',xml,' +',dxh,' -',dxl,
     :        ' cts'
	   fsbest(2) = xml
	   call conflim(nburn,njump,sdp2,chisq,idx,dxl,xml,dxh)	
	   print *,' IR band 2 fp2/fs2  = ',xml,' +',dxh,' -',dxl,' '
	   write(43,*)' IR band 2 fp2/fs2  = ',xml,' +',dxh,' -',dxl,' '
	   sdpbest(2) = xml
	   call conflim(nburn,njump,cx2,chisq,idx,dxl,xml,dxh)	
	   print *,' IR band 2 cx   = ',xml,' +',dxh,' -',dxl,' '
	   write(43,*)' IR band 2 cx   = ',xml,' +',dxh,' -',dxl,' '
	   cxbest(2) = xml
	   call conflim(nburn,njump,cxx2,chisq,idx,dxl,xml,dxh)	
	   print *,' IR band 2 cxx   = ',xml,' +',dxh,' -',dxl,' '
	   write(43,*)' IR band 2 cxx   = ',xml,' +',dxh,' -',dxl,' '
	   cxxbest(2) = xml
	   call conflim(nburn,njump,cy2,chisq,idx,dxl,xml,dxh)	
	   print *,' IR band 2 cy   = ',xml,' +',dxh,' -',dxl,' '
	   write(43,*)' IR band 2 cy   = ',xml,' +',dxh,' -',dxl,' '
	   cybest(2) = xml
	   call conflim(nburn,njump,cyy2,chisq,idx,dxl,xml,dxh)	
	   print *,' IR band 2 cyy   = ',xml,' +',dxh,' -',dxl,' '
	   write(43,*)' IR band 2 cyy   = ',xml,' +',dxh,' -',dxl,' '
	   cyybest(2) = xml
	   
	   else if(ichan(isset).eq.3)then
	   call conflim(nburn,njump,fs3,chisq,idx,dxl,xml,dxh)	
	   print *,' IR band 3 flux fs3 = ',xml,' +',dxh,' -',dxl,' cts'
	   write(43,*)' IR band 3 flux fs3 = ',xml,' +',dxh,' -',dxl,
     :        ' cts'
	   fsbest(3) = xml
	   call conflim(nburn,njump,sdp3,chisq,idx,dxl,xml,dxh)	
	   print *,' IR band 3 fp3/fs3  = ',xml,' +',dxh,' -',dxl,' '
	   write(43,*)' IR band 3 fp3/fs3  = ',xml,' +',dxh,' -',dxl,' '
	   sdpbest(3) = xml
	   call conflim(nburn,njump,rmp31,chisq,idx,dxl,xml,dxh)	
	   print *,' IR band 3 ramp 1   = ',xml,' +',dxh,' -',dxl,' '
	   write(43,*)' IR band 3 ramp 1   = ',xml,' +',dxh,' -',dxl,' '
	   rmp1best(3) = xml
	   call conflim(nburn,njump,rmp32,chisq,idx,dxl,xml,dxh)	
	   print *,' IR band 3 ramp 2   = ',xml,' +',dxh,' -',dxl,' '
	   write(43,*)' IR band 3 ramp 2   = ',xml,' +',dxh,' -',dxl,' '
	   rmp2best(3) = xml
	   
	   else if(ichan(isset).eq.4)then
	   call conflim(nburn,njump,fs4,chisq,idx,dxl,xml,dxh)	
	   print *,' IR band 4 flux fs4 = ',xml,' +',dxh,' -',dxl,' cts'
	   write(43,*)' IR band 4 flux fs4 = ',xml,' +',dxh,' -',dxl,
     :        ' cts'
	   fsbest(4) = xml
	   call conflim(nburn,njump,sdp4,chisq,idx,dxl,xml,dxh)	
	   print *,' IR band 4 fp4/fs4  = ',xml,' +',dxh,' -',dxl,' '
	   write(43,*)' IR band 4 fp4/fs4  = ',xml,' +',dxh,' -',dxl,' '
	   sdpbest(4) = xml
	   call conflim(nburn,njump,rmp41,chisq,idx,dxl,xml,dxh)	
	   print *,' IR band 4 ramp 1   = ',xml,' +',dxh,' -',dxl,' '
	   write(43,*)' IR band 4 ramp 1   = ',xml,' +',dxh,' -',dxl,' '
	   rmp1best(4) = xml
	   call conflim(nburn,njump,rmp42,chisq,idx,dxl,xml,dxh)	
	   print *,' IR band 4 ramp 2   = ',xml,' +',dxh,' -',dxl,' '
	   write(43,*)' IR band 4 ramp 2   = ',xml,' +',dxh,' -',dxl,' '
	   rmp2best(4) = xml
	   
	   end if
	end do


	call conflim(nburn,njump,chisq,chph,idx,dxl,xml,dxh)
	print *,' chisq at best chph = ',xml
	write(43,*)' chisq at best chph = ',xml
	call conflim(nburn,njump,chph,chph,idx,dxl,xml,dxh)
	print *,' Best chph = ',xml
	write(43,*)' Best chph = ',xml

	if(nvel.ge.2)then
	   call conflim(nburn,njump,mpjup,chisq,idx,dxl,xml,dxh)   
	   print *,' Planet mass	= ',xml,' +',dxh,' -',dxl,
     :	           ' M_jup'
           write(43,*)' Planet mass     = ',xml,' +',dxh,' -',dxl,
     :	           ' M_jup'
	   call conflim(nburn,njump,chisq,chsp,idx,dxl,xml,dxh)
	   print *,' chisq at best chsp = ',xml
           write(43,*)' chisq at best chsp = ',xml
	   call conflim(nburn,njump,chsp,chsp,idx,dxl,xml,dxh)
	   print *,' Best chsp = ',xml
           write(43,*)' Best chsp = ',xml
	end if
	
	if(nvset.gt.1)then
	   call conflim(nburn,njump,dgam,chisq,idx,dxl,xml,dxh)
	   print *,' RV zero-point diff = ',xml,' +',dxh,' -',dxl,' m/s'
           write(43,*)' RV zero-point diff = ',xml,' +',dxh,' -',dxl,
     :        ' m/s'
	end if
	
	print *, ' '
        write(43,*) ' '
	print *,' chisq_phot         = ',chpbst
        write(43,*)' chisq_phot         = ',chpbst
	print *,' nphot              = ',ndata
        write(43,*)' nphot              = ',ndata

	if(nvel.ge.2)then	
	print *, ' '
        write(43,*) ' '
	print *,' chisq_spec         = ',chsbst
        write(43,*)' chisq_spec         = ',chsbst
	print *,' nvel               = ',nvel
        write(43,*)' nvel               = ',nvel
	end if
	
	print *,' chsmin             = ',chsmin
        write(43,*)' chsmin             = ',chsmin
	
c  ***	Compute other secondary quantities of interest

	do k = 1, njump
c	   Planet/star flux ratio for geometric albedo p=1:
	   rpsq(k) = rp(k)*rp(k)
	   
c  ***	   Compute planet/star mass ratio 
	   qrat = mpjup(k) / ms(k) / 1047.52d0

c	   Synchronisation timescale for primary (Zahn 1977)
	   tau_s(k) = 1.2 / qrat / qrat / rs(k)**6

c	   Synchronisation timescale for planet (Zahn 1977)
	   tau_p(k) = 1.2 * qrat * qrat / rp(k)**6

c	   Planet surface gravity in jovian units
	   gacc_p = mpjup(k) / rpjup(k) / rpjup(k)
	   
c	   Log planet surface gravity (cgs)
	   logg_p(k) = log10 ( gacc_p * 2288d0 )

c	   Stellar surface gravity in solar units
	   gacc_s = ms(k) / rstar(k) / rstar(k)
	   
c	   Log stellar surface gravity (cgs)
	   logg_s(k) = log10 ( gacc_s * 27400d0 )

c	   Planet density in jovian units
	   rho_p(k) = gacc_p / rpjup(k)
	   
c  ***	   Use DW's optimised FGK relations
c	   Teff in kelvin	
c	   tjh = -4369.5d0*jh + 7188.2d0
	   teql_p(k) = tjh * sqrt(rs(k)/2d0)

c  ***	   True anomaly at mid secondary eclipse
	   nutrans = pi/2d0 - om(k)
	   nuocclt = nutrans + pi
	   
c  ***	   Get corresponding mean anomaly
	   mtrans = meananom(nutrans,ecc(k))
	   mocclt = meananom(nuocclt,ecc(k))
	
c  ***	   Get phase of mid-secondary eclipse	
	   phsec(k) = mod((mocclt-mtrans)/2d0/pi,1d0)
	   if( phsec(k) .lt. 0d0 ) phsec(k) = phsec(k) + 1d0

c  ***     get eclipse time now
	   enow(k)=t(k)+(dfloat(norbnow)+phsec(k))*p(k)

c *** 	   Get flat part of transit and from that T1-T2  time
	   prat=sqrt(d(k))
	   tf=sqrt( ((1d0-prat)**2-b(k)*b(k))/((1d0+prat)**2-b(k)*b(k)) )
	   t1_t2(k)=wf(k)*p(k)*((1d0-tf))/2.d0
	   
c  ***	   Get impact parameter at secondary eclipse
	   eccfactor = (1d0+esinw(k))/(1d0-esinw(k)) 
	   bocc = b(k) * eccfactor
	    

c ***	   Get secondary eclipse duration in days
	   ewf(k)=wf(k)*p(k)*eccfactor
     :	    *sqrt( ((1d0+prat)**2-bocc*bocc)/((1d0+prat)**2-b(k)*b(k)) )
	   
c *** 	   Get flat part of eclipse and from that T1-T2  time
	   tf=sqrt( ((1d0-prat)**2-bocc*bocc)/((1d0+prat)**2-bocc*bocc) )	   
	   et1_t2(k)=ewf(k)*((1d0-tf))/2.d0

c  ***	   Compute relative tidal heating rate
	   tide(k) =  ms(k)**(2.5d0) * rpjup(k)**5d0  
     :	              * (aau(k)/0.05d0)**(-7.5d0) 
     :	              * (ecc(k)/0.05d0)**2d0
     :	              * 1.97380d24

c ***	   Compute roche radius in Rsun (Eggleton 1983)
           mpl=mpjup(k)/1047.52d0
	   qth=(mpl/ms(k))**(1.d0/3.d0)
	   roche(k)=aau(k)*2092.51385d0*
     :             0.49d0*qth*qth/(0.6d0*qth*qth+log(1.d0+qth)) 
	   rocherat(k)=roche(k)/rpjup(k)


c ***	   Total angular momemtum (Levrard 2009)
	   GMsun=1.32725d20
	   Istar=0.06d0*ms(k)*(rstar(k)*6.9599d8)**2d0
	   Ipl=0.06d0*mpl*(rpjup(k)*6.9599d8)**2d0	! wrong coeff for planet

	   ltot(k)=Istar * vs(k)/(rstar(k)*6.9599d5)/sin(deginc(k)*pi/180d0) + 
     :          ms(k) * mpl * sqrt(GMsun/(ms(k)+mpl)) *
     :          sqrt( aau(k) * 1.49598d11 * (1-ecc(k)*ecc(k)) )
	   lcrit=4d0 * GMsun**(1d0/2d0) * ( (ms(k)*mpl)**3d0 * (Istar+Ipl) /
     :              (ms(k)+mpl) / 27d0 )**(1d0/4d0)
	   lrat(k)=ltot(k)/lcrit
	   ltot(k)=1.9892e30*ltot(k)		! SI units

c *** spiral in timescale in Gyr.  Adopt Qs=1e9.  Divide by 1e9 to put in Gyr
           if(fitecc)then
	   tspiral(k)=p(k)/(2d0*pi*48d0*365.256363d0)*
     :                (aau(k)*215.094177d0/rstar(k))**5d0*(ms(k)/mpl)
	   endif

c  ***	   Compute irradiating power
	   irrad(k) =  4d0 * pi * (rpjup(k)*71.492d6)**2d0 
     :	               * 5.67d-8 * teql_p(k)**4d0
	end do
	
	print *, ' '
        write(43,*) ' '
	call conflim(nburn,njump,rpsq,chisq,idx,dxl,xml,dxh)
	print *,' Opp flux ratio(p=1)= ',xml,' +',dxh,' -',dxl,' '
        write(43,*)' Opp flux ratio(p=1)= ',xml,' +',dxh,' -',dxl,' '
	call conflim(nburn,njump,logg_s,chisq,idx,dxl,xml,dxh)
	print *,' Log g(star) [cgs]  = ',xml,' +',dxh,' -',dxl,' '
        write(43,*)' Log g(star) [cgs]  = ',xml,' +',dxh,' -',dxl,' '
	call conflim(nburn,njump,logg_p,chisq,idx,dxl,xml,dxh)
	print *,' Log g(planet) [cgs]= ',xml,' +',dxh,' -',dxl,' '
        write(43,*)' Log g(planet) [cgs]= ',xml,' +',dxh,' -',dxl,' '
	call conflim(nburn,njump,rho_p,chisq,idx,dxl,xml,dxh)
	print *,' Planet density     = ',xml,' +',dxh,' -',dxl,' rho_J'
        write(43,*)' Planet density     = ',xml,' +',dxh,' -',dxl,
     :     ' rho_J'
	call conflim(nburn,njump,tide,chisq,idx,dxl,xml,dxh)
	print *,' Tidal heating rate = ',xml,' +',dxh,' -',dxl,' '
        write(43,*)' Tidal heating rate = ',xml,' +',dxh,' -',dxl,' '
	call conflim(nburn,njump,irrad,chisq,idx,dxl,xml,dxh)
	print *,' Irradiating power  = ',xml,' +',dxh,' -',dxl,' '
        write(43,*)' Irradiating power  = ',xml,' +',dxh,' -',dxl,' '
	call conflim(nburn,njump,rocherat,chisq,idx,dxl,xml,dxh)
	print *,' R_roche Planet/Rp  = ',xml,' +',dxh,' -',dxl,' '
        write(43,*)' R_roche Planet/Rp = ',xml,' +',dxh,' -',dxl,' '
	call conflim(nburn,njump,roche,chisq,idx,dxl,xml,dxh)
	print *,' Rroche Planet(R_J) = ',xml,' +',dxh,' -',dxl,' '
        write(43,*)' Rroche Planet(R_J) = ',xml,' +',dxh,' -',dxl,' '

	call conflim(nburn,njump,ltot,chisq,idx,dxl,xml,dxh)
	print *,' Ltot [SI]          = ',xml,' +',dxh,' -',dxl,' '
        write(43,*)' Ltot [SI]          = ',xml,' +',dxh,' -',dxl,' '
	call conflim(nburn,njump,lrat,chisq,idx,dxl,xml,dxh)
	print *,' Ltot/Lcrit         = ',xml,' +',dxh,' -',dxl,' '
        write(43,*)' Ltot/Lcrit         = ',xml,' +',dxh,' -',dxl,' '
        if (fitecc)then
	call conflim(nburn,njump,tspiral,chisq,idx,dxl,xml,dxh)
	print *,' Spiral-in Time     = ',xml,' +',dxh,' -',dxl,
     :          ' Gyr'
        write(43,*)' Spiral-in Time     = ',xml,' +',dxh,' -',dxl,
     :          ' Gyr'
        endif

	if (xml.gt.1.0) then
	call conflim(nburn,njump,tau_s,chisq,idx,dxl,xml,dxh)
	print *,' tsync (star)       = ',xml,' +',dxh,' -',dxl,' y'
        write(43,*)' tsync (star)       = ',xml,' +',dxh,' -',dxl,' y'
	call conflim(nburn,njump,tau_p,chisq,idx,dxl,xml,dxh)
	print *,' tsync (planet)     = ',xml,' +',dxh,' -',dxl,' y'
        write(43,*)' tsync (planet)     = ',xml,' +',dxh,' -',dxl,' y'
	endif

	call conflim(nburn,njump,teql_p,chisq,idx,dxl,xml,dxh)
	print *,' Stellar Teff(J-H)  = ',tjh,' K'
        write(43,*)' Stellar Teff(J-H)  = ',tjh,' K'
	print *,' Planet Teql (A=0)  = ',xml,' +',dxh,' -',dxl,' K'
        write(43,*)' Planet Teql (A=0)  = ',xml,' +',dxh,' -',dxl,' K'
	print *, ' '
        write(43,*) '  '

	call conflim(nburn,njump,phsec,chisq,idx,dxl,xml,dxh)
	print *,' Phase(sec eclipse) = ',xml,' +',dxh,' -',dxl
        write(43,*)' Phase(sec eclipse) = ',xml,' +',dxh,' -',dxl
	call conflim(nburn,njump,ewf,chisq,idx,dxl,xml,dxh)
	print *,' Duration(sec ecl)  = ',xml,' +',dxh,' -',dxl,
     :          '  days'
        write(43,*)' Duration(sec ecl)  = ',xml,' +',dxh,' -',dxl,
     :          '  days'
	call conflim(nburn,njump,enow,chisq,idx,dxl,xml,dxh)
	print *,' Current epoch eclipse = ',xml,' +',dxh,' -',dxl
        write(43,*)' Current epoch eclipse = ',xml,' +',dxh,' -',dxl

	call conflim(nburn,njump,t1_t2,chisq,idx,dxl,xml,dxh)
	print *,' T1_T2 duration     = ',xml,' +',dxh,' -',dxl,' days '
        write(43,*)' T1_T2 duration     = ',xml,' +',dxh,' -',dxl,
     :          '  days'
	call conflim(nburn,njump,et1_t2,chisq,idx,dxl,xml,dxh)
	print *,' T1_T2 dur (ecl)    = ',xml,' +',dxh,' -',dxl,
     :          ' days '
        write(43,*)' T1_T2 dur (ecl)    = ',xml,' +',dxh,' -',dxl,
     :          '  days'

	close(43)
	
c  ***	The key results for planet-selection purposes are:

c	(1) the probability prp that the planet has a radius
c	    less than 1.5 Rjup, with constrainmr = .true.
c	    "Good" candidates should have prp > 0.5
c
c	(2) the probability prs that the star has a radius
c	    less than 1.2 ms^0.8, with constrainmr = .true.
c	    "Good" candidates should have prp > 0.1
c
c	(3) the difference between the best photometric chisquared
c	    (chpbst) obtained for a run with constrainmr = .true
c	    and that obtained with constrainmr = .false.
c	    "Good" candidates should have a difference in chpbst
c	    of no more than 5 or so.
c
c	(4) the one-sigma error in the fitted period and transit
c	    width. False positives will tend to have less 
c	    well-constrained epochs, periods and transit widths
c	    than objects showing genuine boxy transits.
c
c  ***	Compute the probabilities for (1) and (2) above and write to
c	a one-line summary file bestparms.dat
	call probs(njump,rpjup,rstar,ms,b,prp,prs,prb)

c  ***	Report stellar and planetary parameters

	call unlink('output.csv')
	open(unit=43,file='output.csv',form='formatted',status='new')
	do k = nburn+1, njump

	   write(43,10)t(k),p(k),ms(k),wf(k),d(k),vk(k),vs(k),gam(k),
     :	               rstar(k),rpjup(k),mpjup(k),b(k),
     :	               chph(k),chsp(k),chisq(k),tnow(k),
     :	               vcosl(k),vsinl(k),ecosw(k),esinw(k)
10	   format(f11.6,1x,f10.7,3(1x,f7.5),3(1x,f12.5),
     :	           4(1x,f12.5),3(1x,f12.4),5(1x,f11.6))
	   
	   
c	   write(43,10)t(k),p(k),gam(k),vk(k),vs(k),
c     :	       cosi(k)/rs(k),rss(k),rpj(k),mpj(k),chisq(k),chsp(k)
c10	   format(f9.4,1x,f9.7,1x,f9.4,1x,f6.4,1x,f6.3,1x,f10.6,1x,f6.3,
c     :	          f6.4,1x,f5.3,1x,f5.3,1x,f5.3,1x,f10.3,1x,f10.3)
	end do
	close(43)
	
	call unlink('ch4.csv')
	
	call conflim(nburn,njump,t,chisq,idx,dxl,t0best,dxh) 
	call conflim(nburn,njump,p,chisq,idx,dxl,pbest,dxh)  
	call conflim(nburn,njump,rp,chisq,idx,dxl,rpbest,dxh)	     
	call conflim(nburn,njump,rs,chisq,idx,dxl,rsbest,dxh)	     
	call conflim(nburn,njump,b,chisq,idx,dxl,bbest,dxh)  
	lambest = 0d0
	vkbest = k1
	vsbest = max(vsi,1d0)
	gambest = 0d0
	if(nvel.ge.2)then
	   call conflim(nburn,njump,vk,chisq,idx,dxl,vkbest,dxh)	
	   call conflim(nburn,njump,gam,chisq,idx,dxl,gambest,dxh)	
	   call conflim(nburn,njump,ecc,chisq,idx,dxl,eccbest,dxh)	
	   call conflim(nburn,njump,om,chisq,idx,dxl,ombest,dxh)	
	   call conflim(nburn,njump,vs,chisq,idx,dxl,vsbest,dxh)	
	   call conflim(nburn,njump,lam,chisq,idx,dxl,lambest,dxh)	
	end if

	pr = rpbest / rsbest
	
	sinl = sin(lambest)
	cosl = cos(lambest)
	print *,'lambest, sinl, cosl = ', sinl, cosl
	
	nmod = 1000
	
c  ***	Compute date of periastron relative to transit time t0	
	nutrans = pi/2d0 - ombest
	mtrans = meananom(nutrans,eccbest)
	dttrans = mtrans/2d0/pi * pbest
	tperi = t0best - dttrans
	
c	Eccentric anomaly at transit 
        tanhalfecc = tan(nutrans/2.)*sqrt((1.-eccbest)/(1.+eccbest))
	eccanom = 2.*atan(tanhalfecc)
	
c  ***	Hence get instantaneous planet-star distance 
c	at transit in units of major semi-axis:
	rtrans = 1. - eccbest*cos(eccanom)

c	Derive inclination from impact parameter, stellar radius
c	and orbital separation

c	cosi = bbest*rsbest*rtrans
	cosi = bbest*rsbest/rtrans
	sini = sqrt ( 1d0 - cosi*cosi )
	
	do ipset = 1, npset
	
	   do i = 1, nmod
	      ph = dfloat(i-1)/dfloat(nmod-1) + 0.5d0
	      jd = t0best + ph * pbest
	      
c  ***	      Coordinates of planet in orbital plane ( a = 1 )
              call kepler(jd,tperi,pbest,
     :	 		 eccbest,ombest,vkbest,sini,
     :	 		 numod(i),rau(i),alpha(i),xp(i),yp(i))
c  ***	      Impact parameter in units of primary radius
	      if ( cos(alpha(i)) .gt. 0d0 ) then
c	      if ( alpha(i) .gt. pi/2d0 ) then
	 	 zmod(i) = rau(i) * sin(alpha(i)) / rsbest
	      else
	 	 zmod(i) = 1d0 / rsbest
	      end if
	      
	      numod(i) = numod(i) + gambest
	      
	   end do
	   
c  ***	   Compute model flux
	   call occultsmall(pr,c1(ipset),c2(ipset),c3(ipset),c4(ipset),
     :	 		     nmod,zmod,mumod)
	   
	   if(nvel.ge.2)then	      
c  ***	      Compute model RV curve. 
	      do i = 1, nmod
	      
c  ***	 	 Rotate through 90-i about X axis to get zp
	 	 zp = - yp(i) * cosi
	 	      
c  ***	         Partial transit: ratio of impact parameter of centroid 
c	         of eclipsed part of star to impact parameter of planet's 
c	         centre (linear approximation)
	         if ( zmod(i).gt. 1d0-pr .and. zmod(i).lt. 1d0+pr ) then
		    zscfac = ( 1d0 + (zmod(i)-1d0-pr ) / 2d0 ) / zmod(i)
	         else
	            zscfac = 1d0
	         end if  
		  
c  ***	 	 Rotate through lambda about y axis to get up. Don't
c	         let velocity exceed limb value during partial phases.

	 	 up = xp(i) * cosl - zp * sinl 

c  ***	 	 Code Rossiter effect here. Impact param is z(i),
c	 	 up is x coord in stellar rotational coordinate 
c	 	 system, flux diminution is 1 - mu(i)

	 	 numod(i) = numod(i) - vsbest * ( 1d0 - mumod(i) )
     :	 		   / mumod(i) * up * zscfac / rsbest		 
     
c  ***	         Fudge based on a mistake made earlier which improved the fit
c	         for reasons unknown   
c	 	 numod(i) = numod(i) - vsbest * ( 1d0 - mumod(i) )
c     :	 		   / mumod(i) * up / rsbest 
c     :	                   / min(zscfac*zmod(i),zmod(i))		 	           

	      end do
	   end if
	   
	   write(modfile,'(a6,i2.2,a4)')'pltmod',ipset,'.dat'
	   call unlink(modfile)
	   open(unit=45,file=modfile,form='formatted',status='new')	   
	   do i = 1, nmod
	      ph = dfloat(i-1)/dfloat(nmod-1) + 0.5d0
	      write(45,*) ph, -2.5d0*log10(mumod(i)), numod(i)
	   end do  
	   close(45)
	   
	end do   
	
	do isset = 1, nsset
	
	   do i = 1, nmod
	      ph = dfloat(i-1)/dfloat(nmod-1) 
	      jd = t0best + ph * pbest
	      
c  ***	      Coordinates of planet in orbital plane ( a = 1 )
              call kepler(jd,tperi,pbest,
     :	 		 eccbest,ombest,vkbest,sini,
     :	 		 numod(i),rau(i),alpha(i),xp(i),yp(i))
c  ***	      Impact parameter in units of primary radius
	      if ( cos(alpha(i)) .lt. 0d0 ) then
	 	 zmod(i) = rau(i) * sin(alpha(i)) / rsbest
	      else
	 	 zmod(i) = 1d0 / rsbest
	      end if
	      
	      numod(i) = numod(i) + gambest
	      
	   end do
	   
c  ***	   Compute product of secondary flux and visibility
	   call secvis(pr,nmod,zmod,etamod)
	   
	   
	   write(modfile,'(a7,i2.2,a4)')'pltsmod',ichan(isset),'.dat'
	   call unlink(modfile)
	   open(unit=49,file=modfile,form='formatted',status='new')	   
	   do i = 1, nmod
	      ph = dfloat(i-1)/dfloat(nmod-1) 
	      write(49,*) ph, zmod(i), etamod(i),
     :	                  1d0 + etamod(i)*sdpbest(ichan(isset))
	   end do  
	   close(49)
	   
	end do
	
c  ***	Compute transit model
	print *,t0best,pbest,rpbest,rsbest,
     :	              cosi,vkbest,vsbest,lambest
	call transmod(t0best,pbest,rpbest,rsbest,
     :	              cosi,vkbest,vsbest,lambest,eccbest,ombest,
     :		      npset,np,c1,c2,c3,c4,ndata,hjd,z,mu,
     :	              nvel,hjdrv,vz,nu)

	do pset = 1, npset
	   write(modfile,'(a7,i2.2,a4)')'pltphot',pset,'.dat'
	   call unlink(modfile)
	   open(unit=46,file=modfile,form='formatted',status='new')
	   do i = fobs(pset),lobs(pset)
c  ***	      Determine orbital phase
	      ph = mod(hjd(i)-t0best,pbest)/pbest
	      if ( ph.lt.-0.5d0 ) ph = ph + 2d0
	      if ( ph.lt.0.5d0 ) ph = ph + 1d0
	      write(46,*) ph, mag(i)-adj(pset), err(i), 
     :	                  mag(i)-adj(pset)-mu(i),hjd(i)
	   end do
	   close(46)
	end do
	
	if(nvel.ge.2)then
	call unlink('pltrvel.dat')
	open(unit=47,file='pltrvel.dat',form='formatted',status='new')
	do vset = 1, nvset
	   do i = fvel(vset), lvel(vset)
c  ***	      Determine orbital phase
	      ph = mod(hjdrv(i)-t0best,pbest)/pbest
	      if ( ph.lt.-0.5d0 ) ph = ph + 2d0
	      if ( ph.lt.0.5d0 ) ph = ph + 1d0
c	      if ( ph.gt.1.5d0 ) ph = ph - 1d0
	      write(47,*) hjdrv(i),ph,rv(i)+gadj(1)-gadj(vset),rverr(i),
     :	                              rv(i)-gadj(vset)-nu(i)
	   end do
	end do
	close(47)
	end if
	
	
	if(nsset.ge.1)then
	   call secmod(t0best,pbest,rpbest,rsbest,cosi,vkbest,vsbest,
     :	               lambest,eccbest,ombest,
     :	               nsecl,hjdsecl,zsecl,eta)
	   do isset = 1, nsset
	      write(modfile,'(a7,i2.2,a4)')'pltsecl',ichan(isset),'.dat'
	      call unlink(modfile)
	      open(unit=48,file=modfile,form='formatted',status='new')
	      su = 0d0
	      sv = 0d0
	      sw = 0d0
	      do i = firpt(isset),lirpt(isset)
	         wsc = 1d0 / errsecl(i) / errsecl(i)
		 tsc = log(hjdsecl(i)-hjdsecl(firpt(isset))+1d0/48d0) 
	         su = su + xpos(i)*wsc
	         sv = sv + ypos(i)*wsc
	         sw = sw + wsc
c	         print *,w,spw,sw
 	      end do
	      uhat = su / sw
	      vhat = sv / sw
	      
	      do i = firpt(isset),lirpt(isset)
c  ***	         Determine orbital phase
	         ph = mod(hjdsecl(i)-t0best,pbest)/pbest
	         if ( ph.lt.0d0 ) ph = ph + 1d0
	         psc = 1d0 + sdpbest(ichan(isset))*eta(i)
		 
	         if(ichan(isset).eq.1 .or. ichan(isset).eq.2)then		 
	            xsc = flxsecl(i) / fsbest(ichan(isset)) /
     :		       ( 1d0 
     :		       + cxbest(ichan(isset))*(xpos(i)-uhat) 
     :		       + cxxbest(ichan(isset))*(xpos(i)-uhat)**2 
     :		       + cybest(ichan(isset))*(ypos(i)-vhat)
     :		       + cyybest(ichan(isset))*(ypos(i)-vhat)**2 
     :	                  )
     	         else if(ichan(isset).eq.3 .or. ichan(isset).eq.4)then
		    tsc = log(hjdsecl(i)-hjdsecl(firpt(isset))+1d0/48d0) 
	            xsc = flxsecl(i) / fsbest(ichan(isset)) / 
     :		       ( 1d0 + rmp1best(ichan(isset))*tsc 
     :                       + rmp2best(ichan(isset))*tsc*tsc)

		 end if
		 rsc = xsc - psc

		 write(48,*) hjdsecl(i),ph,xsc, 
     :	                     errsecl(i)/fsbest(ichan(isset)),rsc
	      end do
	   end do
	   close(48)
	end if
		
c  ***	Switch on main-sequence mass-radius constraint
	constrainmr = .true.

c  ***	Load the first eleven successful steps into the amoeba array		
	do k = 1, 11
c  ***	   Use logarithmic scale for non-negative parameters,
c	   and a tanh-like function to restrict impact 
c	   parameter to 0 < b < 1 
	   xfunk(1) = t(k) 
	   xfunk(2) = p(k) 
	   xfunk(3) = log(wf(k))
	   xfunk(4) = log(ms(k))
	   xfunk(5) = log(d(k))
	   xfunk(6) = log(vk(k))
	   xfunk(7) = log(vs(k))
	   xfunk(8) = log( 1d0/(1-b(k))-1d0 )
	   xfunk(9) = log( 2d0/(1-lam(k)/pi)-1d0 )
	   xfunk(10) = ecosw(k)
	   xfunk(11) = esinw(k)
	   
	   do j = 1, 11
	      parms(k,j) = xfunk(j)
	   end do
	   
	   yval(k) = funk(xfunk)	
	end do

c  ***	In the twelfth slot, put the best-fitting set of parameters
c	from the Markov chain

	k = 12
	xfunk(1) = t0best_mc 
	xfunk(2) = pbest_mc 
	xfunk(3) = log(wfbest_mc)
	xfunk(4) = log(msbest_mc)
	xfunk(5) = log(dbest_mc)
	xfunk(6) = log(vkbest_mc)
	xfunk(7) = log(vsbest_mc)
	xfunk(8) = log( 1d0/(1-bbest_mc)-1d0 )
	xfunk(9) = log( 2d0/(1-lambest_mc/pi)-1d0 )
	xfunk(10) = ecoswbest_mc
	xfunk(11) = esinwbest_mc
	
	do j = 1, 11
	   parms(k,j) = xfunk(j)
	end do
	
	yval(k) = funk(xfunk)	     
	

c  ***	Find best solution with main-sequence constraint
	ftol = 1d-5
	call amoeba(parms,yval,12,11,11,ftol,funk,iter)
	call indexx(12,yval,indy)
	kbest = indy(1)
	print *, 'Chisq = ',yval(kbest),' after ',iter,' amoeba steps'
	chisq_con = yval(kbest)

c  ***	Convert proposal parameters to physical parameters

	do j = 1, 11
	   xfunk(j) = parms(kbest,j)
	end do
	t0best = xfunk(1)
	pbest = xfunk(2)
	wfbest = exp(xfunk(3))
	mstbest = exp(xfunk(4))
	dbest = exp(xfunk(5))
	vkbest = exp(xfunk(6))
	vsbest = exp(xfunk(7))
	bbest = 1d0 - 1d0/(1d0 + exp(xfunk(8)))
	lambest = pi * ( 1d0 - 2d0/(1d0 + exp(xfunk(9))))
	ecoswbest = xfunk(10)
	esinwbest = xfunk(11)
	call physparms(pbest,wfbest,mstbest,
     :	               dbest,bbest,vkbest,ecoswbest,esinwbest,
     :	               rsbest,rhobest,aaubest,rstbest,rpbest,
     :	               cosi,dincbest,mpjbest,rpjbest,eccbest,ombest,
     :	               vcoslbest,vsinlbest,vsbest,lambest)

	call eval(t0best,pbest,mstbest,rsbest,rpbest,cosi,
     :	          vkbest,vsbest,gambest,lambest,rstbest,
     :	          eccbest,ombest,
     :	          sdp1best_mc,cx1best_mc,cxx1best_mc,
     :	          cy1best_mc,cyy1best_mc,fs1best_mc,
     :	          sdp2best_mc,cx2best_mc,cxx2best_mc,
     :	          cy2best_mc,cyy2best_mc,fs2best_mc,
     :	          sdp3best_mc,rmp31best_mc,rmp32best_mc,fs3best_mc,
     :	          sdp4best_mc,rmp41best_mc,rmp42best_mc,fs4best_mc,
     :	          c1,c2,c3,c4,ms0,sigms,constrainmr,mu,nu,
     :	          ndata,npset,np,fobs,lobs,adj,hjd,mag,err,
     :	          nvel,nvset,fvel,lvel,
     :	                gadj,hjdrv,rv,rverr,dvjit,
     :	          nsecl,nsset,nirpt,firpt,lirpt,ichan,
     :	          hjdsecl,flxsecl,errsecl,xpos,ypos,
     :	          chphbest,chspbest,chsebest,chisqbest,varscl,varscv)

c     	chisq_con = chphbest + chspbest
	chisq_con = chisqbest
	dchisq_mr = chisqbest - chphbest - chspbest - chsebest

c  ***	Switch off main-sequence mass-radius constraint
	constrainmr = .false.

c  ***	Load the first twelve successful steps into the amoeba array		
	do k = 1, 12
	
	   xfunk(1) = t(k) 
	   xfunk(2) = p(k) 
	   xfunk(3) = log(wf(k))
	   xfunk(4) = log(ms(k))
	   xfunk(5) = log(d(k))
	   xfunk(6) = log(vk(k))
	   xfunk(7) = log(vs(k))
	   xfunk(8) = log( 1d0/(1-b(k))-1d0 )
	   xfunk(9) = log( 2d0/(1-lam(k)/pi)-1d0 )
	   xfunk(10) = ecosw(k)
	   xfunk(11) = esinw(k)
	
	   do j = 1, 11
	      parms(k,j) = xfunk(j)
	   end do
	   	   
	   yval(k) = funk(xfunk)	
	end do	

c  ***	Find best solution without main-sequence constraint
	constrainmr = .false.
	ftol = 1d-5
	call amoeba(parms,yval,12,11,11,ftol,funk,iter)
	call indexx(12,yval,indy)
	kbest = indy(1)
	print *, 'Chisq = ',yval(kbest),' after ',iter,' amoeba steps'
	chisq_unc = yval(kbest)
	
	print *,prp*prs
	print *,chisq_con-chisq_unc
	print *,sqrt ( sigma_p*sigma_w )
	if ( prp*prs .gt. 0.3d0 
     :	     .and. abs( chisq_con-chisq_unc ) .lt. 10d0 ) then
	   print *,swaspid,' is a good candidate. '
	end if

	call unlink('bestparms.dat')
	open(unit=49,file='bestparms.dat',form='formatted',status='new')

c  ***	Report values for hunter peak 1 if ephemeris was entered 
c	manually using ipk = 0, to avoid segfaults
	ipk = max(ipk,1)
	
        write(49,20)swaspid,rpjbest,bbest,rstbest,mstbest,prp,prs,
     :  chisq_con,chisq_unc,sigma_t0,sigma_p,sigma_w,
     :  snrpk(ipk),ntrpk(ipk),aellpk(ipk),
     :  snellpk(ipk),transfpk(ipk),gaprpk(ipk),dchsantpk(ipk),
     :  prb,t0best,pbest,wfbest,-dbest,sigma_d,sigma_rp,sigma_rst,
     :  sigma_mst,jh,ipk,dchisq_mr
20      format(a26,1x,6(f8.4,1x),2(f11.4,1x),3(f11.6,1x),
     :         f11.4,1x,i4,1x,5(f11.6,1x),f8.4,1x,1x,f11.6,1x,
     :         f9.7,2(1x,f7.5),4(1x,f11.6),1x,f8.1,i4,1x,f11.4)

	print *,swaspid,rpjbest,bbest,rstbest,mstbest,prp,prs,
     :	vsbest,lambest,chisq_con,chisq_unc,sigma_t0,sigma_p,sigma_w

	close(49)
	
c  ***	--------------------------------------------------------------	
c  ***	Everything from here on in is expendable for a cut-down planet
c	selection code, though the output files could be used to construct
c	the graphics for a "wanted" poster.	

	mcorr = mxcorr
	call correl(nburn,njump,t,mcorr,c_t,cl_t)
	print *,' Correlation length for t0     = ',cl_t
	call correl(nburn,njump,p,mcorr,c_p,cl_p)
	print *,' Correlation length for period = ',cl_p
	call correl(nburn,njump,d,mcorr,c_d,cl_d)
	print *,' Correlation length for depth  = ',cl_d
	call correl(nburn,njump,wf,mcorr,c_w,cl_w)
	print *,' Correlation length for wf     = ',cl_w
	call correl(nburn,njump,ms,mcorr,c_ms,cl_ms)
	print *,' Correlation length for ms     = ',cl_ms
	call correl(nburn,njump,b,mcorr,c_b,cl_b)
	print *,' Correlation length for b      = ',cl_b
	if(nvel.gt.0)then
	   call correl(nburn,njump,vk,mcorr,c_vk,cl_vk)
	   print *,' Correlation length for vk     = ',cl_vk
	   call correl(nburn,njump,ecc,mcorr,c_ecc,cl_ecc)
	   print *,' Correlation length for ecc    = ',cl_ecc
	   call correl(nburn,njump,om,mcorr,c_om,cl_om)
	   print *,' Correlation length for om     = ',cl_om
c	   call correl(nburn,njump,vs,mcorr,c_vs,cl_vs)
c	   print *,' Correlation length for vs     = ',cl_vs
c	   call correl(nburn,njump,lam,mcorr,c_lam,cl_lam)
c	   print *,' Correlation length for lam    = ',cl_lam
	end if
		
	print *, ' '
	print *,' j = ',j , '    k = ', k
	print *,' All done'	
	
	end

C+	----------------------------------------------------------------
C+
	subroutine newstats(njump,t,p,ms,wf,d,vk,b,vcosl,vsinl,ecosw,esinw,
     :	      kst,ken,
     :	      sigt0,sigper,sigms,sigwf,sigdm,
     :	      sigvk,sigb,sigvcosl,sigvsinl,sigecosw,sigesinw,radvel)

	implicit none
	     
c  ***	Subroutine parameters
     	integer njump
	double precision t(njump)
	double precision p(njump)
	double precision ms(njump)
	double precision wf(njump)
	double precision d(njump)
	double precision vk(njump)
	double precision b(njump)
	double precision vcosl(njump)
	double precision vsinl(njump)
	double precision ecosw(njump)
	double precision esinw(njump)
	integer kst, ken
	double precision sigt0,sigper,sigms,sigwf,sigdm
	double precision sigvk,sigb,sigvcosl,sigvsinl,sigecosw,sigesinw
	logical radvel
	
c  ***	Local variables
	integer i
	integer nx
	double precision st,sp,sm,sw,sd,sk,sv,sb,sl
	double precision stt,spp,smm,sww,sdd,skk,svv,sbb,sll

c  ***	Need to initialise the accumulators in the calling routine; 
c	this allows us to increment them using just the most recent
c	set of values.
	
	nx = ken - kst + 1
	
	st = 0
	stt = 0
	do i = kst,ken
	   st = st + t(i)
	   stt = stt + t(i)*t(i)
	end do
	if ( stt - st*st/dfloat(nx) .gt. 1d-30 ) then
	   sigt0 = sqrt((stt - st*st/dfloat(nx))/dfloat(nx-1))
	end if
	
	sp = 0
	spp = 0
	do i = kst,ken
	   sp = sp + p(i)
	   spp = spp + p(i)*p(i)
	end do
	if ( spp - sp*sp/dfloat(nx) .gt. 1d-30 ) then
	   sigper = sqrt((spp - sp*sp/dfloat(nx))/dfloat(nx-1))
	end if
	
c	sm = 0
c	smm = 0
c	do i = kst,ken
c	   sm = sm + ms(i)
c	   smm = smm + ms(i)*ms(i)
c	end do
c	if ( smm - sm*sm/dfloat(nx) .gt. 1d-30 ) then
c	   sigms = sqrt((smm - sm*sm/dfloat(nx))/dfloat(nx-1))
c	end if
	
	sw = 0
	sww = 0
	do i = kst,ken
	   sw = sw + wf(i)
	   sww = sww + wf(i)*wf(i)
	end do
	if ( sww - sw*sw/dfloat(nx) .gt. 1d-30 ) then
	   sigwf = sqrt((sww - sw*sw/dfloat(nx))/dfloat(nx-1))
	end if
	
	sd = 0
	sdd = 0
	do i = kst,ken
	   sd = sd + d(i)
	   sdd = sdd + d(i)*d(i)
	end do
	if ( sdd - sd*sd/dfloat(nx) .gt. 1d-30 ) then
	   sigdm = sqrt((sdd - sd*sd/dfloat(nx))/dfloat(nx-1))
	end if
	
	if ( radvel ) then
	   sk = 0
	   skk = 0
	   do i = kst,ken
	      sk = sk + vk(i)
	      skk = skk + vk(i)*vk(i)
	   end do
	   if ( skk - sk*sk/dfloat(nx) .gt. 1d-30 ) then
	      sigvk = sqrt((skk - sk*sk/dfloat(nx))/dfloat(nx-1))
	   end if

	   sv = 0
	   svv = 0
	   do i = kst,ken
	      sv = sv + vcosl(i)
	      svv = svv + vcosl(i)*vcosl(i)
	   end do
	   if ( svv - sv*sv/dfloat(nx) .gt. 1d-30 ) then
	      sigvcosl = sqrt((svv - sv*sv/dfloat(nx))/dfloat(nx-1))
	   end if
	   
	   sl = 0
	   sll = 0
	   do i = kst,ken
	      sl = sl + vsinl(i)
	      sll = sll + vsinl(i)*vsinl(i)
	   end do
	   if ( sll - sl*sl/dfloat(nx) .gt. 1d-30 ) then
	      sigvsinl = sqrt((sll - sl*sl/dfloat(nx))/dfloat(nx-1))
	   end if
	   
	   sl = 0
	   sll = 0
	   do i = kst,ken
	      sl = sl + ecosw(i)
	      sll = sll + ecosw(i)*ecosw(i)
	   end do
	   if ( sll - sl*sl/dfloat(nx) .gt. 1d-30 ) then
	      sigecosw = sqrt((sll - sl*sl/dfloat(nx))/dfloat(nx-1))
	   end if
	   
	   sl = 0
	   sll = 0
	   do i = kst,ken
	      sl = sl + esinw(i)
	      sll = sll + esinw(i)*esinw(i)
	   end do
	   if ( sll - sl*sl/dfloat(nx) .gt. 1d-30 ) then
	      sigesinw = sqrt((sll - sl*sl/dfloat(nx))/dfloat(nx-1))
	   end if
	   
	end if

c	sb = 0
c	sbb = 0
c	do i = kst,ken
c	   sb = sb + b(i)
c	   sbb = sbb + b(i)*b(i)
c	end do
c	if ( sbb - sb*sb/dfloat(nx) .gt. 1d-30 ) then
c	   sigb = sqrt((sbb - sb*sb/dfloat(nx))/dfloat(nx-1))
c	   Don't let sigb grow too big
c	   sigb = min(sigb,0.05)
c	end if
	
     	end
C+	----------------------------------------------------------------
	subroutine propose(told,pold,msold,wfold,dold,vkold,
     :	        bold,vcoslold,vsinlold,ecwold,eswold,
     :	        sdp1old,cx1old,cxx1old,cy1old,cyy1old,fs1old,
     :	        sdp2old,cx2old,cxx2old,cy2old,cyy2old,fs2old,
     :	        sdp3old,rmp31old,rmp32old,fs3old,
     :	        sdp4old,rmp41old,rmp42old,fs4old,
     :	        t, p, ms, wf, d, vk, b,vcosl,vsinl,
     :	        ecosw,esinw,
     :	        sdp1,cx1,cxx1,cy1,cyy1,fs1,
     :	        sdp2,cx2,cxx2,cy2,cyy2,fs2,
     :	        sdp3,rmp31,rmp32,fs3,
     :	        sdp4,rmp41,rmp42,fs4,
     :	        sigt0,sigper,sigms,sigwf,sigdm,
     :	        sigvk,sigb,sigvcosl,sigvsinl,sigecosw,sigesinw,
     :	        sigsdp1,sigcx1,sigcxx1,sigcy1,sigcyy1,
     :	        sigsdp2,sigcx2,sigcxx2,sigcy2,sigcyy2,
     :	        sigsdp3,sigrmp31,sigrmp32,
     :	        sigsdp4,sigrmp41,sigrmp42,
     :	        scfac,idum)

 	implicit none
	    
c  *** 	Subroutine parameters
 	real*8 told,pold,msold,wfold,dold,vkold,bold,vcoslold,vsinlold
	real*8 ecwold,eswold
	real*8 t, p, ms, wf, d, vk, b, vcosl, vsinl, ecosw, esinw
	real*8 sdp1old,cx1old,cxx1old,cy1old,cyy1old,fs1old
	real*8 sdp2old,cx2old,cxx2old,cy2old,cyy2old,fs2old
	real*8 sdp3old,rmp31old,rmp32old,fs3old
	real*8 sdp4old,rmp41old,rmp42old,fs4old
	real*8 sigt0,sigper,sigms,sigwf,sigdm,sigvk,sigb
	real*8 sigvcosl,sigvsinl,sigecosw,sigesinw
	real*8 sdp1,cx1,cxx1,cy1,cyy1,fs1
	real*8 sdp2,cx2,cxx2,cy2,cyy2,fs2
	real*8 sdp3,rmp31,rmp32,fs3
	real*8 sdp4,rmp41,rmp42,fs4
	real*8 sigsdp1,sigcx1,sigcxx1,sigcy1,sigcyy1
	real*8 sigsdp2,sigcx2,sigcxx2,sigcy2,sigcyy2
	real*8 sigsdp3,sigrmp31,sigrmp32
	real*8 sigsdp4,sigrmp41,sigrmp42
	real*8 scfac
	integer idum
	double precision pi
		
	real gasdev
	    
	pi = 4d0*atan2(1d0,1d0)
	
 	t = told + sigt0 * dble(gasdev(idum)) * scfac
	p = pold + sigper * dble(gasdev(idum)) * scfac
1	ms = msold + sigms * dble(gasdev(idum)) * scfac
	if ( ms .lt. 0d0 ) go to 1
2	wf = wfold + sigwf * dble(gasdev(idum)) * scfac
	if ( wf .lt. 0d0 ) go to 2
3	d = dold + sigdm * dble(gasdev(idum)) * scfac
	if ( d .lt. 0d0 ) go to 3
4	vk = vkold + sigvk * dble(gasdev(idum)) * scfac
	if ( vk .lt. 0d0 ) go to 4
5	b = bold + sigb * dble(gasdev(idum)) * scfac
	if ( b .lt. 0d0 .or. b.ge.1d0 ) go to 5
	vcosl = vcoslold + sigvcosl * dble(gasdev(idum)) * scfac
	vsinl = vsinlold + sigvsinl * dble(gasdev(idum)) * scfac
	
6	sdp1 = sdp1old + sigsdp1 * dble(gasdev(idum)) * scfac
	if ( sdp1 .lt. 0d0 ) go to 6
	cx1 = cx1old + sigcx1 * dble(gasdev(idum)) * scfac
	cxx1 = cxx1old + sigcxx1 * dble(gasdev(idum)) * scfac
	cy1 = cy1old + sigcy1 * dble(gasdev(idum)) * scfac
	cyy1 = cyy1old + sigcyy1 * dble(gasdev(idum)) * scfac
	
7	sdp2 = sdp2old + sigsdp2 * dble(gasdev(idum)) * scfac
	if ( sdp2 .lt. 0d0 ) go to 7
	cx2 = cx2old + sigcx2 * dble(gasdev(idum)) * scfac
	cxx2 = cxx2old + sigcxx2 * dble(gasdev(idum)) * scfac
	cy2 = cy2old + sigcy2 * dble(gasdev(idum)) * scfac
	cyy2 = cyy2old + sigcyy2 * dble(gasdev(idum)) * scfac
	
8	sdp3 = sdp3old + sigsdp3 * dble(gasdev(idum)) * scfac
	if ( sdp3 .lt. 0d0 ) go to 8
	rmp31 = rmp31old + sigrmp31 * dble(gasdev(idum)) * scfac
	rmp32 = rmp32old + sigrmp32 * dble(gasdev(idum)) * scfac

9	sdp4 = sdp4old + sigsdp4 * dble(gasdev(idum)) * scfac
	if ( sdp4 .lt. 0d0 ) go to 9	
	rmp41 = rmp41old + sigrmp41 * dble(gasdev(idum)) * scfac
	rmp42 = rmp42old + sigrmp42 * dble(gasdev(idum)) * scfac

10	ecosw = ecwold + sigecosw * dble(gasdev(idum)) * scfac
	esinw = eswold + sigesinw * dble(gasdev(idum)) * scfac
	if ( ecosw*ecosw + esinw*esinw .ge. 1d0 ) go to 10

     	end
C+	----------------------------------------------------------------
C+
	subroutine physparms(p,wf,ms,d,b,vk,ecosw,esinw,
     :	                     rs,rh,aau,rstar,rp,cosi,deginc,
     :	                     mpjup,rpjup,ecc,om,vcosl,vsinl,vs,lam)

	implicit none
		
c  ***	Subroutine parameters
	double precision p,wf,ms,d,b,vk
	double precision ecosw,esinw,ecc,om
	double precision vcosl,vsinl,vs,lam
	double precision rs,rh,aau,rstar,rp,cosi,deginc,mpjup,rpjup

c  ***	Local variables
	double precision prat,pyr
	double precision akm,sini,psec,vt,q
	double precision nutrans,tanhalfecc,eccanom,rtrans
	double precision pi
	
	pi = 4d0*atan2(1d0,1d0)
	
c  	rp/rs for grazing transit ...
	prat = ((b-1d0)+sqrt((1d0-b)**2+8d0*d))/2d0
c  	...or for complete transit
	prat = max(prat,sqrt(d))
	
c	Compute v sin i and obliquity
	
	vs = sqrt(vcosl*vcosl + vsinl*vsinl)
	lam = atan2(vsinl,vcosl)
	
c  	Compute rs using eclipse width from 1st to 4th contact
	ecc = sqrt(ecosw*ecosw + esinw*esinw)
	om = atan2(esinw,ecosw)
c	print *,ecosw,esinw,ecc,om

	rs = pi * wf / sqrt((1d0+prat)**2 - b*b)
     :	     * ( 1d0 + esinw ) / sqrt ( 1d0 - ecc*ecc )

c	Compute density in solar units from rs and period
	rh = 0.0134063d0 / rs**3 / p**2
	
c  ***	Compute mass-dependent physical parameters
	pyr = p/365.256363d0
	aau = (pyr*pyr*ms)**(1d0/3d0)
	
c  ***	Stellar radius in solar units
	rstar = rs * aau * 215.094177d0
	
c  ***	Compute planet radius from transit depth
	rp = rs * prat
	
c  ***	True anomaly at time of transit	
	nutrans = pi/2d0 - om
	
c	Eccentric anomaly at transit 
        tanhalfecc = tan(nutrans/2.)*sqrt((1.-ecc)/(1.+ecc))
	eccanom = 2.*atan(tanhalfecc)
	
c  ***	Hence get instantaneous planet-star distance 
c	at transit in units of major semi-axis:
	rtrans = 1. - ecc*cos(eccanom)

c	Derive inclination from impact parameter, stellar radius
c	and orbital separation

c	cosi = b * rs * rtrans
	cosi = b * rs / rtrans
	sini = sqrt ( 1d0 - cosi*cosi )

	deginc = acos(cosi) * 180d0 / pi
	akm = aau * 149598000d0
	psec = p * 86400d0
	vt = 2d0 * pi * akm / psec * sini
	q = vk / ( vt - vk )

c  ***	Compute planet mass in jovian units
	mpjup = q * ms * 1047.52d0
	rpjup = rp*aau*2092.51385d0

	end
CC+	----------------------------------------------------------------

	subroutine orthk1(nvel,hjdrv,rv,err,t0,period,k1,sigvk)
	
	implicit none
	
c  ***	Subroutine parameters
	integer nvel
	real*8 hjdrv(nvel)
	real*8 rv(nvel)
	real*8 err(nvel)
	real*8 t0, period
	real*8 k1, sigvk
	
c  ***	Local variables
	real*8 sx, su, sw
	integer i
	real*8 w, x, u
	real*8 phi, sphi
	real*8 xhat, uhat
	real*8 sxu, suu
	real*8 pi
	
	pi = 4d0*atan2(1d0,1d0)
	
	if(nvel .ge. 2)then
	
c	    Orthogonalise data and sine function, 
c	    adding 50 m/sec additional error to region around transit
	    sx = 0d0
	    su = 0d0
	    sw = 0d0
	    do i = 1, nvel
	       phi = mod( hjdrv(i)-t0, period ) / period
	       if ( phi .gt. 0.05 .and. phi .lt. 0.95 ) then
	          w = 1d0 / err(i) / err(i)
	       else
	          w = 1d0 / ( err(i) * err(i) + .05*.05 )
	       end if
	       sphi = sin( 2d0 * pi * phi )
	       sx = sx + rv(i) * w
	       su = su + sphi * w
	       sw = sw + w
	    end do
	    xhat = sx / sw
	    uhat = su / sw

c	    Optimal scaling of sine function to fit data,	    
c	    adding 50 m/sec additional error to region around transit
	    sxu = 0d0
	    suu = 0d0
	    do i = 1, nvel
	       phi = mod( hjdrv(i)-t0, period ) / period
	       if ( phi .gt. 0.05 .and. phi .lt. 0.95 ) then
	          w = 1d0 / err(i) / err(i)
	       else
	          w = 1d0 / ( err(i) * err(i) + .05*.05 )
	       end if
	       sphi = sin( 2d0 * pi * phi )
	       x = rv(i) - xhat
	       u = sphi - uhat
	       sxu = sxu + x * u * w
	       suu = suu + u * u * w
	    end do
	    k1 = - sxu / suu
	    sigvk = sqrt(1d0 / suu)
	else
	   k1 = 0.1d0
	   sigvk = 0d0
	end if
	
	print *,'k1    = ',k1
	print *,'sigvk = ',sigvk
	
	end	


C+	----------------------------------------------------------------

	subroutine orthse34(n,hjd,flx,err,t0,period,rp,rs,cosi,
     :	                  vk,vsi,lam,ecc,om,
     :	                  sdp,sigsdp,rmp1,sigrmp1,rmp2,sigrmp2,xhat)
	

c  ***	Perform a very approximate least-squares fit to ramp function
c	for Spitzer channels 3 and 4, ignoring higher-order cross-terms.
c	Return estimates of fitting parameters and their uncertainties.

	implicit none
	
c  ***	Subroutine parameters
	integer n
	real*8 hjd(n)
	real*8 flx(n)
	real*8 err(n)
	real*8 t0, period, rp, rs, cosi, vk,vsi,lam,ecc,om
	real*8 z(n)
	real*8 eta(n)
	real*8 sdp, sigsdp
	real*8 rmp1, sigrmp1
	real*8 rmp2, sigrmp2
	
c  ***	Local variables
	real*8 sx, sp, st, sw, sl
	integer i
	real*8 w, x, p, t, l
	real*8 xhat, phat, that, lhat, qhat
	real*8 sxp, spp
	real*8 d, chsdp

c  ***	Compute secondary eclipse model	

	print *,t0,period,rp,rs,cosi,vk,vsi,lam,ecc,om

	call secmod(t0,period,rp,rs,cosi,vk,vsi,lam,ecc,om,
     :	                  n,hjd,z,eta)

c	Orthogonalise data and model. Use logarithmic time interval
c	in days since half an hour before the first data point.
	sx = 0d0
	sp = 0d0
	st = 0d0
	sl = 0d0
	sw = 0d0
	do i = 1, n
	   w = 1d0 / err(i) / err(i)
	   sx = sx + flx(i) * w
	   st = st + hjd(i) * w
	   sl = sl + log(hjd(i)-hjd(1)+1d0/48d0) * w
	   sw = sw + w
	end do
	xhat = sx / sw
	that = st / sw
	lhat = sl / sw

c	Fit linear ramp term in log time
	sxp = 0d0
	spp = 0d0
	do i = 1, n
	   w = 1d0 / err(i) / err(i)
	   x = flx(i) - xhat
	   p = log(hjd(i)-hjd(1)+1d0/48d0) - lhat
	   sxp = sxp + x * p * w
	   spp = spp + p * p * w
	end do
	rmp1 = sxp / spp
	sigrmp1 = sqrt(1d0 / spp)

c	Orthogonalise data with linear ramp subtracted and secondary
c	eclipse model
	sx = 0d0
	sp = 0d0
	sw = 0d0
	do i = 1, n
	   w = 1d0 / err(i) / err(i)
	   x = flx(i) - rmp1 * (log(hjd(i)-hjd(1)+1d0/48d0) - lhat)
	   p = eta(i)
	   sx = sx + x * w
	   sp = sp + p * w
	   sw = sw + w
	end do
	xhat = sx / sw
	phat = sp / sw


c	Optimal scaling of sec ecl profile to fit data
	sxp = 0d0
	spp = 0d0
	do i = 1, n
	   w = 1d0 / err(i) / err(i)
	   t = hjd(i) - that
	   x = flx(i) - xhat 
     :	              - rmp1 * (log(hjd(i)-hjd(1)+1d0/48d0) - lhat)
	   p = eta(i) - phat
	   sxp = sxp + x * p * w
	   spp = spp + p * p * w
	end do
	sdp = sxp / spp
	sigsdp = sqrt(1d0 / spp)
	
c	Orthogonalise residuals and model quadratic ramp
	sx = 0d0
	sp = 0d0
	sw = 0d0
	do i = 1, n
	   w = 1d0 / err(i) / err(i)
	   x = flx(i) - rmp1 * (log(hjd(i)-hjd(1)+1d0/48d0) - lhat)
     :	              - sdp * (eta(i) - phat)
	   p = (log(hjd(i)-hjd(1)+1d0/48d0) - lhat)**2
	   sx = sx + x * w
	   sp = sp + p * w
	   sw = sw + w
	end do
	xhat = sx / sw
	qhat = sp / sw
	
c	Optimal scaling of quadratic ramp term to fit data
	sxp = 0d0
	spp = 0d0
	do i = 1, n
	   w = 1d0 / err(i) / err(i)
	   t = hjd(i) - that
	   x = flx(i) - rmp1 * (log(hjd(i)-hjd(1)+1d0/48d0) - lhat)
     :	              - sdp * (eta(i) - phat)
     :	              - xhat
	   p = (log(hjd(i)-hjd(1)+1d0/48d0) - lhat)**2
     :	              - qhat	  
	   sxp = sxp + x * p * w
	   spp = spp + p * p * w
	end do
	rmp2 = sxp / spp
	sigrmp2 = sqrt(1d0 / spp)
	
	chsdp = 0d0
	do i = 1, n
	   w = 1d0 / err(i) / err(i)
	   t = hjd(i) - that
	   x = flx(i) - rmp1 * (log(hjd(i)-hjd(1)+1d0/48d0) - lhat)
     :	              - rmp2 * ((log(hjd(i)-hjd(1)+1d0/48d0) - lhat)**2 - qhat)
     :	              - xhat
	   p = eta(i) - phat
	   d = x - sdp * p 
	   chsdp = chsdp + d * d * w 
c	   print *,mod(hjd(i)-t0,period)/period,flx(i)-xhat,x,sdp*p,d
c	   Tested graphically. Linear ramp is being removed correctly.
	end do
	
	sdp = sdp / xhat
	sigsdp = sigsdp / xhat
	rmp1 = rmp1 / xhat
	sigrmp1 = sigrmp1 / xhat
	rmp2 = rmp2 / xhat
	sigrmp2 = sigrmp2 / xhat
		
	print *,'sdp     = ',sdp 
	print *,'sigsdp  = ',sigsdp 
	print *,'rmp1    = ',rmp1 
	print *,'sigrmp1 = ',sigrmp1 
	print *,'rmp2    = ',rmp2 
	print *,'sigrmp2 = ',sigrmp2 
	print *,'fs      = ',xhat
	print *,'chsdp   = ',chsdp
	print *,'n       = ',n
	
c  ***	Rescale error bars 
c	do i = 1, n
c	   err(i) = err (i) * sqrt(chsdp/dfloat(n))
c	end do
	
	end	


C+	----------------------------------------------------------------

	subroutine orthse12(n,hjd,flx,err,xpix,ypix,t0,period,rp,rs,cosi,
     :	                  vk,vsi,lam,ecc,om,
     :	                  sdp,sigsdp,cx,sigcx,cxx,sigcxx,
     :	                  cy,sigcy,cyy,sigcyy,xhat)
	

c  ***	Perform a very approximate least-squares fit to intra-pixel
c	sensitivity profile for Spitzer channels 1 and 2, ignoring 
c	higher-order cross-terms.
c	Return estimates of fitting parameters and their uncertainties.

	implicit none
	
c  ***	Subroutine parameters
	integer n
	real*8 hjd(n)
	real*8 flx(n)
	real*8 err(n)
	real*8 xpix(n)
	real*8 ypix(n)
	real*8 t0, period, rp, rs, cosi, vk,vsi,lam,ecc,om
	real*8 z(n)
	real*8 eta(n)
	real*8 sdp, sigsdp
	real*8 cx, sigcx
	real*8 cxx, sigcxx
	real*8 cy, sigcy
	real*8 cyy, sigcyy
	
c  ***	Local variables
	real*8 sx, sp, su, sv, sw
	integer i
	real*8 w, x, p, u, v
	real*8 xhat, phat, uhat, vhat, qhat, rhat
	real*8 sxp, spp, sxu, suu, sxv, svv
	real*8 d, chsdp

c  ***	Compute secondary eclipse model	

	print *,t0,period,rp,rs,cosi,vk,vsi,lam,ecc,om

	call secmod(t0,period,rp,rs,cosi,vk,vsi,lam,ecc,om,
     :	                  n,hjd,z,eta)

c	Orthogonalise data and model. Use logarithmic time interval
c	in days since half an hour before the first data point.
	sx = 0d0
	sp = 0d0
	su = 0d0
	sv = 0d0
	sw = 0d0
	do i = 1, n
	   w = 1d0 / err(i) / err(i)
	   sx = sx + flx(i) * w
	   su = su + xpix(i) * w
	   sv = sv + ypix(i) * w
	   sw = sw + w
	end do
	xhat = sx / sw
	uhat = su / sw
	vhat = sv / sw

c	Fit linear ramp term in log time
	sxp = 0d0
	spp = 0d0
	do i = 1, n
	   w = 1d0 / err(i) / err(i)
	   x = flx(i) - xhat
	   u = xpix(i) - uhat
	   v = ypix(i) - vhat
	   sxu = sxu + x * u * w
	   suu = suu + u * u * w
	   sxv = sxv + x * v * w
	   svv = svv + v * v * w
	end do
	cx = sxu / suu
	sigcx = sqrt(1d0 / suu)
	cy = sxv / svv
	sigcy = sqrt(1d0 / svv)

c	Orthogonalise data with linear ramp subtracted and secondary
c	eclipse model
	sx = 0d0
	sp = 0d0
	sw = 0d0
	do i = 1, n
	   w = 1d0 / err(i) / err(i)
	   x = flx(i) - cx*(xpix(i)-uhat) - cy*(ypix(i)-vhat)
	   p = eta(i)
	   sx = sx + x * w
	   sp = sp + p * w
	   sw = sw + w
	end do
	xhat = sx / sw
	phat = sp / sw


c	Optimal scaling of sec ecl profile to fit data
	sxp = 0d0
	spp = 0d0
	do i = 1, n
	   w = 1d0 / err(i) / err(i)
	   x = flx(i) - xhat 
     :	              - cx*(xpix(i)-uhat) - cy*(ypix(i)-vhat)
	   p = eta(i) - phat
	   sxp = sxp + x * p * w
	   spp = spp + p * p * w
	end do
	sdp = sxp / spp
	sigsdp = sqrt(1d0 / spp)
	
c	Orthogonalise residuals and model quadratic ramp
	sx = 0d0
	sp = 0d0
	sw = 0d0
	do i = 1, n
	   w = 1d0 / err(i) / err(i)
	   x = flx(i) - cx*(xpix(i)-uhat) - cy*(ypix(i)-vhat)
     :	              - sdp * (eta(i) - phat)
	   u = (xpix(i)-uhat)**2
	   v = (ypix(i)-vhat)**2
	   sx = sx + x * w
	   su = su + u * w
	   sv = sv + v * w
	   sw = sw + w
	end do
	xhat = sx / sw
	qhat = su / sw
	rhat = sv / sw
	
c	Optimal scaling of quadratic ramp term to fit data
	sxp = 0d0
	spp = 0d0
	do i = 1, n
	   w = 1d0 / err(i) / err(i)
	   x = flx(i) - cx*(xpix(i)-uhat) - cy*(ypix(i)-vhat)
     :	              - sdp * (eta(i) - phat)
     :	              - xhat
	   u = (xpix(i)-uhat)**2 - qhat	  
	   v = (ypix(i)-vhat)**2 - rhat	  
	   sxu = sxu + x * u * w
	   suu = suu + u * u * w
	   sxv = sxv + x * v * w
	   svv = svv + v * v * w
	end do
	cxx = sxu / suu
	sigcxx = sqrt(1d0 / suu)
	cyy = sxv / svv
	sigcyy = sqrt(1d0 / svv)
	
	chsdp = 0d0
	do i = 1, n
	   w = 1d0 / err(i) / err(i)
	   x = flx(i) - cx*(xpix(i)-uhat) - cy*(ypix(i)-vhat)
     :	              - cxx*(xpix(i)-uhat)**2 -  cyy*(ypix(i)-vhat)**2  
     :	              - xhat
	   p = eta(i) - phat
	   d = x - sdp * p 
	   chsdp = chsdp + d * d * w 
c	   print *,mod(hjd(i)-t0,period)/period,flx(i)-xhat,x,sdp*p,d
c	   Tested graphically. Linear ramp is being removed correctly.
	end do
	
	sdp = sdp / xhat
	sigsdp = sigsdp / xhat
	cx = cx / xhat
	sigcx = sigcx / xhat
	cxx = cxx / xhat
	sigcxx = sigcxx / xhat
	cy = cy / xhat
	sigcy = sigcy / xhat
	cyy = cyy / xhat
	sigcyy = sigcyy / xhat
	
	
	print *,'sdp     = ',sdp 
	print *,'sigsdp  = ',sigsdp 
	print *,'cx      = ',cx 
	print *,'sigcx   = ',sigcx 
	print *,'cy      = ',cy 
	print *,'sigcy   = ',sigcy 
	print *,'cxx     = ',cxx 
	print *,'sigcxx  = ',sigcxx 
	print *,'cyy     = ',cyy 
	print *,'sigcyy  = ',sigcyy 
	print *,'fs      = ',xhat
	print *,'chsdp   = ',chsdp
	print *,'n       = ',n
	
c  ***	Rescale error bars 
c	do i = 1, n
c	   err(i) = err (i) * sqrt(chsdp/dfloat(n))
c	end do
	
	end	


C+	----------------------------------------------------------------
	subroutine orthtp(ndata,hjd,err,nvel,hjdrv,rverr,
     :	                  t0,sigt0,period,sigper,hw1,hw2)

     	implicit none
	
c  ***	Orthogonalise T0 and p by adjusting epoch of transit
c	to centre of mass of data taken in transit.
	
c  ***	Subroutine parameters
	integer ndata
	real*8 hjd(ndata)
	real*8 err(ndata)
	integer nvel
	real*8 hjdrv(nvel)
	real*8 rverr(nvel)
	real*8 t0, sigt0
	real*8 period, sigper
	real*8 hw1,hw2
	
c  ***	Local variables
	real*8 sxw,sw,w
	real*8 t1, t2
	integer i
	real*8 phi, cphi
	integer norb
	real*8 tcog
	real*8 pi
	
	pi = 4d0*atan2(1d0,1d0)
	
	sxw = 0d0
	sw = 0d0
	t1 = 1d38
	t2 = -1d38
	do i = 1, ndata
	   phi = mod( hjd(i)-t0, period ) / period
	   if( phi .lt.0d0 ) phi = phi + 1d0
	   if( phi.lt.hw1 .or. phi.gt.hw2 ) then
	      w = 1d0 / err(i) / err(i)
	      sxw = sxw + hjd(i) * w
	      sw = sw + w
	      t1 = min(t1,hjd(i))
	      t2 = max(t2,hjd(i))
	   end if
	end do
	
	norb = nint ( (t2 - t1 ) / period )
	norb = nint ( (hjd(ndata) - hjd(1))/ period )
	sigper = sigt0 / dfloat(norb)

	do i = 1, nvel
	   phi = mod( hjdrv(i)-t0, period ) / period
	   cphi = cos( 2d0 * pi * phi )
c	   Strictly we should weight this as cos(phi)
	   w = cphi * cphi / rverr(i) / rverr(i)
	   sxw = sxw + hjdrv(i) * w
	   sw = sw + w
	end do
	
	tcog = sxw / sw
	norb = nint((tcog - t0) / period)
	t0 = t0 + dfloat(norb) * period
	
	end
C+	----------------------------------------------------------------
C+
	subroutine readtamuz(lu,photfile,
     :	           swaspid,epoch,period,wday,dmag,dchs,
     :	           mxobs,ndata,hjd,dat,err)

c  ***  This is a more advanced version of rddata. It reads HUNTER
c	parameters as well as photometric data from a SuperWASP
c	post-tamuz (LCFINDER) light curve listing.
	
	implicit none
	integer lu
	character*80 photfile
	integer mxobs
	real*8 hjd(mxobs)
	real*8 dat(mxobs)
	real*8 err(mxobs)
	integer ndata
	integer i
	character*80 record
	character*26 swaspid
	real*8 dummy
	integer ipk
	integer mxpk
	parameter(mxpk = 5)
	real*8 epoch(mxpk)
	real*8 period(mxpk)
	real*8 wday(mxpk)
	real*8 dmag(mxpk)
	real*8 dchs(mxpk)
		
c  ***	Read data
	i = 0
	ipk = 0
	open(unit=lu,file=photfile,status='old')
3	read(lu,'(a)',end=998)record
	if(record(1:23) .eq. ' # Lightcurve for star ') then
	   read(record(24:49),'(a)')swaspid
	   print *,' swaspid = ',swaspid
	   go to 3
	else if (record(1:8) .eq. ' # Peak:')then
	   print *,record(9:10)
	   read(record(9:10),*)ipk
	   print *,'ipk=',ipk
	   read(lu,'(a)',end=998)record
	   read(record(13:22),*)epoch(ipk)
	   print *,'epoch=',epoch(ipk)
	   read(lu,'(a)',end=998)record
	   read(record(13:23),*)period(ipk)
	   print *,'period=',period(ipk)
	   read(lu,'(a)',end=998)record
	   read(record(16:25),*)dummy
	   wday(ipk) = dummy / 24d0
	   print *,'wday=',wday(ipk)
	   read(lu,'(a)',end=998)record
	   read(record(11:25),*)dummy
	   dmag(ipk) = -dummy
	   print *,'dmag=',dmag(ipk)
	   read(lu,'(a)',end=998)record
	   read(record(17:28),*)dchs(ipk)
	   print *,'dchs=',dchs(ipk)
	   go to 3
	else if ( record(1:2) .eq. ' #' ) then
	   go to 3
	end if

c  ***	When we get to here, we've read all the header records
c	so read the light curve data.
	i = i + 1
	read(record,*)hjd(i),dat(i),err(i)
c	print *,hjd(i),dat(i),err(i)
	go to 3
	
998	close(lu)
	ndata = i
	
	end

C+	----------------------------------------------------------------
C+
	subroutine readnewlc(lu,photfile,
     :	           swaspid,jh,rpmj,drpmj,
     :	           epoch,period,wday,dmag,dchs,snred,ntransit,
     :	           aellip,snellip,transfrac,gaprat,dchsanti,
     :	           mxobs,ndata,hjd,dat,err)

c  ***  This is a more advanced version of rddata. 

c  ***	This version (readnewlc) reads the expanded header information
c	as well as photometric data from a .lc file generated by the
c	2007 March version of HUNTER.
	
	implicit none
	integer lu
	character*80 photfile
	integer mxobs
	real*8 hjd(mxobs)
	real*8 dat(mxobs)
	real*8 err(mxobs)
	integer ndata
	integer i
	character*80 record
	character*26 swaspid
	integer ngood
	real*8 jh,rpmj,drpmj
	real*8 dummy
	integer ipk
	integer mxpk
	parameter(mxpk = 5)
	real*8 epoch(mxpk)
	real*8 period(mxpk)
	real*8 wday(mxpk)
	real*8 dmag(mxpk)
	real*8 dchs(mxpk)
	real*8 snred(mxpk)
	integer ntransit(mxpk)
	real*8 aellip(mxpk)
	real*8 snellip(mxpk)
	real*8 transfrac(mxpk)
	real*8 gaprat(mxpk)
	real*8 dchsanti(mxpk)
		
c  ***	Read data
	i = 0
	ipk = 0
	open(unit=lu,file=photfile,status='old')
3	read(lu,'(a)',end=998)record
	if(record(1:21) .eq. '# Lightcurve of star ') then
	   read(record(22:47),'(a)')swaspid
	   print *,'# Lightcurve of star ',swaspid
	   go to 3
	else if (record(1:14) .eq. '# Num good pts')then
	   read(record(22:31),*)ngood
	   print *,'# Num good pts      :',ngood
	   go to 3
	else if (record(1:5) .eq. '# J-H')then
	   read(record(22:31),*)jh
	   if (jh.eq.-99) jh=0.35	!set to ~G5V color
	   if (jh.lt.0.0) jh=0.0	!T-R relation valid Teff<7200,J-H>0
	   if (jh.gt.0.84)jh=0.84	!LD coeffs good to T>3500, J-H<0.84
	   print *,'# J-H               :',jh
	   go to 3
	else if (record(1:12) .eq. '# RPM_J     ')then
	   read(record(22:31),*)rpmj
	   print *,'# RPM_J             :',rpmj
	   go to 3
	else if (record(1:12) .eq. '# RPM_J diff')then
	   read(record(22:31),*)drpmj
	   print *,'# RPM_J diff        :',drpmj
	   go to 3
	else if (record(1:6) .eq. '# PEAK')then
	   print *,record(22:31)
	   read(record(22:31),*)ipk
	   print *,'ipk=',ipk
	   
	   read(lu,'(a)',end=998)record
	   read(record(22:31),*)epoch(ipk)
	   print *,'# Epoch             :',epoch(ipk)
	   
	   read(lu,'(a)',end=998)record
	   read(record(22:31),*)period(ipk)
	   print *,'# Period (days)     :',period(ipk)
	   
	   read(lu,'(a)',end=998)record
	   read(record(22:31),*)dummy
	   wday(ipk) = dummy / 24d0
	   print *,'# Duration  (days)  :',wday(ipk)
	   
	   read(lu,'(a)',end=998)record
	   read(record(22:31),*)dummy
	   dmag(ipk) = -dummy
	   print *,'# Depth (mag)       :',dmag(ipk)
	   
	   read(lu,'(a)',end=998)record
	   read(record(22:31),*)dchs(ipk)
	   print *,'# Delta chisq       :',dchs(ipk)
	   
	   read(lu,'(a)',end=998)record
	   read(record(22:31),*)snred(ipk)
	   print *,'# Signal-Rednoise   :',snred(ipk)
	   
	   read(lu,'(a)',end=998)record
	   read(record(22:31),*)ntransit(ipk)
	   print *,'# Ntransits         :',ntransit(ipk)
	   
	   read(lu,'(a)',end=998)record
	   read(record(22:31),*)aellip(ipk)
	   print *,'# Ellipsoidal var   :',aellip(ipk)
	   
	   read(lu,'(a)',end=998)record
	   read(record(22:31),*)snellip(ipk)
	   print *,'# Ellipsoidal S/N   :',snellip(ipk)
	   
	   read(lu,'(a)',end=998)record
	   read(record(22:31),*)transfrac(ipk)
	   print *,'# Fraction-intransit:',transfrac(ipk)
	   
	   read(lu,'(a)',end=998)record
	   read(record(22:31),*)gaprat(ipk)
	   print *,'# Gap Ratio         :',gaprat(ipk)
	   
	   read(lu,'(a)',end=998)record
	   read(record(22:31),*)dchsanti(ipk)
	   print *,'# Dchisq/Anti-dchisq:',dchsanti(ipk)
	   go to 3
	else if ( record(1:2) .eq. '# ' ) then
	   go to 3
	end if

c  ***	When we get to here, we've read all the header records
c	so read the light curve data.
	i = i + 1
	read(record,*)hjd(i),dat(i),err(i)
c	print *,hjd(i),dat(i),err(i)
	go to 3
	
998	close(lu)
	ndata = i
	if ( ndata .ne. ngood ) then
	   print *,' READNEWLC WARNING: number of light curve records'
	   print *,'  does not match ngood.'
	end if
	
	end

C+	----------------------------------------------------------------
C+
	subroutine getparms(jh,rstar,tjh,infile,a1,a2,a3,a4)
	
c  ***	Given the J-H colour index of a transit candidate,
c	uses DW's polynomial fits to estimate Teff and Mstar,
c	then interpolates nonlinear limb-darkening coefficients
c	from grid of values taken from Claret (2000) for [Fe/H]=0.1
	
	implicit none
c	character*120 record
	double precision jh,rstar,a1,a2,a3,a4
	
	double precision teff(13)
	double precision arr1(13)
	double precision arr2(13)
	double precision arr3(13)
	double precision arr4(13)
	double precision tjh, frac
	integer i, jlo, jhi
	character*120 infile
	
	open(unit=63,file=infile,status='old',form='formatted')
	
c	read(63,*) record
	read(63,*) (teff(i),i=1,13)
	
c	read(63,*) record
	read(63,*) (arr1(i),i=1,13)
	
c	read(63,*) record
	read(63,*) (arr2(i),i=1,13)
	
c	read(63,*) record
	read(63,*) (arr3(i),i=1,13)
	
c	read(63,*) record
	read(63,*) (arr4(i),i=1,13)
	
	close(63)

c  ***	Use DW's optimised FGK relations

c	Radius in Rsun	
	rstar =(((-3.925d-14*tjh 
     :	       + 8.3909d-10)*tjh 
     :	       - 6.555d-6)*tjh
     :	       + 0.02245d0)*tjh
     :	       - 27.9788d0


c	Locate temperature in interpolation grid
	jlo = 0
	call huntdp(teff,13,tjh,jlo)
	
	if(jlo .eq.0) then
	   a1 = arr1(1)	
	   a2 = arr2(1)	
	   a3 = arr3(1)	
	   a4 = arr4(1)	
	else if(jlo .eq.0) then
	   a1 = arr1(13)	
	   a2 = arr2(13)	
	   a3 = arr3(13)	
	   a4 = arr4(13)	
	else
	   jhi = jlo + 1
	   frac = (arr1(jhi)-arr1(jlo))/(teff(jhi)-teff(jlo))
	   a1 = arr1(jlo) + (tjh-teff(jlo)) * frac	
	   frac = (arr2(jhi)-arr2(jlo))/(teff(jhi)-teff(jlo))
	   a2 = arr2(jlo) + (tjh-teff(jlo)) * frac	
	   frac = (arr3(jhi)-arr3(jlo))/(teff(jhi)-teff(jlo))
	   a3 = arr3(jlo) + (tjh-teff(jlo)) * frac	
	   frac = (arr4(jhi)-arr4(jlo))/(teff(jhi)-teff(jlo))
	   a4 = arr4(jlo) + (tjh-teff(jlo)) * frac	
	end if
	
	end
C+	----------------------------------------------------------------
C+	
	SUBROUTINE HUNTDP(XX, N, X, JLO)

C  ***	Given an array XX of length N, and given a value X, returns a value JLO
c	such that X is between XX(JLO) and XX(JLO+1).  XX must be monotonic, 
c	eitherincreasing or decreasing. JLO = 0 or JLO = N is returned to
c	indicate that X is out of range. JLO on input is taken as the initial
c	guess for JLO on output.

	REAL*8 XX(N)
	REAL*8 X
	LOGICAL ASCND

	ASCND = XX(N).GT.XX(1)
	IF(JLO.LE.0 .OR. JLO.GT.N)THEN
	  JLO = 0
	  JHI = N+1
	  GO TO 3
	END IF
	INC = 1
	IF(X.GE.XX(JLO) .EQV. ASCND)THEN
1	  JHI = JLO + INC
	  IF(JHI.GT.N)THEN
	    JHI = N + 1
	  ELSE IF(X.GE.XX(JHI) .EQV. ASCND)THEN
	    JLO = JHI
	    INC = INC + INC
	    GO TO 1
	  END IF
	ELSE
	  JHI = JLO
2	  JLO = JHI - INC
	  IF(JLO.LT.1)THEN
	    JLO = 0
	  ELSE IF(X.LT.XX(JLO) .EQV. ASCND)THEN
	    JHI = JLO
	    INC = INC + INC
	    GO TO 2
	  END IF
	END IF

3	IF(JHI-JLO.EQ.1)RETURN
	JM = (JHI+JLO)/2
	IF(X.GT.XX(JM) .EQV. ASCND)THEN
	  JLO = JM
	ELSE
	  JHI = JM
	END IF
	GO TO 3

	END

C+	----------------------------------------------------------------
C+	
	subroutine eval(t0,period,ms,rs,rp,cosi,vk,vsi,gam,lam,rstar,
     :	                ecc, om, 
     :	                sdp1,cx1,cxx1,cy1,cyy1,fs1,
     :	                sdp2,cx2,cxx2,cy2,cyy2,fs2,
     :	                sdp3,rmp31,rmp32,fs3,
     :	                sdp4,rmp41,rmp42,fs4,
     :	                c1,c2,c3,c4,ms0,sigms,constrainmr,mu,nu,
     :	                ndata,npset,np,fobs,lobs,adj,hjd,mag,err,
     :	                nvel,nvset,fvel,lvel,
     :	                gadj,hjdrv,rv,rverr,dvjit,
     :	                nsecl,nsset,nirpt,firpt,lirpt,ichan,
     :	                hjdsecl,flxsecl,errsecl,xsecl,ysecl,
     :	                chphot,chspec,chsecl,chs,scl,scv)

	implicit none
	
	integer ndata, npset
	integer np(npset)
	integer fobs(npset)
	integer lobs(npset)
	real*8 scl(npset)
	real*8 adj(npset)
	real*8 hjd(ndata)
	real*8 mag(ndata)
	real*8 err(ndata)
	real*8 z(ndata)
	real*8 mu(ndata)
	
	integer nvel, nvset
	integer fvel(nvset)	
	integer lvel(nvset)
	real*8 scv(nvset)
	real*8 gadj(nvset)	
	real*8 hjdrv(nvel)
	real*8 rv(nvel)
	real*8 rverr(nvel)
	real*8 dvjit
	real*8 vz(nvel)
	real*8 nu(nvel)
	
	integer isset, nsset
	integer firpt(nsset),nirpt(nsset),lirpt(nsset),ichan(nsset)
	real*8 fs(nsset)
	real*8 sdp(nsset)
	real*8 rmp1(nsset)
	real*8 rmp2(nsset)
	real*8 cx(nsset)
	real*8 cxx(nsset)
	real*8 cy(nsset)
	real*8 cyy(nsset)

	integer nsecl
	real*8 hjdsecl(nsecl)
	real*8 flxsecl(nsecl)
	real*8 errsecl(nsecl)
	real*8 xsecl(nsecl)
	real*8 ysecl(nsecl)
	real*8 zsecl(nsecl)
	real*8 eta(nsecl)
	
	
c  ***	System parameters
	real*8 t0,period,ms,rs,rp,cosi,vk,vsi,gam,lam,rstar,ecc,om
	real*8 sdp1,cx1,cxx1,cy1,cyy1,fs1
	real*8 sdp2,cx2,cxx2,cy2,cyy2,fs2
	real*8 sdp3,rmp31,rmp32,fs3
	real*8 sdp4,rmp41,rmp42,fs4
	real*8 ms0, sigms, rs0, sigrs
	
c  ***	Limb-darkening coeffs from Claret (2000)
	real*8 c1(npset), c2(npset), c3(npset), c4(npset)
	
c  ***	Accumulators and statistics
	real*8 w, sw, sxw, spw, xhat, phat, r
	real*8 sv, su, uhat, vhat
	real*8 x, t, p, that, sxpw, sppw
	real*8 chpset, chphot, chvset, chspec, chsecl, chs
	integer ipset
	integer ivset
	integer ndof
	logical constrainmr
	
	integer i
	
c  ***	Compute transit model
	call transmod(t0,period,rp,rs,cosi,vk,vsi,lam,ecc,om,npset,np,
     :	               c1,c2,c3,c4,ndata,hjd,z,mu,nvel,hjdrv,vz,nu)

c  ***	Orthogonalise each dataset and corresponding 
c	model points independently.
	chphot = 0d0
	do ipset = 1, npset
	   sxw = 0d0
	   spw = 0d0
	   sw = 0d0
	   do i = fobs(ipset), lobs(ipset)
	      w = 1d0 /err(i)/err(i)
	      sxw = sxw + mag(i) * w
	      spw = spw + mu(i) * w
	      sw = sw + w
	   end do
	   xhat = sxw / sw
	   phat = spw / sw
	   adj(ipset) = xhat - phat
	   
c  ***	   Compute chi-squared from orthogonalised residuals
	   chpset = 0d0
	   do i = fobs(ipset), lobs(ipset)
	      w = 1d0 /err(i)/err(i)
	      r = mag(i) - mu(i) - adj(ipset)
	      chpset = chpset + r * r * w
	   end do
	   ndof = lobs(ipset) - fobs(ipset) + 1
	   ndof = ndof - 1
	   scl(ipset) = chpset / dfloat(ndof)
	   chphot = chphot + chpset
	end do
	
c  ***	Now orthogonalise radial velocities
	chspec = 0d0
	do ivset = 1, nvset
	   sxw = 0d0
	   spw = 0d0
	   sw = 0d0
	   do i = fvel(ivset), lvel(ivset)
	      w = 1d0 /(rverr(i)*rverr(i) + dvjit*dvjit)
	      sxw = sxw + rv(i) * w
	      spw = spw + nu(i) * w
	      sw = sw + w
	   end do
	   if ( sw .gt. 1d-30 ) then
	      xhat = sxw / sw
	      phat = spw / sw
	   else
	      xhat = 0d0
	      phat = 0d0
	   end if
	   gadj(ivset) = xhat - phat
	
	   chvset = 0d0
	   do i = fvel(ivset), lvel(ivset)
	      w = 1d0 /(rverr(i)*rverr(i) + dvjit*dvjit)
	      r = rv(i) - nu(i) - gadj(ivset)
	      chvset = chvset + r * r * w
	   end do
c	   print *,ivset,chvset,gadj(ivset)	!leslie
	   chspec = chspec + chvset
	   ndof = lvel(ivset) - fvel(ivset) + 1
	   ndof = ndof - 1
	   scv(ivset) = chvset / dfloat(ndof)
	end do
	
	if (nsset.ge.1) then
c  ***	   Compute visible fraction of secondary for each 
c	   subset of sec ecl data
	   call secmod(t0,period,rp,rs,cosi,vk,vsi,lam,ecc,om,
     :	               nsecl,hjdsecl,zsecl,eta)
	   do isset = 1, nsset
	      if(ichan(isset).eq.1)then
c	         fs(isset) = fs1
	         sdp(isset) = sdp1	
	         cx(isset) = cx1
	         cxx(isset) = cxx1
	         cy(isset) = cy1
	         cyy(isset) = cyy1
	      else if(ichan(isset).eq.2)then
c	         fs(isset) = fs2
	         sdp(isset) = sdp2	
	         cx(isset) = cx2
	         cxx(isset) = cxx2
	         cy(isset) = cy2
	         cyy(isset) = cyy2
	      else if(ichan(isset).eq.3)then
c	         fs(isset) = fs3
	         sdp(isset) = sdp3	
	         rmp1(isset) = rmp31
	         rmp2(isset) = rmp32
	      else if(ichan(isset).eq.4)then
c	         fs(isset) = fs4
	         sdp(isset) = sdp4	
	         rmp1(isset) = rmp41
	         rmp2(isset) = rmp42
	      end if
	   end do
	end if

c  ***	The sub-pixel sensitivity effect is usually fitted with a quadratic 
c	function of the sub-pixel position, i.e.
c	f' = f * [ c1 + c2*dx + c3*dx**2 + c4*dy + c5*dy**2 ]
c	where f' is the measured flux, f is the underlying flux, 
c	dx and dy are the sub-pixel positions of the star, and 
c	c1-c5 are free parameters.


c  ***	The ramp effect is fitted by Heather Knutson and Dave Charbonneau 
c	as a quadratic in ln(t-t0), i.e.
c	f' = f * [ c1 + c2*ln(t-t0) + c3*ln(t-t0)**2 ]
c	where t is time and t0 is the time of the beginning of the observation. 
	
c  ***	Orthogonalise each subset of sec ecl data

	chsecl = 0d0
	do isset = 1, nsset
	   su = 0d0
	   sv = 0d0
	   sw = 0d0
	   do i = firpt(isset),lirpt(isset)
	      w = 1d0 / errsecl(i) / errsecl(i)
	      su = su + xsecl(i)*w
	      sv = sv + ysecl(i)*w
	      sw = sw + w
c	      print *,w,spw,sw
 	   end do
	   uhat = su / sw
	   vhat = sv / sw
	   
	   sppw = 0d0
	   sxpw = 0d0
	   do i = firpt(isset),lirpt(isset)
	      w = 1d0 / errsecl(i) / errsecl(i)
	      p = 1d0 + sdp(isset)*eta(i)
	      if(ichan(isset).eq.1 .or. ichan(isset).eq.2)then
	         p = p * ( 1d0 
     :		       + cx(isset)*(xsecl(i)-uhat) 
     :		       + cxx(isset)*(xsecl(i)-uhat)**2 
     :		       + cy(isset)*(ysecl(i)-vhat)
     :		       + cyy(isset)*(ysecl(i)-vhat)**2 
     :	                  )
	      else if(ichan(isset).eq.3 .or. ichan(isset).eq.4)then
	         t = log(hjdsecl(i) - hjdsecl(firpt(isset)) + 1d0/48d0)
	         p = p * (1d0+rmp1(isset)*t+rmp2(isset)*t*t)
	      end if
	      x = flxsecl(i)
	      sxpw = sxpw + x * p * w
	      sppw = sppw + p * p * w
 	   end do
	   fs(isset) = sxpw / sppw
c	   print *,'fs =',fs
c	   pause
	   
	   do i = firpt(isset),lirpt(isset)
	      w = 1d0 / errsecl(i) / errsecl(i)
	      p = 1d0 + sdp(isset)*eta(i)
	      if(ichan(isset).eq.1 .or. ichan(isset).eq.2)then
	         p = p * ( 1d0 
     :		       + cx(isset)*(xsecl(i)-uhat) 
     :		       + cxx(isset)*(xsecl(i)-uhat)**2 
     :		       + cy(isset)*(ysecl(i)-vhat)
     :		       + cyy(isset)*(ysecl(i)-vhat)**2 
     :	                  )
	      else if(ichan(isset).eq.3 .or. ichan(isset).eq.4)then
	         t = log(hjdsecl(i) - hjdsecl(firpt(isset)) + 1d0/48d0)
	         p = p * (1d0+rmp1(isset)*t+rmp2(isset)*t*t)
	      end if
	      x = flxsecl(i)
	      r = x - fs(isset) * p
	      chsecl = chsecl + r * r * w
 	   end do
	   
	   if(ichan(isset).eq.1)then
	     fs1 = fs(isset)
	   end if
	   
	   if(ichan(isset).eq.2)then
	     fs2 = fs(isset)
	   end if

	   if(ichan(isset).eq.3)then
	     fs3 = fs(isset)
	   end if

	   if(ichan(isset).eq.4)then
	     fs4 = fs(isset)
	   end if

	end do   
	
	chs = chphot + chspec + chsecl
c	chs = chphot + chspec 
	gam = gadj(1)
	
c  ***	Add Bayesian penalty for silly stellar masses	
	r = ms - ms0
	w = 1d0 / sigms / sigms
	chs = chs + r * r * w
	
c  ***	Add Bayesian penalty for silly v sin i	-- pending
c	r = vsi - vs0
c	w = 1d0 / sigvs / sigvs
c	chs = chs + r * r * w
	
c  ***	Add Bayesian penalty for silly stellar radii
	if ( constrainmr ) then
 	   rs0 = ms0 ** 0.8d0
	   sigrs = 0.8d0 * sigms * rs0 / ms0 
	   r = rstar - rs0
	   w = 1d0 / sigrs / sigrs
	   chs = chs + r * r * w
	end if
	
	end

C+	----------------------------------------------------------------
C+
	subroutine transmod(t0,period,rp,rs,cosi,vk,vsi,lam,ecc,om,
     :	                    npset,np,c1,c2,c3,c4,
     :	                    ndata,hjd,z,mu,nvel,hjdrv,vz,nu)
	
c	t0 = epoch of transit
c	period = period
c	hjd = array of obs dates
c	rp = planet radius / orbital sep
c	rs = stellar radius / orbital sep
c	vk = stellar reflex velocity amp in km/sec
c	vsi = stellar v sin i in km/sec
c	gam = stellar centre-of-mass velocity in km/sec

	implicit none

	integer i, ndata,nvel
	real*8 t0, period, rp, rs, cosi, vk, vsi, lam, ecc, om
	real*8 sini
	integer npset
	integer np(npset)
 	real*8 c1(npset), c2(npset), c3(npset), c4(npset)
	real*8 hjd(ndata)
	real*8 z(ndata)
	real*8 mu(ndata)
	real*8 alpha(ndata)
	real*8 xp(ndata)
	real*8 yp(ndata)
	real*8 rau(ndata)
	real*8 hjdrv(nvel)
	real*8 vz(nvel)
	real*8 vmu(nvel)
	real*8 nu(nvel)
	real*8 pi, p, ph, zp, up
	real*8 cosl, sinl
	real*8 nutrans,mtrans,meananom,dttrans,tperi
	integer ipset
	real*8 zscfac
	
	pi = 4d0*atan2(1d0,1d0)
	
c  ***	Compute date of periastron relative to transit time t0	
	nutrans = pi/2d0 - om
	mtrans = meananom(nutrans,ecc)
	dttrans = mtrans/2d0/pi * period
	tperi = t0 - dttrans

c  ***	Ratio of radii
	p = rp / rs
	
	sini = sqrt(1d0 - cosi*cosi)
	
	do i = 1, ndata

           call kepler(hjd(i),tperi,period,ecc,om,vk,sini,
     :	              mu(i),rau(i),alpha(i),xp(i),yp(i))
		
c  ***	   Impact parameter in units of primary radius
	   if ( cos(alpha(i)) .gt. 0d0 ) then
	      z(i) = rau(i) * sin(alpha(i)) / rs
	   else
	      z(i) = 1d0 / rs
	   end if
	end do
	
c  ***	Compute model light curve subset-by-subset, with appropriate
c	limb-darkening coeffs per subset.
	i = 1
	do ipset = 1, npset
	   call occultsmall(p,c1(ipset),c2(ipset),c3(ipset),c4(ipset),
     :	                 np(ipset),z(i),mu(i))
	   i = i + np(ipset)
	end do
	
c  ***	Convert mu to magnitudes
	do i = 1, ndata
	   mu(i) = -2.5d0*log10(mu(i))
	end do	
	
c  ***	Compute model RV curve. First get photometry for Rossiter.
	do i = 1, nvel
	   
           call kepler(hjdrv(i),tperi,period,ecc,om,vk,sini,
     :	              nu(i),rau(i),alpha(i),xp(i),yp(i))
	   
c  ***	   Impact parameter in units of primary radius
	   if ( cos(alpha(i)) .gt. 0d0 ) then
	      vz(i) = rau(i) * sin(alpha(i)) / rs
	   else
	      vz(i) = 1d0 / rs
	   end if
	end do
	
c  ***	Compute model flux -- use same band as 1st phot dataset
	call occultsmall(p,c1(1),c2(1),c3(1),c4(1),nvel,vz,vmu)
	
c  ***	Rotation matrix elements for misalignment of rotation axes
	cosl = cos(lam)
	sinl = sin(lam)
		   
c  ***	Compute model RV curve. 
	do i = 1, nvel

c  ***	   Rotate through 90-i about X axis to get zp
	   zp = - yp(i) * cosi
	     	
c  ***	   Partial transit: ratio of impact parameter of centroid 
c	   of eclipsed part of star to impact parameter of planet's 
c	   centre (linear approximation)
	   if ( vz(i).gt. 1d0-p .and. vz(i).lt. 1d0+p ) then
	      zscfac = ( 1d0 + (vz(i)-1d0-p) / 2d0 ) / vz(i)
	   else
	      zscfac = 1d0
	   end if   

c  ***	   Rotate through lambda about y axis to get up
	   up =  xp(i) * cosl - zp * sinl  

c  ***	   Code Rossiter effect here. Impact param is vz(i),
c	   x coord is xp, flux diminution is 1 - vmu(i)

	   nu(i) = nu(i) - vsi * ( 1d0 - vmu(i) ) / vmu(i)
     :	            * up * zscfac / rs

c  ***	   Fudge based on a mistake made earlier which improved the fit
c	   for reasons unknown   
c	   nu(i) = nu(i) - vsi * ( 1d0 - vmu(i) )
c     :	 	     / vmu(i) * up / rs 
c     :	   	     / min(zscfac*vz(i),vz(i))			     
	end do
	
	end

C+	----------------------------------------------------------------
C+
      subroutine occultsmall(p,c1,c2,c3,c4,nz,z,mu)
      implicit none
      integer i,nz
c      parameter (nz=201)
      real*8 p,c1,c2,c3,c4,z(nz),mu(nz),i1,norm,
     &       x,tmp,iofr,pi
C This routine approximates the lightcurve for a small 
C planet. (See section 5 of Mandel & Agol (2002) for
C details):
C Input:
C  p      ratio of planet radius to stellar radius
C  c1-c4  non-linear limb-darkening coefficients
C  z      impact parameters (positive number normalized to stellar 
C        radius)- this is an array which MUST be input to the routine
C  NOTE:  nz must match the size of z & mu in calling routine
C Output:
C  mu     flux relative to unobscured source for each z
C
      pi=acos(-1.d0)
      norm=pi*(1.d0-c1/5.d0-c2/3.d0-3.d0*c3/7.d0-c4/2.d0)
      i1=1.d0-c1-c2-c3-c4
      do i=1,nz
        mu(i)=1.d0
        if(z(i).gt.1.d0-p.and.z(i).lt.1.d0+p) then
          x=1.d0-(z(i)-p)**2
          tmp=(1.d0-c1*(1.d0-0.8d0*x**0.25d0)
     &             -c2*(1.d0-2.d0/3.d0*x**0.5d0)
     &             -c3*(1.d0-4.d0/7.d0*x**0.75d0)
     &             -c4*(1.d0-0.5d0*x))
          mu(i)=1.d0-tmp*(p**2*acos((z(i)-1.d0)/p)
     &        -(z(i)-1.d0)*sqrt(p**2-(z(i)-1.d0)**2))/norm
        endif
        if(z(i).le.1.d0-p.and.z(i).ne.0.d0) then
          mu(i)=1.d0-pi*p**2*iofr(c1,c2,c3,c4,z(i),p)/norm
        endif
        if(z(i).eq.0.d0) then
          mu(i)=1.d0-pi*p**2/norm
        endif
      enddo
      return
      end

      function iofr(c1,c2,c3,c4,r,p)
      implicit none
      real*8 r,p,c1,c2,c3,c4,sig1,sig2,iofr
      sig1=sqrt(sqrt(1.d0-(r-p)**2))
      sig2=sqrt(sqrt(1.d0-(r+p)**2))
      iofr=1.d0-c1*(1.d0+(sig2**5-sig1**5)/5.d0/p/r)
     &         -c2*(1.d0+(sig2**6-sig1**6)/6.d0/p/r)
     &         -c3*(1.d0+(sig2**7-sig1**7)/7.d0/p/r)
     &         -c4*(p**2+r**2)
      return
      end
C+
	subroutine rddata(lu,photfile,mxobs,ndata,hjd,dat,err)
	
	implicit none
	integer lu
	character*80 photfile
	integer mxobs
	real*8 hjd(mxobs)
	real*8 dat(mxobs)
	real*8 err(mxobs)
	integer ndata
	integer i
	character*80 record
		
c  ***	Read data
	i = 0
	open(unit=lu,file=photfile,status='old')
3	read(lu,'(a)',end=998)record
	if(record(1:2) .eq. ' #' .or. record(1:2) .eq. '# ')go to 3
	i = i + 1
	read(record,*)hjd(i),dat(i),err(i)
c	print *,hjd(i),dat(i),err(i)
	go to 3
	
998	close(lu)
	ndata = i
	
	end
C+
	subroutine rdspxy(lu,photfile,mxobs,ndata,hjd,dat,err,x,y)
	
	implicit none
	integer lu
	character*80 photfile
	integer mxobs
	real*8 hjd(mxobs)
	real*8 dat(mxobs)
	real*8 err(mxobs)
	real*8 x(mxobs)
	real*8 y(mxobs)
	integer ndata
	integer i
	character*80 record
		
c  ***	Read data
	i = 0
	open(unit=lu,file=photfile,status='old')
3	read(lu,'(a)',end=998)record
	if(record(1:2) .eq. ' #' .or. record(1:2) .eq. '# ')go to 3
	i = i + 1
	read(record,*)hjd(i),dat(i),err(i),x(i),y(i)
c	print *,hjd(i),dat(i),err(i)
	go to 3
	
998	close(lu)
	ndata = i
	
	end
C+
	subroutine methast(idum,chsnew,chsold,reject)
c  ***	Metropolis-Hastings decision maker
	
	implicit none
	real*8 chsnew, chsold
	logical reject
	real*8 dchisq,pjump,prob
	integer idum
	real ran1
	
	dchisq = chsnew - chsold
	
c  ***	Here comes the Markov-chain Monte Carlo decision
c	point. Do we accept the jump or not?

	if(dchisq .lt. 0d0) then
c  ***	   Accept the proposal and move on
	   reject = .false.
	else
c  ***	   Set probability of next jump
	   pjump = exp(-dchisq/2)
c  ***	   Pick a uniform random deviate in range 0 to 1
	   prob = dble(ran1(idum))
	   if ( prob .lt. pjump ) then
c  ***	      OK, accept the proposal
	      reject = .false.
	   else
c  ***	      No, reject the proposal
	      reject = .true.
	   end if
	end if
	
	end	
C+
	FUNCTION RAN1(IDUM)
	
	SAVE

C  ***	Returns a uniform random deviate between 0.0 and 1.0. Set IDUM
c	to any negative value to initialise or reinitialise the sequence

	DIMENSION R(97)
	integer m1,m2,m3
	real rm1,rm2
	PARAMETER(M1=259200, IA1 = 7141, IC1 = 54773, RM1 = 1./M1) 
	PARAMETER(M2=134456, IA2 = 8121, IC2 = 28411, RM2 = 1./M2) 
	PARAMETER(M3=243000, IA3 = 4561, IC3 = 51349)
	DATA IFF/0/
	
	
	

	IF(IDUM.LT.0 .OR. IFF.EQ.0)THEN
	  IFF = 1
	  IX1 = MOD(IC1-IDUM,M1)
	  IX1 = MOD(IA1*IX1+IC1,M1)
	  IX2 = MOD(IX1,M2)
	  IX1 = MOD(IA1*IX1+IC1,M1)
	  IX3 = MOD(IX1,M3)
	  DO 11 J=1, 97
	    IX1 = MOD(IA1*IX1+IC1,M1)
	    IX2 = MOD(IA2*IX2+IC2,M2)
	    R(J) = (FLOAT(IX1)+FLOAT(IX2)*RM2)*RM1
11	  CONTINUE
	  IDUM = 1
	END IF

	IX1 = MOD(IA1*IX1+IC1,M1)
	IX2 = MOD(IA2*IX2+IC2,M2)
	IX3 = MOD(IA3*IX3+IC3,M3)
	J = 1 + (97*IX3)/M3
	IF(J.GT.97 .OR. J.LT.1)PAUSE
	RAN1 = R(J)
	R(J) = (FLOAT(IX1)+FLOAT(IX2)*RM2)*RM1

	RETURN
	END

	FUNCTION GASDEV(IDUM)
	
C  ***	Returns a normally distributed deviate with zero mean and unit
c	variance, using RAN1(IDUM) as the source of uniform deviates.

	SAVE

	DATA ISET/0/
	
	IF(ISET.EQ.0)THEN
1	  V1 = 2.*RAN1(IDUM)-1.
	  V2 = 2.*RAN1(IDUM)-1.
	  R = V1*V1 + V2*V2
	  IF(R.GE.1.)GO TO 1
	  FAC = SQRT(-2.*LOG(R)/R)
	  GSET = V1*FAC
	  GASDEV = V2*FAC
	  ISET = 1
	ELSE
	  GASDEV = GSET
	  ISET = 0
	END IF

	RETURN
	END
	
C+
C+
	SUBROUTINE INDEXX(N,ARRIN,INDX)

	IMPLICIT NONE
	
	INTEGER N
	REAL*8 ARRIN(N)
	INTEGER INDX(N)
	REAL*8  Q
	INTEGER J, L, IR, I, INDXT

	DO 11 J=1, N
	   INDX(J) = J
11	CONTINUE

	L = N/2 + 1
	IR = N
	
10	CONTINUE
	  IF(L.GT.1)THEN
	    L = L-1
	    INDXT = INDX(L)
	    Q = ARRIN(INDXT)
	  ELSE
	    INDXT = INDX(IR)
	    Q = ARRIN(INDXT)
	    INDX(IR) = INDX(1)
	    IR = IR-1
	    IF(IR.EQ.1)THEN
	      INDX(1)=INDXT
	      RETURN
	    ENDIF
	  ENDIF

	  I=L
	  J = L+L
20	  IF(J.LE.IR)THEN
	    IF(J.LT.IR)THEN
	      IF(ARRIN(INDX(J)).LT.ARRIN(INDX(J+1))) J=J+1
	    ENDIF
	    IF(Q.LT.ARRIN(INDX(J)))THEN
	      INDX(I) = INDX(J)
	      I=J
	      J=J+J
	    ELSE
	      J=IR+1
	    ENDIF
	  GO TO 20
	  ENDIF
	  INDX(I) = INDXT
	GO TO 10
	END
C+
	subroutine conflim(m,n,x,chs,idx,dxl,xmed,dxh)
	
	implicit none
	
	integer m,n
	real*8 x(n)
	real*8 chs(n)
	integer idx(n)
c	integer idx2(n)
	integer ilo,ihi,mid
	real*8 dxl,xmed,dxh,xmax,xmin
	
	integer i
	integer nw
	real*8 wrk(n)
c	real*8 wrk2(n)
	real*8 xsrt(n)
	
c  ***	Given an array x from a Monte Carlo simulation, 
c	determine median value and give lower and upper error
c	bars at 15.85 and 84.15 percentile limits. These enclose
c	the central 68.3 percent of the probability distribution
c	projected on to the parameter of interest.
	
c  ***	Sort the data in order of increasing data value. Exclude
c	the first m elements.
	nw = 0
	do i = m+1, n
	   nw  = nw + 1
	   wrk(nw) = x(i)
	end do
	call indexx(nw,wrk,idx)
	
c  ***	Array xsrt contains the x values in ascending order.
	do i= 1,nw
	  xsrt(i) = wrk(idx(i))
	end do
	
	ilo = nint(0.1585d0*dfloat(nw))
	ihi = nint(0.8415d0*dfloat(nw))
	mid = nint( nw / 2d0 ) 
		
	xmed = xsrt(mid)
	xmin = xsrt(ilo)
	xmax = xsrt(ihi)

	dxl = xmed - xmin
	dxh = xmax - xmed

c  ***	Sort the data in order of increasing data value. Exclude
c	the first m elements.
c	nw = 0
c	do i = m+1, n
c	   nw  = nw + 1
c	   wrk2(nw) = chs(i)
c	end do
c	call indexx(nw,wrk2,idx2)
	
c  ***	Pick out the maximum-likelihood value as the one corresponding	
c	to the lowest chi-squared.

c	xml = wrk2(idx(1))
	
	   
999	continue

	end
C+	
	
C+
	subroutine correl(nb,n,x,m,c,cl)
	
c  ***	Computes the autocorrelation function c and correlation 
c	length cl of a Markov chain x. 
c	Recipe is from Tegmark et al 2004.
	
	implicit none
	
	integer nb, n
	real*8 x(n)
	integer m
	real*8 c(m)
	integer i,j, ioff
	real*8 si, sii, sij
	real*8 aii, aij
	real*8 xbar,numer,denom
	real*8 cl
	real*8 half, clo, chi, f, llo, lhi
	integer jlo, jhi
	
c	Exclude first nb points generated during burn-in
	si = 0d0
	do i = nb+1, n
	   si = si + x(i)
	end do
	xbar = si / dfloat(n-nb)
	
	sii = 0d0
	do i = nb+1, n
	   sii = sii + (x(i)-xbar)*(x(i)-xbar)
	end do
	aii = sii / dfloat(n-nb)
	denom = aii
	
	if ( denom .ge. 1d-20 ) then

	   do j = 1, m
	      sij = 0d0
	      ioff = j - 1
	      do i = nb+1, n - ioff
	 	 sij = sij + (x(i)-xbar)*(x(i+ioff)-xbar)
	      end do
	      aij = sij / dfloat(n-nb-ioff)
	      numer = aij 
	      c(j) = numer / denom
	   end do
	   
c  ***	   Determine where correlation function drops below 0.5
	   half = 0.5d0
	   jlo = 1
	   call huntdp(c,m,half,jlo)

c  ***	   Interpolate to get correlation length
	   if(jlo.ge.m)then
	      cl = -dfloat(m)
	      print *,'DBG: CORREL ERROR: m insufficient to determine cl.'
c	      print *,'Dumping correlation function:'
c	      do j = 1, m
c	 	 print *,j-1,c(j)
c	      end do
	   else if ( jlo.lt.1 ) then
	      cl = 0d0
	      print *,'DBG: CORREL ERROR: zero correlation length!.'
	   else
	      jhi = jlo + 1
	      clo = c(jlo)
	      chi = c(jhi) 
	      f = ( half - clo ) / ( chi - clo )
	      llo = dfloat(jlo)
	      lhi = dfloat(jlo+1)
	      cl = llo + ( lhi - llo ) * f
	   end if
	
	else
c  ***	   If we got to here, the variance is zero so return
c	   an obviously silly correlation length.
	   cl = -99d0
	end if
	
	end
C+
	subroutine wrparms2(lu,parfile,t0,period,dmag,wday,k1,vsi,
     :	             sigt0,sigper,sigdm,sigw,sigvk,sigvs,
     :	             mstar,sigms,c1,c2,c3,c4)

     	implicit none
	
	integer lu
	character*80 parfile
	real*8 t0,period,dmag,wday,k1,vsi
	real*8 sigt0,sigper,sigdm,sigw,sigvk,sigvs
c  ***	Limb-darkening coeffs from Claret (2000)
	real*8 mstar,sigms
	real*8 c1, c2, c3, c4
	
c  ***	Read basic parameters derived by HUNTER
	call unlink(parfile)
	open(unit=lu,file=parfile,form='formatted',status='new')
	write(lu,*)t0,period,dmag,wday,k1,vsi
	write(lu,*)sigt0,sigper,sigdm,sigw,sigvk,sigvs
	write(lu,*)mstar,sigms
	write(lu,*)c1,c2,c3,c4
	close(lu)
	
	end
C+	
	subroutine probs(n,rpjup,rstar,mstar,b,prp,prs,prb)
	
	implicit none
	
	integer n, i, jlo
	real*8 rpjup(n)	
	real*8 rstar(n)	
	real*8 mstar(n)	
	real*8 b(n)	
	real*8 frac
	real*8 prp,prs,prb	
	
c  ***	Determine fraction of trials that yield Rp < 1.5 Rj
	jlo = 0
	do i = 1, n
	   if ( rpjup(i) .lt. 1.5d0 ) jlo = jlo + 1
	end do
	
	prp = dfloat(jlo)/dfloat(n)
	
c  ***	Determine fraction of trials that yield rs < 1.2 ms^0.8
	jlo = 0
	do i = 1, n
	   frac = rstar(i) / mstar(i)**0.8d0
	   if ( frac .lt. 1.2d0 ) jlo = jlo + 1
	end do
	prs = dfloat(jlo)/dfloat(n)
	
c  ***	Determine fraction of trials that yield b < 0.8
	jlo = 0
	do i = 1, n
	   if ( b(i) .lt. 0.8d0 ) jlo = jlo + 1
	end do
	prb = dfloat(jlo)/dfloat(n)
	
	end
C+
	function funk(x)
	
c  ***	Wrapper for physparms and eval, for use in amoeba.

	implicit none
	
	real*8 x(11)
	real*8 t,p,wf,ms,d,b,vk
	real*8 vcosl,vsinl,vs,lam
	real*8 ecosw,esinw,ecc,om
	real*8 rs,rh,aau,rstar,rp,cosi,deginc,mpjup,rpjup,gam
	real*8 sdp1,cx1,cxx1,cy1,cyy1,fs1
	real*8 sdp2,cx2,cxx2,cy2,cyy2,fs2
	real*8 sdp3,rmp31,rmp32,fs3
	real*8 sdp4,rmp41,rmp42,fs4

	integer maxobs
	parameter(maxobs=30000)
	integer maxrv
	parameter(maxrv = 200)
	integer maxsecl
	parameter(maxsecl = 10000)
	integer mxpset
	parameter(mxpset = 20)
	integer mxvset
	parameter(mxvset = 5)
	integer mxsset
	parameter(mxsset = 4)
		
	real*8 ms0,sigms
	logical constrainmr
	integer ndata,npset
	real*8 hjd(maxobs)
	real*8 mag(maxobs)
	real*8 err(maxobs)
	real*8 mu(maxobs)
	integer fobs(mxpset)
	integer lobs(mxpset)
	integer np(mxpset)
	real*8 adj(mxpset)
	real*8 c1(mxpset),c2(mxpset),c3(mxpset),c4(mxpset)
	
	integer nvel,nvset
	integer fvel(mxvset)
	integer lvel(mxvset)
	real*8 gadj(mxvset)
	
	real*8 hjdrv(maxrv)
	real*8 rv(maxrv)
	real*8 rverr(maxrv)
	real*8 nu(maxrv)
	
	integer nsecl,nsset
	integer isset
	integer firpt(mxsset)
	integer lirpt(mxsset)
	integer nirpt(mxsset)
	integer ichan(mxsset)
	
	real*8 dvjit
	real*8 chph,chsp,chse,funk
	real*8 varscl(mxpset)
	real*8 varscv(mxvset)
	real*8 pi
	
	real*8 fs(mxsset)
	real*8 sdp(mxsset)
	real*8 rmp1(mxsset)
	real*8 rmp2(mxsset)
	real*8 cx(mxsset)
	real*8 cxx(mxsset)
	real*8 cy(mxsset)
	real*8 cyy(mxsset)

	real*8 hjdsecl(maxsecl)
	real*8 flxsecl(maxsecl)
	real*8 errsecl(maxsecl)
	real*8 xsecl(maxsecl)
	real*8 ysecl(maxsecl)

	
	common	/coeffs/	c1,c2,c3,c4
	common	/masses/	ms0,sigms
	common	/constr/	constrainmr
	common	/phodim/	ndata,npset
	common	/hjdpho/	hjd	
	common	/datpho/	mag	
	common	/errpho/	err
	common	/part01/	fobs	
	common	/part02/	lobs
	common	/np/		np
	common	/rvedim/	nvel,nvset
	common	/hjdrve/	hjdrv
	common	/datrve/	rv
	common	/errrve/	rverr
	common	/jitter/	dvjit
	common	/adjust/	adj
	common	/part03/	fvel
	common	/part04/	lvel
	common	/gadjst/	gadj
	common	/mu/	mu
	common	/nu/	nu
	common	/secdim/	nsecl,nsset
	common	/part05/	firpt
	common	/part06/	lirpt
	common	/part07/	ichan
	common	/nirpt/	nirpt
	common	/hjdsecl/	hjdsecl	
	common	/flxsecl/	flxsecl	
	common	/errsecl/	errsecl	
	common	/xsecl/		xsecl	
	common	/ysecl/		ysecl	
	common	/sdp/		sdp
	common	/rmp1/		rmp1
	common	/rmp2/		rmp2
	common	/cx/		cx
	common	/cxx/		cxx
	common	/cy/		cy
	common	/cyy/		cyy
	common	/fs/		fs
	
	pi = 4d0*atan2(1d0,1d0)
	
	t = x(1)
	p = x(2)
	wf = exp(x(3))
	ms = exp(x(4))
	d = exp(x(5))
	vk = exp(x(6))
	vcosl = x(7)
	b = 1d0 - 1d0/(1d0 + exp(x(8)))
	vsinl = x(9)
	ecosw = x(10)
	esinw = x(11)
	
	if(nsset.ge.1)then
	   do isset = 1, nsset
	      if(ichan(isset).eq.1)then
	         fs1 = fs(isset)
	         sdp1 = sdp(isset)	
	         cx1 = cx(isset)
	         cxx1 = cxx(isset)
	         cy1 = cy(isset)
	         cyy1 = cyy(isset)
	      else if(ichan(isset).eq.2)then
	         fs2 = fs(isset)
	         sdp2 = sdp(isset)	
	         cx2 = cx(isset)
	         cxx2 = cxx(isset)
	         cy2 = cy(isset)
	         cyy2 = cyy(isset)
	      else if(ichan(isset).eq.3)then
	         fs3 = fs(isset)
	         sdp3 = sdp(isset)	
	         rmp31 = rmp1(isset)
	         rmp32 = rmp2(isset)
	      else if(ichan(isset).eq.4)then
	         fs4 = fs(isset)
	         sdp4 = sdp(isset)	
	         rmp41 = rmp1(isset)
	         rmp42 = rmp2(isset)
	      end if
	   end do
	end if
		
c  ***	Convert proposal parameters to physical parameters

	call physparms(p,wf,ms,d,b,vk,ecosw,esinw,
     :	               rs,rh,aau,rstar,rp,
     :	               cosi,deginc,mpjup,rpjup,ecc,om,
     :	               vcosl,vsinl,vs,lam)
	
c  ***	Generate model from parameters; fit to data and compute
c	penalty function chisq(k)

	call eval(t,p,ms,rs,rp,cosi,
     :	          vk,vs,gam,lam,rstar,ecc,om,
     :	          sdp1,cx1,cxx1,cy1,cyy1,fs1,
     :	          sdp2,cx2,cxx2,cy2,cyy2,fs2,
     :	          sdp3,rmp31,rmp32,fs3,
     :	          sdp4,rmp41,rmp42,fs4,
     :	          c1,c2,c3,c4,ms0,sigms,constrainmr,mu,nu,
     :	          ndata,npset,np,fobs,lobs,adj,hjd,mag,err,
     :	          nvel,nvset,fvel,lvel,
     :	          gadj,hjdrv,rv,rverr,dvjit,
     :	          nsecl,nsset,nirpt,firpt,lirpt,ichan,
     :	          hjdsecl,flxsecl,errsecl,xsecl,ysecl,
     :	          chph,chsp,chse,funk,varscl,varscv)

	end	
C+
      SUBROUTINE amoeba(p,y,mp,np,ndim,ftol,funk,iter)
      INTEGER iter,mp,ndim,np,NMAX,ITMAX
      DOUBLE PRECISION ftol,p(mp,np),y(mp),funk
      PARAMETER (NMAX=20,ITMAX=5000)
      EXTERNAL funk
CU    USES amotry,funk
      INTEGER i,ihi,ilo,inhi,j,m,n
      DOUBLE PRECISION rtol,sum,swap,ysave,ytry,psum(NMAX),amotry
      iter=0
1     do 12 n=1,ndim
        sum=0.d0
        do 11 m=1,ndim+1
          sum=sum+p(m,n)
11      continue
        psum(n)=sum
12    continue
2     ilo=1
      if (y(1).gt.y(2)) then
        ihi=1
        inhi=2
      else
        ihi=2
        inhi=1
      endif
      do 13 i=1,ndim+1
        if(y(i).le.y(ilo)) ilo=i
        if(y(i).gt.y(ihi)) then
          inhi=ihi
          ihi=i
        else if(y(i).gt.y(inhi)) then
          if(i.ne.ihi) inhi=i
        endif
13    continue
      rtol=2.d0*abs(y(ihi)-y(ilo))/(abs(y(ihi))+abs(y(ilo)))
      if (rtol.lt.ftol) then
        swap=y(1)
        y(1)=y(ilo)
        y(ilo)=swap
        do 14 n=1,ndim
          swap=p(1,n)
          p(1,n)=p(ilo,n)
          p(ilo,n)=swap
14      continue
        return
      endif
c      if (iter.ge.ITMAX) pause 'ITMAX exceeded in amoeba'
      if (iter.ge.ITMAX) return
      iter=iter+2
      ytry=amotry(p,y,psum,mp,np,ndim,funk,ihi,-1.0d0)
      if (ytry.le.y(ilo)) then
        ytry=amotry(p,y,psum,mp,np,ndim,funk,ihi,2.0d0)
      else if (ytry.ge.y(inhi)) then
        ysave=y(ihi)
        ytry=amotry(p,y,psum,mp,np,ndim,funk,ihi,0.5d0)
        if (ytry.ge.ysave) then
          do 16 i=1,ndim+1
            if(i.ne.ilo)then
              do 15 j=1,ndim
                psum(j)=0.5d0*(p(i,j)+p(ilo,j))
                p(i,j)=psum(j)
15            continue
              y(i)=funk(psum)
            endif
16        continue
          iter=iter+ndim
          goto 1
        endif
      else
        iter=iter-1
      endif
      goto 2
      END
C  (C) Copr. 1986-92 Numerical Recipes Software &H1216.
C+
      FUNCTION amotry(p,y,psum,mp,np,ndim,funk,ihi,fac)
      INTEGER ihi,mp,ndim,np,NMAX
      DOUBLE PRECISION amotry,fac,p(mp,np),psum(np),y(mp),funk
      PARAMETER (NMAX=20)
      EXTERNAL funk
CU    USES funk
      INTEGER j
      DOUBLE PRECISION fac1,fac2,ytry,ptry(NMAX)
      fac1=(1.d0-fac)/ndim
      fac2=fac1-fac
      do 11 j=1,ndim
        ptry(j)=psum(j)*fac1-p(ihi,j)*fac2
11    continue
      ytry=funk(ptry)
      if (ytry.lt.y(ihi)) then
        y(ihi)=ytry
        do 12 j=1,ndim
          psum(j)=psum(j)-p(ihi,j)+ptry(j)
          p(ihi,j)=ptry(j)
12      continue
      endif
      amotry=ytry
      return
      END
C  (C) Copr. 1986-92 Numerical Recipes Software &H1216.
	
       FUNCTION JULDAY(MM,ID,IYYY)
       PARAMETER(IGREG=15+31*(10+12*1582))

       IF(IYYY.EQ.0) PAUSE 'There is no Year Zero.'
       IF(IYYY.LT.0)IYYY=IYYY+1
       IF(MM.GT.2) THEN
          JY = IYYY
          JM = MM+1
       ELSE
          JY = IYYY-1
          JM = MM+13
       ENDIF
       JULDAY=INT(365.25*JY) + INT(30.6001*JM) + ID + 1720995
       IF(ID+31*(MM+12*IYYY).GE.IGREG) THEN
          JA = INT(0.01*JY)
          JULDAY = JULDAY+2-JA+INT(0.25*JA)
       ENDIF
       RETURN
       END

C+
        subroutine kepler(jdate,tperi,period,ecc,omega,kvel,sini,
     :	vrad,rau,alpha,xp,yp)

c       Computes radial velocity in centre-of-mass frame at a given
c       date. Omega should be expressed in radians.

        implicit none

        double precision jdate,tperi,period
        double precision ecc,omega,kvel,sini
        double precision meananom,nu,vrad,tanhalfecc,eccanom,rau,alpha
	double precision xp,yp
        double precision pi

        double precision truanom

        pi=4.*atan2(1.,1.)

        meananom = 2.*pi*dmod(jdate-tperi,period)/period
        nu = truanom(meananom,ecc)
        vrad = kvel*(ecc*cos(omega) + cos(nu+omega))

        tanhalfecc = tan(nu/2.)*sqrt((1.-ecc)/(1.+ecc))
	eccanom = 2.*atan(tanhalfecc)
	
c  ***	Hence get instantaneous planet-star distance 
c	in units of major semi-axis:
	rau = 1. - ecc*cos(eccanom)

c  ***	Compute planet-star-observer phase angle
	alpha = acos(sini*cos(nu + omega - pi/2.))
	
	xp = rau * sin(nu + omega - pi/2.)
	yp = rau * cos(nu + omega - pi/2.)

        end
C+
        function meananom(t_anom,ecc)

c  ***   Inputs are true anomaly (nu) in radians and eccentricity.
c  ***   Output is mean anomaly (M) in radians.

c  ***   ACC at St Andrews 7/03/02.

        implicit none
	
	double precision t_anom,ecc
        double precision tanhalfecc,eccanom,meananom

        tanhalfecc = tan(t_anom/2.)*sqrt((1.-ecc)/(1.+ecc))
	eccanom = 2.*atan(tanhalfecc)

        meananom = eccanom-ecc*sin(eccanom)

        end 
C+
        function truanom(m_anom,ecc)

c  ***   iterative solution to Kepler's equation.
c  ***   Inputs are mean anomaly (M) in radians and eccentricity.
c  ***   Output is true anomaly (nu) in radians.

c  ***   ACC at St Andrews 16/11/99.
c  ***   Converted to f77 by ACC at St Andrews 25/06/00.

        implicit none
        double precision m_anom, ecc, guess, eccanom, tanhalfnu, truanom
        integer its

c  ***   Initial guess

        guess = m_anom
        truanom = 1.7e38
        its = 0

c  ***   Iterate till truanom converges to within 1 arcsec

        do its = 1, 101
           eccanom = m_anom + ecc * sin(guess)
           if(abs(eccanom - guess) .lt. 1./206265.) go to 1
           guess = eccanom
           if(its .gt. 100) pause 'Not converged'
        end do

1       tanhalfnu = sqrt((1.+ecc)/(1.-ecc))*tan(eccanom/2.)
        truanom = atan(tanhalfnu)*2.

        end 
C+
        function glambert( alpha )
c       alpha in radians

	implicit none
        double precision pi, alpha, glambert

        pi = 4. * atan2(1.,1.)

        glambert = ( sin( alpha ) + ( pi - alpha ) * cos( alpha ) ) / pi

        return
        end
C+
        function gvenus( alpha )
c       alpha in radians

	implicit none
        double precision alpha, pi, factor, x, dmag, gvenus

        pi = 4. * atan2(1.,1.)
        factor = 180. / pi / 100.

        x = alpha * factor
        dmag = x * ( 0.09 + x * ( 2.39 - x * 0.65 ) )
        gvenus = 10.**(-0.4*dmag)

        return
        end

C+
	subroutine secmod(t0,period,rp,rs,cosi,vk,vsi,lam,ecc,om,
     :	                  ndata,hjd,z,eta)
	

c  ***	Calculates the fraction eta of the secondary's disk
c	that is visible as a function of orbital phase

	implicit none
	
	integer ndata
	real*8 t0, period, rp, rs, cosi, vk, vsi, lam, ecc, om
	real*8 hjd(ndata) 
	real*8 z(ndata)
	real*8 eta(ndata)
	real*8 nu(ndata)
	real*8 rau(ndata)
	real*8 alpha(ndata)
	real*8 pi,p,ph,zp
	real*8 xp(ndata)
	real*8 yp(ndata)
	integer i
	real*8 sini
	real*8 nutrans,mtrans,meananom,dttrans,tperi
	
	pi = 4d0*atan2(1d0,1d0)
	
c  ***	Compute date of periastron relative to transit time t0	
	nutrans = pi/2d0 - om
	mtrans = meananom(nutrans,ecc)
	dttrans = mtrans/2d0/pi * period
	tperi = t0 - dttrans

c  ***	Ratio of radii
	p = rp / rs
	
	sini = sqrt(1d0 - cosi*cosi)

	do i = 1, ndata

           call kepler(hjd(i),tperi,period,ecc,om,vk,sini,
     :	              nu(i),rau(i),alpha(i),xp(i),yp(i))

c  ***	   Determine orbital phase
c	   ph = mod(hjd(i)-t0,period)/period
	
c  ***	   Coordinates of planet in orbital plane ( a = 1 )
c	   xp = sin ( 2d0 * pi * ph )
c	   yp = cos ( 2d0 * pi * ph )

c  ***	   Rotate through 90-i about X axis to get zp
	   zp = - yp(i) * cosi
		
c  ***	   Impact parameter in units of primary radius. Note
c	   constraint on yp has opposite sign from that in 
c	   transmod; we're interested in secondary eclipse here.
	   if ( cos(alpha(i)) .lt. 0d0 ) then
	      z(i) = rau(i) * sin(alpha(i)) / rs
	   else
	      z(i) = 1d0 / rs
	   end if
c	   print *,xp(i),yp(i),zp,alpha(i),rau(i)/rs,z(i)
	end do
	
	
c  ***	Compute secondary transit shape model for uniformly
c	illuminated secondary
	call secvis(p,ndata,z,eta)
	
	end
C+	
	subroutine secvis(p,nz,z,eta)

c  ***	Calculates the fraction eta of the secondary's disk
c	that is visible as a function of orbital phase. The 
c	function takes on values ranging from 0 (fully occulted)
c	to 1 (fully visible)

	implicit none
	
	real*8 p
	integer nz
	real*8 z(nz)
	real*8 eta(nz)
	real*8 pi,pp,zz,cosa,cosb,a,b,sina,sinb
	integer i

	pi = 4d0*atan2(1d0,1d0)
	
	pp = p*p
	do i = 1, nz
	   if( abs(z(i)) .ge. 1d0 + p )then
	      eta(i) = 1d0
	   else if(abs(z(i)) .le. 1d0 - p )then
	      eta(i) = 0d0
	   else
	      zz = z(i)*z(i)
	      cosa = ( 1d0 - pp + zz ) / 2d0 / z(i)
	      cosb = ( 1d0 - pp - zz ) / 2d0 / z(i) / p
	      a = acos(cosa)
	      b = acos(cosb)
	      sina = sqrt(1d0 - cosa*cosa)
	      sinb = sqrt(1d0 - cosb*cosb)
	      eta(i) = ( b - cosb*sinb + ( a - cosa*sina ) / pp ) / pi
	   end if
	end do
	
	end
