	include 'limit.inc'
	integer maxrv
	parameter(maxrv = 100)
	
	real*8 c1,c2,c3,c4
	real*8 ms0,sigms
	logical constrainmr
	integer ndata,npset
	real*8 hjd(maxobs)
	real*8 mag(maxobs)
	real*8 err(maxobs)
	integer fobs(100)
	integer lobs(100)
	real*8 adj(100)
	integer nvel
	real*8 hjdrv(maxrv)
	real*8 rv(maxrv)
	real*8 rverr(maxrv)
	real*8 dvjit
	real*8 pi
	
	common	/coeffs/	c1,c2,c3,c4
	common	/masses/	ms0,sigms
	common	/constr/	constrainmr
	common	/phodim/	ndata,npset
	common	/hjdpho/	hjd	
	common	/datpho/	mag	
	common	/errpho/	err
	common	/part01/	fobs	
	common	/part01/	lobs
	common	/rvedim/	nvel
	common	/hjdrve/	hjdrv
	common	/datrve/	rv
	common	/errrve/	rverr
	common	/jitter/	dvjit
	common	/adjust/	adj
