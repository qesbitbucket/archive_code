C+	
	program mcmc_newecc
	
c  ***	This is Andrew Cameron's parameter-fitting code for
c	transiting exoplanets and low-mass stars in eccentric 
c	orbits. 

c  ***	History:
c	Based on mcmc_newformat.f
c	Adapted 24-Aug-2007 to work with eccentric orbits.

c  ***	This version reads the new hunter data format.
c  ***	Important: please read the blurb at line 710
	
c  ***	Recent History:

c	v1.3 2007-09-04	ACC at St A 
c	Calculate and report opposition flux ratio for unit albedo,
c	synchronisation timescales for star and planet, Log g for
c	star and planet, planet density in jovian units.

c	v1.4 2007-09-05	ACC at St A 
c	Reduce initial guesstimate of transit epoch uncertainty from
c	5 min to 2 min when setting starting parameters.
c	This reduces the likelihood of jumping too far during burn-in.
	
c	v1.5 2007-09-18	ACC at St A 
c	Report planet/star blackbody equilibrium temperature ratio.

	implicit none
	
	real*8 pi
	
	integer maxobs
	parameter(maxobs=30000)
	real*8 hjd(maxobs)
	real*8 mag(maxobs)
	real*8 err(maxobs)
	integer ndata
	
	integer maxrv
	parameter(maxrv = 100)
	real*8 hjdrv(maxrv)
	real*8 rv(maxrv)
	real*8 rverr(maxrv)
	integer nvel
	
	real*8 t0,period,dmag,wday,k1,vsi
	real*8 sigt0,sigper,sigdm,sigw,sigvk,sigvs
	real*8 sigb, siglam
	real*8 sigecc, sigom
c  ***	Limb-darkening coeffs from Claret (2000)
	real*8 c1, c2, c3, c4
	
	integer i, j, k, idum, jtemp, ktemp
	integer maxjmp
	parameter(maxjmp=100000)
	integer njump
	real*8 t(maxjmp)
	real*8 p(maxjmp)
	real*8 rs(maxjmp)
	real*8 ms(maxjmp)
	real*8 rp(maxjmp)
	real*8 b(maxjmp)
	real*8 d(maxjmp)
	real*8 wf(maxjmp)
	real*8 vk(maxjmp)
	real*8 vs(maxjmp)
	real*8 gam(maxjmp)
	real*8 lam(maxjmp)
	real*8 ecc(maxjmp)
	real*8 om(maxjmp)
	real*8 rh(maxjmp)
	real*8 chisq(maxjmp)
	real*8 chsp(maxjmp)
	real*8 chph(maxjmp)
	real*8 mpjup(maxjmp)
	real*8 rpjup(maxjmp)
	real*8 rstar(maxjmp)
	real*8 aau(maxjmp)
	real*8 deginc(maxjmp)
	integer idx(maxjmp)
	real*8 dxl,xml,dxh
	real*8 sigms,ms0,sigwf
c	real*8 mstar
	real*8 cosi
	real*8 sini
c	real*8 psec,akm,q,vt,sini
	real*8 scfac, oldscfac
c	real*8 dchisq,pjump,prob
	character*80 photfile,rvfile
c	character*80 parfile
	integer lu
	integer mxobs,mxvel
c	real*8 x,u,w,sx,su,sw,sxx,sxu,suu,xhat,uhat
c	integer nx
c	integer status
	real*8 wfrac,hw1,hw2
c	integer norb
	real*8 chsmin,t0best,pbest,wfbest,dbest,vkbest,vsbest,bbest
	real*8 rpjbest,gambest, lambest
	real*8 eccbest,ombest
	real*8 chsbst,chpbst
	real*8 b0,lam0
	real*8 e0,om0
	real*8 rsbest,rpbest
c	real*8 wbest
	logical constrainmr
c	real*8 cphi,sphi
	character*1 yesno
	integer pset, npset
	integer fobs(100)
	integer lobs(100)
	integer np(100)
	real*8 adj(100)
	real*8 varscl(100)
	integer nmod
	real*8 ph,pr,xp,yp,zp,up
	real*8 z(1000),mu(1000),nu(1000),alpha(1000),rau(1000)
	logical radvel
c	logical burned_in
	real*8 dvjit
	character*26 swaspid
	integer maxpk
	parameter(maxpk=5)
	integer ipk
	real*8 eppk(maxpk)
	real*8 ppk(maxpk)
	real*8 wdpk(maxpk)
	real*8 dmpk(maxpk)
	real*8 dcpk(maxpk)
	real*8 snrpk(maxpk)
	integer ntrpk(maxpk)
     	real*8 aellpk(maxpk)
	real*8 snellpk(maxpk)
	real*8 transfpk(maxpk)
	real*8 gaprpk(maxpk)
	real*8 dchsantpk(maxpk)
	real*8 jh,rpmj,drpmj
	real*8 rs0
c	real*8 prp,prs
	real*8 dcmax
	integer ipmax
c	real*8 prat
c	real*8 dch_rs,dch_rp
 	real*8 t0best_mc
  	real*8 pbest_mc 
  	real*8 msbest_mc
  	real*8 wfbest_mc
  	real*8 dbest_mc 
  	real*8 vkbest_mc
  	real*8 vsbest_mc
  	real*8 bbest_mc 
  	real*8 lambest_mc
  	real*8 eccbest_mc
  	real*8 ombest_mc
	real*8 gambest_mc
	real*8 dincbest,rstbest,mstbest
	integer iday(3), id, mm, iyyy, jdtrunc
	real*8 hjdtoday,tnow(maxjmp)
	integer norbnow
c	real*8 t2, t1
	integer mcorr
	integer mxcorr
	parameter(mxcorr = 100)
	real*8 c_t(mxcorr)
	real*8 c_p(mxcorr)
	real*8 c_d(mxcorr)
	real*8 c_w(mxcorr)
	real*8 c_b(mxcorr)
	real*8 c_ms(mxcorr)
	real*8 c_vs(mxcorr)
	real*8 c_vk(mxcorr)
	real*8 c_lam(mxcorr)
	real*8 c_ecc(mxcorr)
	real*8 c_om(mxcorr)
	real*8 cl_t,cl_p,cl_d,cl_w,cl_b,cl_ms,cl_vk,cl_vs
	real*8 cl_lam,cl_ecc,cl_om
	logical reject
	real*8 ratio
	integer kst, ken
c	double precision st,sp,sm,sw,sd,sk,sv,sb
c	double precision stt,spp,smm,sww,sdd,skk,svv,sbb
	integer nburn
	real*8 prp,prs,prb
	integer strikes
	real*8 xfunk(11)
	real*8 funk
	real*8 yval(12)
	integer indy(12)
	real*8 parms(12,11)
	real*8 ftol
	integer iter
	integer kbest
	real*8 chisqbest,chphbest,chspbest
	real*8 dchisq_mr
	real*8 rhobest,aaubest,mpjbest
	real*8 chisq_con,chisq_unc
	real*8 cosl, sinl
	real*8 sigma_t0, sigma_p, sigma_w,sigma_d
        real*8 sigma_rst,sigma_mst,sigma_rp
	real*8 jd
	real*8 nutrans,mtrans,dttrans,meananom,tperi
	real*8 tanhalfecc,eccanom,rtrans

	real*8 rpsq(maxjmp)
	real*8 qrat,gacc_p,gacc_s
	real*8 tau_s(maxjmp)
	real*8 tau_p(maxjmp)
	real*8 logg_s(maxjmp)
	real*8 logg_p(maxjmp)
	real*8 rho_p(maxjmp)
	real*8 teql_p(maxjmp)
	real*8 tjh
	
	common	/coeffs/	c1,c2,c3,c4
	common	/masses/	ms0,sigms
	common	/constr/	constrainmr
	common	/phodim/	ndata,npset
	common	/hjdpho/	hjd	
	common	/datpho/	mag	
	common	/errpho/	err
	common	/part01/	fobs	
	common	/part01/	lobs
	common	/rvedim/	nvel
	common	/hjdrve/	hjdrv
	common	/datrve/	rv
	common	/errrve/	rverr
	common	/jitter/	dvjit
	common	/adjust/	adj
	
	external funk, time
	integer julday, time

	pi = 4d0*atan2(1d0,1d0)
	idum = time()
	idum = mod(idum,50000)
		
	pset = 0
	ndata = 0

	print *,' Enter filename with LCFINDER photometry : '
	read(*,'(a)') photfile
	
c  ***	Read photometric data from LCFINDER output file	
c	photfile = 'phot.dat'
	lu = 42
	mxobs = maxobs
	pset = pset + 1
	fobs(pset) = ndata+1
	call readnewlc(lu,photfile,
     :	           swaspid,jh,rpmj,drpmj,
     :	           eppk,ppk,wdpk,dmpk,dcpk,snrpk,ntrpk,
     :	           aellpk,snellpk,transfpk,gaprpk,dchsantpk,
     :	           mxobs,np(pset),
     :	           hjd(ndata+1),mag(ndata+1),err(ndata+1))
	ndata = ndata + np(pset)
	lobs(pset) = ndata
	     
	print *,' Read another photometric dataset?(Y/N) [N] : '
	read(*,'(a)') yesno
	if ( yesno(1:1).eq.'y' .or. yesno(1:1).eq.'Y' ) then
	   go to 12
	else
	   go to 13
	end if
	
12	print *,' Enter filename with ancillary photometry : '
	read(*,'(a)') photfile
	
c  ***	Read photometric data from file	
c	photfile = 'phot.dat'
	lu = 42
	pset = pset + 1
	fobs(pset) = ndata+1
	call rddata(lu,photfile,mxobs,np(pset),
     :	hjd(ndata+1),mag(ndata+1),err(ndata+1))
	ndata = ndata + np(pset)
	lobs(pset) = ndata
	
	print *,' Read another photometric dataset?(Y/N) [N] : '
	read(*,'(a)') yesno
	if ( yesno(1:1).eq.'y' .or. yesno(1:1).eq.'Y' ) then
	   go to 12
	end if
	
13	npset = pset
	
c  ***	Read radial velocity data from file
	print *,' Enter filename with radial velocity data [none]: '
	read(*,'(a)') rvfile
	if ( rvfile .eq. ' ' .or. rvfile.eq.'none' 
     :	 .or. rvfile.eq.'NONE' ) then
     	   radvel = .false.
	   nvel = 0
	else
	   lu = 44
	   mxvel = maxrv
	   call rddata(lu,rvfile,mxvel,nvel,hjdrv,rv,rverr)
	   radvel = .true.
	   print *,'Enter jitter in km/sec : '
	   read(*,*) dvjit
	end if
	
c  ***	Read J-H colour (no longer needed as J-H is now obtained 
c	by readnewlc from the header of the .lc file.

c	print *,' Enter 2MASS J-H colour : '
c	read(*,*) jh

c  ***	Estimate stellar parameters from J-H
	call getparms(jh,rs0,ms0,c1,c2,c3,c4)
	
	print *,' You may either let the data determine '
	print *,' the stellar radius, or impose a main '
	print *,' sequence mass-radius relation if the '
	print *,' S:N of the photometry is poor.' 
	print *, '  '
11	print *,' Impose main-sequence mass-radius relation?(Y/N)[N] : '
	read(*,'(a)') yesno
	if ( yesno(1:1).eq.'y' .or. yesno(1:1).eq.'Y'  ) then
	   constrainmr = .true. 
	   print *,'Main sequence radius constraint ON'
	else if ( yesno(1:1).eq.'n' .or. yesno(1:1).eq.'N' 
     :	.or. yesno(1:1).eq.' ') then
	   constrainmr = .false.
	   print *,'Main sequence radius constraint OFF'
	else
	   go to 11
	end if
	
	print *,' Enter number of iterations during burn-in: '
	read(*,*) nburn
	
	print *,' Enter number of iterations after burn-in: '
	read(*,*) njump
	
	njump = njump + nburn
	
	dcmax = 0d0
	do ipk = 1, 5
	   print *,' Peak ',ipk,'  dchisq = ',dcpk(ipk)
	   dcmax = max(dcmax,dcpk(ipk))
	   if(dcmax .eq.dcpk(ipk)) ipmax = ipk
	end do
	
	print *,' Select HUNTER peak 1,2,3,4, 5 or 0 to define :'
	read(*,*) ipk
	
	if(ipk .lt.0 ) then
	   ipk = ipmax
	   print *, ' Automatically choosing peak ',ipk
	end if
	
	if(ipk.eq.0)then
	   print *,' Enter t0,p,wday,dmag : '
	   read(*,*) t0, period, wday, dmag
	else
	   t0 = eppk(ipk)
	   period = ppk(ipk)
	   wday = wdpk(ipk)
	   dmag = dmpk(ipk)
	end if
	
3	k1 = 0.1d0
	vsi = 5d0
	sigt0 = 2d0 / 60d0 /24d0  
	sigdm = 0.002d0
	sigw = 0.003d0
	sigvk = 0.0d0
	sigvs = 0.0d0
	siglam = 0.0d0
	sigms = ms0/10d0
	wfrac = wday / period
	sigwf = sigw / period
	hw1 = wfrac / 2d0
	hw2 = 1d0 - hw1

c  ***	Orthogonalise T0 and p by adjusting epoch of transit
c	to centre of mass of data taken in transit.

	call orthtp(ndata,hjd,err,nvel,hjdrv,rverr,
     :	                  t0,sigt0,period,sigper,hw1,hw2)

c  ***	Calculate number of orbits elapsed to current epoch

	call idate(iday)
	id = iday(1)
	mm = iday(2)
	iyyy = iday(3)
	jdtrunc = julday(mm,id,iyyy) - 2450000
	
	hjdtoday = dfloat(jdtrunc)-0.5d0
	
	
	norbnow = nint((hjdtoday-t0)/period)

c  ***	Compute initial estimate of K1
	call orthk1(nvel,hjdrv,rv,err,t0,period,k1,sigvk)

c  ***	Initialise counters

	k = 1
	ktemp = 0
	j = 1
	jtemp = 0
	scfac = .5d0

	b0 = 0.5d0
	sigb = 0.05d0

	lam0 = 0d0
	siglam = 0d0

	e0 = 0.05d0
	sigecc = 0.01d0
	
	om0 = 0d0
	sigom = 2d0 * pi / 360d0
	
c  ***	Define initial parameter set
	t(k) = t0
	p(k) = period
	wf(k) = wfrac
	ms(k) = ms0
	d(k) = dmag
	vk(k) = k1
	vs(k) = vsi
	lam(k) = lam0
	b(k) = b0
	ecc(k) = e0
	om(k) = om0
	
c  ***	Convert proposal parameters to physical parameters

	call physparms(p(k),wf(k),ms(k),d(k),b(k),vk(k),ecc(k),om(k),
     :	               rs(k),rh(k),aau(k),rstar(k),rp(k),
     :	               cosi,deginc(k),mpjup(k),rpjup(k))
	
c  ***	Generate model from parameters; fit to data and compute
c	penalty function chisq(k)

	call eval(t(k),p(k),ms(k),rs(k),rp(k),cosi,
     :	          vk(k),vs(k),gam(k),lam(k),rstar(k),
     :	          ecc(k),om(k),
     :	          c1,c2,c3,c4,ms0,sigms,constrainmr,
     :	          ndata,npset,fobs,lobs,adj,hjd,mag,err,
     :	          nvel,hjdrv,rv,rverr,dvjit,
     :	          chph(k),chsp(k),chisq(k),varscl)


c  ***	Adjust errors so that post-fit chisq ~ N -- commented out because
c	I want to defer this until we've got closer to the optimum solution; 
c	doing it here is premature and will lead to overestimation of the 
c	errors.

c	do pset = 1, npset
c	   do i = fobs(pset),lobs(pset)
c	      err(i) = err(i) * sqrt(varscl(pset))
c	   end do
c	   print *,pset,sqrt(varscl(pset))
c	end do

c  ***	Call eval again to readjust initial chisq
	call eval(t(k),p(k),ms(k),rs(k),rp(k),cosi,
     :	          vk(k),vs(k),gam(k),lam(k),rstar(k),
     :	          ecc(k),om(k),
     :	          c1,c2,c3,c4,ms0,sigms,constrainmr,
     :	          ndata,npset,fobs,lobs,adj,hjd,mag,err,
     :	          nvel,hjdrv,rv,rverr,dvjit,
     :	          chph(k),chsp(k),chisq(k),varscl)

c  ***	Start of main Markov-chain Monte Carlo loop
	chsmin = chisq(k)

	strikes = 0	
2	jtemp = 0

c  ***	Take another 100 jumps
	do ktemp = 1, 100
	
	   k = k + 1
	   
	   if( k.eq.400 ) then	   
c  ***	      By jump 400 we should be near enough to the optimum solution
c	      that it will be safe to scale the error bars on the data 
c	      and force post-fit chisq ~ N.

	      do pset = 1, npset
	 	 do i = fobs(pset),lobs(pset)
	 	    err(i) = err(i) * sqrt(varscl(pset))
	 	 end do
	 	 print *,pset,sqrt(varscl(pset))
	      end do

c  ***	      Call eval to rescale previous chisq
	      call eval(t(k-1),p(k-1),ms(k-1),rs(k-1),rp(k-1),cosi,
     :	 		vk(k-1),vs(k-1),gam(k-1),lam(k-1),rstar(k-1),
     :	 		ecc(k-1),om(k-1),
     :	 		c1,c2,c3,c4,ms0,sigms,constrainmr,
     :	 		ndata,npset,fobs,lobs,adj,hjd,mag,err,
     :	 		nvel,hjdrv,rv,rverr,dvjit,
     :	 		chph(k-1),chsp(k-1),chisq(k-1),varscl)	   
	   end if

c  ***	   Generate new proposal parameters	
1	   call propose(t(k-1),p(k-1),ms(k-1),wf(k-1),d(k-1),
     :	        vk(k-1),vs(k-1),b(k-1),lam(k-1),ecc(k-1),om(k-1),
     :	        t(k),p(k),ms(k),wf(k),d(k),vk(k),vs(k),b(k),lam(k),
     :	        ecc(k),om(k),
     :		sigt0,sigper,sigms,sigwf,sigdm,
     :	        sigvk,sigvs,sigb,siglam,sigecc,sigom,scfac,idum)
     	   
	   jtemp = jtemp + 1
	   
c	   Emergency restart in case algorithm gets stuck
	   if ( jtemp .gt. 5000 ) then
	      print *,' Markov chain stuck at trial ',k 
	      print *, t(k),p(k),ms(k),wf(k),d(k),vk(k),vs(k),b(k),
     :	               ecc(k),om(k)
	      print *, chisq(k)
	      do pset = 1, npset
	         print *, pset, lobs(pset)-fobs(pset)+1,varscl(pset)
	      end do
	      print *,' Backtracking to trial ',k - ktemp 
	      k = k - ktemp
	      strikes = strikes + 1
	      print *, t(k),p(k),ms(k),wf(k),d(k),vk(k),vs(k),b(k),
     :	               ecc(k),om(k)
	      scfac = 0.4d0
	      if(strikes .ge. 3 ) then
	         print *, 'Failed 3 times; restarting'
		 go to 3
	      else
	         go to 2
	      end if
	   end if
	
c  ***	   Convert proposal parameters to physical parameters

	   call physparms(p(k),wf(k),ms(k),d(k),b(k),vk(k),ecc(k),om(k),
     :	               rs(k),rh(k),aau(k),rstar(k),rp(k),
     :	               cosi,deginc(k),mpjup(k),rpjup(k))
	
c	   print *,'finished physparms'
c  ***	   Generate model from parameters; fit to data and compute
c	   penalty function chisq(k)
	   call eval(t(k),p(k),ms(k),rs(k),rp(k),cosi,
     :	          vk(k),vs(k),gam(k),lam(k),rstar(k),
     :	          ecc(k),om(k),
     :	          c1,c2,c3,c4,ms0,sigms,constrainmr,
     :	          ndata,npset,fobs,lobs,adj,hjd,mag,err,
     :	          nvel,hjdrv,rv,rverr,dvjit,
     :	          chph(k),chsp(k),chisq(k),varscl)
     

c  ***	   Save most probable proposal parameters
	   if ( chisq(k) .lt. chsmin ) then
	      t0best_mc = t(k)
	      pbest_mc = p(k)
	      msbest_mc = ms(k)
	      wfbest_mc = wf(k)
	      dbest_mc = d(k)
	      vkbest_mc = vk(k)
	      gambest_mc = gam(k)
	      vsbest_mc = vs(k)
	      bbest_mc = b(k)
	      lambest_mc = lam(k)
	      eccbest_mc = ecc(k)
	      ombest_mc = om(k)
	      chpbst = chph(k)
	      chsbst = chsp(k)
	      chsmin = chisq(k)	   
	   end if
	   
c  ***	   Extrapolate transit epoch to late 2006
	   tnow(k) = t(k) + p(k)*dfloat(norbnow)

	   call methast(idum,chisq(k),chisq(k-1),reject)
c	   print *,'finished methast'
c	   print *,k,chisq(k),chisq(k-1)

	   if(reject) go to 1
	end do	   
	   
c  ***	Every 100 steps, perform housekeeping tasks.

c  ***	Adaptive step-size control 
	print *,' '
	print *,'Completed ',k,' of ',njump,' proposals.'
	
	print *,'Chisq (min)      = ', chsmin

	ratio = dfloat(ktemp) / dfloat(jtemp)
	print *,'Acceptance rate  = ',ratio

	oldscfac = scfac
	print *,'Old scale factor = ',oldscfac

	scfac = scfac * ratio * 4d0
	scfac = max(scfac,0.4d0)
	scfac = min(scfac,1.0d0)
	scfac = min(scfac,oldscfac*1.3d0)
	print *,'New scale factor = ', scfac

c  ***	Recompute statistics for proposal parameters after 
c	500th iteration
	kst = k - 99
	ken = k
	if ( kst .gt. 500 ) then
	   
	   print *,'calling newstats'
	   call newstats(njump,t,p,ms,wf,d,vk,vs,b,lam,ecc,om,
     :	        kst,ken,
     :	        sigt0,sigper,sigms,sigwf,sigdm,
     :	        sigvk,sigvs,sigb,siglam,sigecc,sigom,radvel)
	end if
	   
	print *,sigt0,sigper,sigms,sigwf,sigdm,sigvk,sigvs,sigb
	
c  ***	Are we done yet?	
	if(k .lt. njump) go to 2

c  ***	End of main Markov-chain Monte Carlo loop

c  ***	Report best-fit parameters. Use sorting to establish
c	confidence limits.
     	print *,' Best-fit parameter set: '
	
	call conflim(nburn,njump,t,chisq,idx,dxl,xml,dxh)
	sigma_t0 = ( dxl + dxh )/2d0
	print *,' Transit epoch      = ',xml,' +',dxh,' -',dxl
	
	call conflim(nburn,njump,p,chisq,idx,dxl,xml,dxh)
	sigma_p = ( dxl + dxh )/2d0
	print *,' Orbital period     = ',xml,' +',dxh,' -',dxl,' days'

        call conflim(nburn,njump,d,chisq,idx,dxl,xml,dxh)
        sigma_d = ( dxl + dxh )/2d0
        print *,' Ratio (Rp/Rs)^2    = ',xml,' +',dxh,' -',dxl
	
	call conflim(nburn,njump,wf,chisq,idx,dxl,xml,dxh)
	dxl = dxl * pbest_mc
	xml = xml * pbest_mc
	dxh = dxh * pbest_mc
	sigma_w = ( dxl + dxh )/2d0
	print *,' Transit width      = ',xml,' +',dxh,' -',dxl,' days'
	
	call conflim(nburn,njump,b,chisq,idx,dxl,xml,dxh)
c	bbest_mc = xml
	print *,' Impact parameter   = ',xml,' +',dxh,' -',dxl,' rstar'
	
	vkbest_mc = k1
	vsbest_mc = vsi
	lambest_mc = lam0
	if ( nvel .ge. 2 ) then
	print *, ' '
	call conflim(nburn,njump,vk,chisq,idx,dxl,xml,dxh)	
c	vkbest_mc = xml
	print *,' Reflex velocity    = ',xml,' +',dxh,' -',dxl,' km/sec'
	
	call conflim(nburn,njump,gam,chisq,idx,dxl,xml,dxh)	
	gambest_mc = xml
	print *,' Gamma velocity     = ',xml,' +',dxh,' -',dxl,' km/sec'
	
	call conflim(nburn,njump,vs,chisq,idx,dxl,xml,dxh)	
c	vsbest_mc = xml
	print *,' Rotation v sin  i  = ',xml,' +',dxh,' -',dxl,' km/sec'
	
	call conflim(nburn,njump,lam,chisq,idx,dxl,xml,dxh)	
c	lambest_mc = xml
	dxl = dxl * 180d0 / pi
	xml = xml * 180d0 / pi
	dxh = dxh * 180d0 / pi
	print *,' Rossiter angle lam = ',xml,' +',dxh,' -',dxl,' deg'
	end if
	
	call conflim(nburn,njump,tnow,chisq,idx,dxl,xml,dxh)
	print *,' Current epoch range= ',xml,' +',dxh,' -',dxl
	
	print *, ' '
	call conflim(nburn,njump,rh,chisq,idx,dxl,xml,dxh)
	print *,' Stellar density    = ',xml,' +',dxh,' -',dxl,' rhsun'
c	rhobest = xml
	
	print *, ' '
	call conflim(nburn,njump,ms,chisq,idx,dxl,xml,dxh)
	print *,' Stellar mass       = ',xml,' +',dxh,' -',dxl,' M_sun'
        sigma_mst = ( dxl + dxh )/2d0
	mstbest = xml
	
	call conflim(nburn,njump,rstar,chisq,idx,dxl,xml,dxh)
	print *,' Stellar radius     = ',xml,' +',dxh,' -',dxl,' R_sun'
        sigma_rst = ( dxl + dxh )/2d0
	rstbest = xml
	
	print *, ' '
	call conflim(nburn,njump,aau,chisq,idx,dxl,xml,dxh)
	print *,' Orbital separation = ',xml,' +',dxh,' -',dxl,' AU'
	
	call conflim(nburn,njump,deginc,chisq,idx,dxl,xml,dxh)	
	print *,' Orbital inclination= ',xml,' +',dxh,' -',dxl,' deg'
	
	call conflim(nburn,njump,ecc,chisq,idx,dxl,xml,dxh)	
	print *,' Orb. eccentricity  = ',xml,' +',dxh,' -',dxl,'    '
	
	call conflim(nburn,njump,om,chisq,idx,dxl,xml,dxh)	
	print *,' Arg. periastron om = ',xml,' +',dxh,' -',dxl,' rad'
	
	call conflim(nburn,njump,deginc,chisq,idx,dxl,xml,dxh)	
	print *,' Orbital inclination= ',xml,' +',dxh,' -',dxl,' deg'
	
	print *, ' '
	call conflim(nburn,njump,rpjup,chisq,idx,dxl,xml,dxh)	
	print *,' Planet radius      = ',xml,' +',dxh,' -',dxl,' R_jup'
        sigma_rp = ( dxl + dxh )/2d0
	rpjbest = xml

	call conflim(nburn,njump,chisq,chph,idx,dxl,xml,dxh)
	print *,' chisq at best chph = ',xml
	call conflim(nburn,njump,chph,chph,idx,dxl,xml,dxh)
	print *,' Best chph = ',xml

	if(nvel.ge.2)then
	call conflim(nburn,njump,mpjup,chisq,idx,dxl,xml,dxh)	
	print *,' Planet mass        = ',xml,' +',dxh,' -',dxl,' M_jup'
	call conflim(nburn,njump,chisq,chsp,idx,dxl,xml,dxh)
	print *,' chisq at best chsp = ',xml
	call conflim(nburn,njump,chsp,chsp,idx,dxl,xml,dxh)
	print *,' Best chsp = ',xml
	end if
	
	print *, ' '
	print *,' chisq_phot         = ',chpbst
	print *,' nphot              = ',ndata

	if(nvel.ge.2)then	
	print *, ' '
	print *,' chisq_spec         = ',chsbst
	print *,' nvel               = ',nvel
	end if
	
c  ***	Compute other secondary quantities of interest

	do k = 1, njump
c	   Planet/star flux ratio for geometric albedo p=1:
	   rpsq(k) = rp(k)*rp(k)
	   
c  ***	   Compute planet/star mass ratio 
	   qrat = mpjup(k) / ms(k) / 1047.52d0

c	   Synchronisation timescale for primary (Zahn 1977)
	   tau_s(k) = 1.2 / qrat / qrat / rs(k)**6

c	   Synchronisation timescale for planet (Zahn 1977)
	   tau_p(k) = 1.2 * qrat * qrat / rp(k)**6

c	   Planet surface gravity in jovian units
	   gacc_p = mpjup(k) / rpjup(k) / rpjup(k)
	   
c	   Log planet surface gravity (cgs)
	   logg_p(k) = log10 ( gacc_p * 2288d0 )

c	   Stellar surface gravity in solar units
	   gacc_s = ms(k) / rstar(k) / rstar(k)
	   
c	   Log stellar surface gravity (cgs)
	   logg_s(k) = log10 ( gacc_s * 27400d0 )

c	   Planet density in jovian units
	   rho_p(k) = gacc_p / rpjup(k)

c  ***	   Use DW's optimised FGK relations
c	   Teff in kelvin	
	   tjh = -4369.5d0*jh + 7188.2d0
	   teql_p(k) = tjh * sqrt(rs(k)/2d0)

	end do
	
	print *, ' '
	call conflim(nburn,njump,rpsq,chisq,idx,dxl,xml,dxh)
	print *,' Opp flux ratio(p=1)= ',xml,' +',dxh,' -',dxl,' '
	call conflim(nburn,njump,tau_s,chisq,idx,dxl,xml,dxh)
	print *,' tsync (star)       = ',xml,' +',dxh,' -',dxl,' y'
	call conflim(nburn,njump,tau_p,chisq,idx,dxl,xml,dxh)
	print *,' tsync (planet)     = ',xml,' +',dxh,' -',dxl,' y'
	call conflim(nburn,njump,logg_s,chisq,idx,dxl,xml,dxh)
	print *,' Log g(star) [cgs]  = ',xml,' +',dxh,' -',dxl,' '
	call conflim(nburn,njump,logg_p,chisq,idx,dxl,xml,dxh)
	print *,' Log g(planet) [cgs]= ',xml,' +',dxh,' -',dxl,' '
	call conflim(nburn,njump,rho_p,chisq,idx,dxl,xml,dxh)
	print *,' Planet density     = ',xml,' +',dxh,' -',dxl,' rho_J'
	call conflim(nburn,njump,teql_p,chisq,idx,dxl,xml,dxh)
	print *,' Planet Teql (A=0)  = ',xml,' +',dxh,' -',dxl,' K'
	print *,' Stellar Teff(J-H)  = ',tjh,' K'
	
c  ***	The key results for planet-selection purposes are:

c	(1) the probability prp that the planet has a radius
c	    less than 1.5 Rjup, with constrainmr = .true.
c	    "Good" candidates should have prp > 0.5
c
c	(2) the probability prs that the star has a radius
c	    less than 1.2 ms^0.8, with constrainmr = .true.
c	    "Good" candidates should have prp > 0.1
c
c	(3) the difference between the best photometric chisquared
c	    (chpbst) obtained for a run with constrainmr = .true
c	    and that obtained with constrainmr = .false.
c	    "Good" candidates should have a difference in chpbst
c	    of no more than 5 or so.
c
c	(4) the one-sigma error in the fitted period and transit
c	    width. False positives will tend to have less 
c	    well-constrained epochs, periods and transit widths
c	    than objects showing genuine boxy transits.
c
c  ***	Compute the probabilities for (1) and (2) above and write to
c	a one-line summary file bestparms.dat
	call probs(njump,rpjup,rstar,ms,b,prp,prs,prb)
	
c  ***	Report stellar and planetary parameters

	call unlink('output.csv')
	open(unit=43,file='output.csv',form='formatted',status='new')
	do k = nburn+1, njump

	   write(43,10)t(k),p(k),ms(k),wf(k),d(k),vk(k),vs(k),gam(k),
     :	               rstar(k),rpjup(k),mpjup(k),b(k),
     :	               chph(k),chsp(k),chisq(k),tnow(k),lam(k)*180d0/pi,
     :                 ecc(k),om(k)
10	   format(f11.6,1x,f10.7,3(1x,f7.5),3(1x,f12.5),
     :	           4(1x,f12.5),3(1x,f12.4),4(1x,f11.6))
	   
	   
c	   write(43,10)t(k),p(k),gam(k),vk(k),vs(k),
c     :	       cosi(k)/rs(k),rss(k),rpj(k),mpj(k),chisq(k),chsp(k)
c10	   format(f9.4,1x,f9.7,1x,f9.4,1x,f6.4,1x,f6.3,1x,f10.6,1x,f6.3,
c     :	          f6.4,1x,f5.3,1x,f5.3,1x,f5.3,1x,f10.3,1x,f10.3)
	end do
	close(43)
	

c  ***	Switch on main-sequence mass-radius constraint
	constrainmr = .true.

c  ***	Load the first eleven successful steps into the amoeba array		
	do k = 1, 11
c  ***	   Use logarithmic scale for non-negative parameters,
c	   and a tanh-like function to restrict impact 
c	   parameter to 0 < b < 1 
	   xfunk(1) = t(k) 
	   xfunk(2) = p(k) 
	   xfunk(3) = log(wf(k))
	   xfunk(4) = log(ms(k))
	   xfunk(5) = log(d(k))
	   xfunk(6) = log(vk(k))
	   xfunk(7) = log(vs(k))
	   xfunk(8) = log( 1d0/(1-b(k))-1d0 )
	   xfunk(9) = log( 2d0/(1-lam(k)/pi)-1d0 )
	   xfunk(10) = log( 1d0/(1-ecc(k))-1d0 )
	   xfunk(11) = om(k)
	   
	   do j = 1, 11
	      parms(k,j) = xfunk(j)
	   end do
	   
	   yval(k) = funk(xfunk)	
	end do

c  ***	In the twelfth slot, put the best-fitting set of parameters
c	from the Markov chain

	k = 12
	xfunk(1) = t0best_mc 
	xfunk(2) = pbest_mc 
	xfunk(3) = log(wfbest_mc)
	xfunk(4) = log(msbest_mc)
	xfunk(5) = log(dbest_mc)
	xfunk(6) = log(vkbest_mc)
	xfunk(7) = log(vsbest_mc)
	xfunk(8) = log( 1d0/(1-bbest_mc)-1d0 )
	xfunk(9) = log( 2d0/(1-lambest_mc/pi)-1d0 )
	xfunk(10) = log( 1d0/(1-eccbest_mc)-1d0 )
	xfunk(11) = ombest_mc
	
	do j = 1, 11
	   parms(k,j) = xfunk(j)
	end do
	
	yval(k) = funk(xfunk)	     
	

c  ***	Find best solution with main-sequence constraint
	ftol = 1d-5
	call amoeba(parms,yval,12,11,11,ftol,funk,iter)
	call indexx(12,yval,indy)
	kbest = indy(1)
	print *, 'Chisq = ',yval(kbest),' after ',iter,' amoeba steps'
	chisq_con = yval(kbest)

c  ***	Convert proposal parameters to physical parameters

	do j = 1, 11
	   xfunk(j) = parms(kbest,j)
	end do
	t0best = xfunk(1)
	pbest = xfunk(2)
	wfbest = exp(xfunk(3))
	mstbest = exp(xfunk(4))
	dbest = exp(xfunk(5))
	vkbest = exp(xfunk(6))
	vsbest = exp(xfunk(7))
	bbest = 1d0 - 1d0/(1d0 + exp(xfunk(8)))
	lambest = pi * ( 1d0 - 2d0/(1d0 + exp(xfunk(9))))
	eccbest = 1d0 - 1d0/(1d0 + exp(xfunk(10)))
	ombest = xfunk(11)
	call physparms(pbest,wfbest,mstbest,
     :	               dbest,bbest,vkbest,eccbest,ombest,
     :	               rsbest,rhobest,aaubest,rstbest,rpbest,
     :	               cosi,dincbest,mpjbest,rpjbest)

	call eval(t0best,pbest,mstbest,rsbest,rpbest,cosi,
     :	          vkbest,vsbest,gambest,lambest,rstbest,
     :	          eccbest,ombest,
     :	          c1,c2,c3,c4,ms0,sigms,constrainmr,
     :	          ndata,npset,fobs,lobs,adj,hjd,mag,err,
     :	          nvel,hjdrv,rv,rverr,dvjit,
     :	          chphbest,chspbest,chisqbest,varscl)
     
c     	chisq_con = chphbest + chspbest
	chisq_con = chisqbest
	dchisq_mr = chisqbest - chphbest - chspbest

c  ***	Switch off main-sequence mass-radius constraint
	constrainmr = .false.

c  ***	Load the first twelve successful steps into the amoeba array		
	do k = 1, 12
	
	   xfunk(1) = t(k) 
	   xfunk(2) = p(k) 
	   xfunk(3) = log(wf(k))
	   xfunk(4) = log(ms(k))
	   xfunk(5) = log(d(k))
	   xfunk(6) = log(vk(k))
	   xfunk(7) = log(vs(k))
	   xfunk(8) = log( 1d0/(1-b(k))-1d0 )
	   xfunk(9) = log( 2d0/(1-lam(k)/pi)-1d0 )
	   xfunk(10) = log( 1d0/(1-ecc(k))-1d0 )
	   xfunk(11) = om(k)
	
	   do j = 1, 11
	      parms(k,j) = xfunk(j)
	   end do
	   	   
	   yval(k) = funk(xfunk)	
	end do	

c  ***	Find best solution without main-sequence constraint
	constrainmr = .false.
	ftol = 1d-5
	call amoeba(parms,yval,12,11,11,ftol,funk,iter)
	call indexx(12,yval,indy)
	kbest = indy(1)
	print *, 'Chisq = ',yval(kbest),' after ',iter,' amoeba steps'
	chisq_unc = yval(kbest)
	
	print *,prp*prs
	print *,chisq_con-chisq_unc
	print *,sqrt ( sigma_p*sigma_w )
	if ( prp*prs .gt. 0.3d0 
     :	     .and. abs( chisq_con-chisq_unc ) .lt. 10d0 ) then
	   print *,swaspid,' is a good candidate. '
	end if

	call unlink('bestparms.dat')
	open(unit=49,file='bestparms.dat',form='formatted',status='new')

        write(49,20)swaspid,rpjbest,bbest,rstbest,mstbest,prp,prs,
     :  chisq_con,chisq_unc,sigma_t0,sigma_p,sigma_w,
     :  snrpk(ipk),ntrpk(ipk),aellpk(ipk),
     :  snellpk(ipk),transfpk(ipk),gaprpk(ipk),dchsantpk(ipk),
     :  prb,t0best,pbest,wfbest,-dbest,sigma_d,sigma_rp,sigma_rst,
     :  sigma_mst,jh,ipk,dchisq_mr
20      format(a26,1x,6(f8.4,1x),2(f11.4,1x),3(f11.6,1x),
     :         f11.4,1x,i4,1x,5(f11.6,1x),f8.4,1x,1x,f11.6,1x,
     :         f9.7,2(1x,f7.5),4(1x,f11.6),1x,f8.1,i4,1x,f11.4)

	print *,swaspid,rpjbest,bbest,rstbest,mstbest,prp,prs,
     :	vsbest,lambest,chisq_con,chisq_unc,sigma_t0,sigma_p,sigma_w

	close(49)
	
c  ***	--------------------------------------------------------------	
c  ***	Everything from here on in is expendable for a cut-down planet
c	selection code, though the output files could be used to construct
c	the graphics for a "wanted" poster.	

	mcorr = mxcorr
	call correl(nburn,njump,t,mcorr,c_t,cl_t)
	print *,' Correlation length for t0     = ',cl_t
	call correl(nburn,njump,p,mcorr,c_p,cl_p)
	print *,' Correlation length for period = ',cl_p
	call correl(nburn,njump,d,mcorr,c_d,cl_d)
	print *,' Correlation length for depth  = ',cl_d
	call correl(nburn,njump,wf,mcorr,c_w,cl_w)
	print *,' Correlation length for wf     = ',cl_w
	call correl(nburn,njump,ms,mcorr,c_ms,cl_ms)
	print *,' Correlation length for ms     = ',cl_ms
	call correl(nburn,njump,b,mcorr,c_b,cl_b)
	print *,' Correlation length for b      = ',cl_b
	if(nvel.gt.0)then
	   call correl(nburn,njump,vk,mcorr,c_vk,cl_vk)
	   print *,' Correlation length for vk     = ',cl_vk
	   call correl(nburn,njump,ecc,mcorr,c_ecc,cl_ecc)
	   print *,' Correlation length for ecc    = ',cl_ecc
	   call correl(nburn,njump,om,mcorr,c_om,cl_om)
	   print *,' Correlation length for om     = ',cl_om
c	   call correl(nburn,njump,vs,mcorr,c_vs,cl_vs)
c	   print *,' Correlation length for vs     = ',cl_vs
c	   call correl(nburn,njump,lam,mcorr,c_lam,cl_lam)
c	   print *,' Correlation length for lam    = ',cl_lam
	end if
		
	if(nvel.ge.2)then
	   call conflim(nburn,njump,t,chisq,idx,dxl,t0best,dxh)	
	   call conflim(nburn,njump,p,chisq,idx,dxl,pbest,dxh)	
	   call conflim(nburn,njump,b,chisq,idx,dxl,bbest,dxh)	
	   call conflim(nburn,njump,rp,chisq,idx,dxl,rpbest,dxh)	
	   call conflim(nburn,njump,rs,chisq,idx,dxl,rsbest,dxh)	
	   call conflim(nburn,njump,gam,chisq,idx,dxl,gambest,dxh)	
	   call conflim(nburn,njump,vk,chisq,idx,dxl,vkbest,dxh)	
	   call conflim(nburn,njump,lam,chisq,idx,dxl,lambest,dxh)	
	   call conflim(nburn,njump,ecc,chisq,idx,dxl,eccbest,dxh)	
	   call conflim(nburn,njump,om,chisq,idx,dxl,ombest,dxh)	
	end if
	
	pr = rpbest / rsbest
	
	
c	sinl = sin(lambest)
c	cosl = cos(lambest)
	print *,'lambest, sinl, cosl = ', sinl, cosl
	
	nmod = 1000
	
	
c  ***	Compute date of periastron relative to transit time t0	
	nutrans = pi/2d0 - ombest
	mtrans = meananom(nutrans,eccbest)
	dttrans = mtrans/2d0/pi * pbest
	tperi = t0best - dttrans
	
c	Eccentric anomaly at transit 
        tanhalfecc = tan(nutrans/2.)*sqrt((1.-eccbest)/(1.+eccbest))
	eccanom = 2.*atan(tanhalfecc)
	
c  ***	Hence get instantaneous planet-star distance 
c	at transit in units of major semi-axis:
	rtrans = 1. - eccbest*cos(eccanom)

c	Derive inclination from impact parameter, stellar radius
c	and orbital separation

	cosi = bbest*rsbest*rtrans
	sini = sqrt ( 1d0 - cosi*cosi )

	do i = 1, nmod
	   ph = dfloat(i-1)/dfloat(nmod-1) + 0.5d0
	   jd = t0best + ph * pbest
	   
c  ***	   Coordinates of planet in orbital plane ( a = 1 )
           call kepler(jd,tperi,pbest,
     :	              eccbest,ombest,vkbest,sini,
     :	              nu(i),rau(i),alpha(i))
c  ***	   Impact parameter in units of primary radius
	   if ( cos(alpha(i)) .gt. 0d0 ) then
c	   if ( alpha(i) .gt. pi/2d0 ) then
	      z(i) = rau(i) * sin(alpha(i)) / rsbest
	   else
	      z(i) = 1d0 / rsbest
	   end if
	   
	   nu(i) = nu(i) + gambest
	   
	end do
	
c  ***	Compute model flux
	call occultsmall(pr,c1,c2,c3,c4,nmod,z,mu)
		
	call unlink('pltmod.dat')
	open(unit=45,file='pltmod.dat',form='formatted',status='new')	
	do i = 1, nmod
	   ph = dfloat(i-1)/dfloat(nmod-1) + 0.5d0
	   write(45,*) ph, -2.5d0*log10(mu(i)), nu(i)
	end do	
	close(45)
	
	call unlink('pltphot.dat')
	open(unit=46,file='pltphot.dat',form='formatted',status='new')
	do pset = 1, npset
	   do i = fobs(pset),lobs(pset)
c  ***	      Determine orbital phase
	      ph = mod(hjd(i)-t0best,pbest)/pbest
	      if ( ph.lt.-0.5d0 ) ph = ph + 2d0
	      if ( ph.lt.0.5d0 ) ph = ph + 1d0
	      write(46,*) ph, mag(i)-adj(pset), err(i)
	   end do
	end do
	close(46)
	
	if(nvel.ge.2)then
	call unlink('pltrvel.dat')
	open(unit=47,file='pltrvel.dat',form='formatted',status='new')
	do i = 1, nvel
c  ***	   Determine orbital phase
	   ph = mod(hjdrv(i)-t0best,pbest)/pbest
	   if ( ph.lt.-0.5d0 ) ph = ph + 2d0
	   if ( ph.lt.0.5d0 ) ph = ph + 1d0
c	   if ( ph.gt.1.5d0 ) ph = ph - 1d0
	   write(47,*) hjdrv(i), ph, rv(i), rverr(i)
	end do
	close(47)
	end if
	
	print *, ' '
	print *,' j = ',j , '    k = ', k
	print *,' All done'	
	
	end

C+	----------------------------------------------------------------
C+
	subroutine newstats(njump,t,p,ms,wf,d,vk,vs,b,lam,ecc,om,
     :	      kst,ken,
     :	      sigt0,sigper,sigms,sigwf,sigdm,
     :	      sigvk,sigvs,sigb,siglam,sigecc,sigom,radvel)

	implicit none
	     
c  ***	Subroutine parameters
     	integer njump
	double precision t(njump)
	double precision p(njump)
	double precision ms(njump)
	double precision wf(njump)
	double precision d(njump)
	double precision vk(njump)
	double precision vs(njump)
	double precision b(njump)
	double precision lam(njump)
	double precision ecc(njump)
	double precision om(njump)
	integer kst, ken
	double precision sigt0,sigper,sigms,sigwf,sigdm
	double precision sigvk,sigvs,sigb,siglam,sigecc,sigom
	logical radvel
	
c  ***	Local variables
	integer i
	integer nx
	double precision st,sp,sm,sw,sd,sk,sv,sb,sl
	double precision stt,spp,smm,sww,sdd,skk,svv,sbb,sll

c  ***	Need to initialise the accumulators in the calling routine; 
c	this allows us to increment them using just the most recent
c	set of values.
	
	nx = ken - kst + 1
	
	st = 0
	stt = 0
	do i = kst,ken
	   st = st + t(i)
	   stt = stt + t(i)*t(i)
	end do
	if ( stt - st*st/dfloat(nx) .gt. 1d-30 ) then
	   sigt0 = sqrt((stt - st*st/dfloat(nx))/dfloat(nx-1))
	end if
	
	sp = 0
	spp = 0
	do i = kst,ken
	   sp = sp + p(i)
	   spp = spp + p(i)*p(i)
	end do
	if ( spp - sp*sp/dfloat(nx) .gt. 1d-30 ) then
	   sigper = sqrt((spp - sp*sp/dfloat(nx))/dfloat(nx-1))
	end if
	
c	sm = 0
c	smm = 0
c	do i = kst,ken
c	   sm = sm + ms(i)
c	   smm = smm + ms(i)*ms(i)
c	end do
c	if ( smm - sm*sm/dfloat(nx) .gt. 1d-30 ) then
c	   sigms = sqrt((smm - sm*sm/dfloat(nx))/dfloat(nx-1))
c	end if
	
	sw = 0
	sww = 0
	do i = kst,ken
	   sw = sw + wf(i)
	   sww = sww + wf(i)*wf(i)
	end do
	if ( sww - sw*sw/dfloat(nx) .gt. 1d-30 ) then
	   sigwf = sqrt((sww - sw*sw/dfloat(nx))/dfloat(nx-1))
	end if
	
	sd = 0
	sdd = 0
	do i = kst,ken
	   sd = sd + d(i)
	   sdd = sdd + d(i)*d(i)
	end do
	if ( sdd - sd*sd/dfloat(nx) .gt. 1d-30 ) then
	   sigdm = sqrt((sdd - sd*sd/dfloat(nx))/dfloat(nx-1))
	end if
	
	if ( radvel ) then
	   sk = 0
	   skk = 0
	   do i = kst,ken
	      sk = sk + vk(i)
	      skk = skk + vk(i)*vk(i)
	   end do
	   if ( skk - sk*sk/dfloat(nx) .gt. 1d-30 ) then
	      sigvk = sqrt((skk - sk*sk/dfloat(nx))/dfloat(nx-1))
	   end if

	   sv = 0
	   svv = 0
	   do i = kst,ken
	      sv = sv + vs(i)
	      svv = svv + vs(i)*vs(i)
	   end do
	   if ( svv - sv*sv/dfloat(nx) .gt. 1d-30 ) then
	      sigvs = sqrt((svv - sv*sv/dfloat(nx))/dfloat(nx-1))
	   end if
	   
	   sl = 0
	   sll = 0
	   do i = kst,ken
	      sl = sl + lam(i)
	      sll = sll + lam(i)*lam(i)
	   end do
	   if ( sll - sl*sl/dfloat(nx) .gt. 1d-30 ) then
	      siglam = sqrt((sll - sl*sl/dfloat(nx))/dfloat(nx-1))
	   end if
	   
	   sl = 0
	   sll = 0
	   do i = kst,ken
	      sl = sl + ecc(i)
	      sll = sll + ecc(i)*ecc(i)
	   end do
	   if ( sll - sl*sl/dfloat(nx) .gt. 1d-30 ) then
	      sigecc = sqrt((sll - sl*sl/dfloat(nx))/dfloat(nx-1))
	   end if
	   
	   sl = 0
	   sll = 0
	   do i = kst,ken
	      sl = sl + om(i)
	      sll = sll + om(i)*om(i)
	   end do
	   if ( sll - sl*sl/dfloat(nx) .gt. 1d-30 ) then
	      sigom = sqrt((sll - sl*sl/dfloat(nx))/dfloat(nx-1))
	   end if
	end if

c	sb = 0
c	sbb = 0
c	do i = kst,ken
c	   sb = sb + b(i)
c	   sbb = sbb + b(i)*b(i)
c	end do
c	if ( sbb - sb*sb/dfloat(nx) .gt. 1d-30 ) then
c	   sigb = sqrt((sbb - sb*sb/dfloat(nx))/dfloat(nx-1))
c	   Don't let sigb grow too big
c	   sigb = min(sigb,0.05)
c	end if
	
     	end
C+	----------------------------------------------------------------
	subroutine propose(told,pold,msold,wfold,dold,vkold,
     :	                   vsold,bold,lamold,eccold,omold,	
     :	                   t, p, ms, wf, d, vk, vs, b, lam,ecc,om,
     :	                   sigt0,sigper,sigms,sigwf,sigdm,
     :	                   sigvk,sigvs,sigb,siglam,sigecc,sigom,
     :	                   scfac,idum)
  
 	implicit none
	    
c  *** 	Subroutine parameters
 	real*8 told,pold,msold,wfold,dold,vkold,vsold,bold,lamold
	real*8 eccold,omold
	real*8 t, p, ms, wf, d, vk, vs, b, lam, ecc, om
	real*8 sigt0,sigper,sigms,sigwf,sigdm,sigvk,sigvs,sigb,siglam
	real*8 sigecc,sigom
	real*8 scfac
	integer idum
	double precision pi
		
	real gasdev
	    
	pi = 4d0*atan2(1d0,1d0)
	
 	t = told + sigt0 * dble(gasdev(idum)) * scfac
	p = pold + sigper * dble(gasdev(idum)) * scfac
1	ms = msold + sigms * dble(gasdev(idum)) * scfac
	if ( ms .lt. 0d0 ) go to 1
2	wf = wfold + sigwf * dble(gasdev(idum)) * scfac
	if ( wf .lt. 0d0 ) go to 2
3	d = dold + sigdm * dble(gasdev(idum)) * scfac
	if ( d .lt. 0d0 ) go to 3
4	vk = vkold + sigvk * dble(gasdev(idum)) * scfac
	if ( vk .lt. 0d0 ) go to 4
5	vs = vsold + sigvs * dble(gasdev(idum)) * scfac
	if ( vs .lt. 0d0 ) go to 5
6	b = bold + sigb * dble(gasdev(idum)) * scfac
	if ( b .lt. 0d0 .or. b.ge.1d0 ) go to 6
7	lam = lamold + siglam * dble(gasdev(idum)) * scfac
	if ( lam .lt. -pi .or. lam.ge.pi ) go to 7
8	ecc = eccold + sigecc * dble(gasdev(idum)) * scfac
	if ( ecc .lt. 0d0 .or. ecc.ge.1d0 ) go to 8
9	om = omold + sigom * dble(gasdev(idum)) * scfac
	if ( om .lt. -pi .or. om.ge.pi ) go to 9
         
     	end
C+	----------------------------------------------------------------
C+
	subroutine physparms(p,wf,ms,d,b,vk,ecc,om,
     :	                     rs,rh,aau,rstar,rp,cosi,deginc,mpjup,rpjup)

	implicit none
		
c  ***	Subroutine parameters
	double precision p,wf,ms,d,b,vk,ecc,om
	double precision rs,rh,aau,rstar,rp,cosi,deginc,mpjup,rpjup

c  ***	Local variables
	double precision prat,pyr
	double precision akm,sini,psec,vt,q
	double precision nutrans,tanhalfecc,eccanom,rtrans
	double precision pi
	
	pi = 4d0*atan2(1d0,1d0)
	
c  	rp/rs for grazing transit ...
	prat = ((b-1d0)+sqrt((1d0-b)**2+8d0*d))/2d0
c  	...or for complete transit
	prat = max(prat,sqrt(d))
	
c  	Compute rs using eclipse width from 1st to 4th contact
	rs = pi * wf / sqrt((1d0+prat)**2 - b*b)

c	Compute density in solar units from rs and period
	rh = 0.0134063d0 / rs**3 / p**2
	
c  ***	Compute mass-dependent physical parameters
	pyr = p/365.256363d0
	aau = (pyr*pyr*ms)**(1d0/3d0)
	
c  ***	Stellar radius in solar units
	rstar = rs * aau * 215.094177d0
	
c  ***	Compute planet radius from transit depth
	rp = rs * prat
	
c  ***	True anomaly at time of transit	
	nutrans = pi/2d0 - om
	
c	Eccentric anomaly at transit 
        tanhalfecc = tan(nutrans/2.)*sqrt((1.-ecc)/(1.+ecc))
	eccanom = 2.*atan(tanhalfecc)
	
c  ***	Hence get instantaneous planet-star distance 
c	at transit in units of major semi-axis:
	rtrans = 1. - ecc*cos(eccanom)

c	Derive inclination from impact parameter, stellar radius
c	and orbital separation

	cosi = b * rs * rtrans
	sini = sqrt ( 1d0 - cosi*cosi )

	deginc = acos(cosi) * 180d0 / pi
	akm = aau * 149598000d0
	psec = p * 86400d0
	vt = 2d0 * pi * akm / psec * sini
	q = vk / ( vt - vk )

c  ***	Compute planet mass in jovian units
	mpjup = q * ms * 1047.52d0
	rpjup = rp*aau*2092.51385d0

	end
C+	----------------------------------------------------------------

	subroutine orthk1(nvel,hjdrv,rv,err,t0,period,k1,sigvk)
	
	implicit none
	
c  ***	Subroutine parameters
	integer nvel
	real*8 hjdrv(nvel)
	real*8 rv(nvel)
	real*8 err(nvel)
	real*8 t0, period
	real*8 k1, sigvk
	
c  ***	Local variables
	real*8 sx, su, sw
	integer i
	real*8 w, x, u
	real*8 phi, sphi
	real*8 xhat, uhat
	real*8 sxu, suu
	real*8 pi
	
	pi = 4d0*atan2(1d0,1d0)
	
	if(nvel .gt. 2)then
	
c	    Orthogonalise data and sine function, 
c	    adding 50 m/sec additional error to region around transit
	    sx = 0d0
	    su = 0d0
	    sw = 0d0
	    do i = 1, nvel
	       phi = mod( hjdrv(i)-t0, period ) / period
	       if ( phi .gt. 0.05 .and. phi .lt. 0.95 ) then
	          w = 1d0 / err(i) / err(i)
	       else
	          w = 1d0 / ( err(i) * err(i) + .05*.05 )
	       end if
	       sphi = sin( 2d0 * pi * phi )
	       sx = sx + rv(i) * w
	       su = su + sphi * w
	       sw = sw + w
	    end do
	    xhat = sx / sw
	    uhat = su / sw

c	    Optimal scaling of sine function to fit data,	    
c	    adding 50 m/sec additional error to region around transit
	    sxu = 0d0
	    suu = 0d0
	    do i = 1, nvel
	       phi = mod( hjdrv(i)-t0, period ) / period
	       if ( phi .gt. 0.05 .and. phi .lt. 0.95 ) then
	          w = 1d0 / err(i) / err(i)
	       else
	          w = 1d0 / ( err(i) * err(i) + .05*.05 )
	       end if
	       sphi = sin( 2d0 * pi * phi )
	       x = rv(i) - xhat
	       u = sphi - uhat
	       sxu = sxu + x * u * w
	       suu = suu + u * u * w
	    end do
	    k1 = - sxu / suu
	    sigvk = sqrt(1d0 / suu)
	else
	   k1 = 0.1d0
	   sigvk = 0d0
	end if
	
	print *,'k1    = ',k1
	print *,'sigvk = ',sigvk
	
	end	


C+	----------------------------------------------------------------
	subroutine orthtp(ndata,hjd,err,nvel,hjdrv,rverr,
     :	                  t0,sigt0,period,sigper,hw1,hw2)
     
     	implicit none
	
c  ***	Orthogonalise T0 and p by adjusting epoch of transit
c	to centre of mass of data taken in transit.
	
c  ***	Subroutine parameters
	integer ndata
	real*8 hjd(ndata)
	real*8 err(ndata)
	integer nvel
	real*8 hjdrv(nvel)
	real*8 rverr(nvel)
	real*8 t0, sigt0
	real*8 period, sigper
	real*8 hw1,hw2
	
c  ***	Local variables
	real*8 sxw,sw,w
	real*8 t1, t2
	integer i
	real*8 phi, cphi
	integer norb
	real*8 tcog
	real*8 pi
	
	pi = 4d0*atan2(1d0,1d0)
	
	sxw = 0d0
	sw = 0d0
	t1 = 1d38
	t2 = -1d38
	do i = 1, ndata
	   phi = mod( hjd(i)-t0, period ) / period
	   if( phi .lt.0d0 ) phi = phi + 1d0
	   if( phi.lt.hw1 .or. phi.gt.hw2 ) then
	      w = 1d0 / err(i) / err(i)
	      sxw = sxw + hjd(i) * w
	      sw = sw + w
	      t1 = min(t1,hjd(i))
	      t2 = max(t2,hjd(i))
	   end if
	end do
	
	norb = nint ( (t2 - t1 ) / period )
	sigper = sigt0 / dfloat(norb)

	do i = 1, nvel
	   phi = mod( hjdrv(i)-t0, period ) / period
	   cphi = cos( 2d0 * pi * phi )
c	   Strictly we should weight this as cos(phi)
	   w = cphi * cphi / rverr(i) / rverr(i)
	   sxw = sxw + hjdrv(i) * w
	   sw = sw + w
	end do
	
	tcog = sxw / sw
	norb = nint((tcog - t0) / period)
	t0 = t0 + dfloat(norb) * period
	
	end
C+	----------------------------------------------------------------
C+
	subroutine readtamuz(lu,photfile,
     :	           swaspid,epoch,period,wday,dmag,dchs,
     :	           mxobs,ndata,hjd,dat,err)
     
c  ***  This is a more advanced version of rddata. It reads HUNTER
c	parameters as well as photometric data from a SuperWASP
c	post-tamuz (LCFINDER) light curve listing.
	
	implicit none
	integer lu
	character*80 photfile
	integer mxobs
	real*8 hjd(mxobs)
	real*8 dat(mxobs)
	real*8 err(mxobs)
	integer ndata
	integer i
	character*80 record
	character*26 swaspid
	real*8 dummy
	integer ipk
	integer mxpk
	parameter(mxpk = 5)
	real*8 epoch(mxpk)
	real*8 period(mxpk)
	real*8 wday(mxpk)
	real*8 dmag(mxpk)
	real*8 dchs(mxpk)
		
c  ***	Read data
	i = 0
	ipk = 0
	open(unit=lu,file=photfile,status='old')
3	read(lu,'(a)',end=998)record
	if(record(1:23) .eq. ' # Lightcurve for star ') then
	   read(record(24:49),'(a)')swaspid
	   print *,' swaspid = ',swaspid
	   go to 3
	else if (record(1:8) .eq. ' # Peak:')then
	   print *,record(9:10)
	   read(record(9:10),*)ipk
	   print *,'ipk=',ipk
	   read(lu,'(a)',end=998)record
	   read(record(13:22),*)epoch(ipk)
	   print *,'epoch=',epoch(ipk)
	   read(lu,'(a)',end=998)record
	   read(record(13:23),*)period(ipk)
	   print *,'period=',period(ipk)
	   read(lu,'(a)',end=998)record
	   read(record(16:25),*)dummy
	   wday(ipk) = dummy / 24d0
	   print *,'wday=',wday(ipk)
	   read(lu,'(a)',end=998)record
	   read(record(11:25),*)dummy
	   dmag(ipk) = -dummy
	   print *,'dmag=',dmag(ipk)
	   read(lu,'(a)',end=998)record
	   read(record(17:28),*)dchs(ipk)
	   print *,'dchs=',dchs(ipk)
	   go to 3
	else if ( record(1:2) .eq. ' #' ) then
	   go to 3
	end if

c  ***	When we get to here, we've read all the header records
c	so read the light curve data.
	i = i + 1
	read(record,*)hjd(i),dat(i),err(i)
c	print *,hjd(i),dat(i),err(i)
	go to 3
	
998	close(lu)
	ndata = i
	
	end

C+	----------------------------------------------------------------
C+
	subroutine readnewlc(lu,photfile,
     :	           swaspid,jh,rpmj,drpmj,
     :	           epoch,period,wday,dmag,dchs,snred,ntransit,
     :	           aellip,snellip,transfrac,gaprat,dchsanti,
     :	           mxobs,ndata,hjd,dat,err)
     
c  ***  This is a more advanced version of rddata. 

c  ***	This version (readnewlc) reads the expanded header information
c	as well as photometric data from a .lc file generated by the
c	2007 March version of HUNTER.
	
	implicit none
	integer lu
	character*80 photfile
	integer mxobs
	real*8 hjd(mxobs)
	real*8 dat(mxobs)
	real*8 err(mxobs)
	integer ndata
	integer i
	character*80 record
	character*26 swaspid
	integer ngood
	real*8 jh,rpmj,drpmj
	real*8 dummy
	integer ipk
	integer mxpk
	parameter(mxpk = 5)
	real*8 epoch(mxpk)
	real*8 period(mxpk)
	real*8 wday(mxpk)
	real*8 dmag(mxpk)
	real*8 dchs(mxpk)
	real*8 snred(mxpk)
	integer ntransit(mxpk)
	real*8 aellip(mxpk)
	real*8 snellip(mxpk)
	real*8 transfrac(mxpk)
	real*8 gaprat(mxpk)
	real*8 dchsanti(mxpk)
		
c  ***	Read data
	i = 0
	ipk = 0
	open(unit=lu,file=photfile,status='old')
3	read(lu,'(a)',end=998)record
	if(record(1:21) .eq. '# Lightcurve of star ') then
	   read(record(22:47),'(a)')swaspid
	   print *,'# Lightcurve of star ',swaspid
	   go to 3
	else if (record(1:14) .eq. '# Num good pts')then
	   read(record(22:31),*)ngood
	   print *,'# Num good pts      :',ngood
	   go to 3
	else if (record(1:5) .eq. '# J-H')then
	   read(record(22:31),*)jh
	   if (jh.eq.-99) jh=0.35	!set to ~G5V color
	   if (jh.lt.0.0) jh=0.0	!T-R relation valid Teff<7200,J-H>0
	   if (jh.gt.0.84)jh=0.84	!LD coeffs good to T>3500, J-H<0.84
	   print *,'# J-H               :',jh
	   go to 3
	else if (record(1:12) .eq. '# RPM_J     ')then
	   read(record(22:31),*)rpmj
	   print *,'# RPM_J             :',rpmj
	   go to 3
	else if (record(1:12) .eq. '# RPM_J diff')then
	   read(record(22:31),*)drpmj
	   print *,'# RPM_J diff        :',drpmj
	   go to 3
	else if (record(1:6) .eq. '# PEAK')then
	   print *,record(22:31)
	   read(record(22:31),*)ipk
	   print *,'ipk=',ipk
	   
	   read(lu,'(a)',end=998)record
	   read(record(22:31),*)epoch(ipk)
	   print *,'# Epoch             :',epoch(ipk)
	   
	   read(lu,'(a)',end=998)record
	   read(record(22:31),*)period(ipk)
	   print *,'# Period (days)     :',period(ipk)
	   
	   read(lu,'(a)',end=998)record
	   read(record(22:31),*)dummy
	   wday(ipk) = dummy / 24d0
	   print *,'# Duration  (days)  :',wday(ipk)
	   
	   read(lu,'(a)',end=998)record
	   read(record(22:31),*)dummy
	   dmag(ipk) = -dummy
	   print *,'# Depth (mag)       :',dmag(ipk)
	   
	   read(lu,'(a)',end=998)record
	   read(record(22:31),*)dchs(ipk)
	   print *,'# Delta chisq       :',dchs(ipk)
	   
	   read(lu,'(a)',end=998)record
	   read(record(22:31),*)snred(ipk)
	   print *,'# Signal-Rednoise   :',snred(ipk)
	   
	   read(lu,'(a)',end=998)record
	   read(record(22:31),*)ntransit(ipk)
	   print *,'# Ntransits         :',ntransit(ipk)
	   
	   read(lu,'(a)',end=998)record
	   read(record(22:31),*)aellip(ipk)
	   print *,'# Ellipsoidal var   :',aellip(ipk)
	   
	   read(lu,'(a)',end=998)record
	   read(record(22:31),*)snellip(ipk)
	   print *,'# Ellipsoidal S/N   :',snellip(ipk)
	   
	   read(lu,'(a)',end=998)record
	   read(record(22:31),*)transfrac(ipk)
	   print *,'# Fraction-intransit:',transfrac(ipk)
	   
	   read(lu,'(a)',end=998)record
	   read(record(22:31),*)gaprat(ipk)
	   print *,'# Gap Ratio         :',gaprat(ipk)
	   
	   read(lu,'(a)',end=998)record
	   read(record(22:31),*)dchsanti(ipk)
	   print *,'# Dchisq/Anti-dchisq:',dchsanti(ipk)
	   go to 3
	else if ( record(1:2) .eq. '# ' ) then
	   go to 3
	end if

c  ***	When we get to here, we've read all the header records
c	so read the light curve data.
	i = i + 1
	read(record,*)hjd(i),dat(i),err(i)
c	print *,hjd(i),dat(i),err(i)
	go to 3
	
998	close(lu)
	ndata = i
	if ( ndata .ne. ngood ) then
	   print *,' READNEWLC WARNING: number of light curve records'
	   print *,'  does not match ngood.'
	end if
	
	end

C+	----------------------------------------------------------------
C+
	subroutine getparms(jh,rstar,mstar,a1,a2,a3,a4)
	
c  ***	Given the J-H colour index of a transit candidate,
c	uses DW's polynomial fits to estimate Teff and Mstar,
c	then interpolates nonlinear limb-darkening coefficients
c	from grid of values taken from Claret (2000) for [Fe/H]=0.1
	
	implicit none
c	character*120 record
	double precision jh,rstar,mstar,a1,a2,a3,a4
	
	double precision teff(13)
	double precision arr1(13)
	double precision arr2(13)
	double precision arr3(13)
	double precision arr4(13)
	double precision tjh, frac
	integer i, jlo, jhi
	character*120 infile
	
1	print *,' Enter filename with Claret limb-darkening tables : '
	read(*,'(a)',err=1) infile
	open(unit=63,file=infile,status='old',form='formatted')
	
c	read(63,*) record
	read(63,*) (teff(i),i=1,13)
	
c	read(63,*) record
	read(63,*) (arr1(i),i=1,13)
	
c	read(63,*) record
	read(63,*) (arr2(i),i=1,13)
	
c	read(63,*) record
	read(63,*) (arr3(i),i=1,13)
	
c	read(63,*) record
	read(63,*) (arr4(i),i=1,13)
	
	close(63)

c  ***	Use DW's optimised FGK relations
c	Teff in kelvin	
	tjh = -4369.5d0*jh + 7188.2d0

c	Radius in Rsun	
	rstar =(((-3.925d-14*tjh 
     :	       + 8.3909d-10)*tjh 
     :	       - 6.555d-6)*tjh
     :	       + 0.02245d0)*tjh
     :	       - 27.9788d0

c	Mass in Msun     
     	mstar = rstar**(1d0/0.8d0)

c	Locate temperature in interpolation grid
	jlo = 0
	call huntdp(teff,13,tjh,jlo)
	
	if(jlo .eq.0) then
	   a1 = arr1(1)	
	   a2 = arr2(1)	
	   a3 = arr3(1)	
	   a4 = arr4(1)	
	else if(jlo .eq.0) then
	   a1 = arr1(13)	
	   a2 = arr2(13)	
	   a3 = arr3(13)	
	   a4 = arr4(13)	
	else
	   jhi = jlo + 1
	   frac = (arr1(jhi)-arr1(jlo))/(teff(jhi)-teff(jlo))
	   a1 = arr1(jlo) + (tjh-teff(jlo)) * frac	
	   frac = (arr2(jhi)-arr2(jlo))/(teff(jhi)-teff(jlo))
	   a2 = arr2(jlo) + (tjh-teff(jlo)) * frac	
	   frac = (arr3(jhi)-arr3(jlo))/(teff(jhi)-teff(jlo))
	   a3 = arr3(jlo) + (tjh-teff(jlo)) * frac	
	   frac = (arr4(jhi)-arr4(jlo))/(teff(jhi)-teff(jlo))
	   a4 = arr4(jlo) + (tjh-teff(jlo)) * frac	
	end if
	
	end
C+	----------------------------------------------------------------
C+	
	SUBROUTINE HUNTDP(XX, N, X, JLO)

C  ***	Given an array XX of length N, and given a value X, returns a value JLO
c	such that X is between XX(JLO) and XX(JLO+1).  XX must be monotonic, 
c	eitherincreasing or decreasing. JLO = 0 or JLO = N is returned to
c	indicate that X is out of range. JLO on input is taken as the initial
c	guess for JLO on output.

	REAL*8 XX(N)
	REAL*8 X
	LOGICAL ASCND

	ASCND = XX(N).GT.XX(1)
	IF(JLO.LE.0 .OR. JLO.GT.N)THEN
	  JLO = 0
	  JHI = N+1
	  GO TO 3
	END IF
	INC = 1
	IF(X.GE.XX(JLO) .EQV. ASCND)THEN
1	  JHI = JLO + INC
	  IF(JHI.GT.N)THEN
	    JHI = N + 1
	  ELSE IF(X.GE.XX(JHI) .EQV. ASCND)THEN
	    JLO = JHI
	    INC = INC + INC
	    GO TO 1
	  END IF
	ELSE
	  JHI = JLO
2	  JLO = JHI - INC
	  IF(JLO.LT.1)THEN
	    JLO = 0
	  ELSE IF(X.LT.XX(JLO) .EQV. ASCND)THEN
	    JHI = JLO
	    INC = INC + INC
	    GO TO 2
	  END IF
	END IF

3	IF(JHI-JLO.EQ.1)RETURN
	JM = (JHI+JLO)/2
	IF(X.GT.XX(JM) .EQV. ASCND)THEN
	  JLO = JM
	ELSE
	  JHI = JM
	END IF
	GO TO 3

	END

C+	----------------------------------------------------------------
C+	
	subroutine eval(t0,period,ms,rs,rp,cosi,vk,vsi,gam,lam,rstar,
     :	                ecc, om, 
     :	                c1,c2,c3,c4,ms0,sigms,constrainmr,
     :	                ndata,npset,fobs,lobs,adj,hjd,mag,err,
     :	                nvel,hjdrv,rv,rverr,dvjit,chphot,chspec,chs,scl)

	implicit none
	
	integer ndata, npset
	integer fobs(npset)
	integer lobs(npset)
	real*8 scl(npset)
	real*8 adj(npset)
	real*8 hjd(ndata)
	real*8 mag(ndata)
	real*8 err(ndata)
	real*8 z(ndata)
	real*8 mu(ndata)
	
	integer nvel	
	real*8 hjdrv(nvel)
	real*8 rv(nvel)
	real*8 rverr(nvel)
	real*8 dvjit
	real*8 vz(nvel)
	real*8 nu(nvel)
	
c  ***	System parameters
	real*8 t0,period,ms,rs,rp,cosi,vk,vsi,gam,lam,rstar,ecc,om
	real*8 ms0, sigms, rs0, sigrs
	
c  ***	Limb-darkening coeffs from Claret (2000)
	real*8 c1, c2, c3, c4
	
c  ***	Accumulators and statistics
	real*8 w, sw, sxw, spw, xhat, phat, r
	real*8 chpset, chphot, chspec, chs
	integer ipset
	integer ndof
	logical constrainmr
	
	integer i
	
c  ***	Compute transit model
	call transmod(t0,period,rp,rs,cosi,vk,vsi,lam,ecc,om,
     :	                    c1,c2,c3,c4,ndata,hjd,z,mu,nvel,hjdrv,vz,nu)

c  ***	Orthogonalise each dataset and corresponding 
c	model points independently.
	chphot = 0d0
	do ipset = 1, npset
	   sw = 0d0
	   sxw = 0d0
	   spw = 0d0
	   do i = fobs(ipset), lobs(ipset)
	      w = 1d0 /err(i)/err(i)
	      sxw = sxw + mag(i) * w
	      spw = spw + mu(i) * w
	      sw = sw + w
	   end do
	   xhat = sxw / sw
	   phat = spw / sw
	   adj(ipset) = xhat - phat
	   
c  ***	   Compute chi-squared from orthogonalised residuals
	   chpset = 0d0
	   do i = fobs(ipset), lobs(ipset)
	      w = 1d0 /err(i)/err(i)
	      r = mag(i) - mu(i) - adj(ipset)
	      chpset = chpset + r * r * w
	   end do
	   ndof = lobs(ipset) - fobs(ipset) + 1
	   ndof = ndof - 1
	   scl(ipset) = chpset / dfloat(ndof)
	   chphot = chphot + chpset
	end do
	
c  ***	Now orthogonalise radial velocities
	sw = 0d0
	sxw = 0d0
	spw = 0d0
	do i = 1, nvel
	   w = 1d0 /(rverr(i)*rverr(i) + dvjit*dvjit)
	   sxw = sxw + rv(i) * w
	   spw = spw + nu(i) * w
	   sw = sw + w
	end do
	if ( sw .gt. 1d-30 ) then
	   xhat = sxw / sw
	   phat = spw / sw
	else
	   xhat = 0d0
	   phat = 0d0
	end if
	gam = xhat - phat
	
	chspec = 0d0
	do i = 1, nvel
	   w = 1d0 /(rverr(i)*rverr(i) + dvjit*dvjit)
	   r = rv(i) - nu(i) - gam
	   chspec = chspec + r * r * w
	end do
	
	chs = chphot + chspec
	
c  ***	Add Bayesian penalty for silly stellar masses	
	r = ms - ms0
	w = 1d0 / sigms / sigms
	chs = chs + r * r * w
	
c  ***	Add Bayesian penalty for silly v sin i	-- pending
c	r = vsi - vs0
c	w = 1d0 / sigvs / sigvs
c	chs = chs + r * r * w
	
c  ***	Add Bayesian penalty for silly stellar radii
	if ( constrainmr ) then
 	   rs0 = ms0 ** 0.8d0
	   sigrs = 0.8d0 * sigms * rs0 / ms0 
	   r = rstar - rs0
	   w = 1d0 / sigrs / sigrs
	   chs = chs + r * r * w
	end if
	
	end

C+	----------------------------------------------------------------
C+
	subroutine transmod(t0,period,rp,rs,cosi,vk,vsi,lam,ecc,om,
     :	                    c1,c2,c3,c4,ndata,hjd,z,mu,nvel,hjdrv,vz,nu)
	
c	t0 = epoch of transit
c	period = period
c	hjd = array of obs dates
c	rp = planet radius / orbital sep
c	rs = stellar radius / orbital sep
c	vk = stellar reflex velocity amp in km/sec
c	vsi = stellar v sin i in km/sec
c	gam = stellar centre-of-mass velocity in km/sec

	implicit none

	integer i, ndata,nvel
	real*8 t0, period, rp, rs, cosi, vk, vsi, lam, ecc, om
	real*8 sini
	real*8 c1, c2, c3, c4
	real*8 hjd(ndata)
	real*8 z(ndata)
	real*8 mu(ndata)
	real*8 alpha(ndata)
	real*8 rau(ndata)
	real*8 hjdrv(nvel)
	real*8 vz(nvel)
	real*8 vmu(nvel)
	real*8 nu(nvel)
	real*8 pi, p, ph, xp, yp, zp, up
	real*8 cosl, sinl
	real*8 nutrans,mtrans,meananom,dttrans,tperi
	
	pi = 4d0*atan2(1d0,1d0)

c  ***	Compute date of periastron relative to transit time t0	
	nutrans = pi/2d0 - om
	mtrans = meananom(nutrans,ecc)
	dttrans = mtrans/2d0/pi * period
	tperi = t0 - dttrans
	
	
c  ***	Ratio of radii
	p = rp / rs
	
	sini = sqrt(1d0 - cosi*cosi)
	
	do i = 1, ndata

           call kepler(hjd(i),tperi,period,ecc,om,vk,sini,
     :	              mu(i),rau(i),alpha(i))
	   
c  ***	   Impact parameter in units of primary radius
	   if ( cos(alpha(i)) .gt. 0d0 ) then
	      z(i) = rau(i) * sin(alpha(i)) / rs
	   else
	      z(i) = 1d0 / rs
	   end if
c	   print *,xp,yp,zp,cosi,z(i)
	end do
	
c  ***	Compute model light curve
	call occultsmall(p,c1,c2,c3,c4,ndata,z,mu)
	
c  ***	Convert mu to magnitudes
	do i = 1, ndata
	   mu(i) = -2.5d0*log10(mu(i))
c	   if(mu(i).lt.-1d-10)print *,z(i),mu(i)
	end do	
	
c  ***	Compute model RV curve. 
	do i = 1, nvel
	   
           call kepler(hjdrv(i),tperi,period,ecc,om,vk,sini,
     :	              nu(i),rau(i),alpha(i))
	   
c  ***	   Impact parameter in units of primary radius
	   if ( cos(alpha(i)) .gt. 0d0 ) then
c	   if ( alpha(i) .gt. pi/2d0 ) then
	      vz(i) = rau(i) * sin(alpha(i)) / rs
	   else
	      vz(i) = 1d0 / rs
	   end if
	   
	end do
	
c  ***	Compute model flux
	call occultsmall(p,c1,c2,c3,c4,nvel,vz,vmu)
	
c  ***	Rotation matrix elements for misalignment of rotation axes
c	cosl = cos(lam)
c	sinl = sin(lam)
		   
c	do i = 1, nvel
	
c  ***	   Rotate through lambda about y axis to get up

c	   xp = rau(i) * sin ( alpha(i) ) 
c	   yp = rau(i) * cos ( alpha(i) )
c	   zp = - yp * cosi
c	   up = xp * cosl - zp * sinl

c  ***	   Code Rossiter effect here. Impact param is vz(i),
c	   x coord is xp, flux diminution is 1 - vmu(i)

c	   nu(i) = nu(i) - vsi * up * ( 1d0 - vmu(i) )	   

c	end do
	
	end

C+	----------------------------------------------------------------
C+
      subroutine occultsmall(p,c1,c2,c3,c4,nz,z,mu)
      implicit none
      integer i,nz
c      parameter (nz=201)
      real*8 p,c1,c2,c3,c4,z(nz),mu(nz),i1,norm,
     &       x,tmp,iofr,pi
C This routine approximates the lightcurve for a small 
C planet. (See section 5 of Mandel & Agol (2002) for
C details):
C Input:
C  p      ratio of planet radius to stellar radius
C  c1-c4  non-linear limb-darkening coefficients
C  z      impact parameters (positive number normalized to stellar 
C        radius)- this is an array which MUST be input to the routine
C  NOTE:  nz must match the size of z & mu in calling routine
C Output:
C  mu     flux relative to unobscured source for each z
C
      pi=acos(-1.d0)
      norm=pi*(1.d0-c1/5.d0-c2/3.d0-3.d0*c3/7.d0-c4/2.d0)
      i1=1.d0-c1-c2-c3-c4
      do i=1,nz
        mu(i)=1.d0
        if(z(i).gt.1.d0-p.and.z(i).lt.1.d0+p) then
          x=1.d0-(z(i)-p)**2
          tmp=(1.d0-c1*(1.d0-0.8d0*x**0.25d0)
     &             -c2*(1.d0-2.d0/3.d0*x**0.5d0)
     &             -c3*(1.d0-4.d0/7.d0*x**0.75d0)
     &             -c4*(1.d0-0.5d0*x))
          mu(i)=1.d0-tmp*(p**2*acos((z(i)-1.d0)/p)
     &        -(z(i)-1.d0)*sqrt(p**2-(z(i)-1.d0)**2))/norm
        endif
        if(z(i).le.1.d0-p.and.z(i).ne.0.d0) then
          mu(i)=1.d0-pi*p**2*iofr(c1,c2,c3,c4,z(i),p)/norm
        endif
        if(z(i).eq.0.d0) then
          mu(i)=1.d0-pi*p**2/norm
        endif
      enddo
      return
      end
      
      function iofr(c1,c2,c3,c4,r,p)
      implicit none
      real*8 r,p,c1,c2,c3,c4,sig1,sig2,iofr
      sig1=sqrt(sqrt(1.d0-(r-p)**2))
      sig2=sqrt(sqrt(1.d0-(r+p)**2))
      iofr=1.d0-c1*(1.d0+(sig2**5-sig1**5)/5.d0/p/r)
     &         -c2*(1.d0+(sig2**6-sig1**6)/6.d0/p/r)
     &         -c3*(1.d0+(sig2**7-sig1**7)/7.d0/p/r)
     &         -c4*(p**2+r**2)
      return
      end
C+
	subroutine rddata(lu,photfile,mxobs,ndata,hjd,dat,err)
	
	implicit none
	integer lu
	character*80 photfile
	integer mxobs
	real*8 hjd(mxobs)
	real*8 dat(mxobs)
	real*8 err(mxobs)
	integer ndata
	integer i
	character*80 record
		
c  ***	Read data
	i = 0
	open(unit=lu,file=photfile,status='old')
3	read(lu,'(a)',end=998)record
	if(record(1:2) .eq. ' #')go to 3
	i = i + 1
	read(record,*)hjd(i),dat(i),err(i)
c	print *,hjd(i),dat(i),err(i)
	go to 3
	
998	close(lu)
	ndata = i
	
	end
C+
	subroutine methast(idum,chsnew,chsold,reject)
c  ***	Metropolis-Hastings decision maker
	
	implicit none
	real*8 chsnew, chsold
	logical reject
	real*8 dchisq,pjump,prob
	integer idum
	real ran1
	
	dchisq = chsnew - chsold
	
c  ***	Here comes the Markov-chain Monte Carlo decision
c	point. Do we accept the jump or not?

	if(dchisq .lt. 0d0) then
c  ***	   Accept the proposal and move on
	   reject = .false.
	else
c  ***	   Set probability of next jump
	   pjump = exp(-dchisq/2)
c  ***	   Pick a uniform random deviate in range 0 to 1
	   prob = dble(ran1(idum))
	   if ( prob .lt. pjump ) then
c  ***	      OK, accept the proposal
	      reject = .false.
	   else
c  ***	      No, reject the proposal
	      reject = .true.
	   end if
	end if
	
	end	
C+
	FUNCTION RAN1(IDUM)
	
	SAVE

C  ***	Returns a uniform random deviate between 0.0 and 1.0. Set IDUM
c	to any negative value to initialise or reinitialise the sequence

	DIMENSION R(97)
	integer m1,m2,m3
	real rm1,rm2
	PARAMETER(M1=259200, IA1 = 7141, IC1 = 54773, RM1 = 1./M1) 
	PARAMETER(M2=134456, IA2 = 8121, IC2 = 28411, RM2 = 1./M2) 
	PARAMETER(M3=243000, IA3 = 4561, IC3 = 51349)
	DATA IFF/0/
	
	
	

	IF(IDUM.LT.0 .OR. IFF.EQ.0)THEN
	  IFF = 1
	  IX1 = MOD(IC1-IDUM,M1)
	  IX1 = MOD(IA1*IX1+IC1,M1)
	  IX2 = MOD(IX1,M2)
	  IX1 = MOD(IA1*IX1+IC1,M1)
	  IX3 = MOD(IX1,M3)
	  DO 11 J=1, 97
	    IX1 = MOD(IA1*IX1+IC1,M1)
	    IX2 = MOD(IA2*IX2+IC2,M2)
	    R(J) = (FLOAT(IX1)+FLOAT(IX2)*RM2)*RM1
11	  CONTINUE
	  IDUM = 1
	END IF

	IX1 = MOD(IA1*IX1+IC1,M1)
	IX2 = MOD(IA2*IX2+IC2,M2)
	IX3 = MOD(IA3*IX3+IC3,M3)
	J = 1 + (97*IX3)/M3
	IF(J.GT.97 .OR. J.LT.1)PAUSE
	RAN1 = R(J)
	R(J) = (FLOAT(IX1)+FLOAT(IX2)*RM2)*RM1

	RETURN
	END

	FUNCTION GASDEV(IDUM)
	
C  ***	Returns a normally distributed deviate with zero mean and unit
c	variance, using RAN1(IDUM) as the source of uniform deviates.

	SAVE

	DATA ISET/0/
	
	IF(ISET.EQ.0)THEN
1	  V1 = 2.*RAN1(IDUM)-1.
	  V2 = 2.*RAN1(IDUM)-1.
	  R = V1*V1 + V2*V2
	  IF(R.GE.1.)GO TO 1
	  FAC = SQRT(-2.*LOG(R)/R)
	  GSET = V1*FAC
	  GASDEV = V2*FAC
	  ISET = 1
	ELSE
	  GASDEV = GSET
	  ISET = 0
	END IF

	RETURN
	END
	
C+
C+
	SUBROUTINE INDEXX(N,ARRIN,INDX)

	IMPLICIT NONE
	
	INTEGER N
	REAL*8 ARRIN(N)
	INTEGER INDX(N)
	REAL*8  Q
	INTEGER J, L, IR, I, INDXT

	DO 11 J=1, N
	   INDX(J) = J
11	CONTINUE

	L = N/2 + 1
	IR = N
	
10	CONTINUE
	  IF(L.GT.1)THEN
	    L = L-1
	    INDXT = INDX(L)
	    Q = ARRIN(INDXT)
	  ELSE
	    INDXT = INDX(IR)
	    Q = ARRIN(INDXT)
	    INDX(IR) = INDX(1)
	    IR = IR-1
	    IF(IR.EQ.1)THEN
	      INDX(1)=INDXT
	      RETURN
	    ENDIF
	  ENDIF

	  I=L
	  J = L+L
20	  IF(J.LE.IR)THEN
	    IF(J.LT.IR)THEN
	      IF(ARRIN(INDX(J)).LT.ARRIN(INDX(J+1))) J=J+1
	    ENDIF
	    IF(Q.LT.ARRIN(INDX(J)))THEN
	      INDX(I) = INDX(J)
	      I=J
	      J=J+J
	    ELSE
	      J=IR+1
	    ENDIF
	  GO TO 20
	  ENDIF
	  INDX(I) = INDXT
	GO TO 10
	END
C+
	subroutine conflim(m,n,x,chs,idx,dxl,xml,dxh)
	
	implicit none
	
	integer m,n
	real*8 x(n)
	real*8 chs(n)
	integer idx(n)
	integer idx2(n)
	integer ilo,ihi
	real*8 dxl,xml,dxh,xmax,xmin
	
	integer i
	integer nw
	real*8 wrk(n)
	real*8 wrk2(n)
	real*8 xsrt(n)
c	real*8 sx, sxx, xav, xsd
	
c  ***	Given an array x and associated chisq values from
c	a Monte Carlo simulation, determine max likelihood
c	value and 68.3 percent error limits.
	
c  ***	Sort the data in order of increasing chisq. Exclude
c	the first m elements.
	nw = 0
	do i = m+1, n
	   nw  = nw + 1
	   wrk(nw) = chs(i)
	end do
	call indexx(nw,wrk,idx)
	
c  ***	Also sort the data in order of increasing data value.
	nw = 0
	do i= m+1,n
	  nw = nw + 1
	  wrk2(nw) = x(i)
	end do
	call indexx(nw,wrk2,idx2)
		
c  ***	Array xsrt contains the x values in ascending order.
	do i= 1,nw
	  xsrt(i) = wrk2(idx2(i))
	end do

c  ***	Pick out the maximum-likelihood value as the one corresponding	
c	to the lowest chi-squared.
	xml = wrk2(idx(1))
	
c  ***	Initialise the upper and lower confidence limits, setting them 
c	equal to xml
	xmax = xml
	xmin = xml
	do i = 2, nw
c  ***	   Compare the x value corresponding to next highest value of 
c	   chisq with the upper and lower confidence limits.
	   xmax = max(xmax,wrk2(idx(i)))
	   xmin = min(xmin,wrk2(idx(i)))
	   
c  ***	   Determine where xmax and xmin lie in the sorted x array
	   call huntdp(xsrt,nw,xmin,ilo)
	   call huntdp(xsrt,nw,xmax,ihi)
	   
c  ***	   Update the confidence intervals relative to XML
	   dxl = xml - xmin
	   dxh = xmax - xml
	   
c  ***	   As soon as xmax and xmin enclose 68.3 percent of the x
c	   values, terminate the search.
	   if ( dfloat(ihi-ilo) .ge. 0.683d0*dfloat(nw) ) then
	      go to 999
	   end if
	end do
	   
999	continue

c  ***	Compute mean, stdev for sanity checking 
c	sx = 0d0
c	sxx = 0d0
c	do i = 1, n
c	   sx = sx + x(i)
c	   sxx = sxx + x(i)*x(i)
c	end do
c	xav = sx / n
c	xsd = sqrt((sxx - sx*sx/dfloat(n))/dfloat(n-1))
c	print *,'mean = ',xav,' sd = ',xsd
	
	end
C+	
	
C+
	subroutine correl(nb,n,x,m,c,cl)
	
c  ***	Computes the autocorrelation function c and correlation 
c	length cl of a Markov chain x. 
c	Recipe is from Tegmark et al 2004.
	
	implicit none
	
	integer nb, n
	real*8 x(n)
	integer m
	real*8 c(m)
	integer i,j, ioff
	real*8 si, sii, sij
	real*8 aii, aij
	real*8 xbar,numer,denom
	real*8 cl
	real*8 half, clo, chi, f, llo, lhi
	integer jlo, jhi
	
c	Exclude first nb points generated during burn-in
	si = 0d0
	do i = nb+1, n
	   si = si + x(i)
	end do
	xbar = si / dfloat(n-nb)
	
	sii = 0d0
	do i = nb+1, n
	   sii = sii + (x(i)-xbar)*(x(i)-xbar)
	end do
	aii = sii / dfloat(n-nb)
	denom = aii
	
	if ( denom .ge. 1d-20 ) then

	   do j = 1, m
	      sij = 0d0
	      ioff = j - 1
	      do i = nb+1, n - ioff
	 	 sij = sij + (x(i)-xbar)*(x(i+ioff)-xbar)
	      end do
	      aij = sij / dfloat(n-nb-ioff)
	      numer = aij 
	      c(j) = numer / denom
	   end do
	   
c  ***	   Determine where correlation function drops below 0.5
	   half = 0.5d0
	   jlo = 1
	   call huntdp(c,m,half,jlo)

c  ***	   Interpolate to get correlation length
	   if(jlo.ge.m)then
	      cl = -dfloat(m)
	      print *,'DBG: CORREL ERROR: m insufficient to determine cl.'
c	      print *,'Dumping correlation function:'
c	      do j = 1, m
c	 	 print *,j-1,c(j)
c	      end do
	   else if ( jlo.lt.1 ) then
	      cl = 0d0
	      print *,'DBG: CORREL ERROR: zero correlation length!.'
	   else
	      jhi = jlo + 1
	      clo = c(jlo)
	      chi = c(jhi) 
	      f = ( half - clo ) / ( chi - clo )
	      llo = dfloat(jlo)
	      lhi = dfloat(jlo+1)
	      cl = llo + ( lhi - llo ) * f
	   end if
	
	else
c  ***	   If we got to here, the variance is zero so return
c	   an obviously silly correlation length.
	   cl = -99d0
	end if
	
	end
C+
	subroutine wrparms2(lu,parfile,t0,period,dmag,wday,k1,vsi,
     :	             sigt0,sigper,sigdm,sigw,sigvk,sigvs,
     :	             mstar,sigms,c1,c2,c3,c4)
     
     	implicit none
	
	integer lu
	character*80 parfile
	real*8 t0,period,dmag,wday,k1,vsi
	real*8 sigt0,sigper,sigdm,sigw,sigvk,sigvs
c  ***	Limb-darkening coeffs from Claret (2000)
	real*8 mstar,sigms
	real*8 c1, c2, c3, c4
	
c  ***	Read basic parameters derived by HUNTER
	call unlink(parfile)
	open(unit=lu,file=parfile,form='formatted',status='new')
	write(lu,*)t0,period,dmag,wday,k1,vsi
	write(lu,*)sigt0,sigper,sigdm,sigw,sigvk,sigvs
	write(lu,*)mstar,sigms
	write(lu,*)c1,c2,c3,c4
	close(lu)
	
	end
C+	
	subroutine probs(n,rpjup,rstar,mstar,b,prp,prs,prb)
	
	implicit none
	
	integer n, i, jlo
	real*8 rpjup(n)	
	real*8 rstar(n)	
	real*8 mstar(n)	
	real*8 b(n)	
	real*8 frac
	real*8 prp,prs,prb	
	
c  ***	Determine fraction of trials that yield Rp < 1.5 Rj
	jlo = 0
	do i = 1, n
	   if ( rpjup(i) .lt. 1.5d0 ) jlo = jlo + 1
	end do
	
	prp = dfloat(jlo)/dfloat(n)
	
c  ***	Determine fraction of trials that yield rs < 1.2 ms^0.8
	jlo = 0
	do i = 1, n
	   frac = rstar(i) / mstar(i)**0.8d0
	   if ( frac .lt. 1.2d0 ) jlo = jlo + 1
	end do
	prs = dfloat(jlo)/dfloat(n)
	
c  ***	Determine fraction of trials that yield b < 0.8
	jlo = 0
	do i = 1, n
	   if ( b(i) .lt. 0.8d0 ) jlo = jlo + 1
	end do
	prb = dfloat(jlo)/dfloat(n)
	
	end
C+
	function funk(x)
	
c  ***	Wrapper for physparms and eval, for use in amoeba.

	implicit none
	
	real*8 x(11)
	real*8 t,p,wf,ms,d,b,vk,vs,lam,ecc,om
	real*8 rs,rh,aau,rstar,rp,cosi,deginc,mpjup,rpjup,gam

	integer maxobs
	parameter(maxobs=30000)
	integer maxrv
	parameter(maxrv = 100)
	
	real*8 c1,c2,c3,c4
	real*8 ms0,sigms
	logical constrainmr
	integer ndata,npset
	real*8 hjd(maxobs)
	real*8 mag(maxobs)
	real*8 err(maxobs)
	integer fobs(100)
	integer lobs(100)
	real*8 adj(100)
	integer nvel
	real*8 hjdrv(maxrv)
	real*8 rv(maxrv)
	real*8 rverr(maxrv)
	real*8 dvjit
	real*8 chph,chsp,funk,varscl
	real*8 pi
	
	common	/coeffs/	c1,c2,c3,c4
	common	/masses/	ms0,sigms
	common	/constr/	constrainmr
	common	/phodim/	ndata,npset
	common	/hjdpho/	hjd	
	common	/datpho/	mag	
	common	/errpho/	err
	common	/part01/	fobs	
	common	/part01/	lobs
	common	/rvedim/	nvel
	common	/hjdrve/	hjdrv
	common	/datrve/	rv
	common	/errrve/	rverr
	common	/jitter/	dvjit
	common	/adjust/	adj
	
	pi = 4d0*atan2(1d0,1d0)
	
	t = x(1)
	p = x(2)
	wf = exp(x(3))
	ms = exp(x(4))
	d = exp(x(5))
	vk = exp(x(6))
	vs = exp(x(7))
	b = 1d0 - 1d0/(1d0 + exp(x(8)))
	lam = pi * ( 1d0 - 2d0/(1d0 + exp(x(9))))
	ecc =1d0 - 1d0/(1d0 + exp(x(10)))
	om = x(11)
		
	
c  ***	Convert proposal parameters to physical parameters

	call physparms(p,wf,ms,d,b,vk,ecc,om,
     :	               rs,rh,aau,rstar,rp,
     :	               cosi,deginc,mpjup,rpjup)
	
c  ***	Generate model from parameters; fit to data and compute
c	penalty function chisq(k)

	call eval(t,p,ms,rs,rp,cosi,
     :	          vk,vs,gam,lam,rstar,ecc,om,
     :	          c1,c2,c3,c4,ms0,sigms,constrainmr,
     :	          ndata,npset,fobs,lobs,adj,hjd,mag,err,
     :	          nvel,hjdrv,rv,rverr,dvjit,
     :	          chph,chsp,funk,varscl)

	end	
C+
      SUBROUTINE amoeba(p,y,mp,np,ndim,ftol,funk,iter)
      INTEGER iter,mp,ndim,np,NMAX,ITMAX
      DOUBLE PRECISION ftol,p(mp,np),y(mp),funk
      PARAMETER (NMAX=20,ITMAX=5000)
      EXTERNAL funk
CU    USES amotry,funk
      INTEGER i,ihi,ilo,inhi,j,m,n
      DOUBLE PRECISION rtol,sum,swap,ysave,ytry,psum(NMAX),amotry
      iter=0
1     do 12 n=1,ndim
        sum=0.d0
        do 11 m=1,ndim+1
          sum=sum+p(m,n)
11      continue
        psum(n)=sum
12    continue
2     ilo=1
      if (y(1).gt.y(2)) then
        ihi=1
        inhi=2
      else
        ihi=2
        inhi=1
      endif
      do 13 i=1,ndim+1
        if(y(i).le.y(ilo)) ilo=i
        if(y(i).gt.y(ihi)) then
          inhi=ihi
          ihi=i
        else if(y(i).gt.y(inhi)) then
          if(i.ne.ihi) inhi=i
        endif
13    continue
      rtol=2.d0*abs(y(ihi)-y(ilo))/(abs(y(ihi))+abs(y(ilo)))
      if (rtol.lt.ftol) then
        swap=y(1)
        y(1)=y(ilo)
        y(ilo)=swap
        do 14 n=1,ndim
          swap=p(1,n)
          p(1,n)=p(ilo,n)
          p(ilo,n)=swap
14      continue
        return
      endif
c      if (iter.ge.ITMAX) pause 'ITMAX exceeded in amoeba'
      if (iter.ge.ITMAX) return
      iter=iter+2
      ytry=amotry(p,y,psum,mp,np,ndim,funk,ihi,-1.0d0)
      if (ytry.le.y(ilo)) then
        ytry=amotry(p,y,psum,mp,np,ndim,funk,ihi,2.0d0)
      else if (ytry.ge.y(inhi)) then
        ysave=y(ihi)
        ytry=amotry(p,y,psum,mp,np,ndim,funk,ihi,0.5d0)
        if (ytry.ge.ysave) then
          do 16 i=1,ndim+1
            if(i.ne.ilo)then
              do 15 j=1,ndim
                psum(j)=0.5d0*(p(i,j)+p(ilo,j))
                p(i,j)=psum(j)
15            continue
              y(i)=funk(psum)
            endif
16        continue
          iter=iter+ndim
          goto 1
        endif
      else
        iter=iter-1
      endif
      goto 2
      END
C  (C) Copr. 1986-92 Numerical Recipes Software &H1216.
C+
      FUNCTION amotry(p,y,psum,mp,np,ndim,funk,ihi,fac)
      INTEGER ihi,mp,ndim,np,NMAX
      DOUBLE PRECISION amotry,fac,p(mp,np),psum(np),y(mp),funk
      PARAMETER (NMAX=20)
      EXTERNAL funk
CU    USES funk
      INTEGER j
      DOUBLE PRECISION fac1,fac2,ytry,ptry(NMAX)
      fac1=(1.d0-fac)/ndim
      fac2=fac1-fac
      do 11 j=1,ndim
        ptry(j)=psum(j)*fac1-p(ihi,j)*fac2
11    continue
      ytry=funk(ptry)
      if (ytry.lt.y(ihi)) then
        y(ihi)=ytry
        do 12 j=1,ndim
          psum(j)=psum(j)-p(ihi,j)+ptry(j)
          p(ihi,j)=ptry(j)
12      continue
      endif
      amotry=ytry
      return
      END
C  (C) Copr. 1986-92 Numerical Recipes Software &H1216.
	
       FUNCTION JULDAY(MM,ID,IYYY)
       PARAMETER(IGREG=15+31*(10+12*1582))

       IF(IYYY.EQ.0) PAUSE 'There is no Year Zero.'
       IF(IYYY.LT.0)IYYY=IYYY+1
       IF(MM.GT.2) THEN
          JY = IYYY
          JM = MM+1
       ELSE
          JY = IYYY-1
          JM = MM+13
       ENDIF
       JULDAY=INT(365.25*JY) + INT(30.6001*JM) + ID + 1720995
       IF(ID+31*(MM+12*IYYY).GE.IGREG) THEN
          JA = INT(0.01*JY)
          JULDAY = JULDAY+2-JA+INT(0.25*JA)
       ENDIF
       RETURN
       END

C+
        subroutine kepler(jdate,tperi,period,ecc,omega,kvel,sini,
     :	vrad,rau,alpha)

c       Computes radial velocity in centre-of-mass frame at a given
c       date. Omega should be expressed in radians.

        implicit none

        double precision jdate,tperi,period
        double precision ecc,omega,kvel,sini
        double precision meananom,nu,vrad,tanhalfecc,eccanom,rau,alpha
        double precision pi

        double precision truanom

        pi=4.*atan2(1.,1.)

        meananom = 2.*pi*dmod(jdate-tperi,period)/period
        nu = truanom(meananom,ecc)
        vrad = kvel*(ecc*cos(omega) + cos(nu+omega))

        tanhalfecc = tan(nu/2.)*sqrt((1.-ecc)/(1.+ecc))
	eccanom = 2.*atan(tanhalfecc)
	
c  ***	Hence get instantaneous planet-star distance 
c	in units of major semi-axis:
	rau = 1. - ecc*cos(eccanom)

c  ***	Compute planet-star-observer phase angle
	alpha = acos(sini*cos(nu + omega - pi/2.))

        end
C+
        function meananom(t_anom,ecc)

c  ***   Inputs are true anomaly (nu) in radians and eccentricity.
c  ***   Output is mean anomaly (M) in radians.

c  ***   ACC at St Andrews 7/03/02.

        implicit none
	
	double precision t_anom,ecc
        double precision tanhalfecc,eccanom,meananom

        tanhalfecc = tan(t_anom/2.)*sqrt((1.-ecc)/(1.+ecc))
	eccanom = 2.*atan(tanhalfecc)

        meananom = eccanom-ecc*sin(eccanom)

        end 
C+
        function truanom(m_anom,ecc)

c  ***   iterative solution to Kepler's equation.
c  ***   Inputs are mean anomaly (M) in radians and eccentricity.
c  ***   Output is true anomaly (nu) in radians.

c  ***   ACC at St Andrews 16/11/99.
c  ***   Converted to f77 by ACC at St Andrews 25/06/00.

        implicit none
        double precision m_anom, ecc, guess, eccanom, tanhalfnu, truanom
        integer its

c  ***   Initial guess

        guess = m_anom
        truanom = 1.7e38
        its = 0

c  ***   Iterate till truanom converges to within 1 arcsec

        do its = 1, 101
           eccanom = m_anom + ecc * sin(guess)
           if(abs(eccanom - guess) .lt. 1./206265.) go to 1
           guess = eccanom
           if(its .gt. 100) pause 'Not converged'
        end do

1       tanhalfnu = sqrt((1.+ecc)/(1.-ecc))*tan(eccanom/2.)
        truanom = atan(tanhalfnu)*2.

        end 
C+
        function glambert( alpha )
c       alpha in radians

	implicit none
        double precision pi, alpha, glambert

        pi = 4. * atan2(1.,1.)
        
        glambert = ( sin( alpha ) + ( pi - alpha ) * cos( alpha ) ) / pi
        
        return
        end
C+
        function gvenus( alpha )
c       alpha in radians

	implicit none
        double precision alpha, pi, factor, x, dmag, gvenus

        pi = 4. * atan2(1.,1.)
        factor = 180. / pi / 100.

        x = alpha * factor
        dmag = x * ( 0.09 + x * ( 2.39 - x * 0.65 ) )
        gvenus = 10.**(-0.4*dmag)
        
        return
        end

