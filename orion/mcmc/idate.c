#include <stdlib.h>
#include <time.h>
#include <sys/time.h>

void idate_(int *iday)
{
  time_t tnow=time(NULL);
  struct tm *tval=localtime(&tnow);
  iday[0]=tval->tm_mday;
  iday[1]=tval->tm_mon;
  iday[2]=tval->tm_year;
}

void idate(int *iday)
{
  idate_(idate);
}
