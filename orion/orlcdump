#!/panfs/vol/qeeri/qes/local/bin/perl
#

use English;
use strict qw(vars subs refs);
use File::Path;
use lib qw(/panfs/vol/qeeri/qes/archive_code/orion);
use Cwd;

use Astro::FITS::CFITSIO qw(:longnames :constants);
use FITSUtils;

my ($file, $obj, $peak)=@ARGV;

# Open the file
my $status;
my $fptr=Astro::FITS::CFITSIO::open_file($file, READONLY, $status);
die "Error opening FITS file '$file': status=$status"
    if $status;

# Read the candidates table
my $ncand;
$fptr->movnam_hdu(BINARY_TBL, "CANDIDATES", 0, $status);
$fptr->get_num_rows($ncand, $status);
my $cand=FITSUtils::read_table($fptr, $status);
my ($pgidx, $lcidx, $catidx, $candidx, $found, $firstpeak, $lastpeak);
$found=0;
for(my $idx=0; $idx<$ncand; $idx++) {
    if($$cand{'OBJ_ID'}[$idx] eq $obj) {
	$firstpeak=$idx unless $found;
	$lastpeak=$idx;
	if($$cand{'RANK'}[$idx]==$peak) {
	    $candidx=$idx;
	    $pgidx=$$cand{'PG_IDX'}[$idx];
	    $lcidx=$$cand{'LC_IDX'}[$idx];
	    $catidx=$$cand{'CAT_IDX'}[$idx];
	}
	$found=1;
    }
    last if $found and $$cand{'OBJ_ID'}[$idx] ne $obj;
}
die "Cannot locate object $obj in candidate list"
    unless $catidx;
die "Cannot locate peak $peak for object $obj"
    unless $lcidx && $pgidx;

# Read the catalogue entry
my $ncat;
$fptr->movnam_hdu(BINARY_TBL, "CATALOGUE", 0, $status);
$fptr->get_num_rows($ncat, $status);
my $cat=FITSUtils::read_table_row($fptr, $catidx, $status);

#
print "# Lightcurve of star $obj\n";
print "# Field:\n# Camera:\n# Sequence number\n#\n";
print sprintf("# %-18s:%10.4f\n", "Mean SW mag", $$cat{'MAG_MEAN'});
print sprintf("# %-18s:%10d\n", "Num good pts", $$cat{'NPTS_USED'});
print sprintf("# %-18s:%10.4f\n", "Chi-squared", $$cat{'MAG_CHISQ'});
print sprintf("# %-18s:%10d\n", "Teff_vk", $$cat{'TEFF_VK'});
print sprintf("# %-18s:%10d\n", "Teff_jh", $$cat{'TEFF_JH'});
print sprintf("# %-18s:%10.4f\n", "Rstar_vk (Rsun)", $$cat{'RSTAR_VK'});
print sprintf("# %-18s:%10.4f\n", "Rstar_jh (Rsun)", $$cat{'RSTAR_JH'});
print sprintf("# %-18s:%10.4f\n", "RPM_J", $$cat{'RPMJ'});
print sprintf("# %-18s:%10.4f\n", "RPM_J diff", $$cat{'RPMJ_DIFF'});
print sprintf("# %-18s:%10d\n", "Giant_flg", $$cat{'GIANT_FLG'});
print "#\n";
print sprintf("# %-18s:%10d\n", "Catalogue match", 1);
print sprintf("# %-18s:%10.4f\n", "Vmag", $$cat{'VMAG'});
my $vk=-99.0;
$vk=$$cat{'VMAG'}-$$cat{'KMAG'}
if $$cat{'VMAG'}>-90.0 && $$cat{'KMAG'}>-90.0;
print sprintf("# %-18s:%10.4f\n", "V-K", $vk);
print sprintf("# %-18s:%10.4f\n", "Jmag", $$cat{'JMAG'});
my $jh=-99.0;
$jh=$$cat{'JMAG'}-$$cat{'HMAG'}
if $$cat{'JMAG'}>-90.0 && $$cat{'HMAG'}>-90.0;
print sprintf("# %-18s:%10.4f\n", "J-H", $jh);
print sprintf("# %-18s:%10.4f\n", "MU_RA", $$cat{'MU_RA'});
print sprintf("# %-18s:%10.4f\n", "ERR_MU_RA", $$cat{'MU_RA_ERR'});
print sprintf("# %-18s:%10.4f\n", "MU_DEC", $$cat{'MU_DEC'});
print sprintf("# %-18s:%10.4f\n", "ERR_MU_DEC", $$cat{'MU_DEC_ERR'});
print "#\n";

my $rstar_vk=$$cat{'RSTAR_VK'};
my $rstar_jh=$$cat{'RSTAR_JH'};

# Look for the matching object/peak
for(my $idx=$firstpeak; $idx<=$lastpeak; $idx++) {
    print sprintf("# %-18s:%10d\n", "PEAK", $$cand{'RANK'}[$idx]);
    print sprintf("# %-18s:%10.4f\n", "Epoch", $$cand{'EPOCH'}[$idx]/86400.0+3005.5);
    print sprintf("# %-18s:%10.6f\n", "Period (days)", $$cand{'PERIOD'}[$idx]/86400.0);
    print sprintf("# %-18s:%10.4f\n", "Duration (hours)", $$cand{'WIDTH'}[$idx]/3600.0);
    print sprintf("# %-18s:%10.4f\n", "Depth (mag)", $$cand{'DEPTH'}[$idx]);
    print sprintf("# %-18s:%10.4f\n", "Delta chisq", $$cand{'DELTA_CHISQ'}[$idx]);
    print sprintf("# %-18s:%10.4f\n", "Signal-Rednoise", $$cand{'SN_RED'}[$idx]);
    print sprintf("# %-18s:%10d\n", "Ntransits", $$cand{'NUM_TRANSITS'}[$idx]);
    print sprintf("# %-18s:%10.4f\n", "Ellipsoidal var", $$cand{'AMP_ELLIPSE'}[$idx]);
    print sprintf("# %-18s:%10.4f\n", "Ellipsoidal S/N", $$cand{'SN_ELLIPSE'}[$idx]);
    print sprintf("# %-18s:%10.4f\n", "Fraction-intransit",
		  $$cand{'NPTS_TRANSIT'}[$idx]/$$cat{'NPTS_USED'});
    print sprintf("# %-18s:%10.4f\n", "Gap Ratio", $$cand{'GAP_RATIO'}[$idx]);
    print sprintf("# %-18s:%10.4f\n", "Dchisq/Anti-dchisq", $$cand{'SN_ANTI'}[$idx]);
    my ($rpl_vk, $rpl_jh);
    $rpl_vk=($rstar_vk>-90.0)?sqrt(abs($$cand{'DEPTH'}[$idx]/1.3))*$rstar_vk*9.728:-99.0;
    $rpl_jh=($rstar_jh>-90.0)?sqrt(abs($$cand{'DEPTH'}[$idx]/1.3))*$rstar_jh*9.728:-99.0;
    print sprintf("# %-18s:%10.4f\n", "Rplanet_vk (Rj)", $rpl_vk);
    print sprintf("# %-18s:%10.4f\n", "Rplanet_jh (Rj)", $rpl_jh);
    print "#\n";
}

# Read the photometry
my ($col, $repeat, $offset, $anynul);
$fptr->movnam_hdu(BINARY_TBL, "LIGHTCURVES", 0, $status);
$fptr->get_colnum(CASEINSEN, "HJD", $col, $status);
$fptr->read_descript($col, $lcidx, $repeat, $offset, $status);
my $hjd=[0.0 x $repeat];
$fptr->read_col_dbl($col, $lcidx, 1, $repeat, 0.0, $hjd, $anynul, $status);
$fptr->get_colnum(CASEINSEN, "MAG", $col, $status);
my $mag=[0.0 x $repeat];
$fptr->read_col_flt($col, $lcidx, 1, $repeat, 0.0, $mag, $anynul, $status);
$fptr->get_colnum(CASEINSEN, "MAG_ERR", $col, $status);
my $magerr=[0.0 x $repeat];
$fptr->read_col_flt($col, $lcidx, 1, $repeat, 0.0, $magerr, $anynul, $status);
print "# Column 1: HJD
# Column 2: Tamuz-corrected magnitude
# Column 3: Magnitude error\n";
for(my $idx=0; $idx<$repeat; $idx++) {
    print sprintf("%15.8f  %10.6f  %10.6f\n", $$hjd[$idx]/86400.0+3005.5,
		  $$mag[$idx], $$magerr[$idx]);
}

#
$fptr->close_file($status);

